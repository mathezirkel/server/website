# Configuration

## Feedback and Registration

### Events

The file [events.json](../frontend/data/events.json) contains several keys, each representing a specific type of event.

The data is crucial for managing participant registrations,
including the activation of registration forms,
generating PDF summaries,
and creating mail templates.

#### Properties

| Key | Type | Description | Required |
|-----|------|-------------|----------|
| enabled | bool | Whether the registration for the event is open | all |
| year | str | Year of the event | `wintercamp`, `mathecamp` |
| period | str | Period of the event | `astrotag`, `mai-mathetag`, `wintercamp`, `mathecamp` |
| place | str | Place where the event takes place | `astrotag`, `mai-mathetag`, `wintercamp`, `mathecamp` |
| participationFee | int | participationFee | `astrotag`, `mai-mathetag`, `wintercamp`, `mathecamp` |
| registrationDeadline | str | Deadline for the registration | `astrotag`, `mai-mathetag`, `wintercamp`, `mathecamp` |
| paymentDeadline | str | Deadline for the payment | `wintercamp`, `mathecamp` |
| waitingListResponsePeriod | str | Time period in which participants on the waiting list will receive either a confirmation or a rejection | `wintercamp`, `mathecamp` |

### Zirkel

The file `zirkel.json`, see [example](../frontend/data/zirkel.example.json), contains data of zirkel and events.

It is divided into three main groups: `general`, `praesenzzirkel`, and `korrespondenzzirkel`.
Each key in this group represents a feedback option.

Additionally, the data contained within the groups `praesenzzirkel` and `korrespondenzzirkel` is crucial for managing registration.
In particular, this data is used to create email templates.
Moreover, within the group `praesenzzirkel` each key represents a Zirkel in the corresponding registration form.

Within the groups `prasenzzirkel` and `korrespondenzzirkel` the keys has to match the name of Zirkel
in the [Mathezirkel App](app.mathezirkel-augsburg.de).

#### Common properties

| Key | Type | Description |
|-----|------|-------------|
| descriptionFeedback | str | String displayed in the selection list |
| emails | list[str] | List of email addresses[^1] |
| weight | int | Determines the sequence of the selection list, where a lower value indicates a higher priority; each value must be distinct |

#### Properties of `praesenzzirkel` and `korrespondenzzirkel`

| Key | Type | Description |
|-----|------|-------------|
| descriptionRegistration | str | String displayed in the registration form (`praesenzzirkel` only) |
| instructors | list[str] | List of the instructors of the Zirkel |

[^1] The feedback will be delivered to this email addresses.
Additionally, within the groups `praesenzzirkel` or `korrespondenzzirkel`, the templates include these emails in Cc.

### Notes

Note that

* the keys representing the event in [events.json](../frontend/data/events.json),
* the name of the groups `praesenzzirkel` and `korrespondenzzirkel` in `zirkel.json`,
* the names of the [registration pages](../frontend/content/anmeldung/),
* the [template folders](../backend/registration/templates/email/) for automatic responses, and
* the attribute `form_class_key' of the [registration form class](../backend/registration/forms/multiforms.py)
  of the corresponding event

must coincide.
