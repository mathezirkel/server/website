# Deployment

## Environments

There are two environment represented by branches:

| Name | URL | Usage |
|------|-----|-------|
| `staging` | `staging.mathezirkel-augsburg.de` | Testing |
| `main` | `www.mathezirkel-augsburg.de` | Production |

## CI/CD

Commits trigger the CI/CD pipeline, automatically building, testing, and pushing container images to the registry,
tagged with the shortened commit SHA.

Images from `staging` or `main` are additionally tagged with `staging` and `staging-$CI_COMMIT_SHORT_SHA`
(resp. `main` and `main-$CI_COMMIT_SHORT_SHA`),
and are automatically deployed to the corresponding environments.

## Environment variables

### Frontend

The Frontend container operates an NGINX server designed to serve static files, which are built by Hugo.
Configuration of this server can be done through the [NGINX Config Template](./nginx/default.conf.template)
and the environment variable listed below:

* `BACKEND_SERVICE`

    This is the name of the service through which the backend is accessible.

* `TRUSTED_PROXY`

    This is the subnet of trusted proxies.

### Backend

The backend container runs a Django application, which can be configured utilizing environment variables.
A sample configuration can be found [here](./dev/cabret.example.env).

The following table illustrates the configurable environment variables, aligning with the built-in settings of Django.
Please refer to the [Django documentation](https://docs.djangoproject.com/en/4.2/ref/settings/)
for a more detailed explanation of each setting.

| Variable | Django setting | Default |
|----------|-------------|---------|
| `DJANGO_SECRET_KEY` | `SECRET_KEY` | `django-insecure` |
| `DJANGO_DEBUG` | `DEBUG` | `0` |
| `DJANGO_ALLOWED_HOSTS` | `ALLOWED_HOSTS` | `localhost` |
| `DJANGO_ALLOWED_CIDR_NETS`| `ALLOWED_CIDR_NETS` | `127.0.0.1` |
| `DJANGO_CSRF_TRUSTED_ORIGINS` | `CSRF_TRUSTED_ORIGINS` | `http://localhost` |
| `DJANGO_DEFAULT_FROM_EMAIL` | `DEFAULT_FROM_EMAIL` | `""` |
| `DJANGO_EMAIL_HOST` | `EMAIL_HOST` | `""` |
| `DJANGO_EMAIL_PORT` | `EMAIL_PORT` | `25` |
| `DJANGO_EMAIL_HOST_USER` | `EMAIL_HOST_USER` | `""` |
| `DJANGO_EMAIL_HOST_PASSWORD` | `EMAIL_HOST_PASSWORD` | `""` |
| `DJANGO_EMAIL_USE_TLS` | `EMAIL_USE_TLS` | `0` |
| `DJANGO_EMAIL_USE_SSL` | `EMAIL_USE_SSL` | `0` |

Furthermore, there are the following settings:

* `DJANGO_PRODUCTION`

    When the variable `DJANGO_PRODUCTION` is enabled, the application transitions into production mode
    and automatically adjusts several settings to enhance security, making the assumption that the application operates
    behind an SSL terminating proxy. Below is a table that outlines the values assigned to each setting in production mode.

    | Django setting | Value |
    |----------------|-------|
    | `SECURE_PROXY_SSL_HEADER` | `('HTTP_X_FORWARDED_PROTO', 'https')` |
    | `USE_X_FORWARDED_HOST` | `True` |
    | `SECURE_CONTENT_TYPE_NOSNIFF` | `True` |
    | `SECURE_CROSS_ORIGIN_OPENER_POLICY` | `same-origin` |
    | `SECURE_REFERRER_POLICY` | `same-origin` |
    | `X_FRAME_OPTIONS` | `DENY` |
    | `SESSION_COOKIE_SECURE` | `True` |

    Default: `1`

* `DJANGO_RATELIMITS`

    The `DJANGO_RATELIMITS` variable allows the configuration of rate limits applicable to POST requests.
    The variable expects a space-separated list of rate limits, more information on the syntax can be found
    [here](https://django-ratelimit.readthedocs.io/en/stable/usage.html).

    Default: `5/d`

* `DJANGO_EVENTS_CONFIG_FILE`
    The path to the event data file, see [config.md](config.md).

    Default: `./data/events.json`

* `DJANGO_EVENTS_CONFIG_FILE`
    The path to the zirkel data file, see [config.md](config.md).

    Default: `./data/zirkel.json`

* Redis

    | Variable | Default | Description |
    |----------|---------|-------------|
    | `REDIS_HOST` | `redis` | Hostname of the redis service. |
    | `REDIS_PORT` | `6379` | Port of the redis service. |
    | `REDIS_PASSWORD` | `""` | Password of the redis service. |
