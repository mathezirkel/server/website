from django.forms import Form

class BetterForm(Form):
    template_name = "forms/betterform.html"

    heading = None
    pre_note = None
    post_note = None
    css_classes = "form-wrapper"

    error_css_class = "form-error"
    required_css_class = "required"
