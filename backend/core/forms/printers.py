from __future__ import annotations
from io import BytesIO
from typing import TYPE_CHECKING

from reportlab.lib.pagesizes import A4
from reportlab.platypus import (
    Flowable,
    Paragraph,
    SimpleDocTemplate,
    Spacer,
)
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import cm

if TYPE_CHECKING:
    from reportlab.lib.styles import StyleSheet1
    from reportlab.pdfgen import Canvas
    from core.forms.multiforms import MultiForm

class PDFMultiFormPrinter():

    def __init__(self):
        self.pagesize = A4
        self.width, self.height = self.pagesize
        self.left_margin = 2.5 * cm
        self.top_margin = 3 * cm
        self.right_margin = 2.5 * cm
        self.bottom_margin = 2 * cm

    def header(self, canvas: Canvas, _doc: SimpleDocTemplate) -> None:
        canvas.saveState()
        canvas.restoreState()

    # Needs to be overwritten
    def get_summary(self, _multiform: MultiForm, _styles: StyleSheet1) -> list[Flowable]:
        return []

    def get_content(self, multiform: MultiForm, title: str, styles: StyleSheet1) -> list[Flowable]:
        data = multiform.cleaned_data

        content = []

        content.append(Paragraph(title, styles['Heading1']))

        content += self.get_summary(multiform, styles)

        for form in multiform:
            content.append(Paragraph(f"{form.heading}", styles['Heading2']))
            for field in form:
                if field.name in data:
                    content.extend(field.field.get_flowable(data[field.name],styles))
                    content.append(Spacer(1, 4))

        content.append(
            Paragraph(
                f"<b>Erstellt</b>: {data.get('timestampLocaltime', '')}",
                styles['Normal']
            )
        )

        return content

    def render(self, multiform: MultiForm, title: str) -> bytes:
        buffer = BytesIO()

        # Create PDF
        doc = SimpleDocTemplate(
            buffer,
            pagesize=self.pagesize,
            leftMargin=self.left_margin,
            topMargin=self.top_margin,
            rightMargin=self.right_margin,
            bottomMargin=self.bottom_margin,
        )

        # Create a ReportLab stylesheet
        styles = getSampleStyleSheet()
        styles['Normal'].linkUnderline = True

        content = self.get_content(multiform, title, styles)

        doc.build(content, onFirstPage=self.header)

        # Create pdf_data
        buffer.seek(0)
        pdf_data = buffer.read()
        buffer.close()

        return pdf_data
