from __future__ import annotations
from abc import ABCMeta
from datetime import datetime
from typing import TYPE_CHECKING
from zoneinfo import ZoneInfo

from django.conf import settings
from django.forms import Form
from django.forms.renderers import get_default_renderer
from django.forms.utils import ErrorDict, RenderableMixin
from django.forms.widgets import MediaDefiningClass

if TYPE_CHECKING:
    from typing import Any, Generator
    from django.forms import Field
    from django.forms.renderers import DjangoTemplates
    from core.forms.forms import BetterForm
    from core.forms.printers import PDFMultiFormPrinter

class DeclarativeMultiFormsMetaclass(MediaDefiningClass, ABCMeta):
    """Collect Form classes declared on the base classes."""

    def __new__(mcs, name, bases, attrs):
        # Collect forms from current class and remove them from attrs.
        attrs["declared_form_classes"] = {
            key: attrs.pop(key)
            for key, value in list(attrs.items())
            # Since and evaluates lazy, there is no exception if value is not a class.
            if isinstance(value, type) and issubclass(value, Form)
        }

        new_class = super().__new__(mcs, name, bases, attrs)

        # Walk through the MRO.
        declared_form_classes = {}
        for base in reversed(new_class.__mro__):
            # Collect forms from base class.
            if hasattr(base, "declared_form_classes"):
                declared_form_classes.update(base.declared_form_classes)

            # Form class shadowing.
            for attr, value in base.__dict__.items():
                if value is None and attr in declared_form_classes:
                    declared_form_classes.pop(attr)

        new_class.base_form_classes = declared_form_classes
        new_class.declared_form_classes = declared_form_classes

        return new_class

class BaseMultiForm(RenderableMixin, metaclass=ABCMeta):
    """
    The main implementation of all the MultiForm logic. Note that this class is
    different than MultiForm. See the comments by the MultiForm class for more info. Any
    improvements to the form API should be made to this class, not to the MultiForm
    class.
    """
    default_renderer = None
    default_pdf_renderer = None
    form_order = None

    template_name = "forms/multiform.html"

    form_class_key = None
    description = None
    css_classes = None

    def __init__(
        self,
        data: dict[str, Any] | None = None,
        files: dict[str, Any] | None = None,
        initial: str = None,
        form_order : list[str] = None,
        renderer : DjangoTemplates = None,
    ): # pylint: disable=too-many-arguments
        self.is_bound = data is not None or files is not None
        self.forms = {
            key : form_class(
                data=data,
                files=files,
                initial=initial,
            )
            for key, form_class in self.base_form_classes.items() # pylint: disable=no-member
        }
        self.order_forms(self.form_order if form_order is None else form_order)

        # Initialize multiform renderer. Use a global default if not specified
        # either as an argument or as self.default_renderer.
        if renderer is None:
            if self.default_renderer is None:
                renderer = get_default_renderer()
            else:
                renderer = self.default_renderer
                if isinstance(self.default_renderer, type):
                    renderer = renderer()
        self.renderer = renderer

    @property
    def errors(self) -> ErrorDict:
        """Return an ErrorDict for the data provided for the form."""
        error_dict = ErrorDict()
        for form in self:
            for field, errors in form.errors.items():
                if field not in error_dict:
                    error_dict[field] = errors
                else:
                    error_dict[field] += errors
        return error_dict

    def is_valid(self) -> bool:
        """Return True if all forms are valid, or False otherwise."""
        return all(form.is_valid() for form in self)

    def order_forms(self, form_order: list[str]) -> None:
        """
        Rearrange the forms according to form_order.

        form_order is a list of form names specifying the order. Append forms
        not included in the list in the default order for backward compatibility
        with subclasses not overriding form_order. If form_order is None,
        keep all form in the order defined in the class. Ignore unknown
        forms in form_order to allow disabling form in multiform subclasses
        without redefining ordering.
        """
        if form_order is None:
            return
        forms = {}
        for key in form_order:
            try:
                forms[key] = self.forms.pop(key)
            except KeyError:  # ignore unknown forms
                pass
        forms.update(self.forms)  # add remaining forms in original order
        self.forms = forms

    def __getitem__(self, name: str) -> BetterForm:
        """Return a form with the given name."""
        try:
            form = self.forms[name]
        except KeyError as exc:
            raise KeyError(
                f"Key {name} not found in {self.__class__.__name__}. \
                Choices are: {', '.join(sorted(self.forms))}."
            ) from exc
        return form

    def __iter__(self) -> Generator[BetterForm, Any, None]:
        """Yield the multiform's forms"""
        for form in self.forms:
            yield self[form]

    def get_context(self) -> dict[str, BaseMultiForm | list[BetterForm]]:
        forms = []
        for form in self:
            forms.append(form)
        return {
            "multi_form" : self,
            "forms" : forms,
        }

    @property
    def cleaned_data(self) -> dict[str, Any]:
        # The programmer is responsible to avoid duplicate keys,
        # i.e. by using prefixes.
        cleaned_data = {}
        for form in self:
            cleaned_data.update(form.cleaned_data)
        cleaned_data.update({
            "timestampLocaltime": datetime.now().strftime(settings.DATETIME_FORMAT),
            "timestampUTC": datetime.now(tz=ZoneInfo("UTC")).strftime(settings.DATETIME_FORMAT)
        })
        return cleaned_data

    @property
    def fields(self) -> dict[str, Field]:
        return {
            name: field for form in self for name, field in form.fields.items()
        }

    def generate_pdf(self, printer : PDFMultiFormPrinter, title: str) -> bytes:
        return printer.render(multiform=self, title=title)


class MultiForm(BaseMultiForm, metaclass=DeclarativeMultiFormsMetaclass):
    """A collection of Forms, plus their associated data."""
    # This is a separate class from BaseMultiForm in order to abstract the way
    # self.forms is specified. This class (MultiForm) is the one that does the
    # fancy metaclass stuff purely for the semantic sugar -- it allows one
    # to define a form using declarative syntax.
    # BaseMultiForm itself has no way of designating self.forms.
