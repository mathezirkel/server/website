"""
Better Form Fields Module
=========================

This module enhances standard Django form fields with improved layout and PDF export capabilities.

Each 'Better' form field class inherits from its respective Django form field class,
adding specific display and PDF rendering capabilities.

Classes
-------
PDFExportMixin
    A mixin class that adds methods for generating PDF flowables from form field data.

BetterBooleanField : PDFExportMixin, BooleanField
    A boolean field with improved layout and PDF export functionality.

BetterCaptchaField : PDFExportMixin, CaptchaField
    A captcha field with improved layout and PDF export functionality.

BetterCharField : PDFExportMixin, CharField
    A character field with improved layout and PDF export functionality.

BetterChoiceField : PDFExportMixin, ChoiceField
    A choice field with improved layout and PDF export functionality.

BetterDateField : PDFExportMixin, DateField
    A date field with improved layout and PDF export functionality.

BetterEmailField : PDFExportMixin, EmailField
    An email field with improved layout and PDF export functionality.

BetterListField : PDFExportMixin, CharField
    A field to handle comma-separated lists of strings with improved layout
    and PDF export functionality.

BetterMultiValueField : PDFExportMixin, MultiValueField
    A multi-value field with improved layout and PDF export functionality.

BetterPhoneNumberField : PDFExportMixin, PhoneNumberField
    A phone number field with improved layout and PDF export functionality.

"""

from __future__ import annotations
from abc import ABCMeta, abstractmethod
from datetime import date
from typing import TYPE_CHECKING

from captcha.fields import CaptchaField, CaptchaTextInput
from django.conf import settings
from django.forms import (
    BooleanField,
    CharField,
    ChoiceField,
    DateField,
    EmailField,
    IntegerField,
    MultiValueField,
)
from django.utils.html import escape
from phonenumber_field.formfields import PhoneNumberField
from reportlab.platypus import (
    ListFlowable,
    ListItem,
    Paragraph,
)

if TYPE_CHECKING:
    from reportlab.lib.styles import StyleSheet1
    from reportlab.platypus import Flowable
    from typing import Any

CSS_FIELD_WRAPPER_CLASS = "field-wrapper"

class PDFExportMixin(metaclass=ABCMeta):
    """
    A mixin class to provide PDF export functionality to form fields.

    Methods:
    --------
    get_pdf_label() -> str:
        Abstract method to get the label for PDF export.

    get_flowable(data: Any, styles: StyleSheet1) -> list[Flowable]:
        Abstract method to create flowable elements for PDF export.
    """
    def get_pdf_label(self) -> str:
        """
        Abstract method to get the label for PDF export.

        Returns:
        --------
        str:
            The label of the field as used in the PDF.

        """
        return self.label

    @abstractmethod
    def get_flowable(self, data: Any, styles: StyleSheet1) -> list[Flowable]:
        """
        Abstract method to create flowable elements for PDF export.

        Parameters:
        -----------
        data: Any:
            The data to be converted into a PDF flowable.
        styles: StyleSheet1:
            The styles to be applied to the PDF flowable.

        Returns:
        --------
        list[Flowable]:
            A list of flowable elements for PDF export.
        """

class BetterBooleanField(PDFExportMixin, BooleanField):
    """
    Enhanced boolean field with custom layout and PDF export functionality.

    Attributes:
    -----------
    extra_classes : list[str]
        Additional CSS classes for field styling.
    description : str
        The description of the field. It will be displayed next to the checkbox
        to describe its meaning and is used as the label in PDF export.

    Methods:
    --------
    get_pdf_label() -> str:
        Overrides to return the field's description.

    get_flowable(data: bool, styles: StyleSheet1) -> list[Flowable]:
        Creates a list of PDF flowables for the field's data.
    """
    template_name = "forms/fields/boolean.html"
    extra_classes = [CSS_FIELD_WRAPPER_CLASS, "boolean-field"]

    def __init__(self, description=None, **kwargs):
        """
        Initializes the BetterBooleanField with an optional description.

        Parameters:
        -----------
        description : str, optional
            The description of the field, used as the label in PDF export.
        """
        self.description = description
        super().__init__(template_name=self.template_name, **kwargs)

    def get_pdf_label(self) -> str:
        """
        Overrides to return the field's description.

        Returns:
        --------
        str:
            The description of the field.
        """
        return self.description

    def get_flowable(self, data:bool, styles: StyleSheet1) -> list[Flowable]:
        """
        Creates a list of PDF flowables for the field's data.

        Parameters:
        -----------
        data : bool
            The data to be displayed in the PDF.
        styles : StyleSheet1
            The styles to be applied to the PDF flowable.

        Returns:
        --------
        list[Flowable]:
            A list containing the formatted PDF flowables of the field.
        """
        if data:
            value = "Ja"
        else:
            value = "Nein"
        return [Paragraph(f"<b>{self.get_pdf_label()}</b>: {value}", styles['Normal'])]

class BetterCaptchaTextInput(CaptchaTextInput):
    template_name = "forms/fields/captcha.html"

class BetterCaptchaField(PDFExportMixin, CaptchaField):
    """
    Enhanced captcha field with custom layout. The captcha field is suppressed in the PDF export.

    Attributes:
    -----------
    extra_classes : list[str]
        Additional CSS classes for field styling.

    Methods:
    --------
    get_pdf_label() -> str:
        Overrides to return an empty string.

    get_flowable(data: bool, styles: StyleSheet1) -> list[Flowable]:
        Creates a list of PDF flowables for the field's data.
    """
    extra_classes = [CSS_FIELD_WRAPPER_CLASS, "captcha-field"]

    def __init__(self, **kwargs):
        """
        Initializes the BetterCaptchaField.
        """
        super().__init__(widget=BetterCaptchaTextInput(), **kwargs)

    def get_pdf_label(self) -> str:
        """
        Overrides to return an empty string.

        Returns:
        --------
        str:
            An empty string.
        """
        return ""

    def get_flowable(self, data:list[str], styles: StyleSheet1) -> list[Flowable]:
        """
        Creates a list of PDF flowables for the field's data.

        Parameters:
        -----------
        data : list[str]
            The data to not be displayed in the PDF.
        styles : StyleSheet1
            The styles to be applied to the PDF flowable.

        Returns:
        --------
        list[Flowable]:
            An empty list.
        """
        return []

class BetterCharField(PDFExportMixin, CharField):
    """
    Enhanced char field with custom layout and PDF export functionality.

    Attributes:
    -----------
    extra_classes : list[str]
        Additional CSS classes for field styling.

    Methods:
    --------

    get_flowable(data: bool, styles: StyleSheet1) -> list[Flowable]:
        Creates a list of PDF flowables for the field's data.
    """
    extra_classes = [CSS_FIELD_WRAPPER_CLASS, "char-field"]

    def get_flowable(self, data:str, styles) -> list[Flowable]:
        """
        Creates a list of PDF flowables for the field's data.

        Parameters:
        -----------
        data : str
            The data to be displayed in the PDF.
        styles : StyleSheet1
            The styles to be applied to the PDF flowable.

        Returns:
        --------
        list[Flowable]:
            A list containing the formatted PDF flowables of the field.
        """
        value = escape(data).replace('\n','<br />\n')
        return [Paragraph(f"<b>{self.get_pdf_label()}</b>: {value}", styles['Normal'])]

class BetterChoiceField(PDFExportMixin, ChoiceField):
    """
    Enhanced choice field with custom layout and PDF export functionality.

    Attributes:
    -----------
    extra_classes : list[str]
        Additional CSS classes for field styling.

    Methods:
    --------

    get_flowable(data: bool, styles: StyleSheet1) -> list[Flowable]:
        Creates a list of PDF flowables for the field's data.
    """
    extra_classes = [CSS_FIELD_WRAPPER_CLASS, "choice-field"]

    def get_flowable(self, data:str, styles: StyleSheet1) -> list[Flowable]:
        """
        Creates a list of PDF flowables for the field's data.

        Parameters:
        -----------
        data : str
            The data to be displayed in the PDF.
        styles : StyleSheet1
            The styles to be applied to the PDF flowable.

        Returns:
        --------
        list[Flowable]:
            A list containing the formatted PDF flowables of the field.
        """
        value = dict(self.choices).get(data, "error")
        return [Paragraph(f"<b>{self.get_pdf_label()}</b>: {value}", styles['Normal'])]

class BetterDateField(PDFExportMixin, DateField):
    """
    Enhanced char field with custom layout and normalization and PDF export functionality.

    Attributes:
    -----------
    extra_classes : list[str]
        Additional CSS classes for field styling.

    Methods:
    --------

    to_python(self, value: date) -> str:
        Overrides to normalize date as string.

    get_flowable(data: bool, styles: StyleSheet1) -> list[Flowable]:
        Creates a list of PDF flowables for the field's data.
    """
    extra_classes = [CSS_FIELD_WRAPPER_CLASS, "date-field"]

    def to_python(self, value: date) -> str:
        """
        Returns a date as string. The format is determined by settings.DATE_FORMAT.

        Parameters:
        -----------
        value : date
            The date to be converted to a string.

        Returns:
        --------
        str:
            The date as string.
        """
        if value in self.empty_values:
            return ""
        return super().to_python(value).strftime(settings.DATE_FORMAT)

    def get_flowable(self, data:str, styles: StyleSheet1) -> list[Flowable]:
        """
        Creates a list of PDF flowables for the field's data.

        Parameters:
        -----------
        data : str
            The data to be displayed in the PDF.
        styles : StyleSheet1
            The styles to be applied to the PDF flowable.

        Returns:
        --------
        list[Flowable]:
            A list containing the formatted PDF flowables of the field.
        """
        value = data
        return [Paragraph(f"<b>{self.get_pdf_label()}</b>: {value}", styles['Normal'])]

class BetterEmailField(PDFExportMixin, EmailField):
    """
    Enhanced email field with custom layout and PDF export functionality.

    Attributes:
    -----------
    extra_classes : list[str]
        Additional CSS classes for field styling.

    Methods:
    --------

    get_flowable(data: bool, styles: StyleSheet1) -> list[Flowable]:
        Creates a list of PDF flowables for the field's data.
    """
    extra_classes = [CSS_FIELD_WRAPPER_CLASS, "email-field"]

    def get_flowable(self, data:str, styles: StyleSheet1) -> list[Flowable]:
        """
        Creates a list of PDF flowables for the field's data.

        Parameters:
        -----------
        data : str
            The data to be displayed in the PDF.
        styles : StyleSheet1
            The styles to be applied to the PDF flowable.

        Returns:
        --------
        list[Flowable]:
            A list containing the formatted PDF flowables of the field.
        """
        value =  escape(data)
        return [Paragraph(f"<b>{self.get_pdf_label()}</b>: {value}", styles['Normal'])]

class BetterIntegerField(PDFExportMixin, IntegerField):
    """
    Enhanced integer field with custom layout and PDF export functionality.

    Attributes:
    -----------
    extra_classes : list[str]
        Additional CSS classes for field styling.
    """
    extra_classes = [CSS_FIELD_WRAPPER_CLASS, "integer-field"]

    def get_flowable(self, data:int, styles) -> list[Flowable]:
        """
        Creates a list of PDF flowables for the field's data.

        Parameters:
        -----------
        data : str
            The data to be displayed in the PDF.
        styles : StyleSheet1
            The styles to be applied to the PDF flowable.

        Returns:
        --------
        list[Flowable]:
            A list containing the formatted PDF flowables of the field.
        """
        return [Paragraph(f"<b>{self.get_pdf_label()}</b>: {data}", styles['Normal'])]

class BetterListField(PDFExportMixin, CharField):
    """
    A field to handle comma-separated lists of strings as actual lists ,
    with improved display and PDF export capabilities.

    Attributes:
    -----------
    extra_classes : list[str]
        Additional CSS classes for field styling.

    Methods:
    --------

    to_python(self, value: date) -> str:
        Overrides to normalize date as string.

    get_flowable(data: bool, styles: StyleSheet1) -> list[Flowable]:
        Creates a list of PDF flowables for the field's data.
    """
    extra_classes = [CSS_FIELD_WRAPPER_CLASS, "list-field"]

    def to_python(self, value: str) -> list[str]:
        """
        Splits a string by comma into a list.

        Parameters:
        -----------
        value : str
            The string to be converted into a list.

        Returns:
        --------
        list[str]:
            A list of strings split from the input.
        """
        if value in self.empty_values:
            return []
        return [x.strip() for x in value.split(',')]

    def run_validators(self, value: list[str]) -> None:
        """
        Validates the list as a single string.

        Parameters:
        -----------
        value : list[str]
            The list of strings to be validated.

        Raises:
        -------
        ValidationError:
            If the validation fails.
        """
        value_to_validate = ",".join(value)
        super().run_validators(value_to_validate)

    def get_flowable(self, data:list[str], styles: StyleSheet1) -> list[Flowable]:
        """
        Creates a list of PDF flowables for the field's data.

        Parameters:
        -----------
        data : list[str]
            The data to be displayed in the PDF.
        styles : StyleSheet1
            The styles to be applied to the PDF flowable.

        Returns:
        --------
        list[Flowable]:
            A list containing the formatted PDF flowables of the field.
        """
        content = []
        content.append(Paragraph(f"<b>{self.get_pdf_label()}</b>:", styles['Normal']))
        content.append(
            ListFlowable(
                [
                    ListItem(
                        Paragraph(escape(value), styles['Normal']),
                        leftIndent=20,
                    )
                    for value in data
                ],
                bulletType='bullet',
                leftIndent=10,
                bulletOffsetY=1,
            )
        )
        return content

class BetterMultiValueField(PDFExportMixin, MultiValueField, metaclass=ABCMeta):
    """
    Enhanced multi value field with custom layout and PDF export functionality.
    The field needs to be subclassed.

    Attributes:
    -----------
    extra_classes : list[str]
        Additional CSS classes for field styling.

    """
    extra_classes = [CSS_FIELD_WRAPPER_CLASS, "multi-value-field"]

class BetterPhoneNumberField(PDFExportMixin, PhoneNumberField):
    """
    Enhanced phone number field with custom layout and normalization and PDF export functionality.

    Attributes:
    -----------
    extra_classes : list[str]
        Additional CSS classes for field styling.

    Methods:
    --------

    to_python(self, value: PhoneNumber) -> str:
        Overrides to normalize phone number as string i e164 standard.

    get_flowable(data: bool, styles: StyleSheet1) -> list[Flowable]:
        Creates a list of PDF flowables for the field's data.
    """
    extra_classes = [CSS_FIELD_WRAPPER_CLASS, "phone-number-field"]

    def to_python(self, value:str) -> str:
        """
        Returns a phone number as string in e164 format.

        Parameters:
        -----------
        value : str
            The unformatted phone number.

        Returns:
        --------
        str:
            The phone number as string.
        """
        if value in self.empty_values:
            return ""
        return super().to_python(value).as_e164

    def get_flowable(self, data:str, styles: StyleSheet1) -> list[Flowable]:
        """
        Creates a list of PDF flowables for the field's data.

        Parameters:
        -----------
        data : str
            The data to be displayed in the PDF.
        styles : StyleSheet1
            The styles to be applied to the PDF flowable.

        Returns:
        --------
        list[Flowable]:
            A list containing the formatted PDF flowables of the field.
        """
        value = data
        return [Paragraph(f"<b>{self.get_pdf_label()}</b>: {value}", styles['Normal'])]
