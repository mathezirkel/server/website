from django.forms import BoundField
from django.template import Library

register = Library()

# Currently, there is no way do invoke custom css classes
# to the method css_classes of BoundField within the template
@register.filter
def css_classes(bound_field: BoundField) -> str:
    if hasattr(bound_field.field,"extra_classes"):
        return bound_field.css_classes(bound_field.field.extra_classes)
    return bound_field.css_classes()
