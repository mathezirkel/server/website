from django_minify_html.middleware import MinifyHtmlMiddleware

class MinifyMiddleware(MinifyHtmlMiddleware):
    minify_args = MinifyHtmlMiddleware.minify_args | {
        "ensure_spec_compliant_unquoted_attribute_values" : True,
        "keep_spaces_between_attributes" : True,
        "keep_closing_tags" : True,
        "keep_comments" : False,
        "keep_html_and_head_opening_tags" : True,
        "keep_input_type_text_attr" : True,
        "keep_ssi_comments" : False,
        "do_not_minify_doctype" : True,
        "remove_bangs" : False,
        "remove_processing_instructions" : False
    }
