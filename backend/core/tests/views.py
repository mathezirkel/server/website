from __future__ import annotations
from abc import ABCMeta, abstractmethod
from typing import TYPE_CHECKING
from unittest import mock

from django.core import mail
from django.core.cache import cache
from django.conf import settings
from django.test import TestCase, override_settings

if TYPE_CHECKING:
    from core.tests.data import FormDataManager

@override_settings(
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
            'LOCATION': 'unique-snowflake',
        }
    }
)
class ViewTest(TestCase, metaclass=ABCMeta):
    @classmethod
    @abstractmethod
    def path(cls) -> str:
        pass

    @classmethod
    @abstractmethod
    def template(cls) -> str:
        pass

    @staticmethod
    @abstractmethod
    def referer() -> str:
        pass

    @classmethod
    @abstractmethod
    def success_url(cls) -> str:
        pass

    @staticmethod
    @abstractmethod
    def success_template() -> str:
        pass

    @staticmethod
    def data_manager() -> type[FormDataManager]:
        pass

    def tearDown(self) -> None:
        cache.clear()
        return super().tearDown()

    def test_view_exists_at_desired_location(self) -> None:
        response = self.client.get(self.path())
        self.assertEqual(response.status_code, 200)

    def test_view_uses_correct_template(self) -> None:
        response = self.client.get(self.path())
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template())

    @override_settings(
        RATELIMIT_ENABLE=False,
    )
    @mock.patch('core.forms.fields.BetterCaptchaField.clean', return_value='mocked_value')
    def test_form_valid(self, _mocked_captcha_clean: mock.MagicMock) -> None:
        """
        Test case to verify that the form correctly redirects upon valid submission
        """
        data = self.data_manager().unpack_data_set(self.data_manager().valid_data_set())
        response = self.client.post(
            self.path(),
            data=data,
            HTTP_REFERER=self.referer()
        )
        self.assertRedirects(response, self.success_url())
        # Check whether mails were sent
        self.assertTrue(mail.outbox)

    @override_settings(
        RATELIMIT_ENABLE=False,
    )
    @mock.patch('core.forms.fields.BetterCaptchaField.clean', return_value='mocked_value')
    def test_form_invalid(self, _mocked_captcha_clean: mock.MagicMock) -> None:
        data = self.data_manager().unpack_data_set(self.data_manager().invalid_data_set())
        response = self.client.post(
            self.path(),
            data=data,
            HTTP_REFERER=self.referer()
        )
        self.assertEqual(response.status_code, 200)
        # Check whether mails were sent
        self.assertFalse(mail.outbox)

    @mock.patch('core.forms.fields.BetterCaptchaField.clean', return_value='mocked_value')
    def test_rate_limit(self, _mocked_captcha_clean: mock.MagicMock):
        """
        Test case to verify rate-limiting functionality.
        """
        headers = {
            'HTTP_X_REAL_IP': '192.168.1.1',
        }
        limit = int(settings.RATELIMITS[0].split("/")[0])
        data = self.data_manager().unpack_data_set(self.data_manager().valid_data_set())
        for i in range(limit + 1):
            response = self.client.post(self.path(), data=data, **headers)
            if i < limit:
                self.assertEqual(response.status_code, 302)
            else:
                self.assertEqual(response.status_code, 429)
                self.assertTemplateUsed(response, "429.html")
