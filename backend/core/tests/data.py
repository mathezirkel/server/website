from __future__ import annotations
from abc import ABCMeta, abstractmethod
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from core.forms.forms import BetterForm
    from core.forms.multiforms import MultiForm
    from core.tests import T
    from core.tests.data_generator.forms import FormDataGenerator

class FormDataManager(metaclass=ABCMeta):

    @staticmethod
    def unpack_data_set(data_set: dict[str, None | T | tuple[T]]) -> dict[str, T]:
        result = {}
        for field, data in data_set.items():
            # Do not include field if value is None
            if data is None:
                pass
            # Unpack multi-valued fields
            elif isinstance(data, tuple):
                for i, value in enumerate(data):
                    result[f"{field}_{i}"] = value
            else:
                result[field] = data
        return result

    @staticmethod
    @abstractmethod
    def form_class() -> type[BetterForm] | type[MultiForm]:
        pass

    @staticmethod
    @abstractmethod
    def form_fields() -> list[str]:
        pass

    @classmethod
    @abstractmethod
    def valid_data_set(cls) -> dict[str, T]:
        pass

    @classmethod
    @abstractmethod
    def invalid_data_set(cls) -> dict[str, T]:
        pass

    @classmethod
    @abstractmethod
    def form_data_generator(cls) -> FormDataGenerator:
        pass
