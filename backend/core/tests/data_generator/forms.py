from typing import Generator

from core.tests.data_generator.fields import FieldDataGenerator

class FormDataGenerator():
    def __init__(self, field_data_generators: dict[str, FieldDataGenerator] = None):
        if field_data_generators is None:
            field_data_generators = {}
        self.field_data_generators = field_data_generators

    def __iter__(self) -> Generator[tuple[str, FieldDataGenerator], None, None]:
        for key, generator in self.field_data_generators.items():
            yield key, generator

    def __getitem__(self, key: str) -> FieldDataGenerator:
        return self.field_data_generators[key]
