from __future__ import annotations
from abc import ABCMeta, abstractmethod
from random import choice
from string import ascii_letters, printable, whitespace
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from core.tests import T

_DEFAULT_ALLOWED_CHARACTERS = printable + "ÄÖÜßäöü"

class FieldDataGenerator(metaclass=ABCMeta):

    def __init__(self, required : bool):
        self.required = required

    @property
    @abstractmethod
    def valid_data(self) -> list[T | tuple[T,T]]:
        pass

    @property
    @abstractmethod
    def invalid_data(self) -> list[T | tuple[T,T]]:
        pass


class BetterBooleanFieldDataGenerator(FieldDataGenerator):

    _true_values = [
        "on",
        True,
        1,
        "True",
        "1",
    ]

    _false_values = [
        None,
        False,
        0,
        "False",
        "false",
    ]

    @property
    def valid_data(self) -> list[T]:
        if self.required:
            return self._true_values
        return self._true_values + self._false_values

    @property
    def invalid_data(self) -> list[T]:
        if self.required:
            return self._false_values
        return []

class BetterCharFieldDataGenerator(FieldDataGenerator):

    def __init__(
        self,
        required : bool,
        min_length : int | None = None,
        max_length : int | None = None,
        allowed_characters: str | None = None
    ):
        self.allowed_characters = allowed_characters or _DEFAULT_ALLOWED_CHARACTERS
        self.min_length = min_length
        self.max_length = max_length
        super().__init__(required)

    @property
    def valid_data(self) -> list[str]:
        valid_data: list[str] = []
        if not self.required:
            valid_data.append("")
        if (m := self.min_length):
            trailing_char = choice([char for char in self.allowed_characters if char not in whitespace]) # nosec B311
            if m == 1:
                valid_data.append(trailing_char)
            elif m > 1:
                random_string = "".join([choice(self.allowed_characters) for _ in range(m-2)]) # nosec B311
                valid_data.append(trailing_char + random_string + trailing_char)
        if m := self.max_length:
            trailing_char = choice([char for char in self.allowed_characters if char not in whitespace]) # nosec B311
            if m == 1:
                valid_data.append(trailing_char)
            elif m > 1:
                random_string = "".join([choice(self.allowed_characters) for _ in range(m-2)]) # nosec B311
                valid_data.append(trailing_char + random_string + trailing_char)
            valid_data.append("".join([choice(self.allowed_characters) for _ in range(m)])) # nosec B311
        return valid_data

    @property
    def invalid_data(self) -> list[str]:
        invalid_data: list[str] = []
        if self.required:
            invalid_data.append("")
        if (m := self.min_length):
            trailing_char = choice([char for char in self.allowed_characters if char not in whitespace]) # nosec B311
            if m == 2:
                invalid_data.append(trailing_char)
            elif m > 2:
                random_string = "".join([choice(self.allowed_characters) for _ in range(m-3)]) # nosec B311
                invalid_data.append(trailing_char + random_string + trailing_char)
        if m := self.max_length:
            trailing_char = choice([char for char in self.allowed_characters if char not in whitespace]) # nosec B311
            random_string = "".join([choice(self.allowed_characters) for _ in range(m-1)]) # nosec B311
            invalid_data.append(trailing_char + random_string + trailing_char)
        return invalid_data

class BetterChoiceFieldDataGenerator(FieldDataGenerator):
    def __init__(self, required: bool, choices: list[str]):
        self.choices = choices
        super().__init__(required)

    @property
    def valid_data(self) -> list[str]:
        return [choice for choice, _ in self.choices.items() if choice or not self.required]

    @property
    def invalid_data(self) -> list[str]:
        invalid_data = ["FOOBAR"]
        if self.required:
            invalid_data.append("")
        return invalid_data

class BetterDateFieldDataGenerator(FieldDataGenerator):
    @property
    def valid_data(self) -> list[str]:
        valid_data = [
            "2000-01-01",
            "01.01.2000",
            "01.01.2000",
        ]
        if not self.required:
            valid_data.append("")
        return valid_data

    @property
    def invalid_data(self) -> list[str]:
        invalid_data = [
            "01/13/2000",
        ]
        if self.required:
            invalid_data.append("")
        return invalid_data

class BetterEmailFieldDataGenerator(FieldDataGenerator):

    @classmethod
    def generate_mail_address(cls, length: int) -> str:
        subdomain_length = min(60, length - 5)
        user_length = length - subdomain_length - 4
        user = "".join([choice(ascii_letters) for _ in range(user_length)]) # nosec B311
        subdomain = "".join([choice(ascii_letters) for _ in range(subdomain_length)]) # nosec B311
        return f"{user}@{subdomain}.de"

    def __init__(self, required : bool, min_length : int | None = None, max_length : int = 320):
        self.min_length = min_length
        self.max_length = max_length
        super().__init__(required)

    @property
    def valid_data(self) -> list[str]:
        valid_data = []
        if not self.required:
            valid_data.append("")
        if (m := self.min_length) and m > 5:
            valid_data.append(BetterEmailFieldDataGenerator.generate_mail_address(m))
        if self.max_length <= 5:
            raise ValueError(
                f"Cannot create a valid email address! max_length must be at least 6. \
                Current max_length: {self.max_length}"
            )
        valid_data.append(BetterEmailFieldDataGenerator.generate_mail_address(self.max_length))
        return valid_data

    @property
    def invalid_data(self) -> list[str]:
        invalid_data = []
        invalid_data.append("".join(choice(ascii_letters) for _ in range(10))) # nosec B311
        if self.required:
            invalid_data.append("")
        if (m := self.min_length) and m > 6:
            invalid_data.append(self.generate_mail_address(m-1))
        if (m := self.max_length) > 5:
            invalid_data.append(self.generate_mail_address(m+1))
        return invalid_data

class BetterListFieldDataGenerator(BetterCharFieldDataGenerator):
    def __init__(
        self,
        required : bool,
        min_length : int | None = None,
        max_length : int | None = None,
        allowed_characters: str | None = None
    ):
        allowed_characters = (allowed_characters or _DEFAULT_ALLOWED_CHARACTERS).replace(",","")
        super().__init__(required, min_length, max_length, allowed_characters)

class BetterPhoneNumberFieldDataGenerator(FieldDataGenerator):
    @property
    def valid_data(self) -> list[str]:
        valid_data = [
            "0821 5985806",
            "0821/598-5806",
            "+49 821 5985806",
            "498215985806",
            "8215985806",
        ]
        if not self.required:
            valid_data.append("")
        return valid_data

    @property
    def invalid_data(self) -> list[str]:
        invalid_data = [
            "0123456789",
            "abc",
        ]
        if self.required:
            invalid_data.append("")
        return invalid_data
