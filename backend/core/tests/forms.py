from __future__ import annotations
from abc import ABCMeta, abstractmethod
import logging
from typing import TYPE_CHECKING
from unittest import mock

from django.core import mail
from django.test import SimpleTestCase

if TYPE_CHECKING:
    from core.tests.data import FormDataManager

class FormTestCase(SimpleTestCase, metaclass=ABCMeta):

    @staticmethod
    @abstractmethod
    def data_manager() -> type[FormDataManager]:
        pass

    @classmethod
    @abstractmethod
    def email_assertions(cls) -> list[dict[str, str | list[str]]]:
        pass

    def test_form_contains_exactly_all_fields(self) -> None:
        form = self.data_manager().form_class()()
        with self.subTest(input=f"Form contains at least {self.data_manager().form_fields()}"):
            for field in self.data_manager().form_fields():
                if field not in form.fields:
                    logging.error("The form %s does not contain the necessary field %s.\n", form, field)
                self.assertIn(field, form.fields)
        with self.subTest(input=f"Form contains at most {self.data_manager().form_fields()}"):
            for field in form.fields:
                if field not in self.data_manager().form_fields():
                    logging.error("The form %s contains additional field %s.\n", form, field)
                self.assertIn(field, self.data_manager().form_fields())

    @mock.patch('core.forms.fields.BetterCaptchaField.clean', return_value='mocked_value')
    def test_form_valid(self, _mock_captcha_clean : mock.MagicMock) -> None:
        with self.subTest(input=self.data_manager().valid_data_set()):
            form = self.data_manager().form_class()(
                data=self.data_manager().unpack_data_set(self.data_manager().valid_data_set())
            )
            if not form.is_valid():
                logging.error("The following is invalid, but should be valid:")
                logging.error("Data: %s", self.data_manager().valid_data_set())
                logging.error("Errors: %s\n", form.errors)
            self.assertTrue(form.is_valid())
        for key, generator in self.data_manager().form_data_generator():
            for value in generator.valid_data:
                valid_data_set = self.data_manager().valid_data_set().copy()
                valid_data_set.update({key:value})
                with self.subTest(input=valid_data_set):
                    unpacked_valid_data_set = self.data_manager().unpack_data_set(valid_data_set)
                    form = self.data_manager().form_class()(data=unpacked_valid_data_set)
                    if not form.is_valid():
                        logging.error("The following is invalid, but should be valid:")
                        logging.error("Form class: %s", self.data_manager().form_class())
                        logging.error("Field: %s", key)
                        logging.error("Value: %s", repr(value))
                        logging.error("Errors: %s\n", form.errors)
                    self.assertTrue(form.is_valid())

    @mock.patch('core.forms.fields.BetterCaptchaField.clean', return_value='mocked_value')
    def test_form_invalid(self, _mock_captcha_clean : mock.MagicMock) -> None:
        for key, generator in self.data_manager().form_data_generator():
            for value in generator.invalid_data:
                invalid_data_set = self.data_manager().valid_data_set().copy()
                invalid_data_set.update({key:value})
                with self.subTest(input=invalid_data_set):
                    unpacked_invalid_data_set = self.data_manager().unpack_data_set(invalid_data_set)
                    form = self.data_manager().form_class()(data=unpacked_invalid_data_set)
                    if form.is_valid():
                        logging.error("The following is valid, but should be invalid:")
                        logging.error("Form class: %s", self.data_manager().form_class())
                        logging.error("Field: %s", key)
                        logging.error("Value: %s\n", repr(value))
                    self.assertFalse(form.is_valid())
                    self.assertIn(key, form.errors)

    @mock.patch('core.forms.fields.BetterCaptchaField.clean', return_value='mocked_value')
    def test_submit_form(self, _mock_captcha_clean : mock.MagicMock) -> None:
        form = self.data_manager().form_class()(
            data=self.data_manager().unpack_data_set(self.data_manager().valid_data_set())
        )
        form.is_valid()
        form.submit()
        self.assertEqual(len(mail.outbox), len(self.email_assertions()))
        for i, msg in enumerate(mail.outbox):
            self.assertEqual(set(msg.to), set(self.email_assertions()[i]["to"]))
            self.assertEqual(set(msg.cc), set(self.email_assertions()[i]["cc"]))
            self.assertEqual(set(msg.bcc), set(self.email_assertions()[i]["bcc"]))
            self.assertEqual(set(msg.reply_to), set(self.email_assertions()[i]["reply_to"]))
            self.assertEqual(
                {attachment[0] for attachment in msg.attachments},
                set(self.email_assertions()[i]["attachments"])
            )
            self.assertEqual(msg.subject, self.email_assertions()[i]["subject"])
            if body := self.email_assertions()[i].get("body", ""):
                self.assertEqual(msg.body, body)

    @mock.patch('core.forms.fields.BetterCaptchaField.clean', return_value='mocked_value')
    def test_html_escaping(self, _mock_captcha_clean : mock.MagicMock) -> None:
        malicious_string = '<script>alert("hack");</script>'
        escaped_string = "&lt;script&gt;alert(&quot;hack&quot;);&lt;/script&gt;"

        for i, assertion in enumerate(self.email_assertions()):
            malicious_data = self.data_manager().valid_data_set().copy()
            if not assertion["fieldToBeEscaped"]:
                continue
            malicious_data.update({k : malicious_string for k in assertion["fieldToBeEscaped"]})
            form = self.data_manager().form_class()(data=self.data_manager().unpack_data_set(malicious_data))
            if not form.is_valid():
                logging.error("The following form errors occurred:")
                logging.error("Data: %s", malicious_data)
                logging.error("Errors: %s\n", form.errors)
            self.assertTrue(form.is_valid())
            form.submit()
            body = mail.outbox[i].body
            self.assertTrue(escaped_string in body)
            self.assertFalse(malicious_string in body)
