from __future__ import annotations
from abc import ABCMeta
from typing import TYPE_CHECKING

from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponseRedirect
from django.views.generic.base import ContextMixin, TemplateResponseMixin, View

if TYPE_CHECKING:
    from django.http.request import HttpRequest
    from django.template.response import TemplateResponse
    from typing import Any
    from core.forms.multiforms import MultiForm

class MultiFormMixin(ContextMixin, metaclass=ABCMeta):
    """Provide a way to show and handle multiforms in a request."""

    initial = {}
    multi_form_class = None
    success_url = None

    def get_initial(self) -> str:
        """Return the initial data to use for multiforms on this view."""
        return self.initial.copy()

    def get_multi_form_class(self) -> type:
        """Return the multiform class to use."""
        return self.multi_form_class

    def get_multi_form(self, multi_form_class : type = None) -> MultiForm:
        """Return an instance of the multiform to be used in this view."""
        if multi_form_class is None:
            multi_form_class = self.get_multi_form_class()
        return multi_form_class(**self.get_multi_form_kwargs())

    def get_multi_form_kwargs(self) -> dict[str,Any]:
        """Return the keyword arguments for instantiating the multiform."""
        kwargs = {
            "initial": self.get_initial(),
        }

        if self.request.method in ("POST", "PUT"):
            kwargs.update(
                {
                    "data": self.request.POST,
                    "files": self.request.FILES,
                }
            )
        return kwargs

    def get_success_url(self) -> str:
        """Return the URL to redirect to after processing a valid multiform."""
        if not self.success_url:
            raise ImproperlyConfigured("No URL to redirect to. Provide a success_url.")
        return str(self.success_url)  # success_url may be lazy

    def multi_form_valid(self, _multi_form: MultiForm) -> HttpResponseRedirect:
        """If the multiform is valid, redirect to the supplied URL."""
        return HttpResponseRedirect(self.get_success_url())

    def multi_form_invalid(self, multi_form: MultiForm) -> TemplateResponse:
        """If the multiform is invalid, render the invalid form."""
        return self.render_to_response(self.get_context_data(multi_form=multi_form))

    def get_context_data(self, **kwargs) -> dict[str,Any]:
        """Insert the multiform into the context dict."""
        if "multi_form" not in kwargs:
            kwargs["multi_form"] = self.get_multi_form()
        return super().get_context_data(**kwargs)


class ProcessMultiFormView(View, metaclass=ABCMeta):
    """Render the forms on GET and process them on POST."""

    def get(self, _request: HttpRequest, *_args, **_kwargs) -> TemplateResponse:
        """Handle GET requests: instantiate a blank version of the multiform."""
        return self.render_to_response(self.get_context_data()) # pylint: disable=no-member

    def post(
        self,
        _request: HttpRequest,
        *_args,
        **_kwargs
    ) -> HttpResponseRedirect | TemplateResponse:
        """
        Handle POST requests: instantiate the instance of the multiform with the passed
        POST variables and then check if they are valid.
        """
        # pylint: disable=no-member
        multi_form = self.get_multi_form()
        if multi_form.is_valid():
            return self.multi_form_valid(multi_form)
        return self.multi_form_invalid(multi_form)
        # pylint: enable=no-member

    # PUT is a valid HTTP verb for creating (with a known URL) or editing an
    # object, note that browsers only support POST for now.
    def put(self, *args, **kwargs) -> HttpResponseRedirect | TemplateResponse:
        return self.post(*args, **kwargs)


class BaseMultiFormView(MultiFormMixin, ProcessMultiFormView, metaclass=ABCMeta):
    """A base view for displaying a multiple forms."""


class MultiFormView(TemplateResponseMixin, BaseMultiFormView):
    """A view for displaying multiple forms and rendering a template response."""
