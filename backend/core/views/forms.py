from __future__ import annotations
from abc import ABCMeta
from typing import TYPE_CHECKING

from django.conf import settings
from django.shortcuts import redirect, reverse
from django.utils.decorators import method_decorator
from django.views.generic.base import View, TemplateView
from django_ratelimit.decorators import ratelimit

from core.utils.decorators import apply_multiple_decorators

if TYPE_CHECKING:
    from django.http import HttpRequest

class RatelimitMixin(View, metaclass=ABCMeta):
    """
    A mixin to apply ratelimits o POST requests.

    Ratelimits are configured in settings.RATELIMITS.

    Attributes:
    -----------
    ratelimits : list
        A list of rate-limiting decorators to apply to the dispatch method.

    Methods:
    --------
    dispatch(*args, **kwargs) -> HttpResponse
        Handles HTTP methods and applies rate-limiting.
    """

    ratelimits = [
        ratelimit(key='header:x-real-ip', rate=rate, method='POST', block=True)
        for rate in settings.RATELIMITS
    ]

    @method_decorator(apply_multiple_decorators(ratelimits))
    def dispatch(self, *args, **kwargs):
        """
        Handles HTTP methods and applies rate-limiting.
        """
        return super().dispatch(*args, **kwargs)

class SuccessView(TemplateView, metaclass=ABCMeta):
    """
    Class-based view to display a success message after successful form submission.

    Attributes:
    -----------
    template_name : str
        Path to the template used to render the success message.

    Methods:
    --------
    get(request, *args, **kwargs) -> HttpResponse
        Renders the success message.
    """
    template_name = None
    required_referer_substring = None
    redirect_name = None

    def get(self, request: HttpRequest, *args, **kwargs):
        """
        Renders the success message.
        """
        current_referer = request.META.get("HTTP_REFERER")
        if current_referer and self.required_referer_substring in current_referer:
            return super().get(request, *args, **kwargs)
        return redirect(reverse(self.redirect_name))
