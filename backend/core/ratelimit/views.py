from __future__ import annotations
from typing import TYPE_CHECKING

from django.views.generic import TemplateView

if TYPE_CHECKING:
    from django.http import HttpRequest
class RatelimitView(TemplateView):
    """
    Class-based view to handle rate-limited requests, responding with a 429 status code.

    Attributes:
    -----------
    template_name : str
        Path to the template used to display the rate-limit message.

    Methods:
    --------
    get(request, *args, **kwargs) -> HttpResponse
        Renders the rate-limit message with a 429 status code.

    post(request, *args, **kwargs) -> HttpResponse
        Delegates to the `get` method for handling POST requests
        and responds with a 429 status code.
    """
    template_name = "429.html"

    def get(self, request: HttpRequest, *args, **kwargs):
        """
        Renders the rate-limit message with a 429 status code.
        """
        response = super().get(request, *args, **kwargs)
        response.status_code = 429
        return response

    def post(self, request: HttpRequest, *args, **kwargs):
        """
        Delegates to the `get` method for handling POST requests
        and responds with a 429 status code.
        """
        response = self.get(request, *args, **kwargs)
        response.status_code = 429
        return response
