"""
Django Middleware for Ratelimiting
==================================

This module contains the `RatelimitMiddleware` class used
to use a custom view to handle Ratelimited exceptions.

The code was adapted from https://github.com/jsocol/django-ratelimit,
which is licensed under [Apache-2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).

It was rewritten to make it compatible with class-based views,
as it was originally designed for function-based views.

Settings
--------

To use this middleware, add it to your `MIDDLEWARE` list in Django's
settings. Also, specify the custom view to display when a request is ratelimited
by setting the `RATELIMIT_VIEW` variable in settings.

Classes:
--------
RatelimitMiddlware
    A class to use custom view to handle Ratelimited exceptions.

"""
from __future__ import annotations
from typing import TYPE_CHECKING

from django.conf import settings
from django.utils.module_loading import import_string
from django_ratelimit.exceptions import Ratelimited

if TYPE_CHECKING:
    from typing import Callable
    from django.http import HttpRequest

class RatelimitMiddleware:
    """
    Middleware for handling ratelimited requests.

    This middleware checks if a request has been ratelimited and,
    if so, invokes a custom view specified in settings to return a
    response to the client.

    Parameters:
    -----------
    get_response : Callable
        A callable to get the response for a request. Passed as an argument
        when the middleware is initialized.

    Attributes:
    -----------
    get_response : Callable
        A callable that takes a request and returns a response.
    """

    def __init__(self, get_response: Callable):
        self.get_response = get_response

    def __call__(self, request: HttpRequest) -> Callable:
        return self.get_response(request)

    def process_exception(self, request: HttpRequest, exception: Ratelimited):
        """
        Handle exceptions that occur while processing the request.

        This method checks if the exception is an instance of `Ratelimited` and,
        if so, uses a custom view to generate a response.

        Parameters:
        -----------
        request : HttpRequest
            The request object.

        exception : Exception
            The exception raised during processing.

        Returns:
        --------
        HttpResponse | None
            Returns the custom response for ratelimited requests or `None` if
            the exception is not an instance of `Ratelimited`.
        """
        if not isinstance(exception, Ratelimited):
            return None
        view = import_string(settings.RATELIMIT_VIEW)
        response = view.as_view()(request)

        return response
