"""
Utility Module
========================

This module provides a utility function for applying multiple decorators to a single function.
While decorators in Python are usually applied one by one on top of a function, this utility
simplifies the application of multiple decorators in a more concise and readable manner.

Functions:
----------
apply_multiple_decorators(decorators) -> callable:
    Allows the application of multiple decorators to a view function or any regular function.
    This is especially useful when there's a need to apply several decorators in a specific order.

Example:
--------
Suppose you have two decorators `decorator_a` and `decorator_b`. Instead of doing:

    @decorator_a
    @decorator_b
    def my_function():
        pass

You can use the utility function as:

    @apply_multiple_decorators([decorator_a, decorator_b])
    def my_function():
        pass

This becomes particularly helpful when dealing with a dynamic list of decorators.

"""
from functools import wraps
from typing import Callable

def apply_multiple_decorators(decorators: list[Callable]) -> Callable:
    """
    Applies multiple rate-limiting decorators to a view function.

    Parameters:
    -----------
    decorators : list
        A list of rate-limiting decorator functions to apply.

    Returns:
    --------
    callable
        The decorated view function with multiple rate-limiting decorators applied.
    """
    def decorator(func: Callable) -> Callable:
        @wraps(func)
        def wrapped(*args, **kwargs) -> Callable:
            decorated_func = func
            for dec in reversed(decorators):
                decorated_func = dec(decorated_func)
            return decorated_func(*args, **kwargs)
        return wrapped
    return decorator
