import json
import logging

from django.conf import settings

def get_event_data() -> dict[str, dict[str, bool | int | str]]:
    filename = settings.EVENTS_CONFIG_FILE
    with open(filename, 'r', encoding="utf-8") as event_file:
        return json.load(event_file)

def get_zirkel_data() -> dict[str, dict[str, dict[str, int | str | list[str]]]]:
    filename = settings.ZIRKEL_CONFIG_FILE
    try:
        with open(filename, 'r', encoding="utf-8") as zirkel_file:
            return json.load(zirkel_file)
    except FileNotFoundError:
        logging.warning("Please provide a ZIRKEL_CONFIG_FILE!")
        logging.warning("File not found: '%s'", filename)
        return {}
