"""
Django settings for cabret project.

Generated by 'django-admin startproject' using Django 4.2.

For more information on this file, see
https://docs.djangoproject.com/en/4.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/4.2/ref/settings/
"""

import os
from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv("DJANGO_SECRET_KEY", "django-insecure")


# Production mode
# Assumes to be deployed behind a SSL terminating reverse proxy

PRODUCTION = int(os.getenv("DJANGO_PRODUCTION", "1"))
if PRODUCTION:
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
    USE_X_FORWARDED_HOST = True
    SECURE_CONTENT_TYPE_NOSNIFF = True
    SECURE_CROSS_ORIGIN_OPENER_POLICY = 'same-origin'
    SECURE_REFERRER_POLICY = 'same-origin'
    X_FRAME_OPTIONS = 'DENY'
    SESSION_COOKIE_SECURE = True


# SECURITY WARNING: don't run with debug turned on in production!

DEBUG = int(os.getenv("DJANGO_DEBUG", "0"))


# Allowed Hosts

ALLOWED_HOSTS = os.getenv("DJANGO_ALLOWED_HOSTS", "localhost").strip().split(" ")
ALLOWED_CIDR_NETS = os.getenv("DJANGO_ALLOWED_CIDR_NETS", "127.0.0.1").strip().split(" ")


# CSRF

CSRF_TRUSTED_ORIGINS = os.getenv("DJANGO_CSRF_TRUSTED_ORIGINS", "http://localhost").split(" ")
CSRF_USE_SESSIONS = True


# Ratelimit for requests

RATELIMITS = os.getenv('DJANGO_RATELIMITS', '5/d').split(" ")
RATELIMIT_VIEW = "core.ratelimit.views.RatelimitView"


# Captcha

CAPTCHA_LETTER_ROTATION = (-10, 10)
CAPTCHA_FOREGROUND_COLOR = '#001100'
CAPTCHA_BACKGROUND_COLOR = '#ffffff'
CAPTCHA_NOISE_FUNCTIONS = ()
CAPTCHA_CHALLENGE_FUNCT = 'captcha.helpers.math_challenge'


# Application definition

INSTALLED_APPS = [
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    "django_minify_html",
    'django_ratelimit',
    "phonenumber_field",
    'captcha',
    'core.apps.CoreConfig',
    'feedback.apps.FeedbackConfig',
    'contact.apps.ContactConfig',
    'registration.apps.RegistrationConfig',
]

MIDDLEWARE = [
    'allow_cidr.middleware.AllowCIDRMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    "django.middleware.locale.LocaleMiddleware",
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'core.minify.middleware.MinifyMiddleware',
    'core.ratelimit.middleware.RatelimitMiddleware',
]

ROOT_URLCONF = 'cabret.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
                BASE_DIR / "templates",
            ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'cabret.wsgi.application'


# Database
# https://docs.djangoproject.com/en/4.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}


# Cache

REDIS_HOST = os.environ.get("REDIS_HOST", "redis")
REDIS_PORT = os.environ.get("REDIS_PORT", "6379")
REDIS_PASSWORD = os.environ.get("REDIS_PASSWORD", "")
REDIS_URL = f"redis://:{REDIS_PASSWORD}@{REDIS_HOST}:{REDIS_PORT}"
CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.redis.RedisCache",
        "LOCATION": f"{REDIS_URL}",
    }
}


# Sessions

SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_EXPIRE_AT_BROWSER_CLOSE = True


# Internationalization
# https://docs.djangoproject.com/en/4.2/topics/i18n/

LANGUAGE_CODE = 'de-de'
TIME_ZONE = 'Europe/Berlin'
USE_I18N = True
USE_TZ = True
DATE_FORMAT = '%Y-%m-%d'
DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.2/howto/static-files/

STATIC_URL = 'static/'

# Default primary key field type
# https://docs.djangoproject.com/en/4.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

# Mail configuration

DEFAULT_FROM_EMAIL = os.getenv("DJANGO_DEFAULT_FROM_EMAIL", "")
EMAIL_HOST = os.getenv("DJANGO_EMAIL_HOST", "")
EMAIL_PORT = int(os.getenv("DJANGO_EMAIL_PORT", "25"))
EMAIL_HOST_USER = os.getenv("DJANGO_EMAIL_HOST_USER", "")
EMAIL_HOST_PASSWORD = os.getenv("DJANGO_EMAIL_HOST_PASSWORD", "")
EMAIL_USE_TLS = int(os.getenv("DJANGO_EMAIL_USE_TLS", "0"))
EMAIL_USE_SSL = int(os.getenv("DJANGO_EMAIL_USE_SSL", "0"))
EMAIL_SUBJECT_PREFIX = ""

EMAIL_MATHEZIRKEL = os.getenv("DJANGO_EMAIL_MATHEZIRKEL", "")

# Phone number
PHONENUMBER_DEFAULT_REGION = "DE"
PHONENUMBER_DEFAULT_FORMAT = "E164"

# Mathezirkel config files

EVENTS_CONFIG_FILE = os.getenv("DJANGO_EVENTS_CONFIG_FILE", "./data/events.json")
ZIRKEL_CONFIG_FILE = os.getenv("DJANGO_ZIRKEL_CONFIG_FILE", "./data/zirkel.json")
