"""
URL configuration for cabret project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from __future__ import annotations
from typing import TYPE_CHECKING

from django.http import HttpResponse
from django.urls import path, include

if TYPE_CHECKING:
    from django.urls import URLResolver

urlpatterns : list[URLResolver] = [
    path('feedback/', include('feedback.urls'), name="feedback_app"),
    path('kontakt/', include('contact.urls'), name="contact_app"),
    path('anmeldung/', include('registration.urls'), name="registration_app"),
    path('captcha/', include('captcha.urls'), name="captcha_app"),
    path(
        'healthy/',
        lambda request: HttpResponse('healthy', content_type='text/plain', status=200),
        name="health_endpoint"
    ),
]
