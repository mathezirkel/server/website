"""
Registration Views Module
=====================

This module contains Django views to manage registration forms. It incorporates rate-limiting
capabilities to prevent misuse.

Functions:
----------
None

Classes:
--------
RegistrationFormView(FormView)
    Manages the rendering and processing of the registration form,
    applying rate-limiting when needed.

RegistrationSuccessView(TemplateView)
    Displays a success message after a successful registration form submission.

"""
from __future__ import annotations
from typing import TYPE_CHECKING

from core.views.forms import RatelimitMixin, SuccessView
from core.views.multiforms import MultiFormView
from registration.forms.form_mappings import MULTIFORM_MAPPING

if TYPE_CHECKING:
    from django.http import HttpResponseRedirect
    from registration.forms.multiforms import MetaRegistrationMultiForm

class RegistrationMultiFormView(MultiFormView, RatelimitMixin):
    """
    Manages the rendering and processing of the registration form.

    Attributes:
    -----------
    success_url : str
        The URL to navigate to upon successful form submission.

    Methods:
    --------

    multi_form_valid(form) -> HttpResponse
        Processes the form when validation succeeds.
    """
    success_url = "danke/"

    def get_multi_form_class(self) -> type:
        form_class_key = self.kwargs.get('event_key')
        return MULTIFORM_MAPPING.get(form_class_key)

    def get_template_names(self) -> str:
        form_key = self.kwargs.get('event_key')
        return f"anmeldung/{form_key}/index.html"

    def multi_form_valid(self, multi_form: MetaRegistrationMultiForm) -> HttpResponseRedirect:
        """
        Handles successful form submissions.
        """
        multi_form.submit()
        return super().multi_form_valid(multi_form)

class RegistrationSuccessView(SuccessView):
    template_name = "anmeldung/danke/index.html"
    required_referer_substring = "anmeldung"
    redirect_name = "registration"

    def get_template_names(self) -> str:
        form_key = self.kwargs.get('event_key')
        return f"anmeldung/{form_key}/danke/index.html"
