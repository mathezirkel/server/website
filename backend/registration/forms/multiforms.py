
from __future__ import annotations
from abc import ABCMeta
import json
from typing import TYPE_CHECKING


from django.conf import settings
from django.core.mail import EmailMessage
from django.forms import Field
from django.template.loader import render_to_string
from django.template.exceptions import TemplateDoesNotExist
from django.utils.html import escape

from cabret.utils import data_loader
from core.forms.forms import BetterForm
from core.forms.multiforms import MultiForm
from registration.forms.conf import (
    praesenzzirkel_fields,
    korrespondenzzirkel_fields,
    themenzirkel_fields,
    astrotag_fields,
    mai_mathetag_fields,
    wintercamp_fields,
    mathecamp_fields,
    abschlussveranstaltung_fields,
    mail_fields
)
from registration.forms.forms import (
    base_data,
    event,
    emergency_contacts,
    travel,
    nutrition,
    medical,
    camp,
    certificate,
    miscellaneous,
)
from registration.forms.printers import RegistrationPDFMultiFormPrinter
from registration.forms.utils import get_instructor_string

if TYPE_CHECKING:
    from typing import Any

def dynamic_form_class(
    class_name: str,
    field_group: dict[str, Any],
    field_names: list[str]
) -> type:
    fields = {
        k:v for k,v in field_group.items()
        if not isinstance(v,Field) or k in field_names
    }
    return type(class_name, (BetterForm,), fields)

class MetaRegistrationMultiForm(MultiForm, metaclass=ABCMeta):
    event_name = "Mathezirkel"
    event_id: str | None = None
    event_type: str | None = None
    description = ""
    css_classes = "multi-form"
    is_mail_mathezirkel = True
    is_mail_participant = True

    def submit(self) -> None:
        data = self.cleaned_data

        form_pdf_data = None
        if self.is_mail_participant:
            # Generate pdf_data here to avoid calling the function twice
            form_pdf_data = self.generate_pdf(
                printer=RegistrationPDFMultiFormPrinter(),
                title=f"Anmeldeformular {self.event_name}"
            )
            self.send_mail_participant(data, form_pdf_data)

        if self.is_mail_mathezirkel:
            self.send_mail_mathezirkel(data, form_pdf_data)

    @property
    def cleaned_data(self) -> dict[str, Any]:
        cleaned_data = super().cleaned_data
        cleaned_data = {k.replace("_optional", ""): v for k, v in cleaned_data.items()}
        # Remove captcha
        if (k := "captcha") in cleaned_data:
            cleaned_data.pop(k)
        # Fit needs for import in Mathezirkel app
        if contacts := [
            data for key, data in cleaned_data.items()
            if key.startswith("emergencyContact")
        ]:
            cleaned_data["contacts"] = contacts
        if (v := cleaned_data.get("telephoneSelf")):
            cleaned_data["contacts"].append(
                {
                    "name": "Handy Schüler:in",
                    "telephone" : v
                }
            )
        # Squash courseSlot
        if (k := "courseSlot_praesenzzirkel") in cleaned_data or (l := "courseSlot_korrespondenzzirkel") in cleaned_data:
            cleaned_data["courseSlot"] = cleaned_data.get(k) or cleaned_data.get(l)
        # Wintercamp: Restricted classYear
        if (k := "classYearSenior") in cleaned_data:
            cleaned_data["classYear"] = cleaned_data[k]
        # Additional Emails
        if (k := "emailParents") in cleaned_data:
            cleaned_data["additionalEmails"] = [cleaned_data[k]]
        # HealthImpairment  and drugs need to be merged
        if medical_notes := (cleaned_data.get("healthImpairment", []) + cleaned_data.get("drugs", [])):
            cleaned_data["medicalNotes"] = medical_notes
        # Certificate for closing event
        if "participates" in cleaned_data:
            cleaned_data["certificate"] = True
        # Registration timestamp
        cleaned_data["registrationTimestamp"] = cleaned_data.get("timestampUTC")
        # Event data
        cleaned_data["eventId"] = self.event_id
        cleaned_data["eventType"] = self.event_type
        return cleaned_data

    def get_mailto_link(self, data_form: dict[str,Any], template_name: str, msg_type: str) -> str: # pylint: disable=too-many-locals
        email_self = data_form.get("emailSelf", "")
        email_parents = data_form.get("emailParents", "")

        match self.form_class_key:
            case "praesenzzirkel":
                zirkel = data_form.get("courseSlot_praesenzzirkel", "")
            case "themenzirkel":
                zirkel = data_form.get("courseSlot_themenzirkel", "")
            case "korrespondenzzirkel":
                zirkel = data_form.get("classYear", "").replace("CLASS","")
            case _:
                zirkel = ""

        data_events = data_loader.get_event_data()
        data_event = data_events.get(self.form_class_key, {})
        payment_deadline = data_event.get("paymentDeadline", "")
        participation_fee = data_event.get("participationFee", 0)
        year = data_event.get("year", -1)
        waiting_list_response_period = data_event.get("waitingListResponsePeriod", "")

        current_zirkel = (
            data_loader.get_zirkel_data()
            .get(self.form_class_key,{})
            .get(zirkel,{})
        )

        instructors = current_zirkel.get("instructors", [])
        instructors_emails = current_zirkel.get("emails",[])

        family_name = escape(data_form.get("familyName", ""))
        given_name = escape(data_form.get("givenName", ""))
        subject = f"Mathezirkel - Anmeldung {self.event_name} - {msg_type}"
        recipients = ",".join(filter(None, [email_self, email_parents]))
        cc = ",".join(instructors_emails)

        try:
            message = render_to_string(
                f"../templates/email/{self.form_class_key}/{template_name}",
                {
                    "familyName": family_name,
                    "givenName": given_name,
                    "instructors": get_instructor_string(instructors),
                    "paymentDeadline": payment_deadline,
                    "participationFee": participation_fee,
                    "waitingListResponsePeriod": waiting_list_response_period,
                    "year": str(year),
                }
            )
        except TemplateDoesNotExist:
            return ""

        mailto_link  = (
            f"mailto:{recipients}?cc={cc}&subject={subject}&body={message}"
            .replace(' ', '%20')
            .replace('\n', '%0A')
        )
        return mailto_link

    def send_mail_mathezirkel(self, data: dict[str,Any], form_pdf_data: bytes | None = None) -> None: # pylint: disable=too-many-locals
        family_name = escape(data.get("familyName", ""))
        given_name = escape(data.get("givenName", ""))

        subject = f"Anmeldung {self.event_name} - {family_name} {given_name}"
        recipients = ["anmeldung@mathezirkel-augsburg.de"]

        confirmation_link = self.get_mailto_link(data, "confirmation.txt", "Bestätigung")
        waiting_list_link = self.get_mailto_link(data, "waiting_list.txt", "Warteliste")
        rejection_link = self.get_mailto_link(data, "rejection.txt", "Absage")

        message = "Übersicht:<br><br>"
        for key in mail_fields:
            if key in data:
                value = escape(data[key]).replace('\n', '<br>')
                message += f"{key}: {value}<br>"
        message += "<br><br>"
        if confirmation_link:
            message += f"<a href=\"{confirmation_link}\">Bestätigen</a><br><br>"
        if waiting_list_link:
            message += f"<a href=\"{waiting_list_link}\">Warteliste</a><br><br>"
        if rejection_link:
            message += f"<a href=\"{rejection_link}\">Absagen</a><br><br>"

        # Json
        json_data = json.dumps(data, indent=4, ensure_ascii=False).encode('utf8')
        json_filename = f"{family_name}_{given_name}.mz.json"

        # Attachments
        attachments=[
            (json_filename, json_data, 'application/json'),
        ]
        if form_pdf_data:
            # Form summary PDF
            form_pdf_filename = f"{self.event_name}_{family_name}_{given_name}.pdf"
            attachments.append((form_pdf_filename, form_pdf_data, 'application/pdf'))

        # Send E-Mail
        email = EmailMessage(
            subject=subject,
            body=message,
            to=recipients,
            attachments=attachments,
        )
        email.content_subtype = "html"
        email.send(fail_silently=False)

    def send_mail_participant(self, data: dict[str, Any], form_pdf_data: bytes) -> None: # pylint: disable=too-many-locals
        given_name = escape(data.get("givenName", ""))
        family_name = escape(data.get("familyName", ""))

        subject = f"Mathezirkel - Anmeldung {self.event_name}"
        email_self = data.get("emailSelf", "")
        email_parents = data.get("emailParents", "")
        recipients = [email_self, email_parents]
        reply_to = [settings.EMAIL_MATHEZIRKEL]
        message = render_to_string(
            "../templates/email/acknowledgement_of_receipt.txt",
            {
                "name": given_name,
                "event": self.event_name
            }
        )

        # Form Summary PDF
        form_pdf_filename = f"{self.event_name}_{family_name}_{given_name}.pdf"

        # Terms and conditions
        tos_pdf_filename = f"AGB_{self.event_name}.pdf"
        with open(f'./templates/agb/{self.form_class_key}.pdf', 'rb') as pdf:
            tos_pdf_data = pdf.read()

        # Data protection notice

        privacy_pdf_filename = "Datenschutzhinweise.pdf"
        with open("./templates/datenschutz/veranstaltung/datenschutzhinweise.pdf", 'rb') as pdf:
            privacy_pdf_data = pdf.read()

        # Send E-Mail
        email = EmailMessage(
            subject=subject,
            body=message,
            to=recipients,
            reply_to=reply_to,
            attachments=[
                (form_pdf_filename, form_pdf_data, 'application/pdf'),
                (tos_pdf_filename, tos_pdf_data, 'application/pdf'),
                (privacy_pdf_filename, privacy_pdf_data, 'application/pdf'),
            ]
        )
        email.send(fail_silently=False)

class PraesenzzirkelRegistrationMultiForm(MetaRegistrationMultiForm):
    event_name = "Präsenzzirkel"
    event_id = "954fbed5-38ec-4389-8785-d14207c3abd1"
    event_type = "ZIRKEL"
    form_class_key = "praesenzzirkel"
    base_data_form = dynamic_form_class(
        "praesenz_base_data_form",
        base_data,
        praesenzzirkel_fields
    )
    event_form = dynamic_form_class(
        "praesenz_event_form",
        event,
        praesenzzirkel_fields
    )
    emergency_contacts_form = dynamic_form_class(
        "praesenz_emergency_contacts_form",
        emergency_contacts,
        praesenzzirkel_fields
    )
    miscellaneous_form = dynamic_form_class(
        "praesenz_miscellaneous_form",
        miscellaneous,
        praesenzzirkel_fields
    )

class ThemenzirkelRegistrationMultiForm(MetaRegistrationMultiForm):
    event_name = "Themenzirkel"
    event_id = "a8c61e77-4d72-49ed-aa34-56b46ec14cdb"
    event_type = "ZIRKEL"
    form_class_key = "themenzirkel"
    base_data_form = dynamic_form_class(
        "themen_base_data_form",
        base_data,
        praesenzzirkel_fields
    )
    event_form = dynamic_form_class(
        "themen_event_form",
        event,
        themenzirkel_fields
    )
    emergency_contacts_form = dynamic_form_class(
        "themen_emergency_contacts_form",
        emergency_contacts,
        themenzirkel_fields
    )
    miscellaneous_form = dynamic_form_class(
        "themen_miscellaneous_form",
        miscellaneous,
        themenzirkel_fields
)

class KorrespondenzzirkelRegistrationMultiForm(MetaRegistrationMultiForm):
    event_name = "Korrespondenzzirkel"
    event_id = "c217277d-94c8-49e2-bb8b-7150b8444efc"
    event_type = "ZIRKEL"
    form_class_key = "korrespondenzzirkel"
    base_data_form = dynamic_form_class(
        "korrespondenz_base_data_form",
        base_data,
        korrespondenzzirkel_fields
    )
    event_form = dynamic_form_class(
        "korrespondenz_event_form",
        event,
        korrespondenzzirkel_fields
    )
    miscellaneous_form = dynamic_form_class(
        "korrespondenz_miscellaneous_form",
        miscellaneous,
        korrespondenzzirkel_fields
    )

class AstrotagRegistrationMultiForm(MetaRegistrationMultiForm):
    event_name = "Astrotag"
    event_type = "MATH_DAY"
    form_class_key = "astrotag"
    base_data_form = dynamic_form_class(
        "astrotag_base_data_form",
        base_data,
        astrotag_fields
    )
    event_form = dynamic_form_class(
        "astrotag_event_form",
        event,
        astrotag_fields
    )
    emergency_contacts_form = dynamic_form_class(
        "astrotag_emergency_contacts_form",
        emergency_contacts,
        astrotag_fields
    )
    nutrition_form = dynamic_form_class(
        "astrotag_nutrition_form",
        nutrition,
        astrotag_fields
    )
    miscellaneous_form = dynamic_form_class(
        "astrotag_miscellaneous_form",
        miscellaneous,
        astrotag_fields
    )

class MaiMathetagRegistrationMultiForm(MetaRegistrationMultiForm):
    event_name = "Mai-Mathetag"
    event_type = "MATH_DAY"
    form_class_key = "mai-mathetag"
    base_data_form = dynamic_form_class(
        "mai_mathetag_base_data_form",
        base_data,
        mai_mathetag_fields
    )
    event_form = dynamic_form_class(
        "mai_mathetag_event_form",
        event,
        mai_mathetag_fields
    )
    emergency_contacts_form = dynamic_form_class(
        "mai_mathetag_emergency_contacts_form",
        emergency_contacts,
        mai_mathetag_fields
    )
    nutrition_form = dynamic_form_class(
        "mai_mathetag_nutrition_form",
        nutrition,
        mai_mathetag_fields
    )
    miscellaneous_form = dynamic_form_class(
        "mai_mathetag_miscellaneous_form",
        miscellaneous,
        mai_mathetag_fields
    )

class WintercampRegistrationMultiForm(MetaRegistrationMultiForm):
    event_name = "Wintercamp"
    event_type = "WINTER_CAMP"
    form_class_key = "wintercamp"
    base_data_form = dynamic_form_class(
        "wintercamp_base_data_form",
        base_data,
        wintercamp_fields
    )
    event_form = dynamic_form_class(
        "wintercamp_event_form",
        event,
        wintercamp_fields
    )
    emergency_contacts_form = dynamic_form_class(
        "wintercamp_emergency_contacts_form",
        emergency_contacts,
        wintercamp_fields
    )
    travel_form = dynamic_form_class(
        "wintercamp_travel_form",
        travel,
        wintercamp_fields
    )
    nutrition_form = dynamic_form_class(
        "wintercamp_nutrition_form",
        nutrition,
        wintercamp_fields
    )
    medical_form = dynamic_form_class(
        "wintercamp_medical_form",
        medical,
        wintercamp_fields
    )
    camp_form = dynamic_form_class(
        "wintercamp_camp_form",
        camp,
        wintercamp_fields
    )
    miscellaneous_form = dynamic_form_class(
        "wintercamp_miscellaneous_form",
        miscellaneous,
        wintercamp_fields
    )

class MathecampRegistrationMultiForm(MetaRegistrationMultiForm):
    event_name = "Mathecamp"
    event_type = "MATH_CAMP"
    form_class_key = "mathecamp"
    base_data_form = dynamic_form_class(
        "mathecamp_base_data_form",
        base_data,
        mathecamp_fields
    )
    event_form = dynamic_form_class(
        "mathecamp_event_form",
        event,
        mathecamp_fields
    )
    emergency_contacts_form = dynamic_form_class(
        "mathecamp_emergency_contacts_form",
        emergency_contacts,
        mathecamp_fields
    )
    travel_form = dynamic_form_class(
        "mathecamp_travel_form",
        travel,
        mathecamp_fields
    )
    nutrition_form = dynamic_form_class(
        "mathecamp_nutrition_form",
        nutrition,
        mathecamp_fields
    )
    medical_form = dynamic_form_class(
        "mathecamp_medical_form",
        medical,
        mathecamp_fields
    )
    camp_form = dynamic_form_class(
        "mathecamp_camp_form",
        camp,
        mathecamp_fields
    )
    miscellaneous_form = dynamic_form_class(
        "mathecamp_miscellaneous_form",
        miscellaneous,
        mathecamp_fields
    )

class AbschlussveranstaltungRegistrationMultiForm(MetaRegistrationMultiForm):
    event_name = "Abschlussveranstaltung"
    event_type = "ZIRKEL"
    form_class_key = "abschlussveranstaltung"
    is_mail_participant = False
    base_data_form = dynamic_form_class(
        "abschlussveranstaltung_base_data_form",
        base_data,
        abschlussveranstaltung_fields
    )
    event_form = dynamic_form_class(
        "abschlussveranstaltung_event_form",
        event,
        abschlussveranstaltung_fields
    )
    certificate_form = dynamic_form_class(
        "abschlussveranstaltung_event_form",
        certificate,
        abschlussveranstaltung_fields
    )
    miscellaneous_form = dynamic_form_class(
        "abschlussveranstaltung_miscellaneous_form",
        miscellaneous,
        abschlussveranstaltung_fields
    )
