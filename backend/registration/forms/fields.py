from __future__ import annotations
from typing import TYPE_CHECKING

from django.utils.html import escape
from reportlab.platypus import Paragraph

from core.forms.fields import (
    BetterCharField,
    BetterChoiceField,
    BetterMultiValueField,
    BetterPhoneNumberField,
)

from registration.forms.enums import DEPARTURE_ARRANGEMENT_ENUM
from registration.forms.widgets import (
    DepartureArrangementWidget,
    EmergencyContactWidget
)

if TYPE_CHECKING:
    from reportlab.lib.styles import StyleSheet1
    from reportlab.platypus import Flowable

class DepartureArrangementField(BetterMultiValueField):
    def __init__(self, **kwargs):
        fields = (
            BetterChoiceField(
                required=True,
                choices=DEPARTURE_ARRANGEMENT_ENUM,
            ),
            BetterCharField(
                min_length=2,
                max_length=50,
                required=False
            )
        )
        super().__init__(
            fields=fields,
            required=True,
            require_all_fields=False,
            widget=DepartureArrangementWidget,
            **kwargs
        )
    def compress(self, data_list: list[str|None]) -> str | None:
        if data_list[0] == "SELF":
            return ""
        return data_list[1] or "Eltern"

    def get_flowable(self, data:str, styles: StyleSheet1) -> list[Flowable]:
        value = DEPARTURE_ARRANGEMENT_ENUM["SELF"]
        if data:
            value = data
        return [Paragraph(f"<b>{self.get_pdf_label()}</b>: {escape(value)}", styles['Normal'])]

class EmergencyContactField(BetterMultiValueField):
    def __init__(self, required : bool = True, **kwargs):
        fields = (
            BetterCharField(
                label="Name",
                min_length=2,
                max_length=50,
                required=True,
            ),
            BetterPhoneNumberField(
                label="Telefon",
                required=True,
            )
        )
        super().__init__(
            fields=fields,
            required=required,
            require_all_fields=required,
            widget=EmergencyContactWidget,
            **kwargs
        )
    def compress(self, data_list: list[str|None]) -> dict[str,str]:
        name = ""
        telephone = ""
        if data_list:
            name = data_list[0]
            telephone = data_list[1]
        return {
            "name" : name,
            "telephone": telephone
        }

    def get_flowable(self, data:dict[str,str], styles: StyleSheet1) -> list[Flowable]:
        value = "\n"
        value += f"{self.fields[0].label}: {data['name']}\n"
        value += f"{self.fields[1].label}: {data['telephone']}\n"
        value = escape(value).replace('\n','<br />\n')
        return [Paragraph(f"<b>{self.get_pdf_label()}</b>: {value}", styles['Normal'])]
