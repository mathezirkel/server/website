from datetime import date

def get_current_school_year() -> str:
    today = date.today()
    school_year_start = today.year - 1 if today.month <= 9 else today.year
    return f"{school_year_start}/{(school_year_start + 1) % 100}"

def get_instructor_string(instructors: list[str]) -> str:
    if not instructors:
        return "deinen Zirkelleiter:innen"
    if len(instructors) == 1:
        return f"deine:r Zirkelleiter:in {instructors[0]}"
    instructor_string = " und ".join([", ".join(instructors[:-1]), instructors[-1]])
    return f"deinen Zirkelleiter:innen {instructor_string}"

def get_tos_agreement_string(form_class_key: str) -> str:
    return f'Ich stimme den \
    <a href="https://www.mathezirkel-augsburg.de/agb/{form_class_key}.pdf">Allgemeinen Geschäftsbedingungen</a> zu.'
