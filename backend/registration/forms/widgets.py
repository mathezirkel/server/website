from django.forms import (
    MultiWidget,
    TextInput,
    RadioSelect
)
from phonenumber_field.widgets import RegionalPhoneNumberWidget

from registration.forms.enums import DEPARTURE_ARRANGEMENT_ENUM

class DepartureArrangementWidget(MultiWidget):
    def __init__(self, attrs=None):
        widgets = [
            RadioSelect(
                attrs=attrs,
                choices=DEPARTURE_ARRANGEMENT_ENUM
            ),
            TextInput(
                attrs=attrs
            )
        ]
        super().__init__(widgets, attrs)

    def decompress(self, value: str|None) -> list[str | None]:
        match value:
            case None:
                return [None, None]
            case "":
                return ["SELF", None]
            case "Eltern":
                return ["PICKUP", None]
            case _:
                return ["PICKUP", value]

class EmergencyContactWidget(MultiWidget):
    def __init__(self):
        widgets = [
            TextInput(
                attrs={'placeholder': 'Name'}
            ),
            RegionalPhoneNumberWidget(
                attrs={'placeholder': 'Telefon'}
            )
        ]
        super().__init__(widgets)

    def decompress(self, value: dict[str,str]) -> list[str | None]:
        if value:
            return value.values()
        return [None, None]
