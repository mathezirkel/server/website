from __future__ import annotations
from typing import TYPE_CHECKING

from cabret.utils import data_loader
from registration.forms.multiforms import (
    PraesenzzirkelRegistrationMultiForm,
    ThemenzirkelRegistrationMultiForm,
    KorrespondenzzirkelRegistrationMultiForm,
    AstrotagRegistrationMultiForm,
    MaiMathetagRegistrationMultiForm,
    WintercampRegistrationMultiForm,
    MathecampRegistrationMultiForm,
    AbschlussveranstaltungRegistrationMultiForm,
)

if TYPE_CHECKING:
    from registration.forms.multiforms import MetaRegistrationMultiForm

_MULTIFORMS : list[MetaRegistrationMultiForm] = [
    PraesenzzirkelRegistrationMultiForm,
    ThemenzirkelRegistrationMultiForm,
    KorrespondenzzirkelRegistrationMultiForm,
    AstrotagRegistrationMultiForm,
    MaiMathetagRegistrationMultiForm,
    WintercampRegistrationMultiForm,
    MathecampRegistrationMultiForm,
    AbschlussveranstaltungRegistrationMultiForm
]

MULTIFORM_MAPPING : dict[str,MetaRegistrationMultiForm] = {
    multiform.form_class_key : multiform for multiform in _MULTIFORMS
}

# Avoid to import forms into urls.py
def get_enabled_multiform_class_keys() -> list[MetaRegistrationMultiForm]:
    events= data_loader.get_event_data()
    form_class_keys = [multiform.form_class_key for multiform in _MULTIFORMS]
    enabled_multiform_keys = [
        key for key in form_class_keys
        if events.get(key,{}).get("enabled", False)
    ]
    return enabled_multiform_keys
