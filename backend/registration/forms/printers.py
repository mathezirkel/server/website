from __future__ import annotations
from typing import TYPE_CHECKING

from reportlab.platypus import (
    Image,
    Paragraph,
    Spacer,
)
from reportlab.lib.units import cm

from cabret.utils import data_loader
from core.forms.printers import PDFMultiFormPrinter

if TYPE_CHECKING:
    from reportlab.lib.styles import StyleSheet1
    from reportlab.pdfgen import Canvas
    from reportlab.platypus import Flowable, SimpleDocTemplate
    from registration.forms.multiforms import MetaRegistrationMultiForm

class RegistrationPDFMultiFormPrinter(PDFMultiFormPrinter):

    def get_summary(self, multiform: MetaRegistrationMultiForm, styles: StyleSheet1) -> list[Flowable]:
        event_key = multiform.form_class_key
        content = []
        events = data_loader.get_event_data()
        event = events.get(event_key, {})
        if period := event.get("period", ""):
            content.append(Paragraph(
                f"<strong> Wann?</strong>: {period}",
                styles['Normal']
            ))
            content.append(Spacer(1, 4))
        if place := event.get("place", ""):
            content.append(Paragraph(
                f"<strong> Wo?</strong>: {place}",
                styles['Normal']
            ))
            content.append(Spacer(1, 4))
        if participation_fee := event.get("participationFee", ""):
            content.append(Paragraph(
                f"<strong> Teilnahmebeitrag</strong>: {participation_fee} €",
                styles['Normal']
            ))
            content.append(Spacer(1, 4))
        return content


    def header(self, canvas: Canvas, _doc: SimpleDocTemplate) -> None:
        canvas.saveState()
        logo_width = 3 * cm
        logo_height = 3 * cm
        logo = Image(
            "registration/templates/pdf/zirkelgregor-quadratisch.png",
            logo_width,
            logo_height
        )
        logo.drawOn(
            canvas,
            self.width - self.right_margin - logo_width,
            self.height - self.top_margin - logo_height,
        )
        canvas.restoreState()
