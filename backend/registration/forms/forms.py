from __future__ import annotations
from typing import TYPE_CHECKING

from django.core.validators import RegexValidator
from django.forms import (
    DateInput,
    RadioSelect,
    Textarea
)

from core.forms.fields import (
    BetterBooleanField,
    BetterCaptchaField,
    BetterCharField,
    BetterChoiceField,
    BetterDateField,
    BetterEmailField,
    BetterIntegerField,
    BetterListField,
    BetterPhoneNumberField,
)
from registration.forms.conf import AMOUNT_OF_EMERGENCY_CONTACTS
from registration.forms.enums import (
    BOOLEAN_ENUM,
    CLASS_YEAR_ENUM,
    CLASS_YEAR_SENIOR_ENUM,
    COUNTRY_ENUM,
    NUTRITION_ENUM,
    SEX_ENUM,
    STATISTICS_ENUM,
    TRAVELTYPE_ENUM,
    get_course_choices
)
from registration.forms.fields import (
    DepartureArrangementField,
    EmergencyContactField
)
from registration.forms.utils import get_current_school_year, get_tos_agreement_string

if TYPE_CHECKING:
    from django.forms import Field

base_data : dict[str, str | Field] = {
    "heading": "Stammdaten",
    "givenName" : BetterCharField(
        label="Vorname",
        min_length=2,
        max_length=50,
        required=True,
    ),
    "familyName" : BetterCharField(
        label="Nachname",
        min_length=2,
        max_length=50,
        required=True
    ),
    "gender" : BetterCharField(
        label="Geschlecht",
        min_length=1,
        max_length=30,
        required=False
    ),
    "sex" : BetterChoiceField(
        widget=RadioSelect,
        label="Rechtliches Geschlecht",
        help_text="Für die Zimmereinteilung müssen wir das rechtliche Geschlecht abfragen. \
        Bei Fragen stehen wir gerne zur Verfügung. \
        Sollte die Geschlechtsidentität nicht mit dem rechtlichen Geschlecht übereinstimmen, \
        werden wir eine individuelle Lösung finden.",
        choices=SEX_ENUM,
        required=True
    ),
    "birthDate" : BetterDateField(
        label="Geburtsdatum",
        required=True,
        widget=DateInput(attrs={"type" : "date"}) #YYYY-MM-DD
    ),
    "street" : BetterCharField(
        label="Straße",
        min_length=2,
        max_length=50,
        required=True
    ),
    "streetNumber" : BetterCharField(
        label="Hausnummer",
        min_length=1,
        max_length=10,
        required=True
    ),
    "postalCode" : BetterCharField(
        label="PLZ",
        min_length=4,
        max_length=5,
        required=True,
        validators= [
            RegexValidator(regex=r'^\d{4,5}$')
        ]
    ),
    "city" : BetterCharField(
        label="Ort",
        min_length=2,
        max_length=30,
        required=True
    ),
    "country" : BetterChoiceField(
        label="Land",
        choices=COUNTRY_ENUM,
        required=True,
        initial="Deutschland"
    ),
}

event : dict[str, str | Field] = {
    "heading" : "Veranstaltungsbezogene Daten",
    "classYear" : BetterChoiceField(
        label=f"Klassenstufe (im Schuljahr {get_current_school_year()})",
        choices=CLASS_YEAR_ENUM,
        required=True
    ),
    "classYearSenior" : BetterChoiceField(
        label=f"Klassenstufe (im Schuljahr {get_current_school_year()})",
        choices=CLASS_YEAR_SENIOR_ENUM,
        required=True
    ),
    "courseSlot_praesenzzirkel": BetterChoiceField(
        label="Termin",
        help_text="Bitte wähle aus, an welchem Termin du teilnehmen möchtest.",
        choices=get_course_choices(event_key="praesenzzirkel"),
        required=True,
    ),
    "courseSlot_themenzirkel": BetterChoiceField(
        label="Thema",
        help_text="Bitte wähle aus, zu welchem Thema du dich anmelden möchtest.",
        choices=get_course_choices(event_key="themenzirkel"),
        required=True,
    ),
    "telephone" : BetterPhoneNumberField(
        label="Telefon (für Rückfragen)",
        required=True,
    ),
    "emailParents" : BetterEmailField(
        label="E-Mail Eltern",
        required=True
    ),
    "emailSelf" : BetterEmailField(
        label="E-Mail Schüler:in",
        required=False
    ),
    "telephoneSelf" : BetterPhoneNumberField(
        label="Handynummer Schüler:in",
        required=False,
    ),
    "topicWishes" : BetterListField(
        label="Themenwünsche",
        min_length=2,
        max_length=200,
        required=False,
        help_text="(mehrere Wünsche durch Komma abtrennen)"
    ),
    "participates" : BetterBooleanField(
        widget=RadioSelect(choices=BOOLEAN_ENUM),
        label="Nimmst du an der Abschlussveranstaltung teil?",
        required=False
    ),
    "amountOfGuests": BetterIntegerField(
        label="Wie viele Personen bringst du ca. mit?",
        min_value=0,
        required=False,
    ),
}

emergency_contacts : dict[str, str | Field] = {
    "heading": "Notfallkontakte",
    "pre_note": "Die Notfallkontakte müssen während der Veranstaltung erreichbar sein!",
    **{
        f"emergencyContact_{i}" : EmergencyContactField(
            label="Notfallkontakt",
            required=not i # Make only the first emergency contact required
        )
        for i in range(AMOUNT_OF_EMERGENCY_CONTACTS)
    }
}

travel : dict[str, str | Field] = {
    "heading": "An- und Abreise",
    "arrival" : BetterChoiceField(
        widget=RadioSelect,
        label="Anreise",
        choices=TRAVELTYPE_ENUM,
        required=True
    ),
    "departure" : BetterChoiceField(
        widget=RadioSelect,
        label="Abreise",
        choices=TRAVELTYPE_ENUM,
        required=True
    ),
    "departureNotes" : DepartureArrangementField(
        label="Wer darf Ihr Kind abholen?",
    )
}

nutrition : dict[str, str | Field] = {
    "heading" : "Ernährung",
    "nutrition" : BetterChoiceField(
        widget=RadioSelect,
        label="Ernährung",
        choices=NUTRITION_ENUM,
        required=True
    ),
    "foodRestriction" : BetterCharField(
        label="Unverträglichkeiten",
        min_length=2,
        max_length=200,
        required=False,
        help_text="(mehrere Unverträglichkeiten durch Komma abtrennen)"
    )
}

medical : dict[str, str | Field] = {
    "heading" : "Medizinische Daten",
    "healthImpairment" : BetterListField(
        label="Mein Kind hat folgende gesundheitliche Beeinträchtigungen, z.B. Allergien",
        min_length=2,
        max_length=200,
        required=False,
        help_text="(mehrere Beeinträchtigungen durch Komma abtrennen)"
    ),
    "drugs" : BetterListField(
        label="Mein Kind nimmt folgende Medikamente",
        min_length=2,
        max_length=200,
        required=False,
        help_text="(mehrere Medikamente durch Komma abtrennen)"
    ),
    "removeTicks" : BetterBooleanField(
        label="Zecken",
        description="Betreuer:innen dürfen bei meinem Kind kleinere Fremdkörper, \
        z.&nbsp;B. Zecken oder Splitter, entfernen.",
        required=False
    ),
}

camp : dict[str, str | Field] = {
    "heading" : "Daten für das Camp",
    "roomPartnerWishes" : BetterListField(
        label="Mein Kind möchte gerne mit folgenden Freund:innen auf ein Zimmer",
        min_length=2,
        max_length=200,
        required=False,
        help_text="(mehrere Personen durch Komma abtrennen)"
    ),
    "zirkelPartnerWishes" : BetterListField(
        label="Mein Kind möchte gerne mit folgenden Freund:innen in einen Zirkel",
        min_length=2,
        max_length=200,
        required=False,
        help_text="(mehrere Personen durch Komma abtrennen)"
    ),
    "fractivityWishes" : BetterListField(
        label="Wünsche für Freizeitaktivitäten",
        min_length=2,
        max_length=200,
        required=False,
        help_text="(mehrere Wünsche durch Komma abtrennen)"
    ),
    "instruments" : BetterListField(
        label="Mein Kind bringt folgendes Instrument für musikalische Freizeitaktivitäten mit",
        min_length=2,
        max_length=30,
        required=False,
        help_text="(mehrere Instrumente durch Komma abtrennen)"
    ),
    "pool" : BetterBooleanField(
        label="Pool",
        description="Mein Kind kann schwimmen und darf unter Aufsicht in den Pool.",
        required=False
    ),
    "leavingPremise" : BetterBooleanField(
        label="Verlassen des Geländes",
        description="Mein Kind darf (ab der achten Klasse) in Gruppen \
        von mindestens drei Schüler:innen \
        auch ohne unmittelbare Aufsicht für kurze Zeit das Gelände verlassen.",
        required=False
    ),
    "carpoolDataSharing" : BetterBooleanField(
        label="Fahrgemeinschaft",
        description="Zur Bildung von Fahrgemeinschaften dürfen meine Daten \
        (Name, Ort, E-Mail, Telefonnummer) anderen Teilnehmer:innen mitgeteilt werden.",
        required=False
    ),
}

certificate: dict[str, str, Field] = {
    "heading": "Urkunde per Post",
    "pre_note": "Wenn du nicht an der Abschlussveranstaltung teilnehmen kannst, senden wir dir deine Urkunde gerne per Post zu. Dazu benötigen wir deine Adresse.",
    "street_optional" : BetterCharField(
        label="Straße",
        min_length=2,
        max_length=50,
        required=False,
    ),
    "streetNumber_optional" : BetterCharField(
        label="Hausnummer",
        min_length=1,
        max_length=10,
        required=False,
    ),
    "postalCode_optional" : BetterCharField(
        label="PLZ",
        min_length=4,
        max_length=5,
        required=False,
        validators= [
            RegexValidator(regex=r'^\d{4,5}$')
        ],
    ),
    "city_optional" : BetterCharField(
        label="Ort",
        min_length=2,
        max_length=30,
        required=False,
    ),
}

miscellaneous : dict[str, str | Field] = {
    "heading" : "Sonstiges",
    "statistics" : BetterChoiceField(
        label="Wie hast du von uns erfahren?",
        choices=STATISTICS_ENUM,
        required=False
    ),
    "additionalNotes" : BetterCharField(
        widget=Textarea,
        label="Deine Nachricht",
        min_length=2,
        max_length=1000,
        required=False
    ),
    "captcha" : BetterCaptchaField(),
    "participationPermission" : BetterBooleanField(
        label="Zustimmung zur Anmeldung",
        description='Ich stimme zu, dass mein Kind an der Veranstaltung teilnehmen darf.',
        required=True,
    ),
    **{
        f"termsAndConditionsAgreement_{form_class_key}" : BetterBooleanField(
            label="AGB",
            description=get_tos_agreement_string(form_class_key),
            required=True,
        )
        for form_class_key in [
            "praesenzzirkel",
            "themenzirkel",
            "korrespondenzzirkel",
            "astrotag",
            "mai-mathetag",
            "wintercamp",
            "mathecamp",
        ]
    },
    "privacyAgreement" : BetterBooleanField(
        label="Datenschutz",
        description='Ich stimme der Datenverarbeitung gemäß der \
        <a href="https://www.mathezirkel-augsburg.de/datenschutz">Datenschutzerklärung</a> \
        und den \
        <a href="https://www.mathezirkel-augsburg.de/datenschutz/veranstaltung/datenschutzhinweise.pdf"> \
        Datenschutzhinweisen für Veranstaltungen</a> zu.',
        required=True,
    ),
}
