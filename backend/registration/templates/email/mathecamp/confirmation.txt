Hi {{ givenName }},

wir freuen uns, dass du mit aufs Mathecamp fährst! Hiermit bestätigen wir deine Anmeldung.

Wir bitten dich bzw. deine Eltern, den Teilnahmebeitrag bis zum {{ paymentDeadline }} an unten stehende Kontoverbindung zu überweisen.

Kontoinhaber: Mathematisch-Physikalischer Verein e.V.
IBAN: DE97 7205 0000 0251 0746 96
BIC: AUGSDE77XXX
Betrag: {{ participationFee }} €
Verwendungszweck: Mathecamp {{ year }} {{ familyName }} {{ givenName }}

Ende Juli schicken wir dann noch einmal alle wichtigen Informationen zum Mathecamp per E-Mail, wie zum Beispiel unsere Kontaktdaten vor Ort und eine Packliste.

Falls du noch Fragen oder Wünsche bzw. Anregungen für das Camp hast, kannst du uns jederzeit gerne schreiben.

Viele Grüße
Dein Team vom Mathezirkel
