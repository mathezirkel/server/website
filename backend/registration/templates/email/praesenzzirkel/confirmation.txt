Hi {{ givenName }},

wir freuen uns, dass du beim Präsenzzirkel dabei bist.

In den nächsten Tagen erhältst du alle Infos sowie die Termine direkt von {{ instructors }} (in Cc).

Wenn du noch Fragen hast, kannst du uns jederzeit gerne schreiben.

Viele Grüße
Dein Team vom Mathezirkel
