from registration.tests.data import (
    PraesenzzirkelRegistrationMultiFormDataManager,
    KorrespondenzzirkelRegistrationMultiFormDataManager,
    AstrotagRegistrationMultiFormDataManager,
    MaiMathetagRegistrationMultiFormDataManager,
    WintercampRegistrationMultiFormDataManager,
    MathecampRegistrationMultiFormDataManager,
)
# If you import classes inheriting from TestCase directly, the tests are executed.
from registration.tests import forms

class PraesenzzirkelRegistrationMultiFormTest(forms.MetaRegistrationMultiFormTest):
    @staticmethod
    def data_manager() -> type[PraesenzzirkelRegistrationMultiFormDataManager]:
        return PraesenzzirkelRegistrationMultiFormDataManager

class KorrespondenzzirkelRegistrationMultiFormTest(forms.MetaRegistrationMultiFormTest):
    @staticmethod
    def data_manager() -> type[KorrespondenzzirkelRegistrationMultiFormDataManager]:
        return KorrespondenzzirkelRegistrationMultiFormDataManager

class AstrotagRegistrationMultiFormTest(forms.MetaRegistrationMultiFormTest):
    @staticmethod
    def data_manager() -> type[AstrotagRegistrationMultiFormDataManager]:
        return AstrotagRegistrationMultiFormDataManager

class MaiMathetagRegistrationMultiFormTest(forms.MetaRegistrationMultiFormTest):
    @staticmethod
    def data_manager() -> type[MaiMathetagRegistrationMultiFormDataManager]:
        return MaiMathetagRegistrationMultiFormDataManager

class WintercampRegistrationMultiFormTest(forms.MetaRegistrationMultiFormTest):
    @staticmethod
    def data_manager() -> type[WintercampRegistrationMultiFormDataManager]:
        return WintercampRegistrationMultiFormDataManager

class MathecampRegistrationMultiFormTest(forms.MetaRegistrationMultiFormTest):
    @staticmethod
    def data_manager() -> type[MathecampRegistrationMultiFormDataManager]:
        return MathecampRegistrationMultiFormDataManager
