from registration.tests.data import (
    PraesenzzirkelRegistrationMultiFormDataManager,
    KorrespondenzzirkelRegistrationMultiFormDataManager,
    AstrotagRegistrationMultiFormDataManager,
    MaiMathetagRegistrationMultiFormDataManager,
    WintercampRegistrationMultiFormDataManager,
    MathecampRegistrationMultiFormDataManager,
)
from registration.tests import views

class PraesenzzirkelRegistrationViewTest(views.MetaRegistrationViewTest):
    @staticmethod
    def data_manager() -> type[PraesenzzirkelRegistrationMultiFormDataManager]:
        return PraesenzzirkelRegistrationMultiFormDataManager

class KorrespondenzzirkelRegistrationViewTest(views.MetaRegistrationViewTest):
    @staticmethod
    def data_manager() -> type[KorrespondenzzirkelRegistrationMultiFormDataManager]:
        return KorrespondenzzirkelRegistrationMultiFormDataManager

class AstrotagRegistrationViewTest(views.MetaRegistrationViewTest):
    @staticmethod
    def data_manager() -> type[AstrotagRegistrationMultiFormDataManager]:
        return AstrotagRegistrationMultiFormDataManager

class MaiMathetagRegistrationViewTest(views.MetaRegistrationViewTest):
    @staticmethod
    def data_manager() -> type[MaiMathetagRegistrationMultiFormDataManager]:
        return MaiMathetagRegistrationMultiFormDataManager

class WintercampRegistrationViewTest(views.MetaRegistrationViewTest):
    @staticmethod
    def data_manager() -> type[WintercampRegistrationMultiFormDataManager]:
        return WintercampRegistrationMultiFormDataManager

class MathecampRegistrationViewTest(views.MetaRegistrationViewTest):
    @staticmethod
    def data_manager() -> type[MathecampRegistrationMultiFormDataManager]:
        return MathecampRegistrationMultiFormDataManager
