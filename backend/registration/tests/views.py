from __future__ import annotations
from functools import wraps
from typing import TYPE_CHECKING
from unittest import mock

from django.http import HttpResponse
from django.test import override_settings
from django.urls import include, path, re_path

from core.tests.views import ViewTest
from registration.forms import form_mappings
from registration.views import RegistrationMultiFormView, RegistrationSuccessView

if TYPE_CHECKING:
    from typing import Callable

def patch_url_conf(test_func: Callable) -> Callable:
    @wraps(test_func)
    def wrapper(*args, **kwargs):
        class PatchedURLConf: # pylint: disable=too-few-public-methods
            # Registration urls
            registration_urls = [
                re_path(
                    f"(?P<event_key>({'|'.join(form_mappings.get_enabled_multiform_class_keys())}))/$",
                    RegistrationMultiFormView.as_view(),
                    name='registration'
                ),
                re_path(
                    f"(?P<event_key>({'|'.join(form_mappings.get_enabled_multiform_class_keys())}))/danke/$",
                    RegistrationSuccessView.as_view(),
                    name='registration_success'
                )
            ]
            # Root urls
            urlpatterns = [
                path('feedback/', include('feedback.urls'), name="feedback_app"),
                path('kontakt/', include('contact.urls'), name="contact_app"),
                path("anmeldung/", include((registration_urls, "registration")), name="registration_app"),
                path('captcha/', include('captcha.urls'), name="captcha_app"),
                path(
                    'healthy/',
                    lambda request: HttpResponse('healthy', content_type='text/plain', status=200),
                    name="health_endpoint"
                ),
            ]

        with override_settings(ROOT_URLCONF=PatchedURLConf):
            return test_func(*args, **kwargs)

    return wrapper

@override_settings(
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
            'LOCATION': 'unique-snowflake',
        }
    }
)
class MetaRegistrationViewTest(ViewTest):

    @classmethod
    def path(cls) -> str:
        return f"/anmeldung/{cls.data_manager().form_class().form_class_key}/"

    @classmethod
    def template(cls) -> str:
        return f"anmeldung/{cls.data_manager().form_class().form_class_key}/index.html"

    @staticmethod
    def referer() -> str:
        return "anmeldung"

    @classmethod
    def success_url(cls) -> str:
        return f"/anmeldung/{cls.data_manager().form_class().form_class_key}/danke/"

    @classmethod
    def success_template(cls) -> str:
        return f"anmeldung/{cls.data_manager().form_class().form_class_key}/danke/index.html"

    @mock.patch(
        'registration.forms.form_mappings.get_enabled_multiform_class_keys',
        return_value = []
    )
    @patch_url_conf
    def test_view_does_not_exist_if_disabled(self, _mocked_get_enabled_multiform_class_keys : mock.MagicMock) -> None:
        response = self.client.get(self.path())
        self.assertEqual(response.status_code, 404)
