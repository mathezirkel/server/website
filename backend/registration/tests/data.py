from abc import ABCMeta

from core.tests.data import FormDataManager
from core.tests.data_generator.forms import FormDataGenerator
from registration.forms.conf import (
    praesenzzirkel_fields,
    korrespondenzzirkel_fields,
    astrotag_fields,
    mai_mathetag_fields,
    wintercamp_fields,
    mathecamp_fields
)
from registration.forms.multiforms import (
    PraesenzzirkelRegistrationMultiForm,
    KorrespondenzzirkelRegistrationMultiForm,
    AstrotagRegistrationMultiForm,
    MaiMathetagRegistrationMultiForm,
    WintercampRegistrationMultiForm,
    MathecampRegistrationMultiForm,
)
from registration.tests.data_generator.forms import field_data_generators

class MetaRegistrationMultiFormDataManager(FormDataManager, metaclass=ABCMeta):

    @classmethod
    def valid_data_set(cls) -> dict[str, int | str]:
        valid_data = {
            "givenName" : "Max",
            "familyName" : "Mustermann",
            "gender" : "genderfluid",
            "sex" : "MALE",
            "birthDate" : "2000-01-01",
            "street" : "St. Michael Straße",
            "streetNumber" : 15,
            "postalCode" : "86450",
            "city" : "Altenmünster",
            "country" : "Deutschland",
            "classYear" : "CLASS5",
            "classYearSenior" : "CLASS9",
            "courseSlot_praesenzzirkel" : "5",
            "courseSlot_themenzirkel" : "Graphentheorie",
            "participationType" : "MAIL",
            "telephone" : "08999998000",
            "emailParents" : "erika.mustermann@example.com",
            "emailSelf" : "max.mustermann@example.com",
            "telephoneSelf" : "08999998001",
            "topicWishes" : "Komplexe Zahlen, Zauberwürfel",
            "emergencyContact_0": ("Erika Mustermann", "08999998002"),
            "emergencyContact_1": ("Tobi Mustermann", "08999998003"),
            "emergencyContact_2": ("Oma", "08999998004"),
            "arrival" : "BUS",
            "departure" : "BUS",
            "departureNotes" : ("SELF", ""),
            "nutrition" : "VEGAN",
            "foodRestriction" : "Nüsse, Kaffee",
            "healthImpairment" : "Kaffeeallergie",
            "drugs" : "",
            "removeTicks" : "on",
            "roomPartnerWishes" : "A, B",
            "zirkelPartnerWishes" : "C, D",
            "fractivityWishes" : "Bogenschießen, TheCrew",
            "instruments" : "Tuba",
            "pool" : "on",
            "leavingPremise" : "on",
            # Removed since HTML does not POST unchecked checkboxes
            # "carpoolDataSharing" : "off"
            "statistics" : "NOT_SPECIFIED",
            "additionalNotes" : "Das ist ein Test!\nDanke!",
            "participationPermission" : "on",
            "termsAndConditionsAgreement_praesenzzirkel": "on",
            "termsAndConditionsAgreement_korrespondenzzirkel": "on",
            "termsAndConditionsAgreement_astrotag": "on",
            "termsAndConditionsAgreement_mai-mathetag": "on",
            "termsAndConditionsAgreement_wintercamp": "on",
            "termsAndConditionsAgreement_mathecamp": "on",
            "privacyAgreement" : "on",
        }
        return {
            k:v for k, v in valid_data.items() if k in cls.form_fields()
        }

    @classmethod
    def invalid_data_set(cls) -> dict[str, int | str]:
        valid_data = {
            "givenName" : "Max",
            "familyName" : "Mustermann",
            "gender" : "genderfluid",
            "sex" : "MALE",
            "birthDate" : "2000-01-01",
            "street" : "St. Michael Straße",
            "streetNumber" : 15,
            "postalCode" : "86450",
            "city" : "Altenmünster",
            "country" : "Deutschland",
            "classYear" : "CLASS5",
            "classYearSenior" : "CLASS9",
            "courseSlot_praesenzzirkel" : "5",
            "courseSlot_themenzirkel" : "Graphentheorie",
            "participationType" : "MAIL",
            "telephone" : "08999998000",
            "emailParents" : "erika.mustermann@example.com",
            "emailSelf" : "max.mustermann@example.com",
            "telephoneSelf" : "08999998001",
            "topicWishes" : "Komplexe Zahlen, Zauberwürfel",
            "emergencyContact_0": ("Erika Mustermann", "08999998002"),
            "emergencyContact_1": ("Tobi Mustermann", "08999998003"),
            "emergencyContact_2": ("Oma", "08999998004"),
            "arrival" : "BUS",
            "departure" : "BUS",
            "departureNotes" : ("SELF", ""),
            "nutrition" : "VEGAN",
            "foodRestriction" : "Nüsse, Kaffee",
            "healthImpairment" : "Kaffeeallergie",
            "drugs" : "",
            "removeTicks" : "on",
            "roomPartnerWishes" : "A, B",
            "zirkelPartnerWishes" : "C, D",
            "fractivityWishes" : "Bogenschießen, TheCrew",
            "instruments" : "Tuba",
            "pool" : "on",
            "leavingPremise" : "on",
            # HMTL does not POST unchecked checkboxes
            # Django evaluates missing checkbox fields as False
            # "carpoolDataSharing" : "off"
            "statistics" : "NOT_SPECIFIED",
            "additionalNotes" : "Das ist ein Test!\nDanke!",
            "participationPermission" : "on",
            "termsAndConditionsAgreement_praesenzzirkel": "on",
            "termsAndConditionsAgreement_korrespondenzzirkel": "on",
            "termsAndConditionsAgreement_astrotag": "on",
            "termsAndConditionsAgreement_mai-mathetag": "on",
            "termsAndConditionsAgreement_wintercamp": "on",
            "termsAndConditionsAgreement_mathecamp": "on",
            # HMTL does not POST unchecked checkboxes
            # Django evaluates missing checkbox fields as False
            # PricacyAgreement is required for all forms
            #"privacyAgreement" : "on",
        }
        return {
            k:v for k, v in valid_data.items() if k in cls.form_fields()
        }

    @classmethod
    def form_data_generator(cls) -> FormDataGenerator:
        return FormDataGenerator(
            {
                field_name : generator
                for field_name, generator in field_data_generators.items()
                if field_name in cls.form_fields()
            }
        )

class PraesenzzirkelRegistrationMultiFormDataManager(MetaRegistrationMultiFormDataManager):
    @staticmethod
    def form_class() -> type[PraesenzzirkelRegistrationMultiForm]:
        return PraesenzzirkelRegistrationMultiForm

    @staticmethod
    def form_fields() -> list[str]:
        return praesenzzirkel_fields

class KorrespondenzzirkelRegistrationMultiFormDataManager(MetaRegistrationMultiFormDataManager):
    @staticmethod
    def form_class() -> type[KorrespondenzzirkelRegistrationMultiForm]:
        return KorrespondenzzirkelRegistrationMultiForm

    @staticmethod
    def form_fields() -> list[str]:
        return korrespondenzzirkel_fields

class AstrotagRegistrationMultiFormDataManager(MetaRegistrationMultiFormDataManager):
    @staticmethod
    def form_class() -> type[AstrotagRegistrationMultiForm]:
        return AstrotagRegistrationMultiForm

    @staticmethod
    def form_fields() -> list[str]:
        return astrotag_fields

class MaiMathetagRegistrationMultiFormDataManager(MetaRegistrationMultiFormDataManager):
    @staticmethod
    def form_class() -> type[MaiMathetagRegistrationMultiForm]:
        return MaiMathetagRegistrationMultiForm

    @staticmethod
    def form_fields() -> list[str]:
        return mai_mathetag_fields

class WintercampRegistrationMultiFormDataManager(MetaRegistrationMultiFormDataManager):
    @staticmethod
    def form_class() -> type[WintercampRegistrationMultiForm]:
        return WintercampRegistrationMultiForm

    @staticmethod
    def form_fields() -> list[str]:
        return wintercamp_fields

class MathecampRegistrationMultiFormDataManager(MetaRegistrationMultiFormDataManager):
    @staticmethod
    def form_class() -> type[MathecampRegistrationMultiForm]:
        return MathecampRegistrationMultiForm

    @staticmethod
    def form_fields() -> list[str]:
        return mathecamp_fields
