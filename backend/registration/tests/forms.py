from abc import ABCMeta
import json
import logging
from unittest import mock

from django.conf import settings
from django.template.loader import render_to_string
from freezegun import freeze_time

from core.tests.forms import FormTestCase

class MetaRegistrationMultiFormTest(FormTestCase, metaclass=ABCMeta):
    @classmethod
    def email_assertions(cls) -> list[dict[str, str | list[str]]]:
        event_name = cls.data_manager().form_class().event_name
        return [
            {
                "to": [
                    "erika.mustermann@example.com",
                    "max.mustermann@example.com",
                ],
                "cc": [],
                "bcc": [],
                "reply_to": [settings.EMAIL_MATHEZIRKEL],
                "attachments": [
                    f"{event_name}_Mustermann_Max.pdf",
                    f"AGB_{event_name}.pdf",
                    "Datenschutzhinweise.pdf"
                ],
                "subject":  f"Mathezirkel - Anmeldung {event_name}",
                "body": render_to_string(
                    "../templates/email/acknowledgement_of_receipt.txt",
                    {
                        "name": "Max",
                        "event": event_name
                    }
                ),
                "fieldToBeEscaped": [
                    "familyName",
                    "givenName",
                ],
            },
            {
                "to": [settings.EMAIL_MATHEZIRKEL],
                "cc": [],
                "bcc": [],
                "reply_to": [],
                "attachments": [
                    "Mustermann_Max.mz.json",
                    f"{event_name}_Mustermann_Max.pdf",
                ],
                "subject": f"Anmeldung {event_name} - Mustermann Max",
                "fieldToBeEscaped": [
                    "familyName",
                    "givenName",
                    "additionalNotes",
                ],
            },
        ]

    @mock.patch('core.forms.fields.BetterCaptchaField.clean', return_value='mocked_value')
    @freeze_time("2000-01-01 12:00:00")
    def test_cleaned_data(self, _mock_captcha_clean: mock.MagicMock) -> None:
        form = self.data_manager().form_class()(
            data=self.data_manager().unpack_data_set(self.data_manager().valid_data_set())
        )
        form.is_valid()
        with open(
            f"./registration/tests/data/{self.data_manager().form_class().form_class_key}.mz.json",
            'r',
            encoding="utf-8"
        ) as file:
            cleaned_data_assertion = json.load(file)
        if form.cleaned_data != cleaned_data_assertion:
            logging.error("The cleaned data differs from assertion:")
            logging.error("Form class: %s", self.data_manager().form_class())
            logging.error("Cleaned data: %s", form.cleaned_data)
            logging.error("Assertion: %s", cleaned_data_assertion)
        self.assertEqual(form.cleaned_data, cleaned_data_assertion)
