from __future__ import annotations
from string import digits
from typing import TYPE_CHECKING

from core.tests.data_generator.fields import (
    BetterBooleanFieldDataGenerator,
    BetterCharFieldDataGenerator,
    BetterChoiceFieldDataGenerator,
    BetterDateFieldDataGenerator,
    BetterEmailFieldDataGenerator,
    BetterListFieldDataGenerator,
    BetterPhoneNumberFieldDataGenerator,
)
from registration.forms.conf import AMOUNT_OF_EMERGENCY_CONTACTS
from registration.forms.enums import (
    CLASS_YEAR_ENUM,
    CLASS_YEAR_SENIOR_ENUM,
    COUNTRY_ENUM,
    NUTRITION_ENUM,
    SEX_ENUM,
    TRAVELTYPE_ENUM,
    STATISTICS_ENUM,
    get_course_choices
)

from registration.tests.data_generator.fields import (
    DepartureArrangementFieldDataGenerator,
    EmergencyContactFieldDataGenerator,
)

if TYPE_CHECKING:
    from core.tests.data_generator.fields import FieldDataGenerator

field_data_generators : dict[str, FieldDataGenerator] = {
    "givenName" : BetterCharFieldDataGenerator(
        min_length=2,
        max_length=50,
        required=True,
    ),
    "familyName" : BetterCharFieldDataGenerator(
        min_length=2,
        max_length=50,
        required=True,
    ),
    "gender" : BetterCharFieldDataGenerator(
        min_length=1,
        max_length=30,
        required=False
    ),
    "sex" : BetterChoiceFieldDataGenerator(
        choices=SEX_ENUM,
        required=True,
    ),
    "birthDate" : BetterDateFieldDataGenerator(
        required=True,
    ),
    "street" : BetterCharFieldDataGenerator(
        min_length=2,
        max_length=50,
        required=True
    ),
    "streetNumber" : BetterCharFieldDataGenerator(
        min_length=1,
        max_length=10,
        required=True
    ),
    "postalCode" : BetterCharFieldDataGenerator(
        required=True,
        min_length=4,
        max_length=5,
        allowed_characters=digits,
    ),
    "city" : BetterCharFieldDataGenerator(
        min_length=2,
        max_length=30,
        required=True
    ),
    "country" : BetterChoiceFieldDataGenerator(
        choices=COUNTRY_ENUM,
        required=True,
    ),
    "classYear" : BetterChoiceFieldDataGenerator(
        choices=CLASS_YEAR_ENUM,
        required=True
    ),
    "classYearSenior" : BetterChoiceFieldDataGenerator(
        choices=CLASS_YEAR_SENIOR_ENUM,
        required=True
    ),
    **{
        f"courseSlot_{event_key}": BetterChoiceFieldDataGenerator(
        choices=get_course_choices(event_key=event_key),
        required=True,
    )
        for event_key in ["praesenzzirkel", "themenzirkel"]
    },
    "telephone" : BetterPhoneNumberFieldDataGenerator(
        required=True,
    ),
    "emailParents" : BetterEmailFieldDataGenerator(
        required=True
    ),
    "emailSelf" : BetterEmailFieldDataGenerator(
        required=False
    ),
    "telephoneSelf" : BetterPhoneNumberFieldDataGenerator(
        required=False,
    ),
    "topicWishes" : BetterListFieldDataGenerator(
        min_length=2,
        max_length=200,
        required=False,
    ),
    **{
        f"emergencyContact_{i}" : EmergencyContactFieldDataGenerator(
            required=not i # Make only the first emergency contact required
        )
        for i in range(AMOUNT_OF_EMERGENCY_CONTACTS)
    },
    "arrival" : BetterChoiceFieldDataGenerator(
        choices=TRAVELTYPE_ENUM,
        required=True
    ),
    "departure" : BetterChoiceFieldDataGenerator(
        choices=TRAVELTYPE_ENUM,
        required=True
    ),
    "departureNotes" : DepartureArrangementFieldDataGenerator(),
    "nutrition" : BetterChoiceFieldDataGenerator(
        choices=NUTRITION_ENUM,
        required=True
    ),
    "foodRestriction" : BetterListFieldDataGenerator(
        min_length=2,
        max_length=200,
        required=False,
    ),
    "healthImpairment" : BetterListFieldDataGenerator(
        min_length=2,
        max_length=200,
        required=False,
    ),
    "drugs" : BetterListFieldDataGenerator(
        min_length=2,
        max_length=200,
        required=False,
    ),
    "removeTicks" : BetterBooleanFieldDataGenerator(
        required=False
    ),
    "roomPartnerWishes" : BetterListFieldDataGenerator(
        min_length=2,
        max_length=200,
        required=False,
    ),
    "zirkelPartnerWishes" : BetterListFieldDataGenerator(
        min_length=2,
        max_length=200,
        required=False,
    ),
    "fractivityWishes" : BetterListFieldDataGenerator(
        min_length=2,
        max_length=200,
        required=False,
    ),
    "instruments" : BetterListFieldDataGenerator(
        min_length=2,
        max_length=30,
        required=False,
    ),
    "pool" : BetterBooleanFieldDataGenerator(
        required=False
    ),
    "leavingPremise" : BetterBooleanFieldDataGenerator(
        required=False
    ),
    "carpoolDataSharing" : BetterBooleanFieldDataGenerator(
        required=False
    ),
    "statistics" : BetterChoiceFieldDataGenerator(
        choices=STATISTICS_ENUM,
        required=False
    ),
    "additionalNotes" : BetterCharFieldDataGenerator(
        min_length=2,
        max_length=1000,
        required=False
    ),
    "participationPermission" : BetterBooleanFieldDataGenerator(
        required=True,
    ),
    "privacyAgreement" : BetterBooleanFieldDataGenerator(
        required=True,
    ),
}
