from core.tests.data_generator.fields import (
    FieldDataGenerator,
    BetterCharFieldDataGenerator,
    BetterChoiceFieldDataGenerator,
    BetterPhoneNumberFieldDataGenerator
)
from registration.forms.enums import DEPARTURE_ARRANGEMENT_ENUM

class DepartureArrangementFieldDataGenerator(FieldDataGenerator):

    def __init__(self):
        self.choice_field_data_generator = BetterChoiceFieldDataGenerator(
            required=True,
            choices=DEPARTURE_ARRANGEMENT_ENUM
        )
        self.char_field_data_generator = BetterCharFieldDataGenerator(
            required=False,
            min_length=2,
            max_length=50
        )
        super().__init__(required=True)

    @property
    def valid_data(self) -> list[tuple[str,str]]:
        return [
            (choice, note)
            for choice in self.choice_field_data_generator.valid_data
            for note in self.char_field_data_generator.valid_data
        ]

    @property
    def invalid_data(self) -> list[tuple[str,str]]:
        return [
            (choice, note)
            for choice in self.choice_field_data_generator.invalid_data
            for note in self.char_field_data_generator.valid_data
        ] + [
            (choice, note)
            for choice in self.choice_field_data_generator.valid_data
            for note in self.char_field_data_generator.invalid_data
        ] + [
            (choice, note)
            for choice in self.choice_field_data_generator.invalid_data
            for note in self.char_field_data_generator.invalid_data
        ]

class EmergencyContactFieldDataGenerator(FieldDataGenerator):

    def __init__(self, required: bool):
        self.char_field_data_generator = BetterCharFieldDataGenerator(
            required=True,
            min_length=2,
            max_length=50,
        )
        self.phone_number_field_data_generator = BetterPhoneNumberFieldDataGenerator(
            required=True
        )
        super().__init__(required)

    @property
    def valid_data(self) -> list[tuple[str,str]]:
        valid_data =  [
            (name, phone)
            for name in self.char_field_data_generator.valid_data
            for phone in self.phone_number_field_data_generator.valid_data
        ]
        if not self.required:
            valid_data.append(('',''))
        return valid_data

    @property
    def invalid_data(self) -> list[tuple[str,str]]:
        invalid_data = [
            (name, phone)
            for name in self.char_field_data_generator.invalid_data
            for phone in self.phone_number_field_data_generator.valid_data
        ] + [
            (name, phone)
            for name in self.char_field_data_generator.valid_data
            for phone in self.phone_number_field_data_generator.invalid_data
        ] + [
            (name, phone)
            for name in self.char_field_data_generator.invalid_data
            for phone in self.phone_number_field_data_generator.invalid_data
        ]
        if not self.required:
            invalid_data.remove(('',''))
        return invalid_data
