"""
URL Patterns for Registration Application
=====================================

This module defines the URL patterns for the Registration application. It uses
Django's built-in `path` function to map URLs to their corresponding views.

Attributes:
-----------
urlpatterns : list
    A list of URL patterns as defined by Django's `path` function. This list is
    processed by Django to route incoming HTTP requests to the appropriate view
    based on the path.

"""

from django.urls import URLPattern, re_path

from registration.forms import form_mappings
from registration.views import RegistrationMultiFormView, RegistrationSuccessView

urlpatterns : list[URLPattern] = [
    re_path(
        f"(?P<event_key>({'|'.join(form_mappings.get_enabled_multiform_class_keys())}))/$",
        RegistrationMultiFormView.as_view(),
        name='registration'
    ),
    re_path(
        f"(?P<event_key>({'|'.join(form_mappings.get_enabled_multiform_class_keys())}))/danke/$",
        RegistrationSuccessView.as_view(),
        name='registration_success'
    )
]
