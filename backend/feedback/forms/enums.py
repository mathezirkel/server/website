from cabret.utils import data_loader

def get_feedback_choices() -> dict[str,str]:
    data_zirkel = data_loader.get_zirkel_data()
    choices = {
        **{"": "Auswählen"},
        **{
            k:v["descriptionFeedback"] for k, v in sorted(
                (
                    (f"{group}:{name}", data)
                    for group, events in data_zirkel.items()
                    for name, data in events.items()
                ),
                key=lambda x: x[1]["weight"]
            )
        }
    }
    return choices
