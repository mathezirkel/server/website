"""
Feedback Form Module
====================

This module defines form classes used by the Feedback Django application.

Classes:
--------
FeedbackForm : forms.Form
    A Django form for collecting anonymous feedback.

"""
from django.forms import Textarea
from django.core.mail import EmailMessage
from django.utils.html import escape

from cabret.utils import data_loader
from core.forms.fields import (
    BetterBooleanField,
    BetterCaptchaField,
    BetterCharField,
    BetterChoiceField
)
from core.forms.forms import BetterForm
from feedback.forms.enums import get_feedback_choices

class FeedbackForm(BetterForm):
    """
    Feedback Form Class.

    Defines a form for collecting anonymous feedback related to various events.

    Configuration:
    --------------
    The available events are loaded from a JSON file located at `./feedback/config/conf.json`.

    Attributes:
    -----------
    events: dict
        A dict containing the available events.

    choices : list
        A list of tuple pairs, where the first element is the event key and the
        second is the event name. The list is populated from the JSON file.

    event_field : BetterChoiceField
        A choice field for selecting the event.

    message_field : BetterCharField
        A text area for inputting the feedback message.

    captcha_field : BetterCaptchaField
        A field for captcha verification.

    privacy_agreement_checkbox : BetterBooleanField
        A checkbox for agreeing to privacy terms.
    """

    event_field = BetterChoiceField(
        label="Veranstaltung",
        required=True,
        choices=get_feedback_choices(),
        initial=""
    )
    message_field = BetterCharField(
        widget=Textarea,
        label="Deine Nachricht",
        min_length=2,
        max_length=1000,
        required=True
    )
    captcha_field = BetterCaptchaField()
    privacy_agreement_checkbox = BetterBooleanField(
        label="Datenschutz",
        description='Ich stimme der Datenverarbeitung gemäß der \
        <a href="https://www.mathezirkel-augsburg.de/datenschutz">Datenschutzerklärung</a> zu.',
        required=True
    )

    post_note = "Die Nachricht wird anonymisiert an dein:e Zirkelleiter:innen gesendet."

    def submit(self) -> None:
        """
        Send Feedback as Email.

        Sends the collected feedback as an email to the respective event's email
        address. The email subject contains the event name, and the email body
        contains the feedback message.

        Raises:
        -------
        smtplib.SMTPException: The function will attempt to send the email, and if it cannot,
        it will raise an SMTPException.
        """

        event = self.cleaned_data['event_field'].split(":", maxsplit=1)
        message = escape(self.cleaned_data['message_field'])

        current_zirkel = data_loader.get_zirkel_data().get(event[0],{}).get(event[1],{})
        subject = f"Anonymes Feedback zu {current_zirkel.get('descriptionFeedback')}"
        recipients = current_zirkel.get("emails")

        email = EmailMessage(
            subject=subject,
            body=message,
            to=recipients
        )

        email.send(fail_silently=False)
