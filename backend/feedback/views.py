"""
Feedback Views Module
=====================

This module contains Django views for handling feedback forms.

Classes:
--------
FeedbackFormView(FormView)
    Class-based view to handle the rendering and processing of the feedback form.

FeedbackSuccessView(TemplateView)
    Class-based view to display a success message after successful form submission.

"""
from __future__ import annotations
from typing import TYPE_CHECKING

from django.views.generic.edit import FormView

from core.views.forms import RatelimitMixin, SuccessView
from feedback.forms.forms import FeedbackForm

if TYPE_CHECKING:
    from django.http import HttpResponseRedirect

class FeedbackFormView(FormView, RatelimitMixin):
    """
    Class-based view to handle the rendering and processing of the feedback form.

    Attributes:
    -----------
    template_name : str
        Path to the template used to render the form.

    form_class : Type[Form]
        Form class used for handling form submissions.

    success_url : str
        URL to redirect to upon successful form submission.

    Methods:
    --------
    form_valid(form) -> HttpResponse
        Handles successful form submissions.
    """
    template_name = "feedback/index.html"
    form_class = FeedbackForm
    success_url = "danke/"

    def form_valid(self, form: FeedbackForm) -> HttpResponseRedirect:
        """
        Handles successful form submissions.
        """
        form.submit()
        return super().form_valid(form)

class FeedbackSuccessView(SuccessView):
    template_name = "feedback/danke/index.html"
    required_referer_substring = "feedback"
    redirect_name = "feedback"
