"""
URL Patterns for Feedback Application
=====================================

This module defines the URL patterns for the Feedback application. It uses
Django's built-in `path` function to map URLs to their corresponding views.

Attributes:
-----------
urlpatterns : list
    A list of URL patterns as defined by Django's `path` function. This list is
    processed by Django to route incoming HTTP requests to the appropriate view
    based on the path.

"""
from __future__ import annotations
from typing import TYPE_CHECKING

from django.urls import path
from feedback.views import FeedbackFormView, FeedbackSuccessView

if TYPE_CHECKING:
    from django.urls import URLResolver

urlpatterns : list[URLResolver] = [
    path('', FeedbackFormView.as_view(), name='feedback'),
    path('danke/', FeedbackSuccessView.as_view(), name='feedback_success')
]
