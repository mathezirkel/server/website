from core.tests import views
from feedback.tests.data import FeedbackFormDataManager

class FeedbackViewTest(views.ViewTest):
    @classmethod
    def path(cls) -> str:
        return "/feedback/"

    @classmethod
    def template(cls) -> str:
        return "feedback/index.html"

    @staticmethod
    def referer() -> str:
        return "feedback"

    @classmethod
    def success_url(cls) -> str:
        return "/feedback/danke/"

    @staticmethod
    def success_template() -> str:
        return "feedback/danke/index.html"

    @staticmethod
    def data_manager() -> type[FeedbackFormDataManager]:
        return FeedbackFormDataManager
