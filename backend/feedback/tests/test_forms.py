# If you import classes inheriting from TestCase directly, the tests are executed.
from core.tests import forms
from feedback.tests.data import FeedbackFormDataManager
class FeedbackFormTest(forms.FormTestCase):
    @staticmethod
    def data_manager() -> type[FeedbackFormDataManager]:
        return FeedbackFormDataManager

    @classmethod
    def email_assertions(cls) -> list[dict[str, str | list[str]]]:
        return [
            {
                "to": ["mathezirkel@math.uni-augsburg.de"],
                "cc": [],
                "bcc": [],
                "reply_to": [],
                "attachments": [],
                "subject": "Anonymes Feedback zu Allgemein",
                "body": "Das ist ein Test!",
                "fieldToBeEscaped": ["message_field"],
            },
        ]
