from __future__ import annotations
from typing import TYPE_CHECKING

from core.tests.data_generator.fields import (
    BetterBooleanFieldDataGenerator,
    BetterCharFieldDataGenerator,
    BetterChoiceFieldDataGenerator,
)
from feedback.forms.enums import get_feedback_choices

if TYPE_CHECKING:
    from core.tests.data_generator.fields import FieldDataGenerator

field_data_generators : dict[str,FieldDataGenerator] = {
    "event_field" : BetterChoiceFieldDataGenerator(
        choices=get_feedback_choices(),
        required=True,
    ),
    "message_field" : BetterCharFieldDataGenerator(
        min_length=2,
        max_length=1000,
        required=True,
    ),
    "privacy_agreement_checkbox" : BetterBooleanFieldDataGenerator(
        required=True,
    ),
}
