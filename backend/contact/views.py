"""
Contact Views Module
=====================

This module contains Django views to manage contact forms. It incorporates rate-limiting
capabilities to prevent misuse.

Functions:
----------
None

Classes:
--------
ContactFormView(FormView)
    Manages the rendering and processing of the contact form, applying rate-limiting when needed.

ContactSuccessView(TemplateView)
    Displays a success message after a successful contact form submission.

"""
from __future__ import annotations
from typing import TYPE_CHECKING

from django.views.generic.edit import FormView
from core.views.forms import RatelimitMixin, SuccessView
from contact.forms import ContactForm

if TYPE_CHECKING:
    from django.http import HttpResponseRedirect
class ContactFormView(FormView, RatelimitMixin):
    """
    Manages the rendering and processing of the contact form.

    Attributes:
    -----------
    template_name : str
        The template path used for rendering the form.

    form_class : Type[Form]
        The class responsible for handling form data and validations.

    success_url : str
        The URL to navigate to upon successful form submission.

    Methods:
    --------
    form_valid(form) -> HttpResponse
        Processes the form when validation succeeds.
    """
    template_name = "kontakt/index.html"
    form_class = ContactForm
    success_url = "danke/"

    def form_valid(self, form: ContactForm) -> HttpResponseRedirect:
        """
        Handles successful form submissions.
        """
        form.submit()
        return super().form_valid(form)

class ContactSuccessView(SuccessView):
    template_name = "kontakt/danke/index.html"
    required_referer_substring = "kontakt"
    redirect_name = "contact"
