"""
Contact Form Module
====================

This module defines the `ContactForm` class used by the Contact Django application to facilitate
email communication between the user and the website owner.

Classes:
--------
ContactForm : forms.Form
    A Django form to enable users to send an email to the website owner.
    This form collects the user's email, the message they want to send,
    and ensures that they are genuine users via a captcha verification.

"""

from django.forms import Textarea
from django.conf import settings
from django.core.mail import EmailMessage
from django.utils.html import escape

from core.forms.fields import (
    BetterBooleanField,
    BetterCaptchaField,
    BetterCharField,
    BetterEmailField
)
from core.forms.forms import BetterForm

class ContactForm(BetterForm):
    """
    Contact Form Class.

    This form provides fields that users need to fill out when they want to get in touch
    with the website owner. The user's email and the message are required, and the captcha ensures
    that it's not a bot sending the email.

    Attributes:
    -----------
    email_field : BetterEmailField
        An input field where the user can provide their email address.

    message_field : BetterCharField
        A text area where the user can type their message.

    captcha_field : BetterCaptchaField
        A captcha verification field to prevent bots from sending automated messages.

    privacy_agreement_checkbox : BetterBooleanField
        A checkbox indicating that the user agrees to the privacy terms of the website.

    """

    email_field = BetterEmailField(
        label="E-Mail",
        required=True
    )
    message_field = BetterCharField(
        widget=Textarea,
        label="Deine Nachricht",
        min_length=2,
        max_length=1000,
        required=True
    )
    captcha_field = BetterCaptchaField()
    privacy_agreement_checkbox = BetterBooleanField(
        label="Datenschutz",
        description='Ich stimme der Datenverarbeitung gemäß der \
        <a href="https://www.mathezirkel-augsburg.de/datenschutz">Datenschutzerklärung</a> zu',
        required=True,
    )

    def submit(self) -> None:
        """
        Send User's Message as Email.

        After the form has been validated, this function sends the user's message to the specified
        recipient (the website owner). The email includes the sender's address, so the recipient
        knows who sent the message.

        Raises:
        -------
        smtplib.SMTPException: The function will attempt to send the email, and if it cannot,
        it will raise an SMTPException.
        """
        subject = "Kontaktanfrage"
        sender = [self.cleaned_data['email_field']]
        message = escape(self.cleaned_data['message_field'])

        recipient = [settings.EMAIL_MATHEZIRKEL]

        email = EmailMessage(
            subject=subject,
            body=message,
            to=recipient,
            reply_to=sender
        )

        email.send(fail_silently=False)
