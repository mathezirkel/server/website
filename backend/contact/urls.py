"""
URL Patterns for Contact Application
=====================================

This module defines the URL patterns for the Contact application. It uses
Django's built-in `path` function to map URLs to their corresponding views.

Attributes:
-----------
urlpatterns : list
    A list of URL patterns as defined by Django's `path` function. This list is
    processed by Django to route incoming HTTP requests to the appropriate view
    based on the path.

"""
from __future__ import annotations
from typing import TYPE_CHECKING

from django.urls import path
from contact.views import ContactFormView, ContactSuccessView

if TYPE_CHECKING:
    from django.urls import URLResolver

urlpatterns : list[URLResolver] = [
    path('', ContactFormView.as_view(), name='contact'),
    path('danke/', ContactSuccessView.as_view(), name='contact_success')
]
