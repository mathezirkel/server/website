# If you import classes inheriting from TestCase directly, the tests are executed.
from core.tests import views
from contact.tests.data import ContactFormDataManager

class ContactViewTest(views.ViewTest):
    @classmethod
    def path(cls) -> str:
        return "/kontakt/"

    @classmethod
    def template(cls) -> str:
        return "kontakt/index.html"

    @staticmethod
    def referer() -> str:
        return "kontakt"

    @classmethod
    def success_url(cls) -> str:
        return "/kontakt/danke/"

    @staticmethod
    def success_template() -> str:
        return "contact/danke/index.html"

    @staticmethod
    def data_manager() -> type[ContactFormDataManager]:
        return ContactFormDataManager
