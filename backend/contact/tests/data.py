from core.tests.data import FormDataManager
from core.tests.data_generator.forms import FormDataGenerator
from contact.forms import ContactForm
from contact.tests.data_generator.forms import field_data_generators

class ContactFormDataManager(FormDataManager):
    @staticmethod
    def form_class() -> type[ContactForm]:
        return ContactForm

    @staticmethod
    def form_fields() -> list[str]:
        return [
            "email_field",
            "message_field",
            "captcha_field",
            "privacy_agreement_checkbox"
        ]

    @classmethod
    def valid_data_set(cls) -> dict[str, str]:
        return {
            "email_field" : "example@example.com",
            "message_field": "Das ist ein Test!",
            "privacy_agreement_checkbox": "on",
        }

    @classmethod
    def invalid_data_set(cls) -> dict[str, str]:
        return {
            "email_field" : "example@example.com",
            "message_field": "Das ist ein Test!",
            # HMTL does not POST unchecked checkboxes
            # Django evaluates missing checkbox fields as False
            # privacy_agreement_checkbox is required
            #"privacy_agreement_checkbox": "on",
        }

    @classmethod
    def form_data_generator(cls) -> FormDataGenerator:
        return FormDataGenerator(field_data_generators)
