from __future__ import annotations
from typing import TYPE_CHECKING
from core.tests.data_generator.fields import (
    BetterBooleanFieldDataGenerator,
    BetterCharFieldDataGenerator,
    BetterEmailFieldDataGenerator,
)

if TYPE_CHECKING:
    from core.tests.data_generator.fields import FieldDataGenerator

field_data_generators : dict[str, FieldDataGenerator] = {
    "email_field" : BetterEmailFieldDataGenerator(
        required=True,
    ),
    "message_field" : BetterCharFieldDataGenerator(
        min_length=2,
        max_length=1000,
        required=True,
    ),
    "privacy_agreement_checkbox" : BetterBooleanFieldDataGenerator(
        required=True,
    ),
}
