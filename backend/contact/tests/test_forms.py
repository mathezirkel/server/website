from django.conf import settings

# If you import classes inheriting from TestCase directly, the tests are executed.
from core.tests import forms
from contact.tests.data import ContactFormDataManager

class ContactFormTest(forms.FormTestCase):

    @staticmethod
    def data_manager() -> type[ContactFormDataManager]:
        return ContactFormDataManager

    @classmethod
    def email_assertions(cls) -> list[dict[str, str | list[str]]]:
        return [
            {
                "to": [settings.EMAIL_MATHEZIRKEL],
                "cc": [],
                "bcc": [],
                "reply_to": ["example@example.com"],
                "attachments": [],
                "subject": "Kontaktanfrage",
                "body": "Das ist ein Test!",
                "fieldToBeEscaped": ["message_field"],
            },
        ]
