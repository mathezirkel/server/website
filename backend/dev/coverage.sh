#!/bin/sh

set -e

coverage run ./manage.py test
echo ""
coverage report
echo ""
coverage html
