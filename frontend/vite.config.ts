import { fileURLToPath, URL } from "node:url";
import { defineConfig } from "vite";
import ViteWebfontDownload from "vite-plugin-webfont-dl";

// https://vitejs.dev/config/
export default defineConfig({
    build: {
        rollupOptions: {
            input: {
                main: '@/main.ts',
                kryptographie: '@/kryptographie/index.js'
            },
            output: {
                // Set a static name for assets, without a hash
                assetFileNames: `[ext]/[name].[ext]`,
                // Set static names for chunks and entry files as well if needed
                chunkFileNames: `js/[name].js`,
                entryFileNames: `js/[name].js`,
            }
        },
        emptyOutDir: true,
        assetsInlineLimit: 0,
        chunkSizeWarningLimit: 1000,
    },
    publicDir: false,
    resolve: {
        alias: {
            "@": fileURLToPath(new URL("./src", import.meta.url)),
        },
    },
    plugins: [
        ViteWebfontDownload(
            [
                "https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300..800;1,300..800&display=swap",
            ],
            { injectAsStyleTag: true }
        ),
    ],
});
