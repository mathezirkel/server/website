import "katex/dist/katex.css"
import "katex/dist/katex.js"
import renderMathInElement from 'katex/contrib/auto-render';

export default function renderMath(): void {
    renderMathInElement(document.body, {
        delimiters: [
        { left: '\\[', right: '\\]', display: true },   // block math
        { left: '\\(', right: '\\)', display: false },  // inline math
        ],
        throwOnError: false,
    });
}
