import "./styles/main.scss"

import renderMath from "./katex";
import navbar from "./navbar";

document.addEventListener("DOMContentLoaded", () => {
    navbar();
    renderMath();
});
