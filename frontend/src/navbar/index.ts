export default function navbar(): void {
    const nav = document.getElementById("nav");
    const hamburger = document.getElementById("hamburger");

    function toggleNav(): void {
        nav?.classList.toggle("nav-dropped");
    }

    // Event listener for mouse interaction
    hamburger?.addEventListener("click", toggleNav);

    // Event listener for keyboard interaction
    hamburger?.addEventListener("keydown", (event: KeyboardEvent) => {
        if (event.key === "Enter") {
            toggleNav();
        }
    });
}
