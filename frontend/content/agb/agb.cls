\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{agb}[2024/04/05 LaTeX class]

\LoadClass[a4paper,ngerman,fontsize=9pt]{scrartcl}

\RequirePackage[top=2cm, left=2cm, right=2cm, bottom=2cm, footskip=1cm]{geometry}
\RequirePackage{scrlayer-scrpage}

\newcommand{\@event}{}
\DeclareRobustCommand*{\datum}[1]{\gdef\@event{#1}}
\newcommand{\makeheader}{%
    \begin{center}
        \begin{huge}
            \textbf{
                Allgemeine Geschäftsbedingungen\\\smallskip
                für die Teilnahme am {\event} des Mathezirkels
            }
        \end{huge}
    \end{center}
}
\AddToHook{begindocument/end}{\makeheader}

\setlength{\parskip}{\smallskipamount}
\setlength{\parindent}{0cm}
%\setkomafont{section}{\Large}
%\setkomafont{subsection}{\large}

\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000

\cfoot{}
\ofoot{\pagemark}
