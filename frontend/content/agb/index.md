---
title: "Allgemeine Geschäftsbedingungen"
keywords:
- AGB
---

Unter den folgenden Links können die AGBs der einzelnen Angebote eingesehen werden:

* [Präsenzzirkel](./praesenzzirkel.pdf)
* [Korrespondenzzirkel](./korrespondenzzirkel.pdf)
* [Astrotag](./astrotag.pdf)
* [Mai-Mathetag](./mai-mathetag.pdf)
* [Wintercamp](./wintercamp.pdf)
* [Mathecamp](./mathecamp.pdf)
