---
title: Über uns
_build:
  render: never
  list: never
---

Wir, der Mathezirkel Augsburg, sind eine engagierte Gemeinschaft von Student:innen, Doktorand:innen und Absolvent:innen
der Mathematik an der Universität Augsburg.
Unser Ziel ist es, unsere Leidenschaft für faszinierende mathematische Themen mit Kindern und Jugendlichen im Alter
von der 5. bis zur 13. Klasse zu teilen und zu vermitteln.

Unser Angebot ermöglicht einen spannenden Einblick in mathematische Themen, die über den Schulstoff hinausgehen.
Wir öffnen die Tür zur faszinierenden Welt der "echten" Mathematik und bieten dabei
verschiedene Möglichkeiten der Teilnahme an.
Unsere Veranstaltungen finden sowohl vor Ort an der Universität Augsburg statt als auch über Korrespondenzbriefe,
um Interessenten aus der Ferne anzusprechen.

Ein besonderes Highlight ist unser neuntägiges Mathecamp, das während der Sommerferien stattfindet.
Hier steht nicht nur Mathematik im Fokus, sondern auch ein abwechslungsreiches Freizeitprogramm,
das Spaß und Spiel nicht zu kurz kommen lässt.

Wir sind begeistert von Mathematik und freuen uns darauf, diese Begeisterung mit Ihnen und Ihren Kindern zu teilen.
Tauchen Sie mit uns ein in die Welt der Mathematik und lassen Sie sich von ihrer Faszination begeistern!
