---
title: Fehler
type: error
url: /403_csrf.html
errorCode:
  - 4
  - 0
  - 3
---

CSRF-Verifizierung fehlgeschlagen!

Um das Formular zu nutzen, musst du Session Cookies aktivieren.
Falls du Session Cookies bereits aktiviert hast, schließe den Browser vollständig und versuche es erneut.
