---
title: WINTercamp 2023
---

## Rückblick

<div class="highlight-box">

20\. bis 24. Februar 2023

30 Teilnehmer:innen

5 Betreuer:innen

5 wunderschöne Tage!

</div>

## Fotogalerie

[Hier](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/wintercamp/2023/fotos/) findest du die Bilder
des diesjährigen WINTercamps.
Die Zugangsdaten hast du von uns per Mail erhalten.
Bitte gib die Zugangsdaten nicht an Fremde weiter und verbreite einzelne Fotos nur,
wenn du die Erlaubnis aller auf dem Foto abgebildeten Personen hast.

Möchtest du eine ganze Galerie in Orginalqualität herunterladen, kannst du oben links auf das Save-Icon klicken.

Hast du selbst noch Fotos oder Videos, die du gerne teilen möchtest? Dann
schick sie uns an
[mathezirkel@math.uni-augsburg.de](mailto:mathezirkel@math.uni-augsburg.de).

## WINTercamp 2024

Ihr fandet das WINTercamp zu kurz? Nächstes Jahr 2 Tage mehr?
Perfekt, notiert euch schonmal den 10. bis 16. Februar 2024 fett im Kalender!

<div class="centering">
    <figure>
        {{< responsive-image
            src="images/wintergregor.png"
            alt="Wintergregor"
            width=500
            loading="lazy"
        >}}
    </figure>
</div>
