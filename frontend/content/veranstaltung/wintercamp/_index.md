---
title: Wintercamp
type: event
eventName: wintercamp
image: images/zirkelgregor.png
registrationLinks:
  - url: /anmeldung/wintercamp/
feedback: True
donate: False
summary: |
    Aktuell ist kein Wintercamp für 2025 geplant.
---

**Aktuell ist kein Wintercamp für 2025 geplant!**

Seit 2023 gibt es auch in den Winterferien ein Mathecamp. Das Camp ist im Vergleich zum Sommercamp deutlich kleiner.
Die gemütliche und winterliche Atmosphäre bietet eine hervorragende Möglichkeit für einen intensiven
mathematischen Austausch.

Das Wintercamp richtet sich speziell an ältere Schüler:innen von der 9. bis 13. Jahrgangsstufe.

<figure>
    {{< responsive-image
        src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/wintercamp.jpg"
        alt="Bild zur Veranstaltung"
    >}}
</figure>

Vormittags finden mathematische Seminare statt, in denen du in Kleingruppen spannende Bereiche der Mathematik
kennenlernst. Selbstverständlich gibt es für verschiedene Alters- und Vorkenntnisstufen angepasste Kurse mit
unterschiedlichen Themen und Schwierigkeiten, sodass immer für jede:n etwas dabei ist.
Zusätzlich gibt es nachmittags weitere, optionale Arbeitsgruppen.

Willst du einen Schneemann bauen? Auch wenn der Fokus des Camps stärker auf der Mathematik liegt, bietet das Haus
mit seiner Turnhalle, der Kegelbahn und den Werkräumen auch im Winter ausreichend Möglichkeit für ein buntes
Freizeitprogramm. Dieses besteht u. a. aus Programmierworkshops, Mal- und Nähstunden, einer Nachtwanderung sowie
aus Spielen und den verschiedensten Sportarten. Darüber hinaus werden wir bei schönem Wetter auch viel im Freien
machen können.

Das Wintercamp verspricht eine unvergessliche Zeit voller Mathematik und Spaß!

### Vergangene Camps

* [Wintercamp 2024](./2024/)
* [Wintercamp 2023](./2023/)
