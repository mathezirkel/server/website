---
title: Auftaktveranstaltung
eventName: auftaktveranstaltung
type: event
image: images/zirkelgregor.png
registrationLinks:
  - text: Anmeldung Präsenzzirkel
    url: /anmeldung/praesenzzirkel/
  - text: Anmeldung Themenzirkel
    url: /anmeldung/themenzirkel/
  - text: Anmeldung Korrespondenzzirkel
    url: /anmeldung/korrespondenzzirkel/
feedback: False
donate: True
draft: True
---

Um euch einen Einblick in den Mathezirkel zu geben, findet am Samstag, den 12.10.2024, um 10 Uhr
unsere Eröffnungsveranstaltung im Hörsaal 1004 des [T-Gebäudes](https://assets.mathezirkel-augsburg.de/static/lageplan/lageplan-T.pdf)
der Uni Augsburg statt.
Eine Anmeldung dafür ist nicht erforderlich.

<figure>
    {{< responsive-image
        src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/auftakt.jpg"
        alt="Bild zur Veranstaltung"
    >}}
</figure>

Als Highlight steht ein allgemeinverständlicher mathematischer Vortrag von Prof. Dr. Tatjana Stykel
mit dem Thema *"Eine Reise in die Welt der Matrizen -- Mathematik in der Bildverarbeitung"* auf dem Programm.

Nach dem Vortrag haben wir noch ein wenig Zeit eingeplant, um ins Gespräch zu kommen.
Bei Snacks und Getränken auf Spendenbasis habt ihr die Möglichkeit,
schon mal Zirkelleiter:innen und andere Teilnehmer:innen kennen zu lernen.

### Auftakträtsel

Gregor strandet auf einer von zwei benachbarten Inseln.
Die Zwerge der einen Insel lügen stets, die der anderen sagen immer die Wahrheit.
Gregor möchte mit nur einer Ja/Nein-Frage herausfinden, ob er sich auf der Wahrheits- oder der Lügeninsel befindet.
Da sich die Zwerge gegenseitig besuchen, weiß Gregor noch nicht einmal, von welcher Insel der Zwerg,
den er anspricht, eigentlich stammt.
Was kann Gregor fragen?

Hat dir das Rätsel gefallen?
Die Auflösung gibt es dann bei der Auftaktveranstaltung.

Die Anmeldung für die Präsenz- und Korrespondenzzirkel sowie die Themenseminare ist
spätestens ab Samstag, den 12. Oktober, möglich!
