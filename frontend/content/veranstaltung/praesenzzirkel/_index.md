---
title: Präsenz- und Themenzirkel
type: event
eventName: praesenzzirkel
image: images/zirkelgregor.png
registrationLinks:
  - text: Anmeldung Präsenzzirkel
    url: /anmeldung/praesenzzirkel/
  - text: Anmeldung Themenzirkel
    url: /anmeldung/themenzirkel/
feedback: True
donate: False
summary: |
    Du hast Spaß an Mathematik?
    Dann kannst du dich mit anderen Teilnehmer:innen und ehrenamtlichen Tutor:innen an der Uni Augsburg treffen.
    Gemeinsam lernt ihr in kleinen Gruppen interaktiv spannende mathematische Konzepte kennen und bearbeitet knifflige Aufgaben.
---

Unsere Präsenz- und Themenzirkel bieten eine hervorragende Gelegenheit,
weiterführende faszinierende Themen der Mathematik zu entdecken.
Dazu triffst du dich mit anderen Teilnehmer:innen und ehrenamtlichen Tutor:innen an der Uni Augsburg.
Gemeinsam lernt ihr in kleinen Gruppen interaktiv spannende mathematische Konzepte kennen und bearbeitet
knifflige Aufgaben.

Die Zirkel sind nicht als Nachhilfe oder Wiederholung von Schulstoff gedacht,
sondern behandeln weiterführende Inhalte außerhalb des Schulstoffs.

<figure>
    {{< responsive-image
        src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/praesenz.jpg"
        alt="Bild zur Veranstaltung"
    >}}
</figure>

Mögliche Themen sind beispielsweise:

* Logikrätsel
* Magische Quadrate
* Graphentheorie
* Verschlüsselung
* Relativitätstheorie

Selbstverständlich sind die Inhalte an die Klassenstufen angepasst.
Unsere Themen und Fragestellungen sind definitiv anders als das, was du aus der Schule kennst.

## Präsenzzirkel

Präsenzzirkel finden ca. alle zwei Wochen statt und bieten viele verschiedene Themen.

Die konkreten Termine macht du mit deine:r Zirkelleiter:in per Mail aus.

Im ersten Halbjahr finden die Zirkel an folgenden Tagen statt:

* Klasse 5: Donnerstag 17.30 bis 19.00 Uhr
* Klasse 6: Freitag 17.30 bis 19.00 Uhr
* Klasse 7: Donnerstag 17.30 bis 19.00 Uhr
* Klasse 8: Mittwoch 17.30 bis 19.00 Uhr
* Klasse 9: Donnerstag 17.30 bis 19.00 Uhr
* Klasse 10: Freitag 17.30 bis 19.00 Uhr

## Themenzirkel

Themenzirkel bieten dir die Chance, dich in wenigen Terminen intensiv mit einem speziellen Thema zu beschäftigen.

Eine nachträgliche Anmeldung ist bis zum ersten Termin des jeweiligen Themenzirkels möglich.

<div class="topic-list">

### Josephus-Problem

**Klassenstufen**: 8 bis 10

**Termine**:

* Mittwoch, 19.02.2025, 17.30 bis 19.00 Uhr
* Mittwoch, 26.02.2025, 17.30 bis 19.00 Uhr

In diesem Zirkel beschäftigen wir uns mit dem Josephus-Problem.
Das Josephus-Problem ist ein spannendes Rätsel aus der Mathematik.

Stell dir vor, eine Gruppe von Menschen steht im Kreis.
Sie spielen ein Spiel, bei dem jede zweite Person ausscheiden muss, bis nur noch Josephus übrig bleibt.

Die Frage ist: **Wo muss Josephus stehen, damit er als Letzter übrig bleibt?**

Durch einfaches Ausprobieren entwickeln wir zunächst eine Lösungsidee.
Doch Ideen sind nicht viel mehr als Vermutungen, welche überprüft und bewiesen werden müssen.
Gemeinsam werden wir Josephus helfen, sein Problem zu lösen, indem wir nicht nur eine Idee aufstellen,
sondern diese auch beweisen.

### Das Rutschenproblem oder die Geburtsstunde der Variationsrechnung

**Klassenstufen**: 11 bis 13

**Termine**:

* Freitag, 14.02.2025, 17.30 bis 19.00 Uhr
* 3 bis 4 weitere Termine (noch offen)

In diesem Themenseminar werden wir uns mit der Form einer optimalen Rutsche beschäftigen.
Hierbei geht es darum, am schnellsten von einem Startpunkt S zu einem Ziel Z zu kommen,
wenn man nur von der Schwerkraft ausgeht. Wir werden herausﬁnden, ob dies die gerade
Verbindungslinie, oder am Ende doch eine ganz andere, nicht oﬀensichtliche Kurve, ist.
Dies ist ein Beispiel für Minimierungsprobleme, welche ihr in einfacher Form bereits aus der
Schule kennt. Beispielsweise lassen sich die Extrempunkte der Funktion \(f(x)=x^3-42x-42\)
mittels der ersten und zweiten Ableitung bestimmen. Dies wird auch die Strategie für das
Rutschenproblem werden und wird allgemein Variationsrechnung genannt.

### Laufende oder bereits abgeschlossene Themenzirkel

**Eine nachträgliche Anmeldung ist leider nicht mehr möglich!**

#### Entfernungen auf dreidimensionalen Objekten

**Klassenstufen**: 5 bis 7

**Termine**:

* Mittwoch, 15.01.2025, 17.30 bis 19.00 Uhr
* Mittwoch, 29.01.2025, 17.30 bis 19.00 Uhr

In diesem Kurs werden wir den kürzesten Weg zwischen zwei Punkten auf der Oberfläche verschiedener 3D-Objekte untersuchen
Dabei werden wir einen Würfel basteln und gemeinsam überlegen,
wie wir auf seiner Oberfläche den schnellsten Weg von einer Ecke zur anderen finden können.
Schritt für Schritt erkunden wir spielerisch verschiedene Strategien und lernen,
wie man Netzpläne nutzt, um diese Wege zu verstehen.

#### Taschenrechner aus Dominosteinen

**Klassenstufen**: 5 bis 7

**Termine**:

* Freitag, 29.11.2024, 17.30 bis 19.00 Uhr
* Freitag, 07.02.2025, 17.30 bis 19.00 Uhr

In diesem Zirkel bauen wir gemeinsam unseren eigenen Taschenrechner -- aus Dominosteinen!
Zusammen entdecken wir, wie wir mit Dominosteinen rechnen können. Wir lernen, die Steine so aufzustellen,
dass sie eine Kettenreaktion auslösen und einfache Rechenaufgaben lösen.
Sobald wir den ersten Stein anstoßen, können wir beobachten, wie die Lösung Schritt für Schritt entsteht.
Es wird ein großes, spannendes Domino-Abenteuer – und wir werden sehen, dass Rechnen richtig Spaß machen kann!

#### Mathematik hinter Dobble

**Klassenstufen**: 7 bis 9

**Termine**:

* Montag, 25.11.2024, 18.00 bis 19.00 Uhr
* Montag, 02.12.2024, 18.00 bis 19.00 Uhr
* Montag, 09.12.2024, 18.00 bis 19.00 Uhr

Dobble ist ein beliebtes Kartenspiel, das aus 55 Karten besteht, von denen jede 8 Symbole zeigt.
Obwohl es verschiedene Varianten gibt, bleibt das Spielprinzip immer gleich:
Es geht darum, das gemeinsame Symbol zwischen zwei Karten so schnell wie möglich zu finden.
Doch wie ist es überhaupt möglich, so viele Karten zu erstellen, sodass auf zwei beliebigen Karten genau ein Symbol übereinstimmt?
Dahinter steckt ganz schon viel Mathematik -- genauer gesagt Geometrie!

Im Zirkel werden wir die aus der Schule bekannte Geometrie (Koordinatensystem in der Ebene) erweitern und
sowohl endliche als auch projektive Geometrie kennenlernen.

Dabei werden wir untersuchen, ob es möglich ist, noch mehr Karten oder Symbole hinzuzufügen,
ohne das grundlegende Spielprinzip zu verändern.

#### Graphentheorie

**Klassenstufen**: 9 bis 13

**Termine**:

* Freitag, 13.12.2024, 17.30 bis 19.00 Uhr
* Freitag, 17.01.2025, 17.30 bis 19.00 Uhr
* Freitag, 31.01.2025, 17.30 bis 19.00 Uhr

Graphen sind Netzwerke, die aus Knoten und Kanten bestehen.
Viele Phänomene in Natur und Gesellschaft können durch Graphen modelliert werden,
z.B. Social Media. Wir besprechen folgende Themen:

(1) Planarität. Was für Netzwerke kann ich überschneidungsfrei auf ein Blatt Papier zeichnen?
Diese Frage ist zum Beispiel bei Bahnschienen relevant.
Weniger relevant aber trotzdem cool: Auf einem Donut kann man viel mehr Netzwerke ohne Überschneidung zeichnen!

(2) Färbungen. Wie viele Farben benötigt man, um eine Landkarte einzufärben,
sodass alle Nachbarländer unterschiedliche Farben haben?
(Sonst wäre die Karte ja schwierig zu lesen...)
Und was hat das mit dem Google Chrome Logo zu tun?

(3) Algorithmen. Wir wollen auf Instagram ein Produkt bewerben,
sodass alle User einer Stadt die Werbung mindestens einmal sehen.
Wie viele Influencer:innen müssen dafür mindestens ein Produkt-Placement machen?
Und wie finden wir diese?

#### Mathematische Rätsel mit Computern lösen

**Klassenstufe**: 9 bis 13

**Termine**:

* Dienstag, 12.11.2024, 17.30 bis 19.00 Uhr
* Dienstag, 19.11.2024, 17.30 bis 19.00 Uhr
* Dienstag, 26.11.2024, 17.30 bis 19.00 Uhr

Hast du Spaß daran, knifflige Rätsel zu lösen und möchtest wissen, wie Computer dabei helfen können?
In diesem Zirkel beschäftigen wir uns mit spannenden Problemen, die mit normalem Rechnen kaum lösbar sind. Ob es darum geht,
Primzahlen zu entdecken, das n-Queens-Problem zu meistern oder Sudoku-Rätsel mit einem SAT-Solver zu knacken:
Du lernst, wie man Algorithmen entwickelt, um auch die schwierigsten Herausforderungen zu bewältigen.
Gemeinsam zeigen wir der Brute-Force-Methode, dass es auch schlauer geht!

#### Die spezielle und die allgemeine Relativitätstheorie Albert Einsteins

**Klassenstufen**: 11 bis 13

**Termine**: Freitag 17.30 bis 19.00 Uhr -- Ca. alle 2 Wochen über das gesamte Schuljahr

Über das ganze Schuljahr hinweg werden wir uns etwa alle zwei oder drei Wochen treffen,
um uns mit der Allgemeinen Relativitätstheorie (ART) Albert Einsteins zu beschäftigen.

Die ART, die die Raumzeit und die Gravitation beschreibt, gehört zu den schönsten Theorien, die die Physik zu bieten hat,
wenn sie nicht sogar die schönste Theorie überhaupt ist.
Von der ART werden Phänomene wie die vorhergesagt, dass die Uhren in der Nähe eines Schwerefeldes langsamer gehen,
dass es schwarze Löcher gibt, aus denen es kein Entrinnen mehr gibt,
und dass Gravitationswellen von sich beschleunigenden Massen abgestrahlt werden.
Die ART beschreibt aber auch die Expansion des Kosmos seit dem Urknall und seine Zukunft.
Die ART hat den Ruf, schwer verständlich zu sein.
Wir werden uns von dem Gegenteil überzeugen.
Dazu werden wir uns in Ruhe die mathematischen Grundlagen erarbeiten und danach streben, die Dinge im Detail und
nicht nur oberflächlich zu verstehen.
Voraussetzung dazu ist ein Interesse an Mathematik und Physik und Neugierde, Neues zu lernen.

</div>
