---
title: Mai-Mathetag
type: event
eventName: mai-mathetag
image: images/zirkelgregor.png
registrationLinks:
  - url: /anmeldung/mai-mathetag/
feedback: True
donate: False
summary: |
    Am Mai-Mathetag hast du die Gelegenheit, gemeinsam mit anderen mathematikbegeisterten Schüler:innen
    einen Tag voller Mathematik und Spaß zu verbringen.
---

Am Mai-Mathetag hast du die Gelegenheit, gemeinsam mit anderen mathematikbegeisterten Schüler:innen
einen Tag voller Mathematik und Spaß zu verbringen.

<figure>
    {{< responsive-image
        src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mai-mathetag.jpg"
        alt="Bild zur Veranstaltung"
    >}}
</figure>

Vormittags finden mathematische Seminare in Kleingruppen statt,
in denen du gemeinsam mit ehrenamtlichen Tutor:innen spannende mathematische Themen interaktiv entdeckst.
Nachmittags gibt es neben einem kleinen mathematischen Wettkampf ein buntes Freizeitangebot,
welches u. a. Programmierkurse (für jedes Niveau), eine Campus-Rallye sowie weitere mathematische Seminare, beinhaltet.

Die Mathetage bieten insbesondere denen eine großartige Möglichkeit,
die aufgrund ihres Wohnorts nicht regelmäßig an den Präsenzzirkeln teilnehmen können.
Außerdem bekommt ihr einen kleinen Einblick in das, was euch auf dem Mathecamp erwartet.

Für die Verpflegung erheben wir in der Regel einen kleinen Unkostenbeitrag.

### Mai-Mathetag 2025

Die Anmeldephase startet voraussichtlich im April 2025.
