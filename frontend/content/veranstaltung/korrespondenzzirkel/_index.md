---
title: Korrespondenzzirkel
type: event
eventName: korrespondenzzirkel
image: images/zirkelgregor.png
registrationLinks:
  - url: /anmeldung/korrespondenzzirkel/
feedback: True
donate: False
summary: |
    Wir senden dir alle vier bis sechs Wochen per E-Mail einen Brief zu.
    Darin findest du eine Einführung in ein mathematisches Thema mit ausführlichen Erklärungen, Beispielen und Aufgaben.
    Die Aufgaben kannst du freiwillig bearbeiten.
    Wenn du uns deine Lösungen zusendest, korrigieren wir diese gerne für dich.
---

Du möchtest dich lieber eigenständig zu Hause mit spannenden mathematischen Themen beschäftigen?
Dann ist der Korrespondenzzirkel etwas für dich!

<figure>
    {{< responsive-image
        src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/korrespondenz.jpg"
        alt="Bild zur Veranstaltung"
    >}}
</figure>

Wir senden dir alle vier bis sechs Wochen per E-Mail einen Brief zu.
Darin findest du eine Einführung in ein mathematisches Thema mit ausführlichen Erklärungen, Beispielen und Aufgaben.
Die Aufgaben kannst du freiwillig bearbeiten.
Wenn du uns deine Lösungen zurücksendet, korrigieren wir diese gerne für dich.

Die Themen sind an die entsprechenden Klassenstufen angepasst und enthalten sowohl einfache als auch anspruchsvolle
Aufgaben, sodass für jede:n etwas dabei ist.

Mögliche Themen sind beispielsweise:

* Logikrätsel
* Magische Quadrate
* Graphentheorie
* Verschlüsselung
* Relativitätstheorie
