---
title: Mathecamp 2024
---

## Über uns

<div class="centering-flex-col">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/heatmap.png"
            alt="Herkunft der Teilnehmer:innen und Betreuer:innen"
            width="500"
            class="rounded-corners"
            loading="lazy"
        >}}
        <figcaption>Hier kommen wir her!</figcaption>
    </figure>
    <div class="pie-chart-area">
        <div class="pie-chart">
            <figure>
                <img
                    src="piechart-participants.svg"
                    width="200px"
                    alt="Aufteilung der Teilnehmer:innen nach Geschlecht"
                >
                <figcaption>112 Teilnehmer:innen</figcaption>
            </figure>
        </div>
        <div class="pie-chart">
            <figure>
                <img
                    src="piechart-grades.svg"
                    width="200px"
                    alt="Aufteilung der Teilnehmer:innen nach Klasse"
                >
            <figcaption>112 Teilnehmer:innen</figcaption>
            </figure>
        </div>
        <div class="pie-chart">
            <figure>
                <img
                    src="piechart-instructors.svg"
                    width="200px"
                    alt="Aufteilung der Betreuer:innen nach Geschlecht"
                >
            <figcaption>24 Betreuer:innen</figcaption>
            </figure>
        </div>
    </div>
</div>

## Fotos

<div class="gallery-area">

<div class="gallery">

<figure>
    {{< responsive-image
        src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/mathematik.JPG"
        alt="Vorschaubild Bildergallerien"
        loading="lazy"
    >}}
</figure>

### Mathematik

* [Vortrag 5 -- 8](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/vortrag-klein)
* [Vortrag 9 -- 12](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/vortrag-gross)
* [Zirkel](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/zirkel)

</div>

<div class="gallery">

<figure>
    {{< responsive-image
        src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/fraktivitaeten.JPG"
        alt="Vorschaubild Bildergallerien"
        loading="lazy"
    >}}
</figure>

### Fraktivitäten

* [Basteln](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/basteln)
* [Bootfahren](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/bootfahren)
* [Jugger](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/jugger)
* [Light Painting](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/light-painting)
* [Musik](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/musik)
* [Nachtwanderung](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/nachtwanderung)
* [Tanzkurs](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/tanzkurs)
* [Turnhalle](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/turnhalle)
* [Weitere Fraktis](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/fraktis)

</div>

<div class="gallery">

<figure>
    {{< responsive-image
        src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/mehr.JPG"
        alt="Vorschaubild Bildergallerien"
        loading="lazy"
    >}}
</figure>

### Mehr

* [Bunter Abend](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/bunter-abend)
* [Cocktail Party](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/cocktail-party)
* [Essen](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/essen)
* [Kennenlernspiel](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/kennenlernspiel)
* [Tribunal](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/tribunal)
* [Wandertag](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/wandertag)
* [Sonstiges](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/sonstiges)

</div>

</div>

Die Zugangsdaten hast du von uns per Mail erhalten.
Bitte
gib die Zugangsdaten nicht an Fremde weiter und verbreite einzelne Fotos
nur, wenn du die Erlaubnis von allen auf dem Foto abgebildeten Personen
hast.

Möchtest du eine ganze Galerie in Orginalqualität herunterladen,
kannst du oben links auf das Save-Icon klicken.

Hast du selbst noch Fotos oder Videos, die du gerne teilen möchtest?
Dann schickt sie uns an [mathezirkel@math.uni-augsburg.de](mailto:mathezirkel@math.uni-augsburg.de).

Falls du noch Sachen vermisst, gibt es eine Galerie von
[Fundsachen](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2024/fotos/fundsachen).
Solltest
du etwas wiedererkennen, schreib uns auch eine E-Mail.

## Zirkel und Mathematik

### Endliche projektive Geometrie und das Spiel Dobble

In der 6.
und 7.
Klasse haben wir die Mathematik hinter dem Spiel Dobble erforscht.
Dabei haben wir u.
a.
gelernt, dass zwei parallele Geraden einen Schnittpunkt im Unendlichen haben und
man dem Spiel zwei Karten hinzufügen könnte.
Die Vorlage für den Zirkel war ein [spannendes Video](https://www.youtube.com/watch?v=vyYSEDGUdlg) von DorFuchs.

### Kabel-Knoten-Rätsel

Unmöglicherweise gibt es dazu ein ganzes Video von Mathematiker Matt Parker und Physiker Steve Mould,
[*Solving the mystery of the impossible cord*](https://www.youtube.com/watch?v=g3R_tc7YrFI).
ACHTUNG: Die Lösung zu kennen hat absolut gar keinen Nutzen für euer Leben.
Die Lösung selbst zu finden ist
aber spaßig und spannend.
Du hast die Lösung noch nicht gefunden?
Nächstes Jahr liegt das Rätsel bestimmt wieder im Mathecamp aus.
Grundlage für dieses und andere Knoten-Rätsel sind Kurveninvarianten, wie die *Windungszahl*,
die wir uns dieses Jahr zum Beispiel im Zirkel der 8.
Klasse angeschaut haben.

<div class="grid-gallery">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/kabelknoten-ungeloest.png"
            alt="Kabelknoten ungelöst"
            height="300"
            class="max-height-300"
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/kabelknoten-geloest.png"
            alt="Kabelknoten gelöst"
            height="300"
            class="max-height-300"
            loading="lazy"
        >}}
    </figure>
</div>

### Mathe und Musik

In der 8.
Klasse haben wir uns damit beschäftigt, wo und wie viel Mathematik in der Musik zu finden ist.
Dabei haben wir erfahren, dass Pythagoras mit seinem Monochord eine wichtigen Grundstein
zur Entstehung der heutigen Tonleiter gelegt hat.
Das Video, auf dem der Zirkel aufbaut, findest du [hier](https://www.youtube.com/watch?v=nK2jYk37Rlg).

<div class="centering">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/mathe-musik.png"
            alt="Ausschnitt für Violine 1 und 2 aus 'Heart of Courage'"
            width="1000"
            loading="lazy"
        >}}
        <figcaption>Ausschnitt aus 'Heart of Courage'</figcaption>
    </figure>
</div>

### Matboj

Auch dieses Jahr gab es wieder einen epischen Wettkampf! Wenn du dich schon einmal für den Matboj
auf dem Mathecamp nächstes Jahr vorbereiten möchtest,
kannst du dich genauer mit den [Regeln](https://mathezirkel.gitlab.io/content/matboj-regelwerk/Matboj_Regelwerk.pdf) auseinandersetzen.
Vielleicht findest du ja eine optimale Strategie!

### Quadratisches Reziprozitätsgesetz

Es geht um die Fragestellung, wann für \(a\) und \(n\) die Kongruenz

\[
    x^2 \equiv a \mod n
\]

eine Lösung \(x\) hat oder nicht.

Das quadratische Reziprozitätsgesetz liefert eine einfache Antwort darauf, obwohl die Aussage zunächst völlig absurd scheint.
Der von uns diskutierte Beweis mittels Abzählen von Gitterpunkten ist im fantastischen "[BUCH der Beweise](https://de.wikipedia.org/wiki/Das_Buch_der_Beweise)"
von Martin Aigner und Günter Ziegler zu finden.
Schaut da gerne mal rein!

Mit dem quadratischen Reziprozitätsgesetz lässt sich z.
B.
auch die [ISL 2022 N8](https://artofproblemsolving.com/community/c6h3107338p28104291)
leicht lösen.
Das ist nominell eine der härtesten Zahlentheorie-Aufgaben der IMO Shortlist.
Die IMO Shortlist ist ein Pool von Aufgaben, der international benutzt wird,
um Nationalmannschaften für die Internationale Mathematik Olympiade auszuwählen.
Achtung: Um die Aufgabe so lösen zu können, muss man die Definition der Legendre-Symbole \((\frac{a}{p})\)
auf Zahlen \(p\), welche nicht unbedingt Primzahlen sind, ausdehen!

## Fraktivitäten

### Apfelbaumbesichtigung

In der letzten Nacht unternahmen wir nach der Sternenwanderung noch einen weiteren kleinen Ausflug.
Vielleicht sollte es künftig eine verpflichtende Vorbereitungs-Frakti geben, wie man nachts durch Violau spaziert,
ohne das ganze Dorf aufzuwecken.

Unser Weg führte uns zum prächtigen Apfelbaum bei St. Michael, den wir ausgiebig bewunderten.
Anschließend setzten wir uns im wunderschönen Mondschein auf die sehr gemütliche Bank
vor der Fatima-Madonna – oder vielmehr dem Sockel, auf dem sie einst stand – und ließen die Woche in Ruhe Revue passieren.
Gerade als wir aufbrachen um weiter zu spazieren, wurden wir vom Regen überrascht.
Wir bissen also in den sauren Apfel und rannten schnell zurück ins Heim.

### Basteln

Im Werkraum gab es fast jeden oder zumindest jeden zweiten Tag die Möglichkeit, sich kreativ auszutoben.
Dieses Jahr hatten wir ganz viele Schuhkartons dabei.
Die Idee war nämlich, Casitas - passend zum Film Encanto - zu gestalten.
Aber natürlich waren auch jegliche andere Bastelprojekte willkommen, z. B. ein neues Zuhause für Gregor.
Auch die Malerei kam nicht zu kurz und so entstanden neben Bleistiftzeichnungen wunderschöne Kunstwerke
aus Acrylfarbe und natürlich Glitzer.

<div class="grid-gallery">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/skizze-bleistift.JPG"
            alt="Bleistiftskizze: 3 Köpfe mit Kapuze"
            width="500"
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/haus-gregor.JPG"
            alt="Bastelei: Haus für das Mathecamp-Maskottchen Gregor"
            width="500"
            loading="lazy"
        >}}
    </figure>
</div>

### Boomerang bergen

"Sachen werfen" war eine der nützlichsten Fraktivitäten im Camp.
Doch diesmal blieb sie leider nicht ohne Folgen: Ein Boomerang war nach der Aktivität unauffindbar.
Gemeinsam machten sich die Teilnehmenden in einer eigenen Fraktivität auf den Weg, um den verschollenen Boomerang
wieder zu finden.
Und es gelang – nach 30 Sekunden ...

### Bunter Abend

Traditionell sitzen am letzten Abend nochmal alle zusammen und lassen die Highlights der Woche auf sich wirken.
Der Abend wurde vom Orchester mit "Never gonna give you up" und "Jupiter" eröffnet.
Anschließend performte der Chor "We don't talk about Bruno" aus Encanto, bevor es mit einem Teaser zur Mathecamp-Sitcom
und einer neuen Folge des Mathecamp-Podcasts weiterging.
Wie immer wurde der Abend mit einem KIKA-Tanzalarm abgerundet.

### Diabolo

Obwohl das Wetter leider nicht mitgespielt hat, konnten wir auf Deck I gemeinsam Diabolo spielen.
Während manche von euch zum ersten Mal erfolgreich ein Diabolo gedreht haben, haben sich andere bereits
an forgeschrittenen Tricks versucht.

Wenn du zuhause weiterüben möchtest, findes du auf dem YouTube-Kanal *Spyros Bros* sehr gute Anleitungen, z. B. [dieses Video](https://www.youtube.com/watch?v=KBqa5QA461Y).

Falls du dir ein eigenes Diabolo zulegen möchtest sind hier die Links zu den Diabolos, die Taulex dabei hatte:

* groß: [Diabolo Circus](https://www.henrys-online.de/en/shop/juggling/diabolos/15/diabolo-circus)
* mittel: [Diabolo Jazz](https://www.henrys-online.de/en/shop/juggling/diabolos/109/diabolo-jazz)
* klein: [Diabolo Kolibri](https://www.henrys-online.de/en/shop/juggling/diabolos/44/diabolo-kolibri?number=A15806)

Welche Stäbe man benutzen möchte ist Präferenzsache:

* [Carbon-Stäbe](https://www.henrys-online.de/en/shop/juggling/diabolo-parts/80/diabolo-handsticks-carbon-350)
* [Aluminium-Stäbe](https://www.henrys-online.de/en/shop/juggling/diabolo-parts/33/diabolo-handsticks-alu-325)
* Holz-Stäbe (billig, aber für spätere Tricks nicht ausreichend)

Tipps:

* Die Größe des Diabolos sollte zur Größe und Kraft der antreibenden Person passen.
* Bei Diabolos mit Kugellager werden einige Tricks einfacher, dafür sind andere Tricks, wie bspw.
*Aufzug*, nicht mehr möglich.
* Sollte die Schnur kaputt gehen, kann diese leicht mit Hilfe der [10m-Rolle](https://www.henrys-online.de/en/shop/juggling/diabolo-parts/129/diabolostring-10m-roll)
ausgetauscht werden.

### Fraktivitäten-Plenum

Ohrwurm noch nicht vorbei? Das waren die diesjährigen Lieder, die uns um Punkt 12:58 eingestimmt haben:

* [2 Unlimited - Get Ready For This](https://www.youtube.com/watch?v=iPOmFUid3vA) (Taulex)
* [Among Us Drip Theme Song](https://www.youtube.com/watch?v=grd-K33tOSM) (Lukas)
* [Chug Jug With You (Fortnite American Boy Parody)](https://www.youtube.com/watch?v=PPfi7o2Pa64) (Mai)
* [Die Gummibärenbande - Intro](https://www.youtube.com/watch?v=YO1GBsuzTWU) (Jotrocken)
* [Gloria Estefan - Conga](https://www.youtube.com/watch?v=8gXj8Gcp31w) (Leonie)
* [Aqua - Barbie Girl](https://www.youtube.com/watch?v=Y7FrLagBXng) (Sven)

### Freundschaftsarmbänder

Auf diesem Mathecamp wurden wieder viele neue Freundschaften geschlossen und alte besiegelt.
Deswegen haben wir uns ein Mal in der Bücherstube getroffen, um uns gegenseitig zu erklären und zu erinnern,
wie man verschiedene Freundschaftsarmbänder knüpft.
Hinterher sah man eigentlich die ganze Zeit irgendjemanden irgendwo ein Band knüpfen.
Es ist eine physische Manifestation von etwas Unsichtbarem und es hat uns riesige Freude gemacht eines zu bekommen oder
die Freude des Gegenübers beim Empfang und Tragen eines Bändchens zu beobachten.

### Gute-Nacht-Geschichten

Wie jedes Jahr passend direkt jeweils vor der Bettruhe, aber natürlich geeignet für alle,
die um die Uhrzeit noch wach sein dürfen.

#### Für die Kleinen (5-6, bis vor 21:30 Uhr)

Gelesen haben wir das Buch [*Frankie*](https://www.penguin.de/buecher/jochen-gutsch-frankie/buch/9783328601838)
über einen verträumten Kater, der sich eigenwillig an einen Menschen bindet, der scheinbar alles verloren hat.

In dieser fiktiven Geschichte können die Tiere teilweise mit anderen Tieren -- inklusive Menschen -- sprechen,
wodurch wir beim Lesen auch erfahren, dass Füchse gute Trauerredner sind, Schafe immer alles mit ihrer Herde abklären müssen,
und man sich bei Elstern lieber die Ohren zuhält, bevor sie einen in Grund und Boden beschimpfen.
Außerdem erzählt uns Frankie von seiner Vergangenheit, seinem Liebeskummer und seinen Träumen.
Das alles, während er mehr über seinen Menschen *Gold* und die Welt fernab des Müllbergs lernt.

#### Für die weniger Kleinen (7-9, bis vor 22:00 Uhr)

Zuerst haben wir das Buch [*Celeste: oder Die Welte der gläsernen Türme*](https://www.amazon.de/Céleste-Timothée-Fombelle/dp/3836952912)
gelesen.
In dieser Science-Fiction-Geschichte geht es Celeste -- ein Mädchen, welches ein behütetes Leben in der Eliteschicht
des fiktiven Asiens führt.
Im Laufe der Geschichte wird sie auf die ärmlichen Zustände der Unterschicht aufmerksam und beginnt,
die ungerechten gesellschaftlichen Strukturen zu hinterfragen und sich gegen das System aufzulehnen.
Es gibt allerdings ein Problem.
Sie hat eine Krankheit - eine, die sie auf Leben und Tod mit den Vorgängen unserer Erde verbindet.

Außerdem haben wir das Buch [*Die wundersamen Koffer des Monsieur Perle*](https://www.amazon.de/Die-wundersamen-Koffer-Monsieur-Perle/dp/3836958791)
angefangen.
Der Roman handelt vom Feenprinz Iliân, der in die unsrige Welt verbannt wird und sich im Paris der 1930er-Jahre wiederfindet.
Um in seine Welt zurück zu gelangen, muss er Beweise für deren Existenz finden.
Auf seiner Suche, die bis in die Gegenwart andauert, wird deutlich, dass es sich dabei nicht nur um physische Objekte,
sondern auch um Erinnerungen und Hoffnungen handeln kann.

### Jugendsprache

Wir Betreuenden sind leider nicht mehr ganz auf der Höhe, was die Jugendsprache angeht.
Doch dankenswerterweise haben uns die Teilnehmenden in einer Fraktivität über die neuesten Wörter informiert.
Das war voll Schere! +100 Aura für die Vortragenden!

### Kennenlernspiel am 1. Tag

Dank eurer schnell organisierten Petition und Davids Einfallsreichtum im Zug nach Hause,
haben wir dieses Jahr nicht das Gelände nach Hinweisen und Stationen abgesucht,
sondern eine schnelle Minispiel-Sammlung durchgespielt.

#### 1, 2 oder 3 oder 4 oder 5 oder n? Oder ist das nicht eigentlich kontinuierich?

Wir haben euch Fragen gestellt, zu denen ihr euch von niedrig zu hoch anordnen solltet.
Und das waren die Fragen, soweit wir uns daran erinnern können:

* Zum wie vielten Mal im Mathecamp?
* Aufstehzeit (früh bis spät)
* Bevorzugte Höhenmenter im Urlaub (Strand bis Berge)
* Luxusurlaub am liebsten in der Natur (7 vs.
Wild) oder hochzivilisiert und all inclusive?
* Für so viel Geld würde ich jetzt sofort eine ganze Zitrone am Stück essen.
* Bildschirmzeit

#### Drachenspiel

Um die Drachensuche in Violau zu unterstützen, sollten wir Fahndungsfotos mit eurer Hilfe nachstellen.
Dafür hatten wir von den betroffenen Violauern Beschreibungen der Drachen bekommen.
X Füße und Y Hände mussten dabei den Boden berühren.

* 112 Köpfe, 224 Beine, 56 Hände
* 1 Kopf, 2 Füße, 2 Hände
* 5 Köpfe, 5 Füße
* 3 Köpfe, 2 Füße, 6 Hände
* 7 Köpfe, 6 Füße, 4 Hände

#### Schere-Stein-Papier: Fan-Edition

Alle starteten ohne Fans und suchten nach Leuten, gegen die sie ein Duell Schere-Stein-Papier spielen konnten.
Verliert man ein Duell, wird man selbst und alle eigenen Fans zu den Fans der anderen Person.
Zuerst haben wir euch gebeten, als Fans so laut wie möglich anzufeuern.
Und in der zweiten Runde solltet ihr nur noch flüsternd anfeuern.

### Kjald-Hängegurke

Die Kjald-Hängegurke ist eine weitestgehend unerforschte Spezies, die auf dem Wandertag von Kathas Gruppe gesichtet wurde.
Sofort fand sich eine Gruppe ambitionierter Forscher:innen,
die umfassende Untersuchungen über diese neuartige Spezies anstellten und
diese auch mit uns in einem informativen Vortrag teilten.

<div class="centering">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/haengegurke.JPG"
            alt="Skizze der Kjald-Hängegurke"
            width="300"
            loading="lazy"
        >}}
    </figure>
</div>

### Lagerfeuer

Am Mittwochabend nach dem Wandertag saßen wir gemütlich beim Lagerfeuer zusammen.
Für das richtige Ambiente wurden auch Lagerfeuer-Lieder gesungen.
Diese wurden zunächst von David auf der Bariton-Ukulele und einem Teilnehmer auf der Gitarre begleitet.
Anschließend haben Katha und eine andere Teilnehmerin die Begleitung professionell übernommen.
Sie spielten Lieder an, welche sie fleißig in der Lagerfeuer-Gitarren-Frakti geübt hatten.

### Lightning Talks

Natürlich gab es auch wieder Lightning Talks.
Bei Lightning Talks können alle Teilnehmenden etwas vortragen -- ob spontan oder vorbereitet,
der Fantasie sind keine Grenzen gesetzt!
Es gibt nur ein Limit: Die Vorträge dürfen nicht länger als 2 Minuten dauern.
Heraus kam ein buntes Programm an Themen, von Aeneas bis Zwiebeln oder unangenehmer Stille war alles dabei!

### Mathecamp-Sitcom

Die erste Folge der Mathecamp-Sitcom war eine große Belastungsprobe für unsere Lachmuskeln!
Die Sitcom dreht sich um drei (vielleicht etwas spezielle) Teilnehmende, die den ganz normalen Wahnsinn des Mathecamps erleben,
und drei fabelhafte Betreuende, die den ganzen Wahnsinn ertragen müssen.
Fortsetzung folgt!!!

<div class="grid-gallery">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/sitcom-1.png"
            alt="Bild aus der Sitcom: Tanja als Louisa"
            width="350"
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/sitcom-2.png"
            alt="Bild aus der Sitcom: Marc als Marx"
            width="350"
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/sitcom-3.png"
            alt="Bild aus der Sitcom: Mai als Felixa"
            width="350"
            loading="lazy"
        >}}
    </figure>
</div>

### Nachtwanderung

Tanja’s böse KI wollte uns alle vernichten! Doch die Teilnehmenden des Mathecamps konnten es rechtzeitig verhindern,
indem sie nachts in Kleingruppen verschiedensten Aufgaben gelöst haben.
Obwohl eine abtrünnige Gruppe mit der KI zusammenarbeiten wollte,
ist es am Ende gelungen die KI zu löschen und den schädlichen Upload zu verhindern.
Wie immer hat das Gute letztendlich gesiegt!

### Omerta

Omerta ist eines der beliebtesten Karten-Spiele im Camp.
Die [Regeln](https://www.brettspielabend.net/2020/01/12/omerta/) sind einfach erklärt -- und doch ist es knifflig!
Welcher Gangster erstellt das beste Schmuggel-Imperium?

### OpenStreetMap

Auf dem Mathecamp haben uns die [StreetComplete](https://streetcomplete.app/) Quests zu Orten und Fragen geführt,
an denen wir sonst wohl nicht vorbeigekommen wären.
Wir haben den Zustand und den Untergrund von Wegen klassifiziert, den Unterschied zwischen Bildstöcken, Kapellen und Wegkreuzen
kennengelernt und die Karte um die Hausnummer des Hauses von Christophs Tochter erweitert.

<div class="centering">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/osm.png"
            alt="OSM-Karte von Violau"
            width="1000"
            loading="lazy"
        >}}
    </figure>
</div>

Probiert es doch gerne mal bei euch zu Hause aus! Ladet euch dazu einfach die App herunter:

* [Google Play Store](https://play.google.com/store/apps/details?id=de.westnordost.streetcomplete)
* [F-Droid](https://f-droid.org/packages/de.westnordost.streetcomplete/)
* [iOS](https://apps.apple.com/de/app/go-map/id592990211)

Wenn man komplizierte Bearbeitungen vornehmen will oder gerne neue Strukturen eintragen möchte, bietet es sich an,
einen [Editor aus dem OpenStreetMap Wiki](https://wiki.openstreetmap.org/wiki/Editors) auszusuchen,
die gegenüber StreetComplete deutlich mächtigere (und auch kompliziertere) Bearbeitungsmöglichkeiten zulassen.

Unsere Änderungen (und alle anderen Eintragungen) kann man auf der offiziellen Website von [OpenStreetMap](https://www.openstreetmap.org/)
einsehen.
Da die Kartendaten mit einer freien Lizenz versehen sind, finden die Kartendaten bei vielen anderen Projekten Verwendung,
so z. B. bei der App [OrganicMaps](https://organicmaps.app/),
die auf allen gängigen mobilen Betriebssystemen zur Navigation verwendet werden kann.

### Programmieren

#### AlphaGeometry

Googles Forschungsabteiltung DeepMind hat eine KI entwickelt, mit der man Geometrie-Aufgaben lösen kann.
Wir haben die Architektur dieses Programms und eine Einordnung dieser neuen technischen Entwicklung diskutiert.

Schaut euch gerne das zugehörige [Nature Paper](https://www.nature.com/articles/s41586-023-06747-5)
oder besser noch den [Code](https://github.com/google-deepmind/alphageometry) an.
Wenn ihr Hilfe beim Einrichten braucht, meldet euch gerne bei Meru.

Unter diesen beiden Links findet ihr Informationen darüber, wie Computer aktuell in der Mathematik eingesetzt werden können:

* [Liquid Tensor Experiment](https://xenaproject.wordpress.com/2020/12/05/liquid-tensor-experiment/)
    -- eine Challenge von Peter Scholze an die Lean Community
* [Machine Assisted Proof](https://www.youtube.com/watch?v=AayZuuDDKP0)
    -- ein Youtube-Vortrag von Terence Tao

#### GIMP

Kein Mathecamp ohne GIMP!
Natürlich haben wir auch dieses Mal die coolen Fotos, die von den Betreuern in ihren besten Momenten geschossen wurden,
genutzt und mit GIMP neu arrangiert.
Dabei sind sehr coole Collagen entstanden, die zeigen, wie cool das Mathecamp ist.

<div class="centering">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/gimp.png"
            alt="Collage, die im GIMP Workshop entstanden ist"
            width="600"
            loading="lazy"
        >}}
    </figure>
</div>

#### Lean

Mit Lean haben wir eine Programmiersprache kennengelernt, mit der man mathematische Beweise so aufschreiben kann,
dass ein Computer sie verstehen und überprüfen kann.
Das [Natural Number Game](https://adam.math.hhu.de/) bietet einen spielerischen Einstieg in diese Sprache.
Weiterführende Informationen zu Lean findest du auf [lean-lang.org](https://lean-lang.org/).
Dort gibt es auch die Möglichkeit [Lean-Code direkt im Browser zu schreiben](https://live.lean-lang.org/).

#### LaTeX und TikZ

Möchtest du hingegen mathematische Texte (bzw. allgemein wissenschaftliche (oder unwissenschaftliche :-) Texte)
für andere Menschen aufschreiben, dann bietet sich dafür die Sprache LaTeX an.
Einen knappen Überblick dessen, was wir im LaTeX-Kurs gemacht haben, findest du in
[dieser Präsentation](https://students.fim.uni-passau.de/~grafl/MS/LaTeX.html) (weiter mit Pfeiltasten) --
ausführlichere Anleitungen zu LaTeX gibt es überall im Internet.
Und weil uns LaTeX alleine zu langweilig war, haben wir uns auch noch ein bisschen mit Ti*k*Z beschäftigt
-- dem "Zeichenprogramm" von LaTeX (Ti*k*Z steht übrigens für "Ti*k*Z ist *kein* Zeichenprogramm"
und hat eine sehr übersichtliche [Anleitung mit gut 1000 Seiten](https://tikz.dev).

#### Programmierwettbewerb

Auch dieses Jahr gab es wieder einen Programmierwettbewerb, bei dem wir herausgefunden haben,
wer den besten [Seekers](https://gitlab.com/mathezirkel/content/seekers)-Bot schreiben kann.
An mehreren Terminen und mit einem packendem Finale haben wir viel über Python und Bewegungen im 2-dimensionalen Raum gelernt.

#### QR-Codes

Bei einer Programmier-Session haben wir uns angeschaut, wie ein Computer einen QR-Code auf einem Foto erkennen kann.
Insbesondere, wenn dieser durch die Fotoperspektive verzerrt oder verschoben ist.
[Hier](https://jonas-kell.github.io/qr-code-decoder/#/) findest du eine Erklärung am Live-Bild.
Außerdem kann die App QR-Codes generieren und Live-Einblicke in die verschiedenen Schritten des Verarbeitens geben.
Den zugehörigen Programm-Code mit Erklärungen findest du  [hier](https://github.com/jonas-kell/qr-code-decoder).

### Rope Skipping

Im Rahmen dieser Fraktivität haben wir verschiedene Elemente des Rope Skippings -- oder einfacher: Seilspringen -- ausprobiert.
Das Springen alleine im Single Rope kann man auch zuhause ganz einfach nachmachen.
Ihr braucht nur ein Seil, am besten mit Griffen.
Wenn ihr euch auf das Seil mit beiden Füßen draufstellt,
sollten die Seilenden bei Anfängern auf beiden Seiten bis unter die Arme gehen unddann kann man schon direkt loslegen,
z. B. mit Fußsprüngen oder einem CrissCross (dabei überkreuzt man die Arme und springt über das Seil).
Wer Inspiration sucht, findet ganz viel im Internet,
und zwar auch für Fortgeschrittene :-)
Vielen hat außerdem das Long Rope oder Double Dutch Spaß bereitet.
Dabei werden ein bzw.
zwei lange Seile von zwei Personen geschwungen und man kann dann entweder durch das Seil durchlaufen oder
darin verschiedene Tricks machen.

### Rubics Cube Solving Roboter

In mehreren Sessions mit unterschiedlichsten Disziplinen (Theorie, Konstruktion, Networking, Programmieren, Hardware-Kalibrierung)
haben wir versucht auf diesem Camp einen Roboter zu konstruieren, der ohne Hilfe (wie das Kennen der Verdreh-Schritte oder
dem aktuellen Stand des Würfels) einen Zauberwürfel in den gelösten Zustand überführen kann.

Wegen hardware-technischen Limitationen sind wir leider auf dem Camp nicht fertig geworden.
Um die Präzisionsprobleme lösen zu können, musste noch ein weiterer Kontroll-Stein gekauft werden.

Inzwischen ist der Roboter fertig und voll funktionsfähig.
Eine komplette Dokumentation des Projektes, inklusive Bildern und Videos, findet ihr [hier](https://github.com/jonas-kell/mz-cube-robot).

<div class="grid-gallery">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/cube-solver-1.png"
            alt="Darstellung der Farberkennung des Cube-Solver-Roboters"
            class="max-height-300"
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/cube-solver-2.jpg"
            alt="Foto des Cube-Solver-Roboters"
            class="max-height-300"
            loading="lazy"
        >}}
    </figure>
</div>

### Spikeball

Dieses [YouTube-Video](https://www.youtube.com/watch?v=T3vLN-KHUlQ&t=0s) erklärt nochmal ausführlich die Regeln.
Auf dem Mathecamp verwenden wir immer das originale [*SPIKEBALL-Pro-Set*](https://www.roundnet-deutschland.de/shop/spikeball-pro-set.htm),
weil es sehr robust ist.
Ein [Highlight-Video](https://www.youtube.com/watch?v=mo4qTH-VYGM) zum Nacheifern (auf Englisch) findest du auf YouTube.
Zudem findet man viele weitere Videos mit Tipps und Tricks auf YouTube - einfach mal danach suchen!

### Tanzkurs

In diesem Jahr lag der Fokus auf Rumba, Disco Fox, Cha Cha Cha, Jive, Walzer, Salsa und Wiener Walzer.

Neben den verschiedenen Grundhaltungen wurden auch besondere Schritte, Figuren und Drehungen (Damen-Solo) geübt.

Die Lieder, die wir verwendet haben, findet ihr auf dieser [Spotify-Playlist](https://open.spotify.com/playlist/0rvek087ACcx74eNTLdQa0)
(bis Lied 28) und auf dieser [YouTube-Playlist](https://www.youtube.com/playlist?list=PLwsCJhMSOdZxXohetDcWp760U-MGzOdaU).

Wenn ihr zuhause weiter üben wollt, findet ihr unter den Interpreten
[Vio Friedmann (Ballroom Music)](https://open.spotify.com/artist/7DvEnV07OjEknuprtV0run/discography/all)
und [Tanz Orchester Klaus Hallen](https://open.spotify.com/artist/2DFJXZTqpDBQmtP7MmpjpA/discography/all)
viele gute Lieder zum Tanzen, die oft auch einen Tipp zum Tanzstil im Titel enthalten.
Bis auf (Wiener) Walzer und Tango lässt sich eigentlich jeder Tanzstil auch direkt im Wohnzimmer üben.

Bekannte Tanzstile, die wir gar nicht gelernt haben:

* Tango (für Anfänger geeignet)
* Quickstep (für Anfänger geeignet)
* Foxtrott
* Slowfox
* Samba
* Paso Doble
* Bachata

### Sternenwanderung

Auch dieses Jahr haben wir an einem Abend nach Einbruch der Dämmerung den Nachthimmel über Violau betrachtet.
Diesmal konnte man den Saturn erkennen und sogar die seine Ringe erahnen!

### Tribunal

Zum ersten Mal in der Geschichte des Mathecamps gab es ein Gerichtsverfahren.
Es sollte entschieden werden, ~~dass~~ ob Betreuer Jonas K für seine ~~für einen Betreuer unrühmlichen~~ umstrittenen Taten
zu Tischdienst verdonnert werden soll.
Da es sich hierbei um ~~einen Schauprozess mit vorab festgelegtem Urteil~~ einen fairen Prozess mit zu ermittelndem Urteil
handelte, wurde weitestgehend die Struktur einer realen *Hauptverhandlung* nachgestellt,
wie sie zum Beispiel [hier nachgelesen werden kann](https://ht-strafrecht.de/strafverfahren/ablauf-strafverfahren/hauptverhandlung/).
Dabei haben wir freizügig eigentlich wichtige Formalitäten aus Zeitgründen weggelassen.
Wie ihr alle wisst, wurde *J\*\*\*\* K\*\*\** (Name der Redaktion bekannt) zu einem Tischdienst verurteilt und
musste für den Rest des Mathecamps den Namen *"Jotrocken"* tragen, um den Wasserschaden wiedergutzumachen.
Nach erfolglosen Versuchen, in Revision zu gehen, wurde das Urteil rechtskräftig.

### Vokabeln meistern mit *Anki*

Anki ist ein vollautomatisierter Karteikarten-Kasten, für den man selbst die Karten erstellen muss.
Im Internet finden sich viele hilfreiche Anleitungen, meistens aber auf Englisch.
Bei Fragen gerne auch an [Taulex](mailto:alex.mai@posteo.net) wenden.

[Hier](https://ankiweb.net/shared/decks) findest du die offizielle Seite, auf der viele Leute ihre Anki-Deck-Dateien teilen
und von anderen Usern bewerten lassen können.

#### Download

* [PC](https://apps.ankiweb.net/) -- kostenlos
* [Android](https://play.google.com/store/apps/details?id=com.ichi2.anki&hl=en&pli=1) -- kostenlos
* [iOS](https://apps.apple.com/de/app/ankimobile-flashcards/id373493387) -- kostenpflichtig,
aber damit wird die Entwicklung finanziert

### Völkerball / Dodgeball

Schonmal Völkerball nach dem offiziellen Dodgeball-Regelwerk gespielt? Im Sportunterricht jedenfalls nicht!
Doch Sven hat sich ein Herz gefasst, um mit uns diese Sportart einzuüben -- sie könnte übrigens bald olympisch werden!

### Wandertag

Ihr habt euch beim Wandertag verlaufen oder ewig nach einer Station gesucht? Ihr dachtet,
dass unsere neue App nur eine coolere Version von Geocaching ist.
Falsch gedacht: Wir können auch alle Wandertag-Routen Revue passieren lassen und denkwürdige Foto-Momente anschauen.

<div class="centering">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2024/wandertag.jpg"
            alt="Foto vom Wandertag"
            width="1000"
            loading="lazy"
        >}}
    </figure>
</div>

[Hier](https://wandertag-app.mathezirkel-augsburg.de/#/recordings/mc-24) könnt ihr die verschiedenen Routen anschauen.
Das Passwort ist dasselbe wie für die Fotogalerie.
Dieses habt ihr per E-Mail erhalten.
Da die App Fotos anzeigt, bitten wir euch das Passwort nicht weiterzugeben.

Ihr müsst zuerste die Route auswählen, die ihr sehen wollt.
Anschließend könnt ihr mit dem Slider die gewünschte Tageszeit einstellen und so die Route nachverfolgen.
Bei manchen Routen sind leider die Positionsdaten sehr verrauscht und daher springt die Linie stark hin und her.
Wenn man den Slider langsam vorschiebt, kann man aber trotzdem relativ gut das Mittel des Rauschens als Strecke erkennen.

Wenn ihr einen Haken bei "Routen anzeigen" setzt, könnt ihr sehen, wie die Route eigentlich geplant war.

An manchen Stellen tauchen kleine Foto-Symbole auf.
Wenn ihr diese anklickt, könnt ihr die Fotos, die mit der App im Rahmen der Foto-Aufgaben entstanden sind, anschauen.

### Werwolf-Alternative des Tages

Werwolf ist ein interaktives Gesellschaftspiel.
Doch bei weitem nicht das Einzige! Jeden Tag hat Jonas(s) eine Alternative mit ähnlicher Spielmechanik angeboten.
Viele davon sind mindestens genauso spannend und verzwickt wie das Original!

Angeboten wurden:

* Werwolf Vollmondnacht / Eine-Nacht-Werwolf
* Quanten-Werwolf
* Omerta
* Bang! the Bullet
* Coup
* Secret Hitler

### Zauberwürfel

Die neue zweiseitige Anleitung zum Lösen eines 3x3 Zauberwürfels findest du auf [unserer Webseite](https://mathezirkel-augsburg.de/materialien/)
([Direkter Downloadlink](https://mathezirkel.gitlab.io/content/zauberwuerfel/zauberwuerfel.pdf)).
Die Anleitung basiert auf der besonders anfängerfreundlichen Methode aus dem Video [GameOne: Cube-Tutorial mit Ian](https://www.youtube.com/watch?v=ceGeDZ23Wn4).

## Abschluss und Mathecamp 2025

Schön, dass du bis hierher durchgehalten hast!
Vermutlich hätten wir mit den zusätzlichen Materialien auch noch ein Camp füllen können :-)

<div class="centering">
    <figure>
        {{< responsive-image
            src="gregor-mc-24.jpg"
            alt="Gregor Design 2024"
            width="500"
            loading="lazy"
        >}}
    </figure>
</div>

Apropos, das Mathecamp 2025 findet vom 23. bis 31. August 2025 statt.
Wir freuen uns schon auf dich!
