---
title: Mathecamp 2020 - Klasse 5 bis 8
---

## Rückblick

<div class="highlight-box">
14. bis 18. August 2020

62 Teilnehmer\*innen

10 Betreuer\*innen

5 wunderschöne Tage
</div>

## Materialien zu manchen Zirkeln und Fraktivitäten

### Abstand zweier Punkte

Wolltet ihr schon immer mal wissen, wie weit zwei Punkte im dreidimensionalen Raum entfernt sind? Dann schaut euch doch mal
[dieses PDF](https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2020/klein/abstand-zweier-punkte-im-dreidimensionalen-raum.pdf)
an.
Viel Spaß beim Lösen!

### Alienzirkel (Cosmic Call)

Im Jahr 1999 sendete die Menschheit eine Radiobotschaft an ausgewählte Sterne, in der Hoffnung,
dass eine dort lebende Zivilisation die Nachrichtempfangen, verstehen und auf sie antworten würde: die Cosmic-Call-Botschaft.
Diese Nachricht wurde natürlich nicht auf Deutsch oder Englisch formuliert, sondern bedient sich einer mathematischen Symbolsprache.
Im Zirkel unterzogen wir die Botschaft einem Test: Können wenigstens wir Menschen, die wir einen riesigen Heimvorteil haben,
die Nachricht entziffern?

Ihr könnt die Nachrichten und Erklärungen unter [Materialien](/materialien/) nochmal ansehen.

### Beweise ohne Worte

Beweise spielen in der Mathematik eine ganz zentrale Rolle und füllen auch häufig ein ganzes Buch.
Dass dies aber nicht immer so sein muss, sieht man an den Beispielen aus unserer Sammlung an Beweisen ohne Worte.
Hier geht es darum, anhand eines Bildes zu verstehen, wieso eine Aussage gilt.
Beispielsweise möchte man mit Hilfe einer Skizze herausfinden,
ob der Umfang eines Kreises genauso viele Punkte hat wie eine unendlich lange Gerade.

Wenn ihr daran gerne weiter tüfteln wollt, findet ihr unter [Materialien](/materialien/) die Aufgaben.

### Conway's Armee in der 7. Klasse

Wir haben uns angeschaut, wie weit wir mit Conways Armee nach oben kommen können.
Dabei haben wir schnell gemerkt, dass wir nicht sonderlich weit kommen.
Den Beweis haben wir nicht gemacht.
Allerdings kann dieser (mit etwas Arbeit und Nachschauen vieler weiterer Dinge) auch von euch verstanden werden.
Ein tolles Video zum Beweis findet ihr im Numberphile-Youtube-Kanal [hier](https://www.youtube.com/watch?v=Or0uWM9bT5w).

Generell ist der Numberphile-Kanal ein toller Mathematik-Kanal!

### Jonglieren

Falls du jonglieren lernen möchtest oder schon immer mal deine eigenen Jonglierbälle basteln wolltest,
findest du [hier](https://utopia.de/ratgeber/jonglieren-lernen-eine-anleitung-fuer-anfaenger/) Tipps.
Eine Video-Anleitung für Anfänger gibt es auch [hier](https://www.youtube.com/watch?v=e6GbXfkXbAA) auf Youtube.

Wenn du natürlich schon jonglieren kannst, dein Repertoire aber noch um weitere Tricks ergänzen willst,
kannst du dich gerne [hier](https://spielelux.de/jongliertricks-fortgeschrittene/) inspirieren lassen.

### Kegeln

Der exzessive Kegelkonsum trug dazu bei, dass wir diskutieren,
das Mathecamp im nächsten Jahr durch ein Kegelcamp zu ersetzen. :-D
Die Verhandlungen mit der deutschen Kegelvereinigung laufen bereits.
Damit ihr schon mal für nächstes Jahr trainieren könnt, findet ihr unter [http://www.kegelspiele.info](http://www.kegelspiele.info)
einige Kegelspiele.

### Kette im Eimer

In einem Nachmittagszirkel haben wir einen wundersamen Effekt bei einer geradezu herausexplodierenden Kette
aus einem Behälter beobachtet und zusammen erklärt.
In einem Lightning Talk wurde der Effekt gezeigt und nur ganz knapp erklärt.
Wer sich die ganze Erklärung gönnen möchte, kann das mit der wissenschaftlichen Arbeit dazu oder mit diesen Videos machen:

* Paper von der Royal Society aus England: [https://royalsocietypublishing.org/doi/full/10.1098/rspa.2013.0689](https://royalsocietypublishing.org/doi/full/10.1098/rspa.2013.0689)
* Video von der Royal Society aus England: [https://www.youtube.com/watch?v=-eEi7fO0\_O0](https://www.youtube.com/watch?v=-eEi7fO0_O0)
* Ursprung des Effekts hier: [https://www.youtube.com/watch?v=\_dQJBBklpQQ](https://www.youtube.com/watch?v=_dQJBBklpQQ)
* Vortrag zum Paper von Steve Mould hier: [https://www.youtube.com/watch?v=wmFi1xhz9OQ](https://www.youtube.com/watch?v=wmFi1xhz9OQ)

### Pi-Kärtchen

Zum selbst ausdrucken:

* [Pi](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/pi-lernen.pdf)
* [Tau](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/tau-lernen.pdf)
* [Euler'sche Zahl](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/e-lernen.pdf)
* [Zeta](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/zeta-lernen.pdf)

### Schnitzeljagd für das große Camp

Drei Teilnehmerinnen haben eine wunderschöne Schnitzeljagd für das [große Camp](../gross) organisiert!
Dabei galt es eine Caesarverschlüsselung zu knacken,
Annas Lieblingsformel (Übrigens das [Euler-Produkt](https://de.wikipedia.org/wiki/Euler-Produkt)) rauszufinden und
einen Text in Braille zu übersetzen.

### Spikeball

[Spikeball](https://www.roundnet-deutschland.de/) ist mittlerweile eine Trendsportart
und wird auch auf dem Mathecamp viel gezockt.
Nächstes Jahr werden wir übrigens [so spielen](https://www.youtube.com/watch?v=tRGM_BJoBH8).

### Tabata

Ein paar von euch haben sich gemeinsam mit uns beim Tabata gequält.
Die Songs findet ihr [hier](https://open.spotify.com/playlist/7qYN8bqDZELU4ab02MIYYN?si=I9VMAcGzTF27adfEFksAbg).

### Video- und Bildbearbeitung

Wir haben die freie Software kdenlive zum Videos schneiden, GIMP zum Bilder (Rastergrafiken) bearbeiten
und Inkscape für Vektorgrafiken verwendet.
Die drei Programme gibt es für alle Linux/Ubuntu Installationen kostenlos.
GIMP und Inkscape sind auch für Windows verfügbar.

### Wettersonden

Der Deutsche Wetterdienst und die Bundeswehr starten regelmäßig Wettersonden an Heliumballons.
In meinem Vortrag berichte ich darüber wie man diese Sonden 'jagen' und finden kann, und was man dazu alles braucht.
Ich habe außerdem grob erzählt, was eine Wettersonde auf dem Weg bis fast in den Weltraum alles so macht.

Unter [wetterson.de](https://wetterson.de) könnt ihr Sonden tracken.
Dort gibt es eine Live-Karte mit allen aktiven Sonden.
Einen Fund könnt ihr unter [radiosondy.info](https://radiosondy.info) eintragen.

### Zauberwürfel

Die Anleitung zum Lösen des Zauberwürfels findet ihr auf [Tims Webseite](https://timbaumann.info/zauberwuerfel).

## Mathecamp 2021

Das Mathecamp 2021 findet vom 14.08.2021 bis 22.08.2021 statt.
Das ist eine Woche vor der üblichen Mathecampwoche in den bayrischen Sommerferien! Wir freuen uns schon auf dich!
