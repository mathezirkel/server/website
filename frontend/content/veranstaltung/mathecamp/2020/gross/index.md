---
title: Mathecamp 2020 - Klasse 9 bis 12
---

## Rückblick

<div class="highlight-box">
19. bis 23. August 2020

73 Teilnehmer:innen

11 Betreuer:innen

5 wunderschöne Tage in Violau!!
</div>

Il existe même maintenant une [version française](index_fr.html) de ce site!

## Das waren eure Mathecamp-Highlights

Werwolf!
Klavier spielen (und dabei zuhören)!
Lightning Talks!
Wahlen-Vortrag!
Powerpoint-Karaoke (insbesondere Sven über seine Heimat Serbien)!
Wie man mit ZFC eine Hydra tötet!
Kaffee!
Alte Bekannte treffen!
Viele große Gemeinschaftsräume!
Starke Gruppengemeinschaft!
Stable Marriage Problem!
In Gruppen schwierige Aufgaben lösen!
Gutes Essen (insbesondere Salat)!
Orchester (auch zum Zuhören)!
Gemeinsam Musik hören!
Chinesische Märchen!
Ultimate Frisbee!
Schachlaufen!
Schlangenforscherlied singen!
Rätsel des Tages (schon nach 4 -- 5 Stunden gelöst)!
Tanzkurs!
Wanderung!
Kegeln!
Programmieren!
Abschätzungen (Fermi-Probleme)!
Nachtwanderung!

## Materialien zu einigen der Zirkel und Fraktivitäten

### Alienzirkel (Cosmic Call)

Im Jahr 1999 sendete die Menschheit eine Radiobotschaft an ausgewählte Sterne, in der Hoffnung,
dass eine dort lebende Zivilisation die Nachricht empfangen, verstehen und auf sie antworten würde: die Cosmic-Call-Botschaft.
Diese Nachricht wurde natürlich nicht auf Deutsch oder Englisch formuliert, sondern bedient sich einer mathematischen Symbolsprache.
Im Zirkel unterzogen wir die Botschaft einem Test: Können wenigstens wir Menschen, die wir einen riesigen Heimvorteil haben,
die Nachricht entziffern? Wenn du gerne weiter knobeln möchtest,
findest du unter [Materialien](/materialien) die Botschaften.

### Beweise ohne Worte

Beweise spielen in der Mathematik eine ganz zentrale Rolle und füllen auch häufig ein ganzes Buch.
Dass dies aber nicht immer so sein muss, sieht man an den Beispielen aus unserer Sammlung an Beweisen ohne Worte.
Hier geht es darum, anhand eines Bildes zu verstehen, wieso eine Aussage gilt.
Beispielsweise möchte man mit Hilfe einer Skizze herausfinden,
ob der Umfang eines Kreises genauso viele Punkte hat wie eine unendlich lange Gerade.
Wenn du daran gerne (weiter) tüfteln willst, findest du unter [Materialien](/materialien) die Aufgaben.

### Ecken, Kanten und Polyeder

Dieser Zirkel von Kai beruht auf dem wunderbaren Vortrag von Ingo und Matthias zur vierdimensionalen Geometrie.
Wenn du den Vortrag noch nicht gehört hast,
kannst du dir gerne die [Folien](https://www.speicherleck.de/iblech/stuff/slides-weihnachtsvorlesung.pdf) dazu anschauen.

### Epidemiologie

Wenn du dich noch weiter mit dem SIR-Modell beschäftigen willst, empfehlen wir dir dazu das [Video von Numberphile](https://www.youtube.com/watch?v=k6nLfCbAzgo).

### Faires Münzwurf Problem

Wenn du dich noch weiter damit beschäftigen möchtest, wie man mit Münzen Wahrscheinlichkeiten simulieren kann, findest du
[hier](https://timodenk.com/blog/fair-coin-from-biased-coin/) das Problem mit Lösung und eine Python-Implementierung.
Eine ausführlichere Erklärung zum zweiten Teil des Zirkels gibt es beispielsweise [hier](https://www.alexirpan.com/2015/08/23/simulating-a-biased-coin-with-a-fair-one.html).

### Go

Dieses Jahr haben wir auf dem Mathecamp sehr fleißig Go gespielt (und es gab sogar Teilnehmer\*innen,
die als komplette Anfänger die Fortgeschrittenen Spieler\*innen haushoch geschlagen haben).
Wenn du noch nicht genug davon hast und zuhause weiter spielen willst,
kannst du dir [hier](https://micans.org/goboards/src/boards/plain/gb-9x9-plain-A4.pdf) ein kleines Go-Brett ausdrucken.
Zusätzlich brauchst du dann noch zwei unterschiedlich farbige Plättchen und eine\*n Mitspieler\*in.

Für Programmier-Fans: Viel spannender als ein Go-Spiel auszudrucken wäre natürlich, ein Go-Spiel selbst zu programmieren,
sodass man zu zweit an einem Computer spielen kann, oder?

Man kann Go auch online gegen andere Leute spielen, zum Beispiel [hier](https://online-go.com/).
Auf der Seite gibt es auch eine schöne Einführung in die [Go-Regeln](https://online-go.com/learn-to-play-go)
(für die man sich nicht anmelden muss).

### Gute-Nacht-Geschichten

> Einst war der ganze Llano ein blühendes Gartenland, in dem Mais und Gemüse,
> Früchte aller Sorten und die schönsten Bäume wuchsen. ...

So beginnt die Geschichte vom Burschen, der den Regengott tötete.
Dies war eines der mexikanischen und chinesischen Märchen, das wir uns als Gute-Nacht-Geschichte zu Gemüte geführt haben.
Wer sich in diese Welt noch weiter hineinversetzen möchte, dem können wir folgende Bücher empfehlen:

* Chinesische Märchen - Märchen der Han im Insel-Verlag, 1981, herausgegeben von Rainer Schwarz und
* Märchen aus Mexiko im Eugen-Diederichs-Verlag, 1987, herausgegeben von Felix Karlinger und Maria Antonia Espadinha,
  ISBN 3-424-00613-0

### Jonglieren

Falls du jonglieren lernen möchtest oder schon immer mal deine eigenen Jonglierbälle basteln wolltest,
findest du [hier](https://utopia.de/ratgeber/jonglieren-lernen-eine-anleitung-fuer-anfaenger/) Tipps.
Eine Video-Anleitung für Anfänger gibt es auch [hier](https://www.youtube.com/watch?v=e6GbXfkXbAA) auf Youtube.

Wenn du schon sehr gut mit drei Bällen jonglieren kannst, dein Repertoire aber noch um weitere Tricks ergänzen willst,
kannst du dich gerne [hier](https://spielelux.de/jongliertricks-fortgeschrittene/) inspirieren lassen.

### Keplersche Gesetze

Wieso bewegen sich Planeten eigentlich auf Ellipsen? Diese Fragen haben wir uns in einem der Zirkel gestellt.
Wenn du noch mehr dazu wissen willst, empfehlen wir dir [dieses Youtube-Video](https://www.youtube.com/watch?v=xdIjYBtnvZU).

### Knoten

In einem der Zirkel haben wir unsere Fesselkünste verfeinert und uns ganz viel mit Knoten beschäftigt.
Wenn du dich noch weiter mit dem sehr großen Feld der Knotentheorie beschäftigen willst
(die auch mit sehr vielen anderen Teilbereichen der Mathematik verknüpft ist), kannst du dir beispielsweise ein Video
von einem [Vortrag von John Conway](https://www.msri.org/workshops/167/schedules/25880) anschauen.
Einen mathematischen Artikel, der auch einige Beweise dazu enthält, findest du [hier](https://www.sciencedirect.com/science/article/pii/S0196885896905114).

### Komplexe Zahlen

In diesem Zirkel haben wir uns damit beschäftigt, welche Auswirkungen es haben kann, wenn wir nur eine Zahl
zu den reellen Zahlen hinzufügen.
Wenn du daran weiter knobeln möchtest, kannst du einfach die Aufgaben auf dem
[Zirkelblatt](https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2020/gross/komplexe-zahlen.pdf)
bearbeiten, die wir nicht mehr geschafft haben.
Bei Fragen kannst du dich natürlich gerne jederzeit melden.

Den witzigen [DorFuchs-Song](https://www.youtube.com/watch?v=HRzQAnUl1C4) zu den komplexen Zahlen
möchten wir dir natürlich auch nicht vorenthalten.

### Lightning-Talks

Auch dieses Jahr haben wir wieder super kurzen und absolut informativen Vorträgen lauschen dürfen.
Dabei hat jede\*r Vortragende\*r maximal drei Minuten Zeit um ein Thema ihrer\*seiner Wahl dem Publikum nah zu bringen.
Neben lustigen Städtenamen, sogenannten "Babes" und der thüringischen Landespolitik ging es auch um die korrekte
Verwendung von Emojis -- ein sehr emotionales Thema!

### Nachtwanderung

Auf dem Mathecamp wurden wir bei der Nachtwanderung von griechischen Sagen und Mythen durch verschiedene Stationen begleitet.

### Nähen

Da uns das Thema Mund-Nase-Bedeckung vermutlich noch eine Weile begleiten wird,
haben wir dir eine [Anleitung für eine selbstgenähte Maske](https://www.youtube.com/watch?v=PR9BtlPbjLU) rausgesucht.

Wenn du dir lieber eine coole Tasche (mit Reißverschluss) selbst nähen willst,
die dir ungemein dabei helfen wird Ordnung zu halten,
kannst du [diese Anleitung](https://zeitglueck.com/2017/06/23/federmaeppchen-naehen-fuer-anfaenger/) nutzen.
Alle Nähte, die mit der Nähmaschine genäht werden, kannst du natürlich auch von Hand nähen.

### Paradoxa der Wahrscheinlichkeitstheorie

Bekanntermaßen ist die Wahrscheinlichkeitstheorie eine sehr merkwürdige Disziplin und hält viele Überraschungen bereit.
Vielen solchen Beispielen haben wir uns in diesem Doppelzirkel gewidmet, dabei ging es z.B. um Gewinnshows mit Ziegen,
Taxis, die bei einem Verbrechen beobachtet wurden, Losverkäufer, die ihre Urnen zusammenschmeißen und die Kinder der Nachbarn.
Da wir nicht alles geschafft haben und ein paar Aufgaben/Rätsel/Paradoxa etwas komplizierter sind, gibt es sie
[hier](https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2020/gross/paradoxa-der-wahrscheinlichkeitstheorie.pdf)
nochmal zum Weiterarbeiten.
Viel Spaß!

Bei Fragen kannst du uns gerne an [mathezirkel@math.uni-augsburg.de](mailto:mathezirkel@math.uni-augsburg.de) schreiben.

### Pen and Paper

Wenn ihr auf dem Camp auf den Geschmack von Pen and Paper gekommen seid und zu Hause weiter spielen möchtet,
können euch die folgenden Links weiterhelfen:

* Link zu PDF-Download/ Beispielcharakteren/ Programm zur Charaktererstellung: [https://ilarisblog.wordpress.com/downloads/](https://ilarisblog.wordpress.com/downloads/)
* Link zum Regelbuch im Onlineshop: [https://www.f-shop.de/rollenspiele/das-schwarze-auge-aventurien/limitiert-sondereditionen/68095/ilaris-das-alternative-regelwerk-fuer-dsa](https://www.f-shop.de/rollenspiele/das-schwarze-auge-aventurien/limitiert-sondereditionen/68095/ilaris-das-alternative-regelwerk-fuer-dsa)

### Pi-Kärtchen

Wenn in deiner Sammlung noch eine der Karten mit mathematischen Funfacts bzw. Konstanten fehlt,
kannst du dir diese gerne unter folgenden Links runterladen und ausdrucken.

* [Pi](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/pi-lernen.pdf)
* [Tau](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/tau-lernen.pdf)
* [Euler'sche Zahl](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/e-lernen.pdf)
* [Zeta](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/zeta-lernen.pdf)

### Programmieren

Viele schöne Probleme (von denen wir einen Teil gemacht haben),
sind unter [https://projecteuler.net/](https://projecteuler.net/) zu finden.
Die ersten Probleme sind relativ einfach und werden tendenziell mit aufsteigender Nummer deutlich schwieriger.
Wenn ihr für Aufgaben sehr viel Zeit benötigt oder sie gar nicht hinbekommt, lasst euch nicht entmutigen
und versucht einfach ein Anderes.

### Quantencomputing

Die Quantenangst geht um: Bald wird alle unsere Verschlüsselung nutzlos sein, unsere Banktransfere werden öffentlich
einsehbar und unsere privaten Katzenbilder werden publik.
Und das liegt alles daran, dass es momentan große Fortschritte mit Quantencommputern gibt.
Bereits 2012 ist es auf einem Quantencomputer gelungen, die Zahl 21 in die Primfaktoren 3 und 7 zu zerlegen, ein Riesenfortschritt!

In diesem Kurs haben wir an mehreren Tagen die Grundlagen vom Quantencomputing kennengelernt: Qubits, Messungen und Gatter.
Am Ende ging es auch noch kurz an eine Programmiereinheit, wo wir in Python mit Hilfe von [Qiskit](https://qiskit.org/)
ein paar einfache Schaltkreise nachgebaut und simuliert haben.
Wenn du dich selbstständig noch mehr mit dem Thema auseinander setzen möchtest,
sei dir [https://arxiv.org/abs/1905.00282](https://arxiv.org/abs/1905.00282) empfohlen.
Außerdem gibt es unter [https://algassert.com/quirk](https://algassert.com/quirk) einen grafischen
Online-Quantencomputer-Simulator zum Ausprobieren.
Viel Erfolg!

### Schnitzeljagd des kleinen Camps

Das [kleine Camp](../klein/) hat für uns eine wunderschöne Schnitzeljagd organisiert!
Dabei galt es eine Caesarverschlüsselung zu knacken,
Annas Lieblingsformel (Übrigens das [Euler-Produkt](https://de.wikipedia.org/wiki/Euler-Produkt)) rauszufinden
und einen Text in Braille zu übersetzen.

### Seekers

Wer die letzten Jahre schon dabei war, hat vielleicht schonmal von Seekers gehört.

Ziel bei Seekers ist es kurzgesagt einen eigenen Bot in Javascript zu programmieren und damit so viele Punkte wie möglich
zu sammeln.
Auch dieses Jahr soll es wieder ein Turnier geben, aber in etwas anderer Form!
Auf dem Mathecamp selbst hatten wir wenig Zeit und deshalb findet dieses Jahr ein online Seekers-Turnier statt.
Das Spiel, das du noch um deine Strategie ergänzen kannst,
findest du auf Jonas [Github](https://github.com/Ankylotech/seekers-js) und bei Fragen kannst du dich gerne bei ihm melden.

### Stable Marriage

Du kennst das ja: Immer wieder möchte man eine Gruppe von Freund\*innen so paarweise verkuppeln,
dass alle so zufrieden wie möglich sind.
Eine Lösung für dieses Problem haben wir in einem der Zirkel behandelt und uns
mit dem [Gale-Shapley-Algorithmus](https://en.wikipedia.org/wiki/Gale%E2%80%93Shapley_algorithm) beschäftigt.
Zu dem Thema gibt es [hier](https://www.youtube.com/watch?v=Qcv1IqHWAzg) und [hier](https://www.youtube.com/watch?v=LtTV6rIxhdo)
zwei sehr coole Videos von Numberphile (auf englisch):

Wenn du selbst nocheinmal ein Matching machen möchtest, kannst du [hier](http://elearning.oms.rwth-aachen.de/Stable_Matching/matching.html)
deine Kupplerfähigkeiten unter Beweis stellen.

Und denk immer an das Wichtigste: Hauptsache es gibt keinen Skandal! ;-)

### Tanzkurs

Tanzen geht auch ohne Körperkontakt! Zum Beispiel mit Freestyle.
Der Name ist jedoch etwas irreführend, weil ja alle (mehr oder weniger) das gleiche tanzen :-)

Wer Lust hat, daheim Freestyle zu üben, kann sich die im Tanzkurs verwendeten Tracks
in [Samuels Spotify Playlist](https://open.spotify.com/playlist/17lBmh5eBUy6FGa4ULM6Bs?si=eiLMMG3_RpeKv-yiMSUiug) geben.
Mit dabei sind auch Cupid Shuffle und Cha Cha Slide!

### Topologie

Auf Wikipedia kann man ziemlich viel über Topologie lesen.
Manches ist ohne Vorwissen natürlich kaum zu verstehen,
aber dafür gibt es auch viele Bilder, die schon sehr viel aussagen! :-)

Wir haben das Konzept von zueinander homotopen Wegen kennengelernt.
Und auch das Komponieren von zwei Wegen, die an einem festen Punkt starten und enden, kam kurz vor.
Beides zusammen ergibt die Definition der Fundamentalgruppe,
über die du auf [Wikipedia](https://de.wikipedia.org/wiki/Fundamentalgruppe#Anschauliche_Erkl%C3%A4rung_am_Beispiel_des_Torus)
mehr erfahren kannst:

Das Rätsel mit Elektrizitäts-, Wasser- und Gaswerk haben wir auf einem Quadrat untersucht,
bei dem die Kanten auf verschiedene Weise miteinander verklebt waren.
Hier sind ein paar von den Flächen, die man so erhält:

* [Der Torus](https://de.wikipedia.org/wiki/Torus#Konstruktion_aus_einem_Quadrat_oder_W%C3%BCrfel)
* [Die Klein'sche Flasche](https://de.wikipedia.org/wiki/Kleinsche_Flasche#Konstruktion)
* [Die (reelle) projektive Ebene](https://de.wikipedia.org/wiki/Projektive_Ebene#Die_reelle_projektive_Ebene_als_nicht-orientierbare_Fl%C3%A4che)

Es gibt auch auf dem super coolen Youtube-Kanal Numberphile viele Videos (auf englisch), die mit Topologie zu tun haben.
Zum Beispiel [dieses](https://www.youtube.com/watch?v=AAsICMPwGPY) über die Klein'sche Flasche:

### Yoga

Wenn dich auf dem Camp die Yoga-Motivation gepackt hat, ist vielleicht
dieser [30 Tage Yoga-Plan](http://www.madymorrison.com/yoga/gowiththeflow-yoga-challenge) spannend für dich.
Hier erwartet dich jeden Tag ein Youtube-Video zu einer Yoga-Einheit, mal kurz mal lang, mal für abends mal für morgens.
Man kann den Plan auch sehr gut als Anfänger absolvieren.

Zum gegenseitigen Motivieren und dran bleiben gibt es eine WhatsApp-Gruppe
und wir starten gemeinsam am 21.09.2020 mit dem Plan.

### Zauberwürfel

Die Anleitung zum Lösen eines 3x3 Zauberwürfels findest du auf [Tims Webseite](https://timbaumann.info/zauberwuerfel).

## Abschluss und Mathecamp 2021

Schön, dass du bis hier her durchgehalten hast! Vermutlich hätten wir mit den zusätzlichen Materialien
auch noch ein Camp füllen können :-)

Apropos, das Mathecamp 2021 findet vom 14.08.2021 bis 22.08.2021 statt.
Das ist eine Woche vor der üblichen Mathecampwoche in den bayrischen Sommerferien! Wir freuen uns schon auf dich!
