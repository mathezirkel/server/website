---
title: Mathecamp
type: event
eventName: mathecamp
image: images/zirkelgregor.png
registrationLinks:
  - url: /anmeldung/mathecamp/
feedback: True
donate: False
summary: |
    Mitten in den Sommerferien hast du die Gelegenheit, neun Tage mit uns im Bruder-Klaus-Heim in Violau zu verbringen.
    Wir werden gemeinsam vielfätige mathematische Themen entdecken und verstehen.
    Dabei kommt die Freizeit aber auf keinen Fall zu kurz.
    Außerdem ergeben sich hier viele Möglichkeiten, Gleichgesinnte kennenzulernen und neue Freundschaften zu knüpfen.
---

Mitten in den Sommerferien hast du die Gelegenheit, neun Tage mit uns im Bruder-Klaus-Heim in Violau zu verbringen.
Wir werden gemeinsam vielfältige mathematische Themen entdecken und verstehen.
Dabei kommt die Freizeit aber auf keinen Fall zu kurz.
Außerdem ergeben sich hier viele Möglichkeiten, Gleichgesinnte kennenzulernen und neue Freundschaften zu knüpfen.

<figure>
    {{< responsive-image
        src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp.jpg"
        alt="Bild zur Veranstaltung"
    >}}
</figure>

An jedem Tag werden vormittags zwei Arbeitsgruppen stattfinden.
In diesen behandeln wir spannende Bereiche der Mathematik, die in der Schule nicht thematisiert werden und
die du noch nicht aus den Zirkeln kennst.
Selbstverständlich gibt es für verschiedene Alters- und Vorkenntnisstufen
angepasste Kurse mit unterschiedlichen Themen und Schwierigkeiten, sodass immer für jede:n etwas dabei ist.

Zusätzlich gibt es nachmittags weitere, optionale Arbeitsgruppen.
Daneben haben wir ein buntes Freizeitprogramm geplant.
Dieses besteht u.a. aus Programmierworkshops, Mal- und Nähstunden, musikalischen Angeboten sowie aus Spielen und
den verschiedensten Sportarten.
Bei schönem Wetter werden wir auch viel draußen machen können.
Das Haus bietet einen Fußballplatz,
ein Beachvolleyball-Feld, einen See zum Bootfahren und vieles mehr!
Außerdem besteht die Möglichkeit, spazieren zu gehen oder am Abend am Lagerfeuer zu sitzen.

### Mathecamp 2025

Das Mathecamp 2025 findet vom 23. bis 31. August 2025 statt. Die Anmeldephase beginnt voraussichtlich im Juni.
Eine vorzeitige Reservierung von Plätzen ist leider nicht möglich.

### Vergangene Camps

* [Mathecamp 2024](./2024/)
* [Mathecamp 2023](./2023/)
* [Mathecamp 2022](./2022/)
* Mathecamp 2021 - [Klasse 5 bis 8](./2021/klein/) und [Klasse 9 bis 12](./2021/gross/)
* Mathecamp 2020 - [Klasse 5 bis 8](./2020/klein/) und [Klasse 9 bis 12](./2020/gross/)
* [Mathecamp 2019](./2019/)
* [Mathecamp 2018](./2018/)
* [Mathecamp 2017](./2017/)
* [Mathecamp 2016](./2016/)
* [Mathecamp 2015](./2015/)
* Mathecamp 2014
