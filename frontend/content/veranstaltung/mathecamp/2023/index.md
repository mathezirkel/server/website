---
title: Mathecamp 2023
---

## Harte Fakten

<div class="centering-flex-col">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/heatmap.png"
            alt="Herkunft der Teilnehmer:innen"
            width="500"
            class="rounded-corners"
            loading="lazy"
        >}}
        <figcaption>Hier kommt ihr her!</figcaption>
    </figure>
    <div class="pie-chart-area">
        <div class="pie-chart">
            <figure>
                <img
                    src="piechart-participants.svg"
                    width="200px"
                    alt="Aufteilung der Teilnehmer:innen nach Geschlecht"
                >
                <figcaption>139 Teilnehmer:innen</figcaption>
            </figure>
        </div>
        <div class="pie-chart">
            <figure>
                <img
                    src="piechart-grades.svg"
                    width="200px"
                    alt="Aufteilung der Teilnehmer:innen nach Klasse"
                >
                <figcaption>139 Teilnehmer:innen</figcaption>
            </figure>
        </div>
        <div class="pie-chart">
            <figure>
                <img
                    src="piechart-instructors.svg"
                    width="200px"
                    alt="Aufteilung der Betreuer:innen nach Geschlecht"
                >
                <figcaption>27 Betreuer:innen</figcaption>
            </figure>
        </div>
    </div>
</div>

## Fotos

<div class="gallery-area">

<div class="gallery">

<figure>
    {{< responsive-image
        src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/vorschau_fraktivitaeten.jpeg"
        alt="Vorschaubild Bildergallerien"
        loading="lazy"
    >}}
</figure>

### Fraktivitäten

* [Allgemein](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/fraktivitaeten/allgemein/)
* [Basteln & Co](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/fraktivitaeten/basteln/)
* [Bootfahren](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/fraktivitaeten/bootfahren/)
* [Chor](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/fraktivitaeten/chor/)
* [Dancen](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/fraktivitaeten/dancen/)
* [Gartic Phone](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/fraktivitaeten/gartic-phone/)
* [Karaoke](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/fraktivitaeten/karaoke/)
* [Mantsche Pantsche](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/fraktivitaeten/mantsche-pantsche/)
* [OSM](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/fraktivitaeten/osm/)
* [Pool](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/fraktivitaeten/pool/)
* [Sport](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/fraktivitaeten/sport/)

</div>

<div class="gallery">

<figure>
    {{< responsive-image
        src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/vorschau_ballon.jpeg"
        alt="Vorschaubild Bildergallerien"
        loading="lazy"
    >}}
</figure>

### Ballon

[Mehr](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/ballon/)

</div>

<div class="gallery">

<figure>
    {{< responsive-image
        src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/vorschau_mehr.jpeg"
        alt="Vorschaubild Bildergallerien"
        loading="lazy"
    >}}
</figure>

### Mehr

* [Anreise](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/anreise/)
* [Bunter Abend](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/bunter-abend/)
* [Disco](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/disco/)
* [Essen](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/essen/)
* [Geländespiel](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/gelaendespiel/)
* [Gregor](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/gregor/)
* [Gruppenbilder](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/gruppenbilder/)
* [Kennenlernspiel](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/kennenlernspiel/)
* [Matboj](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/matboj/)
* [Nachtwanderung](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/nachtwanderung/)
* [Plena](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/plena/)
* [Sonstiges](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/sonstiges/)
* [Vorträge](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/vortraege/)
* [Zirkel](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/zirkel/)

</div>

</div>

Die Zugangsdaten hast du von uns per Mail erhalten. Bitte
gib die Zugangsdaten nicht an Fremde weiter und verbreite einzelne Fotos
nur, wenn du die Erlaubnis von allen auf dem Foto abgebildeten Personen
hast.

Möchtest du eine ganze Galerie in Orginalqualität herunterladen,
kannst du oben links auf das Save-Icon klicken.

Hast du selbst noch Fotos oder Videos, die du gerne teilen möchtest?
Dann schickt sie uns an [mathezirkel@math.uni-augsburg.de](mailto:mathezirkel@math.uni-augsburg.de).

Falls du noch Sachen vermisst, gibt es eine Galerie von
[Fundsachen](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/fotos/fundsachen/). Solltest
du etwas wiedererkennen, schreib uns auch eine E-Mail.

## Zirkel und Mathematik

### Additionstheoreme für Sinus und Cosinus

Was ist der Cosinus der Summe zweier Winkel? Die Antwort darauf liefert das Additionstheorem für den Cosinus:

\[
    \cos(\alpha + \beta) = \cos(\alpha) \cos(\beta) - \sin(\alpha) \sin(\beta)
\]

Wir haben dieses Theorem dann auf drei verschiedenen Wegen bewiesen: mit Geometrie, mit Algebra und mit Analysis.
Wenn ihr das nochmal nachlesen wollt, könnt ihr das
[hier](https://raw.githubusercontent.com/GraffL/Mathezirkel/4f5c7c4006b5abf159b79fd46b3d067c0ccb5bf8/Additionstheoreme%20Sinus%20Cosinus/Skript.pdf)
tun.

Die Inspiration für eine der Anwendungen des Additionstheorems, stammt aus diesem sehr schönen
[Mathologer-Video](https://www.youtube.com/watch?v=sDfzCIWpS7Q).

### Bertrands Postulat

Der Mathematiker Paul Erdős hatte die Vision von einem sogenannten BUCH, in dem Gott die perfekten Beweise
für mathematische Sätze aufbewahrt. Vor ein paar Jahren haben Martin Aigner und Günter Ziegler einen ersten Entwurf
von diesem "BUCH der Beweise" veröffentlicht.

Eines der dort beschriebenen Theoreme ist Bertrands Postulat. Dieses besagt, dass es für alle natürlichen Zahlen
\(n\) eine Primzahl \(p\) mit \(n < p < 2n\) gibt.
Paul Erdős selbst fand dafür einen Beweis, der abschätzt, welche Primfaktoren im Binomialkoeffizienten \(\binom{2n}{n}\)
wie oft vorkommen.

Schaut gerne mal in dieses tolle [Buch](https://link.springer.com/book/10.1007/978-3-642-02259-3) rein!

### Brick-Factory-Problem

Ein schönes ungelöstes mathematisches Problem ist das sogenannte *Brick-Factory-Problem*.
Als Zirkelthema der 8. Klasse haben wir uns damit auseinandergesetzt, wie viele Kreuzungen es auf den Schienen zwischen
Produktions- und Lagerstätten mindestens gibt, wenn jede Produktionsstätte mit jeder Lagerstätte verbunden sein soll.
Einen [schönen Überblick](https://www.youtube.com/watch?v=xBfAYxxRsjY) über das Problem liefert Numberphile.
Generell kann man [Numberphile](https://www.youtube.com/c/numberphile/videos) als Quelle für gute Erklärungen
zu mathematischen Problemen heranziehen.

### Clustering und Wort-Einbettungen

Wir haben verschiedene Methoden des clusterbasierten Lernens kennengelernt.
Details kann man nochmal [hier](https://de.wikipedia.org/wiki/Clusteranalyse) nachlesen.

Des Weiteren haben wir gegen Ende des Zirkels versucht, ob uns Worteinbettung die Frage beantworten kann

\[
    \text{Bäcker} - \text{Brot} + \text{Fleisch} = \text{???}
\]

Und haben erfolgreich die Antwort "Metzger" erhalten. Für andere Wörter hat es leider nicht sonderlich gut funktioniert.
Das Problem war hier, dass zu unterschiedliche Beträge der einzelnen Wortvektoren das Ergebnis kaputt machen.
Das kommt davon, wenn man versucht, alles selbst zu machen, denn im Nachgang kam mit etwas googeln heraus,
dass das Problem bereits von schlaueren Leuten gelöst wurde
([Kompliziertes Paper, dass das ordentlich macht](https://aclanthology.org/W14-1618.pdf)).

Wer mit funktionierendem Code herumspielen will, der kann diesen
[hier](https://gist.github.com/jonas-kell/7f95a9535d14881dfe1a6582386ea383) anschauen.
Oder aber, man schaut sich ein [YouTube-Video](https://www.youtube.com/watch?v=gQddtTdmG_8)
(auf Englisch) an, dass das Phänomen beschreibt.

### Lichtgeschwindigkeit mit einer Mikrowelle messen

Auf dem Camp haben wir die Lichtgeschwindigkeit mit einer Mikrowelle bestimmt.
Dazu haben wir ein Pappstück, das mit einer Zuckerpaste beschichtet war, in der Mikrowelle erhitzt
und den Abstand von zwei Stellen, an denen die Zuckerpaste verbrannt war, gemessen. Damit haben wir
die Wellenlänge der Mikrowelle bestimmt. Weiter können wir auf der Mikrowelle die Frequenz von 2.445 GHz ablesen.
Der Abstand zwischen zwei verbrannten Stellen (in unserem Fall haben wir 12,3 cm gemessen) entspricht der Wellenlänge.
Multiplizieren wir die beiden Größen, so erhalten wir die Ausbreitungsgeschwindigkeit:

\[
    12.3 \,\text{cm} \cdot 2.445\,\text{GHz} = 300700\,\frac{\text{km}}{\text{s}}
\]

Wer mehr über die Funktionsweise einer Mikrowelle lernen möchte, sei der Wikipedia-Artikel über
[Mikrowellen](https://de.wikipedia.org/wiki/Mikrowellenherd) ans Herz gelegt.
Eine nicht ganz ernst gemeinte Erklärung der Funktionsweise und auch unseres Versuches lässt sich auch auf
[YouTube](https://www.youtube.com/watch?v=nqTDCkuVADw) finden.
Wie immer gilt, dass es sich bei Mikrowellen um Geräte mit starker elektromagnetischer Strahlungund Hochspannung
handelt, so dass Experimente potentiell gefährlich sind, insbesondere, wenn man Änderungen an der Mikrowelle selbst
vornimmt.

### Entropie in Physik und Informatik

In diesem Zirkel haben wir unter anderem gelernt was Schwarzkörperstrahlung ist und was die Hauptsätze
der Thermodynamik aussagen.

Warum das im Zusammenspiel von Erde und Sonne für unser Überleben wichtig ist, haben wir uns überlegt.
Zudem ist es eine etwas unintuitive Konsequenz der Antwort auf die Frage:
> *Wenn die Sonne Energie auf die Erde strahlt und wir verwenden die Energie der Sonne,
> wie viel Energie strahlt die Erde zurück in den Weltraum?*

Als Inspiration für diese Kernfrage diente dieses
[YouTube-Video](https://www.youtube.com/watch?v=DxL2HoqLbyA) (auf Englisch).
Für die mathematischen und physikalischen Konzepte gibt es viele gut verständliche Einträge auf Wikipedia.

### Matboj

Auch dieses Jahr gab es wieder einen epischen Wettkampf!
Wenn du dich schon einmal für den Matboj auf dem Mathecamp nächstes Jahr vorbereiten möchtest, kannst du dich genauer
mit den [Regeln](https://mathezirkel.gitlab.io/content/matboj-regelwerk/Matboj_Regelwerk.pdf) auseinandersetzen.
Vielleicht findest du ja eine optimale Strategie!

### Mathematik auf dem Billardtisch

Da seit Jahren das Thema gefordert ist und auch dieses Jahr der Krankheit von Teilnehmenden zum Opfer fiel,
möchte ich hier wenigstens mal das Buch anteasern, auf dem das ganze Thema basieren würde.
Das Buch heißt: [Geometrie und Billard von Serge Tabachnikov](https://link.springer.com/book/10.1007/978-3-642-31925-9)
\- es wird 2024 (sofern ich als Betreuer wieder dabei bin), aber endlich der entsprechende Vortrag/Zirkel dazu gehalten
\- versprochen!

### Optimale Schuhbindungen

Im Zirkel über optimale Schuhbindungen haben wir ein ganz alltägliches Problem mathematisch analysiert.
Wie kann man eigentlich seine Schuhe binden und was bedeutet in diesem Zusammenhang optimal
Wir haben dazu dann zwei Eigenschaften der Optimalität kennengelernt.
Aufgrund der Klassenstufe (Klasse 6) blieb der Beweis im Zirkel offen.
Diesen könnt ihr mit folgendem [YouTube-Video](https://www.youtube.com/watch?v=CSw3Wqoim5M)
nachvollziehen (auf Englisch).

### Planare Graphen und unmögliche Straßenschilder

In diesem Zirkel haben wir planare Graphen kennengelernt und ein Kriterium dafür kennengelernt,
dass ein zusammenhängender Graph nicht planar sein kann: Die *magische* Formel

\[
    \text{Ecken} - \text{Kanten} + \text{Flächen} = 2
\]

Mit deren Hilfe haben wir dann bewiesen, dass ein in Großbritannien verwendetes Straßenschild mathematisch
unmöglich ist. StandUp-Mathematiker Matt Parker erklärt in [diesem Video](https://www.youtube.com/watch?v=btPqKAGyajM)
wie er eine Petition gestartet hat, um das zu ändern.

### SAT

In dem Zirkel über SAT (*satisfiability*) haben wir das Problem der *Erfüllbarkeit* einer aussagenlogischen Formel
kennengelernt. Gerne könnt ihr auch daheim probieren, einige der kennengelernten Probleme mit einem PC zu lösen.
Grundlegende Programmierkenntnisse sind dafür ausreichend. Für Python gibt es die Bibliothek
[pysat](https://pysathq.github.io), mit der sich viele verschiedene SAT-Solver verwenden lassen. Ebenfalls bekannt ist
[z3](https://www.microsoft.com/en-us/research/project/z3-3/). Diese Bibliothek ist sogar noch mächtiger und unterstützt
nicht nur SAT, sondern auch [Satisfiability Modulo Theories](https://en.wikipedia.org/wiki/Satisfiability_modulo_theories).
Inspiration für zu lösende Probleme lassen sich unter anderem hier finden:

* [Sudoku](https://users.aalto.fi/~tjunttil/2020-DP-AUT/notes-sat/solving.html)
* [Übersicht über verschiedene Probleme](https://cs.uwaterloo.ca/~cbright/reports/sat-maple.pdf)

### Spiel und Spaß mit GeoGebra

In diesem Zirkel haben wir verschiedene Problemstellungen mithilfe von GeoGebra untersucht und dabei auch das ein oder
andere neue Werkzeug entdeckt.

Zum Beispiel hat eine durstige Giraffe den kürzesten Weg zur nächsten Oase gesucht, wollte aber unbedingt noch eine
Trinkpause am (Süßwasser-)Meer einlegen. Für alle die sich dieses Problem (noch einmal) genauer anschauen wollen,
die Datei findet ihr [hier](https://www.geogebra.org/m/gdbrbyds).
GeoGebra hat auch eine 3D-Ansicht, mit der wir den [Weg einer Fliege](https://www.geogebra.org/m/w22apbyz) auf einer
Getränkedose analysiert haben.

Zum Schluss durfte das neu erlernte Wissen zur Erstellung eigener Kunstwerke mit GeoGebra eingesetzt werden.
Dabei sind einige tolle Kreationen entstanden. Seht selbst:

<div class="grid-gallery">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/geogebra/geogebra_1.jpg"
            alt="Geogebra Impression"
            width=300
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/geogebra/geogebra_2.jpg"
            alt="Geogebra Impression"
            width=300
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/geogebra/geogebra_3.jpg"
            alt="Geogebra Impression"
            width=300
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/geogebra/geogebra_4.jpg"
            alt="Geogebra Impression"
            width=300
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/geogebra/geogebra_5.jpg"
            alt="Geogebra Impression"
            width=300
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/geogebra/geogebra_6.jpg"
            alt="Geogebra Impression"
            width=300
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/geogebra/geogebra_7.jpg"
            alt="Geogebra Impression"
            width=300
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/geogebra/geogebra_8.jpg"
            alt="Geogebra Impression"
            width=300
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/geogebra/geogebra_9.png"
            alt="Geogebra Impression"
            width=300
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/geogebra/geogebra_10.png"
            alt="Geogebra Impression"
            width=300
            loading="lazy"
        >}}
    </figure>
</div>

## Fraktivitäten

### Ausgefallene Nachtwanderung und Gewitter

Obwohl die Oberstufe eine ganz tolle Nachtwanderung organisiert hatte (die Betreuenden mussten sogar Verträge
unterschreiben!), konnte diese ob eines Unwetters nicht stattfinden.
Trotzdem hatte es in dieser Nacht eine ganz tolle Atmosphäre im Bruder-Klaus-Heim und es hat viel Spaß gemacht aus der
Sicherheit Blitz und Donner zu beobachten.

### Bakterien und Pilze züchten

Wir haben an verschiedenen Orten mit Wattetupfern Proben entnommen und auf Petrischalen mit Nährlösung übertragen.
Solch eine Nährlösung kann man entweder selbst herstellen oder schon mit Nährlösung bestückte Petrischalen für einen
schmalen Taler erwerben. Das Experiment kann man auch gut zu Hause wiederholen, man sollte allerdings darauf achten,
dass potenziell gesundheitsschädliche Bakterien und Pilze heranwachsen können, sodass man Betrachtung und Entsorgung
mit der notwendigen Sicherheitsvorkehrungen begegnen sollte.

### Bastelfraktivität

Dieses Jahr drehte sich beim Basteln ganz viel um Planeten und andere Himmelskörper. Wer Lust hatte, konnte sich seinen
eigenen Fantasie-Planeten aus einem Luftballon, Zeitungspapier und Kleister bauen und ihn anschließend noch kreativ mit
Farbe, Glitzer, … gestalten. Auch Steckbriefe zu den Planeten wurden fleißig ausgefüllt. Natürlich gab es auch noch
viele andere Möglichkeiten, wie z. B. Styroporkugeln, Leinwände und Acrylfarbe, um sich die Zeit im Werkraum zu
vertreiben.

### Knigge

Nachdem es während des Camps immer wieder zu Patzern beim Eindecken des Mittagstisches gekommen war, haben wir in dem
nicht ganz Ernst gemeinten Zirkel zu Knigge
[grundlegende Regeln](https://praxistipps.focus.de/knigge-beim-essen-diese-benimmregeln-gelten-bei-tisch_111799) zu
Tischmanieren gelernt. Ferner haben wir betrachtet, wie
[richtig eingedeckt](https://g-wie-gastro.de/abteilungen/service/fachkunde/eindecken.html) wird. Und es gilt natürlich:

> *"Das Essen wird zum Mund geführt und nicht umgekehrt."*

### Mathecamp-Song und Schlangenforscher

Damit ihr den Mathecamp-Song und auch den Schlangenforscher,
der ja schon lange Jahre zur Tradition des Mathecamps gehört nicht vergesst,
hier die Videos vom Bunten Abend und den Text vom Mathecamp-Song zum Üben:

* [Schlangenforscher](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/videos/schlangenforscher.mp4)
* [Mathecamp-Song](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2023/videos/mathecampsong.mp4)
* [Mathecamp-Song Text](https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/mathecampsong.pdf)

### OpenStreetMap

Auf dem Mathecamp haben uns die [StreetComplete](https://streetcomplete.app/) Quests zu Orten und Fragen geführt,
an denen wir sonst wohl nicht vorbeigekommen wären.
Die Frage nach den Öffnungszeiten des Wallfahrtladens hat zu einem sehr guten, spontanen Vortrag über den Fatima-Tag
geführt. Wie heißt eigentlich der Friedhof von Violau? Als uns ein Gewitter in St. Michael überrascht hat, konnten wir
den Ort in Ruhe besichtigen und hatten ein bisschen Zeit für uns. Sehr zuvorkommend war die Mitarbeiterin, die uns
etliche Schirme für den Rückweg ausgeliehen hat. Letztlich ist StreetComplete eine schöne Möglichkeit die Einheimischen
(samt ihrer Bulldoggen) kennenzulernen.

Probiert es doch gerne mal bei euch zu Hause aus! Ladet euch dazu einfach die App herunter:

* [Google Play Store](https://play.google.com/store/apps/details?id=de.westnordost.streetcomplete)
* [F-Droid](https://f-droid.org/packages/de.westnordost.streetcomplete/)
* [iOS](https://apps.apple.com/de/app/go-map/id592990211)

Wenn man komplizierte Bearbeitungen vornehmen will oder gerne neue Strukturen eintragen möchte, bietet es sich an,
einen [Editor aus dem OpenStreetMap Wiki](https://wiki.openstreetmap.org/wiki/Editors) auszusuchen, die gegenüber
StreetComplete deutlich mächtigere (und auch kompliziertere) Bearbeitungsmöglichkeiten zulassen.

Unsere Änderungen (und alle anderen Eintragungen) kann man auf der offiziellen Website von
[OpenStreetMap](https://www.openstreetmap.org/) einsehen. Da die Kartendaten mit einer freien Lizenz versehen sind,
finden die Kartendaten bei vielen anderen Projekten Verwendung zum Beispiel bei der App
[OrganicMaps](https://organicmaps.app/), die auf allen gängigen mobilen Betriebssystemen zur Navigation verwendet
werden kann.

### Programmieren

Wir haben dieses Jahr mit Python die Fibonacci-Reihe generiert, Schneeflocken gemalt, Matherätsel gelöst und vieles
mehr. Wenn ihr Lust habt noch mehr zu programmieren, könnt ihr mit der süßen Schildkröte Tommy auf
[trinket.io](https://trinket.io/turtle) schöne Bilder malen oder zum Beispiel die Kochsche Schneeflocke.
[Hier](https://www.python-lernen.de/python-turtle.htm) findet ihr nochmal eine kleine Einführung in PyTurtle und wenn
ihr die Kochsche Schneeflocke oder weitere Fraktale programmieren wollt, dann kann euch
[dieser Blogeintrag](https://gitlab.com/mathezirkel-taulex/zirkel-python-turtle-fraktale/-/blob/main/tutorials/README.md)
weiterhelfen. Ihr könnt Python natürlich auch direkt auf eurem Computer [installieren](https://www.python.org/),
um dann weitere [Euler-Probleme](https://projecteuler.net/archives) zu lösen oder andere Projekte zu realisieren.
Wenn ihr noch nach Kursen sucht, um eure Pythonkenntnisse zu erweitern, könnt ihr zum Beispiel bei diesen beiden
starten: [Youtube Playlist](https://www.youtube.com/playlist?list=PL_pqkvxZ6ho3u8PJAsUU-rOAQ74D0TqZB) oder
[HPI Kurs](https://open.hpi.de/courses/pythonjunior2020).

### Rave

Ein Rave bezeichnet frei nach der deutschen Wikipedia eine einmalige Tanzveranstaltung mit elektronischer Musik in
eigens dafür präparierten Locations, wie zum Beispiel Bunkern oder Bootshäusern. Der konzeptionelle Schwerpunkt liegt
auf ekstatischem Tanz in Kombination mit Kegeln.

### Rope Skipping

Im Rahmen dieser Fraktivität haben wir verschiedenste Elemente des Rope Skipping -- oder einfacher: Seilspringen --
ausprobiert. Dabei kam nicht nur das Single Rope (in dem man alleine vielfältige Tricks machen kann) zum Einsatz,
sondern auch das Long Rope und Double Dutch (wenn ein oder zwei lange Seile von zwei Personen geschwungen wird)
bereitete vielen Teilnehmenden Spaß.

### Spikeball

Dieses [YouTube-Video](https://www.youtube.com/watch?v=T3vLN-KHUlQ&t=0s) erklärt nochmal ausführlich alle Regel.
Das Set, das wir im Mathecamp verwenden (cool, weil es extrem robust ist), ist das originale *SPIKEBALL-Pro-Set*.
Ein [Highlight-Video](https://www.youtube.com/watch?v=mo4qTH-VYGM) zum Nacheifern (auf Englisch) findest du auf YouTube.
Zudem findet man viele weitere Videos mit Tipps und Tricks auf YouTube - einfach mal danach suchen!

### Sterne schauen

Wir sind mit der Oberstufe in einer Nacht zum Sterneschauen gegangen. Obwohl es sehr bewölkt war, konnten wir
zwischendurch den Jupiter (!), die Andromedagalaxie und die Plejaden mit dem Teleskop bewundern.
Wenn ihr daheim auch Sterne anschauen wollt und nicht wisst, welches Sternbild oder welchen Planeten ihr vor euch habt,
probiert es doch mal mit [Stellarium](https://stellarium.org/de/), einer kostenlosen Software für PC und Smartphone.
Erst kürzlich z.B. war Lotti auf dem Weg mit dem Flieger nach Neuseeland und hat den schönsten Sternhimmel seit langem
über den Wolken gesehen. Quasi instant hat sie erst eine Zeichnung angefertigt, dann ein Bild gemacht und Spacy
geschickt. Nur mit der Info ob man aus dem rechten oder linken Sitzplatz schaut, sowie der Zeit und der Position
des Flugzeugs, konnten beide genau den Ausschnitt des Himmels rekonstruieren.
Stellt sich heraus, dass Lotti fast den ganzen Skorpion, einen Teil des Schützen und das Teleskop
(ein Sternbild, das man nur am Südsternhimmel sieht) fotografiert hat. Falls ihr also Sternbilderfragen habt:
Wir sind jetzt Experten :)
Lotti und Spacy  <3

<div class="grid-gallery">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/sterne/sterne_1.jpg"
            alt="Sterne schauen"
            width=300
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/sterne/sterne_2.jpg"
            alt="Sterne schauen"
            width=300
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/sterne/sterne_3.jpg"
            alt="Sterne schauen"
            width=300
            loading="lazy"
        >}}
    </figure>
</div>

### Sudoku-Solver Programmier-Challenge

Wir haben versucht möglichst intuitiv und ohne externe Hilfe einen rekursiven "Backtracking-Algorithmus" zu
implementieren, der einen Computer dazu bringen kann, Sudokus für uns zu lösen.

Der Code den wir dabei zusammen geschrieben haben ist
[hier](https://gist.github.com/jonas-kell/3578d1f42944183825ba39b4519e1ee9) nochmal hochgeladen.

### Stratosphärenballon

#### Was ist passiert?

Zum zehnjährigen Jubiläum des Camps wurde beschlossen zum zweiten Mal in der Geschichte des Mathecamps einen
Stratosphärenballon steigen zu lassen und wieder zu bergen.
Beim ersten Mal gab es noch viel zu lernen und optimieren.
Mit der gesammelten Erfahrung, viel Plangungsaufwand und einer riesigen Heliumflasche im Gepäck reisten wir im Camp an.
Dort angekommen wurde die Sonde von mehreren Teilnehmern perfekt zusammengebaut und getestet.

Der Start war geplant für Montag den 21.08.2023 um 13:30. Da das Wetter an diesem Tag perfekt mitspielte,
konnten die Vorbereitungen und der "Launch" unserer kleinen Weltraummission wie geplant erfolgen.
Nachdem wir von Christoph die weltgrößte Rohrzange ausgeliehen hatten (da wir zu Beginn die Schutzkappe der Gasflasche
nicht aufbekamen...) konnte das Helium in den Ballon fließen und kurz danach hatten wir "Liftoff".

Dank des Satelliten-Senders, der im Ballon montiert war konnten wir die Flugbahn live von Violau aus verfolgen, auch
wenn die Höhenaufzeichnung leider nicht ordentlich vom Sender unterstützt wurde.

Die Positionsdaten könnt ihr [hier](https://gist.github.com/jonas-kell/5ebcb68e19fd422ebafa5d20da93202b) nochmal
genau analysieren.

Am Abend des selben Tages rückte das Bergungsteam aus und konnte nach einer späktakulären Baumkletteraktion
das Flugobjekt bergen.

An Bord: Astronaut-Space-Gregor, Perlen als Andenken, Stromversorgung und Kameras, GPS-Tracker.

#### Woher bekommt man einen Stratosphärenballon

Man könnte denken, dass man spezielle Berechtigungen braucht um einen Wetterballon zu kaufen.
Aber so ziemlich alle Materialien sind einfach im [Internet bestellbar](https://www.stratoflights.com/).
Dort gibts auch tolle Anleitungen und Leitfäden, schaut doch mal rein.

Allerdings muss man für den Betrieb noch zwei andere, wichtige Dinge beschaffen: eine Betriebserlaubnis und Helium.

Bei dem Beantragen der Betriebserlaubnis war Christoph wieder eine tolle Hilfe und bald waren die Unterschriften von
Bürgermeister, Grundstückseigentümer und Co. eingesammelt und die Starterlaubnis wurde vom "Luftamt-Südbayern" erteilt.

Mit dem technischen Know-How und Equipment für Transport und Befüllung wurden wir vom
"Institut für Physik der Universität Augsburg" versorgt. Vielen Dank dafür.

#### Einblicke

<div class="grid-gallery">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/ballon/ballon-fuellen.png"
            alt="Ballon füllen"
            width=300
            loading="lazy"
        >}}
        <figcaption>Ballon füllen</figcaption>
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/ballon/ballon-start.png"
            alt="Ballonstart"
            width=300
            loading="lazy"
        >}}
        <figcaption>Ballonstart</figcaption>
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2023/ballon/hoechster-fotografierter-punkt.png"
            alt="Höchster fotografierter Punkt"
            width=300
            loading="lazy"
        >}}
        <figcaption>Höchster fotografierter Punkt</figcaption>
    </figure>
</div>

#### Videos

Im Vergleich zum ersten Stratosphärenflug hatten wir mehr Erfolg mit den Videoaufnahmen und wir haben dieses viel mehr
hochauflösendes Video auf dem Flug herausschlagen können.

Die Rohaufnahmen der Kameras findet ihr hier:

* [Seitliche Kamera](https://www.youtube.com/watch?v=PSj2esZwsDc)
* [Untere Kamera](https://www.youtube.com/watch?v=kdxX4-znLWA)

Und auch das ganze Abenteuer mit Drohnenaufnahmen könnt ihr jetzt
[hier auf Youtube](https://www.youtube.com/watch?v=DWXXfzSEbkE) in einem spätakulären 6-minütigen Spielfilm neu erleben.

#### Wie hoch ist das alles denn?

Wir schätzen, dass der Ballon am höchsten Punkt etwa 36km über dem Erdboden geschwebt ist.

Wenn du einen sehr schönen Einblick in die Entfernungen und Höhen in der Atmosphäre und dem Himmel anschauen möchtest,
können wir dir den [Space Elevator](https://neal.fun/space-elevator/) von [neal](https://neal.fun) empfehlen.

### Tabata-Workouts

Auf dem Camp haben wir zwei eskalative Tabate-Workout-Runden gemacht. Tabata ist eine HIIT (high intensity interval
training) - Trainingsmethode, die den Körper auf außerordentliche Weise belastet und bei regelmäßiger Durchführung
gerade im Herz-Kreislauf-System deutliche Verbesserungen erzielt. Eine Analyse hat herausgefunden, dass 4 Minuten
Tabata ungefähr so effizient für die Ausdauer ist, wie eine 20-minütige Laufeinheit mit 70% der maximalen Herzfrequenz.
Wichtig beim Tabata ist, dass man die Übungen zwar mit voller Intensität, aber trotzdem technisch korrekt durchführt
und zudem die Grenzen seines Körpers beachtet (Verletzungsgefahr!). Um diese volle Intensität zu erreichen, gibt es sehr
motivierende Musik. Die Tabata-Playlist vom diesjährigen MC könnt ihr [hier](https://spotify.link/hhxkkz4kCDb) finden.

### Völkerball / Dodgeball

Auf dem Camp haben wir dieses Jahr das sehr beliebte Völkerball aufgrund der Enge in der Halle durch das vermutlich noch
beliebtere Dodgeball ersetzt. Tatsächlich ist dies eine völlig eigenständige Sportart, in der es sogar
Weltmeisterschaften und Ligen gibt. Wer sich ein paar Tricks abschauen möchte, kann dies
[hier](https://www.youtube.com/watch?v=Spu6OlAZHUo) tun.

### Zauberwürfel

Die Anleitung zum Lösen eines 3x3 Zauberwürfels findest du auf [Tims
Website](https://timbaumann.info/zauberwuerfel).

## Mathecamp Organisation

Damit das Mathecamp reibungslos ablaufen und für die Zukunft verbessert werden kann, stehen die Bereuer:innen in
ständigem Kontakt zueinander. Dank eurer Hilfe konnte die Mail, die im Anschluss an das Camp versendet wurde, auf ein
neues Sprachniveau gehoben werden. Damit ihr an unserem sprachlichen Fortschritt teilhaben könnt, folgt eine gekürzte
Version von Orga-Mail VII:

<div class="highlight-box">

{{< comment "<!-- markdownlint-disable-next-line MD001 MD022 -->" >}}
#### Betreff: Es war uns ein Fest

Yo ihr Swiffer was crunsht?🧹🤔

Unbelievable, dass das Camp schon wieder mehr als eine Woche vorbei ist. Auch dieses Jahr war es trotz der vielen
Krankheitsfälle -- Wallah Krise! -- wieder überlegen! Vielen Dank für euren unbeschreiblichen Einsatz, real rap!
Ohne euch wäre das nicht möglich gewesen -- SIUUUUUUUUU!!!
Wir hoffen, ihr seid inzwischen wieder double am rizzen. Denn die nächste Orgamail wartet auf euch, meine Kerl:innen:

##### Fristen📆

* 09\. September
    * Terminumfrage Nachbesprechung
    * Bilder hochladen
    * Rechnungen abgeben
* 2 Tage vor der Nachbesprechung
    * Infos auf dem Feedbackpad eintragen

##### Cleanup: Say Bye-bye to Data🗑️

Wenn ihr noch irgendwelche Namen von Kiddos in Bits und Bytes oder analog rumfliegen habt, haut die Dinger weg!🤯🔥
Aber schreddern und nicht einfach wegschmeißen!📇 Kommt rum, wenn ihr keinen habt, wir helfen euch,
das Zeug zu vernichten!🤝💥

##### Nachbesprechung🗣️

Nach dem Camp ist bekanntlich vor dem Camp, whoop whoop🎉 Damit das Camp nächstes Jahr ein noch größerer Turn-Up wird,
machen wir eine Nachbesprechung. Natürlich via Zoom, denn wir sind voll "tinderjährig" und halten das online ab.
Hier ist der Link zur Dudle (Deadline: 09. September), damit wir den besten Termin finden können: [Link]

Damit unser Gelaber nicht in einer NoSleep-Sleep-Over-Party endet, sammeln wir die Themen (Feedback, Kritik,
Verbesserungsvorschläge, andere Diskussionspunkte) bereits im Vorfeld. Deadline: zwei Tage vor der Nachbesprechung🔔
[Link]

##### Schön-War's-Website🥰

Wie immer ballern wir 'ne freshe Seite für alle die am Start waren raus, um die heißesten Vibes vom Camp abzufeiern.
Dafür brauchen wir eure wylden Aktivitäten, krassen Zirkel-Actions, alles, was bei euch sonst noch abging und das Ganze
am besten mit Links oder anderem Stuff, der das noch krasser macht🎉 Aber passt auf, dass ihr keine privaten Infos
von den Kiddos dropt, das ist tabu, man.
Hier ist der Deal: Hau den Stuff auf dieses Pad, Leute! [Link]

Wenn ihr wissen wollt, wie wir das im letzten Jahr gerockt haben, checkt das hier: [Link]

##### Fotos🌅

Lasst die Kamera rollen und bringt den Swag online! Zusammen mit der Schön-War's-Website erhalten die Teilnehmer:innen
auch eine Fotogalerie. Ihr habt  fette Shots auf dem Camp gemacht?📸🔥 Dann haut die Dinger bis zum 09. September
in die Cloud!

##### Zirkelstuff📝

Also, um sicherzustellen, dass auch die Betreuer:innen in den nächsten Jahren was von eurer Arbeit haben, gönnt
der Cloud eure Sachen.
Das muss absolut nicht fancy sein, ihr könnt ruhig eure handgeschriebenen Notizen abfeuern.
Hauptsache, man kann peilen, was da abgeht.
Am besten noch 'ne kleine Memo dazu, was hier fly oder cringe war, damit wir alle davon lernen können😉✍️

##### Rechnungen📉

Fette Rechnungen vom Camp werden noch bis spätestens Samstag, 09. September von den Oberorgababos entgegengenommen.
Kein wyldes Rumoxidieren hier, wir brauchen diese Infos ASAP, sonst wird's kritisch💸💥

##### After-Party🎉

Nach dem Camp ist vor dem Rave, dieses Jahr machen wir 'n fettes Nachtreffen mit unseren 12. Klässler:innen,
die dieses Jahr zum letzten Mal am Start waren🎓👋

##### Lost and Found🕵🏼

Es ist wie immer einiges liegen geblieben. In der Cloud findet ihr Fotos von den Fundsachen.
Vielleicht ist ja euer stuff dabei👀📸

Und jetzt, ihr ~~N~~PCs👤, let's fetz!🫶🏼

Louisa, David und Felix

</div>

## Abschluss und Mathecamp 2024

Schön, dass du bis hierher durchgehalten hast! Vermutlich hätten wir mit den zusätzlichen Materialien
auch noch ein Camp füllen können :-)

Apropos, das Mathecamp 2024 findet vom 17. bis 25. August 2024 statt. Wir freuen uns schon auf dich!

<div class="centering">
    <figure>
        <img src="gregor-mc-23.svg" width="500" alt="Gregor Jubiläum 10 Jahre">
    </figure>
</div>
