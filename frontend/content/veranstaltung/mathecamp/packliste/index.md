---
title: Packliste
---

## Bitte bring unbedingt folgende Dinge mit

* Einzunehmende Medikamente
* Krankenversicherungsnachweis (z.B. Gesundheitskarte)
* 3-teilige Bettwäsche (Kissen- und Deckenbezug und Bettlaken)
* Kulturbeutel und Handtuch
* Kleidung (auch für Freizeit, Sport, Wandertag und kalte Tage)
* Tüte für Schmutzwäsche
* Hausschuhe
* Trinkflasche aus Plastik, die mit deinem Namen beschriftet ist
* Mäppchen mit Stiften, Geodreieck und Radierer
* Block oder Heft zum Mitschreiben
* Sonnencreme
* Für den **Wandertag**:
    * Kleiner Rucksack
    * Kopfbedeckung
    * Brotzeitdose für Lunchpaket
    * Passendes Schuhe

## Gerne kannst du freiwillig mitnehmen

* Geld für Getränke (Wasser und Tee gibt es kostenlos vor Ort)
* Musikinstrument (insbesondere falls auf der Anmeldung angegeben; Klaviere und Flügel sind vorhanden), Notenständer
* Briefmarke (für eine Mathecamp-Postkarte)
* Taschenlampe
* Sportschuhe für draußen (mit Profil) und Hallenschuhe
* Handtuch, das du draußen verwenden kannst
* Wasserpistole
* Tischtennisschläger
* Handy für den Wandertag
