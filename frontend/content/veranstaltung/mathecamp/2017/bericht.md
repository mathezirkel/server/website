---
title: Mathecamp 2017 - Betreuerbericht
---

Am Freitag, den 18.08.2017 war es soweit. Die Planungen und alles, was im Vorfeld war, wurde endlich
Realität. Ein paar komisch aussehende Gestalten versammelten sich wild umherirrend an der
Universität in Augsburg. Es war ein normaler Tag und keiner wusste, wieso das passiert. Außer
natürlich alle, die vom Mathecamp wussten. Nach anfänglichen Schwierigkeiten und dem wilden
Ausdrucken von Materialien und Listen, Erstellen von Urkunden, Einkaufen von lebensnotwendigen
Dingen und Sortieren von Materialien begann das Mathecamp. Mehrere Menschen trugen komische
weiße Boxen die Treppe runter und müllten damit mehrere Autos gleichzeitig zu. Die Materialien für
neun Tage Campirsinn waren gepackt und drei Betreuer, Max, Sven und Dominik machten sich auf den
Weg nach Violau, um das Bruder-Klaus-Heim auf seine Aufgabe vorzubereiten. 118 Kinder und
insgesamt 30 verschiedene Betreuer sollten in den kommenden neun Tagen stressfrei und entspannt
auf dem Mathecamp gemeinsam Spaß haben. Das will gut vorbereitet sein. Nach der Ankunft und dem
„Entmüllen“ der Autos ging es an den Rundgang durchs Haus. Immerhin waren die drei vorzeitig
angereisten Betreuer auch schon ein Jahr nicht mehr hier gewesen. Nach dem Rundgang und einem
leckeren Abendessen ging es darum, Zimmer einzuteilen, Zirkelgruppen zu erstellen und auszuhängen
und Ankunftslisten für den nächsten Tag vorzubereiten. Nach mehreren Stunden intensiver Arbeit ging
es doch viel zu spät ins Bett.

Bereits um 7:30 Uhr von atemberaubend guter Musik geweckt, waren die drei bereits übermüdet und
kaputt. Doch die Freude über die Ankunft der Kinder machte selbst die müdesten Männer munter und
so wurde noch schnell alles Restliche vorbereitet und ein Teaser-Video für die krassen
Mathecampshirts aufgebaut und in Dauerschleife abgespielt. Gerade rechtzeitig, denn dann begann
das große Kofferschleppen für die Betreuer. Kinder kamen pausenlos an, checkten ein und wollten
direkt auf ihr Zimmer. Gefühlte 3000 Stufen und mehrere total erschöpfte Betreuer später war es
vollbracht. Alle Kinder waren auf ihren Zimmern und bereit dafür neun Tage gemeinsam zu verbringen.
Das Programm begann.

Mit einer Welcome-Here-Rede von Sven startete das Mathecamp. Er erklärte kurz den Tagesablauf
und einige Hausregeln und schon war es Zeit, das erste Mittagessen einzunehmen. Ein Chaosspiel im
Anschluss sorgte dann für ersten Kontakt der Schüler und für viel Spaß bei allen Teilnehmenden.

Dann ging es das erste Mal nach Klassenstufen getrennt in die Zirkelräume. Ein Willkommenszirkel
stand an und die Kinder konnten gleichaltrige sofort kennenlernen. Alle Kinder waren bereit, einen
vorgefertigten Regelzettel zu unterschreiben und verpflichteten sich damit, alle Regeln einzuhalten.

Nach den Kennenlernzirkeln standen nur noch das Abendessen und die ersten musikalischen Treffen
auf dem Programm. Der Rest war damit beschäftigt, das erste Mal Fußball und Frisbee zu spielen oder
einfach das Haus kennen zu lernen. So endete der erste Tag mit vielen ereignisreichen Aktionen.

Für die folgenden Tage wurde nun auch die offizielle Pinnwand eröffnet. An dieser Pinnwand fanden
sich alle wichtigen Dinge für den Tag und das Camp. Es wurden freiwillige mathematische
Nachmittagszirkel genauso angekündigt wie sportliche Aktionen, Nähen, Armbänder basteln,
Musizieren oder andere noch viel bessere Freizeitaktionen. Doch bevor man sich mit den
Freizeitaktionen beschäftigen konnte, standen ein großes Frühstücksbuffet und das allmorgendliche
Plenum auf der Tagesordnung. Frisch gestärkt waren alle Kinder bereit, die serious-Ansagen von Max
in sich aufzusaugen und zu beherzigen. Es ging dabei vor allem um Verhaltensregeln und wichtige
Ansagen für das Campleben. Im not-so-serious-Teil war die Zeit für Späße und weniger ernste Ankündigungen.
Es wurden vor allem die Freizeitaktivitäten vorgestellt oder neue Betreuer
willkommen geheißen. Direkt im Anschluss ging es in die verpflichtenden Vormittagszirkel, in denen
nach Jahrgangsstufe getrennt und in noch kleinere Gruppen eingeteilt mathematisch passende
Themen behandelt und erklärt wurden. So lernte die fünfte Klasse beispielsweise das Weltall kennen
oder kümmerte sich um Binärzahlen und das Josephus—Problem. Die Älteren dagegen stiegen tiefer
in die Mathematik ein und lernten Beweisschemata und andere Zahlensysteme kennen. Nach zwei
dieser doch recht anstrengenden Zirkel gab es Gott sei Dank die nächste kulinarische Stärkung aus der
Küche. Das Mittagessen stand an. Es gab täglich eine andere Suppe, einen Salat und eine Hauptspeise
und einen Nachtisch. Die Kinder waren für das Tischdecken und –abräumen (btw. Ist das eine sehr sehr
tolle Strafe für Fehlverhalten) selbst zuständig. Nach dem Mittagessen und einer kurzen Mittagspause
standen dann die sog. Fraktivitäten auf dem Plan. Es gab ganz vielseitige Angebote und Themen, zu
denen man gehen oder die man wahrnehmen konnte. Das Angebot ging dabei vom Fußboden-Power-
Napping über mathematische Themen, physikalische Experimente mit Kondensator-Mikrophonen,
Programmierwettbewerben und –tutorials, Vorträgen, sportliche Aktivitäten (Slackline, Schwimmen,
Tischtennis, Fußball, Frisbee) oder auch ruhigere Dinge wie Nähen, Lesen, Spiele spielen oder oder
oder. Jeder Betreuer bot dabei das an, was er besonders gut kann. Nach den anstrengenden Dingen
brauchten viele Kinder dann noch eine Stärkung. Diese gab es dann immer beim Nachmittagskuchen.
Nach dieser Stärkung waren alle bereit für neue Dinge und so konnten weitere Fraktivitäten
wahrgenommen werden. Vor allem die freiwilligen Mathematikthemen lagen den Betreuern am
Herzen. Mal besser, mal weniger gut besucht wurden dort vor allem Themen behandelt, die sich Kinder
gewünscht hatten oder die etwas tiefer in die Mathematik einstiegen. Nach dem Abendessen gab es
dann gegen 21 Uhr noch Gute-Nacht-Geschichten. Damit endeten die aufregenden Camptage und man
konnte gut zur Ruhe kommen. Für die 5./6. Klässler war dann um 21:30 Bettruhe angesagt, für die 7. –
9\. Klasse um 22:00 Uhr.

Für all diejenigen, die es garnicht erwarten konnten, den Camptag zu beginnen, gab es ein ausuferndes
Frühsportangebot. Dabei war uns dieses Jahr vor allem die Vielfalt wichtig. Es sollte für jeden etwas
dabei sein und das haben wir ganz gut hinbekommen. Das Frühsportangebot begann eine Stunde vor
der eigentlichen Weckzeit mit dem Weckdienst der Betreuer. Diese gingen in alle Zimmer, die an
Frühsport interessiert waren, um die entsprechenden Menschen zu wecken. Um 7 Uhr teilten sich
dann die Gruppen auf. Es gab Todesübungen, Jogginggruppen, Schwimmen, Bootfahren, Slacklining,
Frisbee, uvm. Und so waren es immer recht kleine Gruppen, die die jeweilige Sportart ausführten. Den
Betreuern, die sich nach den nächtlichen Teamsitzungen und Vorbereitungen mehr aus dem Bett
quälen mussten, war es aber ebenfalls ein guter und erfrischender Start in den Tag und mit der
morgendlichen Anstrengung war der restliche Tag mehr Erholung als Stress. Die Kinder waren
rechtzeitig zum Frühstück frisch geduscht und konnten somit entspannt mit den restlichen
Teilnehmern in den Tag starten.

Die Tage im Camp ähnelten sehr diesem beschriebenen Tag. Es gab jedoch auch Special-Things.

So wurden von Kindern selbst mehrere Wettbewerbe (Malwettbewerb, Tischtennisturniere,
Kreativwettbewerb) organisiert. Diese Wettbewerbe stellten immer ein Highlight im Campalltag dar,
da Teilnehmer für Teilnehmer aktiv wurden.

Zudem gab es vor allem für die größeren Teilnehmer jeden Abend ab 21:00 Uhr verbotene
Geheimzirkel. Doch wie der Name schon sagt, bleibt es für immer ein Geheimnis, was dort passiert.
Gehalten werden diese Zirkel jedes Jahr von Ingo, der sich mit interessierten Teilnehmern an einen
Tisch setzt und über ein vorher bekanntgegebenes Thema spricht. Eine einmalige Institution, die
immer sehr sehr gut besucht ist.

Der aufregendste Tag ist sicherlich immer der Wandertag. Dieses Jahr war es am Mittwoch so weit. Die
Kinder trafen sich nicht in ihren Zirkeln sondern es ging auf eine Wanderung um Violau herum. Das Ziel
war etwa. 8,5 km entfernt. Ein Spielplatz mit angrenzendem Sportplatz und natürlichem Kneippbecken
wurde vom Wander-Orgateam ausgewählt. Die Kinder waren vielleicht nicht gerade begeistert, so weit
wandern zu müssen, das Ziel entschädigte aber doch für die Qualen, die sie unterwegs erleiden
mussten. Christoph Mayer, der Heimleiter, brachte uns zu unserer Mittagsstätte ein warmes Essen,
Obst, Salat und Getränke. Nach einem dreistündigen Aufenthalt ging es dann wieder zurück nach
Violau. Auf dem Weg zurück spaltete sich die Gruppe in zwei Teile. Ein Teil nahm den direkten kurzen
Weg zurück. Der zweite Teil verlängerte die Wanderung ein wenig und machte gewisse Umwege, bevor
sie dann zurück zum Heim ging. Ein wie jedes Jahr schöner Ausflug, um auch mal vom Bruder-Klaus-
Heim wegzukommen. Am Abend des Wandertags standen auch schon traditionell dann noch zwei
Vorträge auf dem Programm. Wie in den Jahren zuvor konnten wir zwei externe Vortragende nach
Violau einladen, die für jeweils die passende Altersstufe (5. - 8. Klasse und 9. – 12. Klasse) einen
mathematischen Vortrag hielten. Bei den jüngeren ging es um Didos Problem und den Kayeka-Fisch,
die älteren wurden in die allgemeine (und später auch die spezielle) Relativitätstheorie eingeführt. An
dieser Stelle nochmal ein großes Dankeschön an die zwei Gastredner/innen Prof. Lisa Beck und Prof.
Marc Nieper-Wißkirchen für ihr Engagement und ihre tollen Vorträge. Nach den Vorträgen fielen dann
die meisten Kids ziemlich erschöpft in ihr Bett. Die Betreuer hatten noch nie so viel Freude beim
Durchsetzen der Nachtruhe. Vor allem als Kinder völlig verschlafen auf den Gang traten, um das Licht
auszumachen, waren die Kontrolleure beruhigt.

Am Donnerstag gab es direkt das nächste Highlight. Am Abend blieb die Küche kalt, aber nur, weil wir
an anderer Stelle unser Essen bekamen. Das Bruder-Klaus-Heim verfügt über einen echten Holzofen,
in dem am Donnerstagabend Pizza gebacken wurde. Den Kindern und Betreuern schmeckte diese
vorzüglich und alle waren glücklich, dass man dieses Angebot warnehmen konnte.

Der Night-Cache-Freitag begann dann eigentlich zunächst wie ein normaler Tag. Am Nachmittag gab
es eine Poolparty mit Musik und Spaß im eiskalten Pool. Doch eine Sache war anders als sonst. An
unserem Fraktivitätenboard stand etwas von einem Night-Cache. Die Kinder waren verwundert. Davon
wussten wir ja garnichts! Manche fragten sich auch, was ist überhaupt ein Night-Cache. Die Betreuer
erklärten, dass es anstatt der Nachtwanderung dieses Jahr einen Night-Cache gibt. Denn die
Nachtwanderung wurde uns angeblich aus vielen Gründen verboten. Die Kinder freuten sich über das
Aussetzen der Nachtruhe (so ein Night-Cache geht ja schließlich nur, wenn es komplett dunkel ist) und
standen dann pünktlich zum Treffpunkt parat. Es ging in kleinen Gruppen los und ins Gelände. Hmm,
komisch, der Weg ist nur mit Knicklichtern beleuchtet. Was ist das hier?...Vielen Kindern war schon
klar, dass der Night-Cache vielleicht doch eher eine Nacht-Grusel-Wanderung sein wird und waren
teilweise dementsprechend ängstlich. Es ging durch einen dunklen Wald, der von den ältesten
Teilnehmern (10./11./12. Klasse) dementsprechend präpariert wurde. Nach den ersten Geistern und
Zombies auf unserem Weg trafen wir auf einen geheimen Geschichtenerzähler. Dieser erzählte uns
eine Legende über einen Mord im Bruder-Klaus-Heim. Dann ging es, für ein paar noch ängstlicher
weiter. Der Trau-Dich-Pfad wurde für einige Kinder zu einer echten Mutprobe. Alleine, getrennt von
der Gruppe, nur ausgestattet mit einem Knicklicht und einer Hand an einem Seil sollten die Kinder ein
Stück durch den Wald laufen. Das haben jedoch alle Kinder mit Bravur gemeistert, auch wenn sich der
ein oder andere ganz schön fürchtete. Am Ende kamen aber alle Kinder ohne Probleme oder bleibende
Eindrücke gut am Bruder-Klaus-Heim an und verarbeiteten das gerade Erlebte am Lagerfeuer mit
Gitarrenmusik und Gesang.

Der Samstag war gekommen. Der Letzte Tag des Camps. Es machte sich aber überhaupt keine
Abschiedsstimmung breit. Dafür war keine Zeit. Es standen noch einmal Zirkel und Fraktivitäten auf
dem Programm und abends war ja auch noch der große bunte Abend angekündigt. Jede Klassenstufe
überlegte sich hierfür einen Programmpunkt. Es gab „1,2 oder 3“, Pantomime, der verrückte
Wissenschaftler, ein Karaoke-Imitationswettbewerb und noch vieles mehr. Ein bunt gemischtes, von
den Klassenstufen sehr sehr gut vorbereitetes Programm machte den Abend zu einem würdigen
Abschluss. Zusätzlich gab es Auftritte des Orchesters, des Chors, ein Sketch der Betreuer und die Stop-
Motion-Filmprojekte wurden vorgeführt. Ein dreistündiges Programm kam dabei heraus und jeder der
Teilnehmer hatte Spaß. Nach dem sehr sehr langen offiziellen Programmteil war es natürlich nicht
möglich, die Kinder gleich ins Bett zu schicken. Unter der Prämisse, dass alle direkt nach dem bunten
Abend noch packen, wurde die Nachtruhe auf 1 Uhr verschoben. Damit den Kindern in dieser Zeit nicht
langweilig wurde, wurde eine Disko auf die Beine gestellt, in der alle gemeinsam Tanzen, Musik hören
und Spaß haben konnten. Der letzte Abend endete damit auch für alle. Naja Fast alle. Eine Gruppe von
älteren Schülern und ein paar Betreuern ist nachts noch zu einer Joggingrunde aufgebrochen. Nun wird
überlegt, zusätzlich zum Frühsport ganz regulär Abendsport anzubieten.

Am Sonntag war großer Aufräum- und Abreisetag. Die Zirkelgruppen waren für das Aufräumen ihrer
Zirkelräume selber verantwortlich, die Betreuer mussten alle Materialien verräumen und verladen und
das gesamte Bruder-Klaus-Heim in Ordnung bringen. Neun Tage Spaß, Action, Lernen, Mathematik,
Musik, Ruhe, Sport, gutes Essen, Gemeinsamkeit, Stress, wenig Schlaf, (hier weitere krasse Begriffe
einfügen) gingen zu Ende.

Uns Betreuern war es eine große Ehre, die Zeit mit euch zu verbringen. Es gab sehr sehr viele schöne
Momente, die wir gemeinsam genießen konnten. Wir hatten super viel Spaß mit euch, ihr wart eine
super(große) Truppe. Die Verschiedenheit aller Teilnehmer macht es jedes Jahr zu einem großen
Erlebnis, diese neun Tage in Violau verbringen zu können. Wir freuen uns schon (mit euch) aufs nächste
Jahr und hoffen, dass es euch genauso gefallen hat, wie uns.

Bis nächstes Jahr und einen guten Start in die Schule oder ins Studium/ in eine Berufsausbildung
wünschen euch

Die Betreuer und Organisatoren des Mathecamps 2017
