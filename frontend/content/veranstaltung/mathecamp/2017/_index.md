---
title: Mathecamp 2017
---

## Rückblick aufs Mathecamp 2017

<div class="highlight-box">
19. bis 27. August 2017

118 Teilnehmende

27 Betreuende

9 wunderschöne Tage!

</div>

## Betreuerbericht

[Hier](./bericht/) findet ihr einen Bericht vom Camp.

## Materialien zu manchen Zirkeln und Fraktivitäten

### 4D-Vortrag von Matthias und Ingo

Vortragsfolien: [https://rawgit.com/iblech/mathezirkel-kurs/master/vierdimensionale-geometrie/slides-33c3.pdf](https://rawgit.com/iblech/mathezirkel-kurs/master/vierdimensionale-geometrie/slides-33c3.pdf)

Aufzeichnung (auf Englisch): [https://www.youtube.com/watch?v=ct0\_g1amEpw](https://www.youtube.com/watch?v=ct0_g1amEpw)

Vierdimensionales Mutterfraktal des Apfelmännchens und aller Juliamengen: [https://rawgit.com/MatthiasHu/FractalsWebGL/4d/page.html](https://rawgit.com/MatthiasHu/FractalsWebGL/4d/page.html)

Haskell-Code: [https://github.com/MatthiasHu/4d-solids-brutal](https://github.com/MatthiasHu/4d-solids-brutal)

Englischer Vortrag von Matt Parker: [https://www.youtube.com/watch?v=1wAaI\_6b9JE](https://www.youtube.com/watch?v=1wAaI_6b9JE)
(Things to See and Hear in the Fourth Dimension)

Spiele: [http://miegakure.com/](http://miegakure.com/), [http://4dtoys.com/](http://4dtoys.com/)

### Alienzirkel (Cosmic Call)

Im Jahr 1999 sendete die Menschheit eine Radiobotschaft an ausgewählte Sterne, in der Hoffnung,
dass eine dort lebende Zivilisation die Nachricht empfangen, verstehen und auf sie antworten würde: die Cosmic-Call-Botschaft.
Diese Nachricht wurde natürlich nicht auf Deutsch oder Englisch formuliert, sondern bedient sich einer mathematischen Symbolsprache.
Im Zirkel unterzogen wir die Botschaft einem Test: Können wenigstens wir Menschen, die wir einen riesigen Heimvorteil haben,
die Nachricht entziffern?

Ihr könnt die Nachrichten und Erklärungen unter [Materialien](/materialien/) nochmal ansehen.

### Bar- und QR-Codes

Barcodes: [https://de.wikipedia.org/wiki/European\_Article\_Number](https://de.wikipedia.org/wiki/European_Article_Number)

QR Codes Lesen grob erklärt: [https://www.youtube.com/watch?v=yiLjWBfQyF4&index=8&list=WL](https://www.youtube.com/watch?v=yiLjWBfQyF4&index=8&list=WL)

QR Code Bilder: [https://en.wikipedia.org/wiki/QR\_code](https://en.wikipedia.org/wiki/QR_code) -
fürs Übersetzen der Binärzahlen sucht euch im Internet eine ASCII Tabelle mit Binärdarstellung,
z.B. hier: [http://www.rapidtables.com/code/text/ascii-table.htm](http://www.rapidtables.com/code/text/ascii-table.htm)

### Dynamische Labyrinthe

Die dynamischen Labyrinthe ("Maschinen bauen") könnt ihr auch am Computer ausprobieren:

[http://www.ikm.uni-osnabrueck.de/aktivitaeten/dl/dynamische-labyrinthe.htm](http://www.ikm.uni-osnabrueck.de/aktivitaeten/dl/dynamische-labyrinthe.htm)

Mit Händen zu experimentieren macht natürlich mehr Spaß;
in den Präsenzzirkeln während des Schuljahrs habt ihr dazu bei uns die Möglichkeit!

### Haskell

Haskell ist eine rein funktionale Programmiersprache.
Matthias, Ingo und Xaver sowie Felix empfehlen sie mit Nachdruck all denen,
die schon mit anderen Programmiersprachen vertraut sind.
Ein guter Einstieg ist das kostenlose Buch [http://learnyouahaskell.com/chapters](http://learnyouahaskell.com/chapters).

### Knotenmemory

Hier gibt es die Datei zum Selberausdrucken und Spielen des Knotenmemories:
[http://www.speicherleck.de/iblech/stuff/knotenmemory-einfach.pdf](http://www.speicherleck.de/iblech/stuff/knotenmemory-einfach.pdf)
Die schwierigere Version gibt es noch nicht digital, wird aber auf Anfrage gerne gescannt :-)

### p-adische Zahlen: Die Welt aus den Augen einer Primzahl

[https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2017/10-adische-zahlen/10-adische-zahlen.pdf](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2017/10-adische-zahlen/10-adische-zahlen.pdf)

### Pi-Kärtchen

Zum selbst ausdrucken: [http://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/pi-lernen.pdf](http://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/pi-lernen.pdf)

### Seekers

Auf Windows-Computern installiert man Seekers in drei Schritten wie folgt:

1. Anaconda von [https://www.anaconda.com/download/](https://www.anaconda.com/download/) herunterladen und installieren
  (Wichtig: Version 3.x, nicht 2.x)
2. Aus dem Startmenü heraus den "Anaconda Command Prompt" öffnen und den Befehl `pip install pygame` eingeben.
3. Seekers selbst von
  [https://github.com/MatthiasHu/seekers/archive/master.zip](https://github.com/MatthiasHu/seekers/archive/master.zip)
  herunterladen und irgendwo entpacken.

Man startet das Spiel dann, indem man wie in Schritt 2 den "Anaconda Command Prompt" öffnet
und in dem sich öffnenden Fenster eingibt:

```shell
python c:\\...\\seekers.py c:\\...\\meine-ki.py c:\\...\\deren-ki.py
```

Diesen Befehl kann man leider nicht wortwörtlich so abtippen, denn statt der Auslassungszeichen muss man
die vollständigen Dateipfade angeben.
Am einfachsten tippt man diese nicht selbst ein, sondern zieht die entsprechende Datei aus dem Dateimananger in das Anaconda-Fenster.
Dann wird automatisch der vollständige Pfad ergänzt.

### Turingmaschinen und Superturingmaschinen

* [https://de.wikipedia.org/wiki/Turingmaschine](https://de.wikipedia.org/wiki/Turingmaschine)
* [https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2016/turingmaschinen/aufgaben.pdf](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2016/turingmaschinen/aufgaben.pdf)
* [https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2017/superturingmaschinen/superturingmaschinen.pdf](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2017/superturingmaschinen/superturingmaschinen.pdf)

### Verbotene Geheimzirkel

* Quantenchemie: [https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2017/verbotene-geheimzirkel/quantenchemie.pdf](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2017/verbotene-geheimzirkel/quantenchemie.pdf)
* Löbs Paradoxon und Gödels Unvollständigkeitssatz: [https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2017/verbotene-geheimzirkel/loebs-paradoxon-und-goedel.pdf](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2017/verbotene-geheimzirkel/loebs-paradoxon-und-goedel.pdf)
* Effektiver Topos: [https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2017/verbotene-geheimzirkel/effektiver-topos.pdf](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2017/verbotene-geheimzirkel/effektiver-topos.pdf)
* Relativitätstheorie (geleitet von Peter U.): [https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2017/verbotene-geheimzirkel/relativitaetstheorie.pdf](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2017/verbotene-geheimzirkel/relativitaetstheorie.pdf)
* Mehr zu Superturingmaschinen und zum effektiven Topos: [https://rawgit.com/iblech/mathezirkel-kurs/master/superturingmaschinen/slides-warwick2017.pdf](https://rawgit.com/iblech/mathezirkel-kurs/master/superturingmaschinen/slides-warwick2017.pdf)
* Mehr zu Gödel: [http://rawgit.com/iblech/mathezirkel-kurs/master/thema11-goedel/skript.pdf](http://rawgit.com/iblech/mathezirkel-kurs/master/thema11-goedel/skript.pdf)

### Weltall

Super süße Erklärvideos zu Rosetta von der ESA:

* [https://www.youtube.com/watch?v=HD2zrF3I\_II](https://www.youtube.com/watch?v=HD2zrF3I_II)
* [https://www.youtube.com/watch?v=trljrwTbr4w](https://www.youtube.com/watch?v=trljrwTbr4w)
* [https://www.youtube.com/watch?v=s35Jlwobcqk](https://www.youtube.com/watch?v=s35Jlwobcqk)
* [https://www.youtube.com/watch?v=9pTd\_JHULBM](https://www.youtube.com/watch?v=9pTd_JHULBM)
* [https://www.youtube.com/watch?v=0YwrcZVM1MA](https://www.youtube.com/watch?v=0YwrcZVM1MA)
* [https://www.youtube.com/watch?v=AvkPFXdpOQQ](https://www.youtube.com/watch?v=AvkPFXdpOQQ)
* [https://www.youtube.com/watch?v=gDmp4ZhSuIc](https://www.youtube.com/watch?v=gDmp4ZhSuIc)

### Zauberwürfel

Die Anleitung zum Lösen des Zauberwürfels ist online: [http://timbaumann.info/zauberwuerfel](http://timbaumann.info/zauberwuerfel)

### Zirkel von Rolf

[https://www.youtube.com/watch?v=YXCmMS3r7pg](https://www.youtube.com/watch?v=YXCmMS3r7pg)
[https://www.youtube.com/watch?v=n9FFBXBccow](https://www.youtube.com/watch?v=n9FFBXBccow)

## Eine Liste aller Fraktivitäten

* 3D-Drucken
* 4D-Vortrag und -Spiele
* Armbänder flechten
* Bootfahren
* Bunter Abend
* Chaosspiel
* Chor
* Drohne
* Dynamische Labyrinthe
* Fußboden- / Tisch-Powernap
* Freiwillige Nachmittagszirkel
* Frisbee
* Frühsport
* Fußball
* Goile Schoische aka Kondensator Mikrofon AG
* Gute-Nacht-Geschichten
* Hexaflexagons
* Jonglieren
* Lamas
* Mathemagische Zaubertricks
* Meteorologie
* Nachtwanderung
* Nähen
* Orchester
* Origami
* Pantomime
* Papierfliegerwettbewerb
* Pizzabacken
* π lernen
* Pool
* Schachlaufen
* Seekers
* Slacklinen
* Stickbombs
* Tanzen
* Tischtennis
* Verbotener Geheimzirkel
* Vortrag von Marc
* Vortrag von Lisa
* Wanderung
* Werwolf / Mafia
* Wettbewerb zu größten Zahlen
* Zauberwürfel
* Zettelmordspiel
