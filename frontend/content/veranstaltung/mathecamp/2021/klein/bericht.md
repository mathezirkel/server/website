---
title: Mathecamp 2021 - Betreuer:innenbericht
---

## Ein Betreuer:innenbericht

Gerade ist das Mathecamp 2021 mit der zweiten Hälfte und den Klassenstufen 5 – 8 zu Ende gegangen.
Grund genug, für die zweite Hälfte einen Bericht zu schreiben.

Am Mittwoch, den 18.08 ging es für 72 Kinder und 12 Betreuende los.
Mit negativem Test und der Selbstauskunft im Gepäck machten sich die Kinder entweder mit dem Mathezirkelbus von Augsburg
oder aber privat auf den Weg nach Violau zum Bruder-Klaus-Heim, um 5 spannende Tage zu erleben.

Nach der Ankunft und den aufgeteilten Begrüßungsplena ging es für alle Teilnehmenden zunächst zum Abendessen.
Bei einem leckeren Vesperbuffet blieben keine Wünsche offen.
Nach dem Abendessen lernten sich dann die Kids gegenseitig in ihren Zirkeln kennen.
Die Mathezirkel sind ein wichtiger Teil des Mathecamps und finden immer vormittags zweimal statt.
In den Zirkeln werden mathematische Themen von den Betreuenden aufgearbeitet und vorgestellt.
Dabei gilt eigentlich immer: Wir vermitteln keine Schulmathematik, sondern spannende Themen aus dem Alltag,
der grundlegenden Theorie, der Geschichte oder auch aus angrenzenden Bereichen wir Physik oder Informatik.
Der Kennenlernzirkel beinhaltet dabei meist keine oder nur sehr wenig Mathematik.
Die Kinder haben die Regeln kennengelernt und sicherlich auch verstanden.

Die folgenden Tage brachten allen Kindern viel Spaß.
Eine ausführliche Tagesbeschreibung, wie die Tage genau abliefen gibt es im Anschluss.
Ein weiteres Highlight dieses Jahr war sicherlich der Wandernachmittag, der als kleine Wanderrallye durchgeführt wurde.
Aufgrund der Abstands- und Coronaregelungen mussten wir den Wandernachmittag in kleineren Gruppen durchführen.
Und diese Not machte erfinderisch.
Mit vielen Fragen, die die Gruppen nur durch aufmerksames Umhersehen auf dem Weg beantworten konnten,
wurde die Aufmerksamkeit und der Teamgedanke der Gruppe genauso gestärkt, wie auch durch die Aufgabe,
selbstständig den Weg anhand einer Beschreibung zu suchen.
An 5 Stationen war dann zusätzlich noch Geschick gefordert.
Beim „Kuli in Flasche einfädeln“ machten die Kids aber eine sehr gute Figur.
An der zweiten Station musste dann eine Nudel übergeben werden und zwar nur mit Hilfe von Spaghetti in den Händen der Kids.
Die dritte Station war danach „Würfel stapeln“ angesagt, bevor es an der vierten Station darum ging,
sich möglichst viele Begriffe in einer vorgegebenen Zeit einzuprägen.
Ein Team schaffte an dieser Station sogar, sich die komplette Liste einzuprägen und diese im Anschluss wiederzugeben.
Die letzte Station war ein „Körperteile-Raten“.
Betreuende machten Bilder von Körperteilen und die Kids mussten diese zuordnen.
Mit kleinen fiesen Kniffen und Tricks wurden die Kids dabei aber auf die falsche Fährte gelockt.
So tauschten zwei Betreuende zum Beispiel die Brille und ein männlicher Betreuer ließ sich die Fingernägel lackieren.
Am Ende der Rallye wurde das Ergebnis ausgewertet und die besten Teams erhielten am letzten Tag einen Preis.

Ein weiteres, besonders erwähnenswertes Highlight dürfte für die Kids der letzte Abend gewesen sein.
Hier wurde die Nachtruhe auf 24:00 Uhr ausgeweitet sowie ganz besondere Fraktivitäten angeboten.
Der Abend startete mit der Poolparty um 19:00 Uhr, der sogar eine Live-Band beiwohnte.
Auch wenn die Musik nicht ganz den Geschmack der Kids getroffen hat, so wurde am Ende anständig applaudiert.
Neben Freestyle-Tanzen, einem Mathecamp-Quiz, Powerpoint-Karaoke und ausufernden Kegelsessions
wurden auch mathematische Inhalte angeboten und von den Kids eifrig besucht.
Um 21:30 Uhr gab es dann im Himmelreich noch ein Kino, an dem die Kids teilnehmen konnten.
Da das Alternativprogramm für die meisten aber sehr gut war, war dieses Kino sogar recht schwach besucht – was wiederum
für die tolle Vielfalt der anderen angebotenen Fraktivitäten spricht.
Der Abend wurde am Ende noch mit einer Nachtwanderung abgerundet.
Hundemüde, aber sehr glücklich waren jedenfalls die meisten Kids, als wir sie um 24:00 Uhr auf ihren Zimmern besuchten
und die Nachtruhe kontrollierten.

Am letzten Tag fanden nichtsdestotrotz ganz normal die Mathezirkel statt.
Nach dem Mittagessen wurde dann gepackt, so dass die Kids pünktlich um 15:45 Uhr mit dem Bus oder ab 16:00 Uhr
mit ihren Eltern abreisen konnten.
Leider war das Mathecamp somit schon wieder viel zu schnell um.

Die Zeit war wie jedes Jahr hammerschön für uns Betreuende.
Es waren auch für uns super anstrengende 5 Tage und 4 Nächte, jedoch hat sich jede Sekunde gelohnt.
Auch oder gerade in diesen doch recht aufwühlenden Tagen macht es umso mehr Spaß, gemeinsam Zeit mit den Kids zu verbringen
und ihnen ein Lächeln - wenn auch des Öfteren hinter einer Maske - entlocken zu können.
Wir Betreuende jedenfalls hatten eine supercoole Zeit und freuen uns schon auf das MC2022 – dann hoffentlich wieder 9-tägig
und mit allen Klassenstufen.
Denn dann – das versprechen wir euch – werdet ihr sagen: Das Mathecamp 2021 war schon cool, 2022 toppt das aber nochmal
um Längen.

Wir wünschen euch allen jedenfalls noch schöne Ferien, erholt euch gut und dann einen guten Start ins neue Schuljahr.
Bleibt alle so wie ihr seid und vor allem gesund!

Euer Team vom Mathecamp 2021.2 sowie das Team vom Mathecamp 2021.1 und alle anderen vom Team des Mathezirkels
der Universität Augsburg.

(berichtet von Domi dem Betreuer)

## Ein typischer Tag im Mathecamp für die Kids

Nachdem die Kids am ersten Abend angeben können, ob sie zum Frühsport geweckt werden möchten,
machen sich die ersten Betreuenden bereits um 6:50 Uhr auf den Weg, um die sportwilligen Kids zu wecken.
Beim Frühsport wurden Bootfahren, Schwimmen, Joggen, Spikeball oder auch Freestyle-Tanzen angeboten.
Für alle nicht Frühsportler begann der Tag um 7:45 Uhr mit dem Aufwachen durch die Hausmusik oder der Betreuenden,
die mit einer Musikbox durch die Gänge wanderten.
Nach dem Frühstücksbuffet ab 8:10 Uhr machte man sich ab 9:00 Uhr daran, in den mathematischen Zirkeln die spannenden
Themen zu verstehen und vielleicht das ein oder andere mitzunehmen.
Nach den Zirkeln folgte das Mittagessen, das immer aus einer Suppe, einem Salat, einer Hauptspeise und einem Dessert bestand.
Dann war die Zeit der Fraktivitäten gekommen:

Bootfahren, Tanzen, Musikhören, Zauberwürfel lösen, Programmieren, Pool, Fußball, Völkerball, Spikeball, Malen, Basteln,
Kegeln, Billard, mathematische Rätsel, Jonglieren, Montagsmaler, Pantomime, Verstecken, Vorträge, Handball, Sachen werfen,
Frisbee, Gute-Nacht-Geschichten und noch weitere Dinge konnten die Kids auswählen.
Dabei gab es ganz verschiedene Fraktivitäten zu verschiedenen Zeiten.
Der Ort und die Beginnzeit konnten die Kids am Fraktivitätenboard herausfinden.

Um 15:30 Uhr gab es dann Kaffee und Kuchen, der von allen Kids sowie den meisten Betreuenden fleißig genutzt wurde.
Danach gab es weitere Fraktivitäten, sowie die Nachmittagszirkel, von denen dieses Jahr einer besucht werden musste.
Die Kids fanden die Themen zum großen Teil sehr gut.
Nach dem Ende der Nachmittagszirkel ging es dann Richtung Abendessen und im Anschluss war nochmal Zeit sich auszutoben.

## Ein nicht ganz ernstgemeinter Tagesablauf im Leben eine:r Betreuer:in

Nachdem man die ersten vier Wecker verschlafen hatte, konnte man gerade noch rechtzeitig zum Frühstück aufwachen.
Für die morgendliche Dusche blieb natürlich keine Zeit.
Nachdem man sich an den Frühstückstisch gesetzt hatte, ließ man eines seiner:ihrer Tischkinder erst einmal einen Kaffee
bringen und einen gut beladenen Teller mit Essen.
Solange der:die Betreuende aß, durften die Kinder am Tisch natürlich nicht sprechen, es war ja auch viel zu früh am Morgen.
Nachdem der:die Tischregent:in schließlich abgeschlossen hatte zu speisen, durften auch die Kids etwas zu essen holen.
Allerdings gab es hier ein kleineres Buffet, an dem es nur Butter und Wasser gab.

Nach dem Frühstück ging es auch für die Betreuenden in die Zirkel.
Ohne eine Sekunde Vorbereitung wurde einfach irgendwas erzählt, worauf der:die Betreuende gerade kam.
Heftaufschriebe gab es nicht und vor allem die Themen waren meist einfach so erfunden.
Die Zirkel sind aber auch ein lästiges Übel im Leben eine:r Betreuer:in.
Danach fand zunächst mal das Betreuendenchillen an.
Die Kids durften in dieser Zeit den Speisesaal herrichten und dem:der Tischregierenden einen kalten Eimer Wasser
sowie einen Palmwedel bereithalten.
Als der:die Betreuende kam, wurde ihm:ihr direkt ein Kaltgetränk, sowie etwas vom Snackbuffet angeboten.

Im Anschluss daran wurde gegessen.
Für die Betreuenden gab es wieder eigene Speisen und eigene Getränke.

Die Nachmittagsgestaltung war für die Betreuenden dann schließlich die größte Last.
Man musste so tun, als ob man Spaß daran hätte, mit den Kids etwas zu unternehmen.
Eigentlich wäre man viel lieber an einem ruhigen Pool gelegen, hätte lieber untereinander Karten gespielt
oder hätte einfach in der Sonne mit einem kalten Fruchtsaft gesessen.
Doch man musste ja die anwesenden Kids ein bisschen bespaßen.

Der Kaffee und Kuchen war eigentlich auch nur für die Betreuenden vorgesehen.
Damit dies nicht so auffiel, hatte man den Kids einfach auch ein paar langweilige und supereinfache Kuchen hingestellt,
während die Betreuenden vom wahren Kuchenbuffet essen konnten.
Neben Sachertorte, Schwarzwälder Kirsch, Käse-Sahne und einer kleinen Variation von Schokofrüchten aller Art war
vor allem der teure Kaffee ein Hochgenuss.
Die Kids, die mit ihrem Wasserbecher dasaßen blickten neidisch zu unserem eigenen Tisch herüber.

Nach dem Kaffee wurden die Kids verpflichtend in die Nachmittagszirkel gesteckt, damit wenigstens ein bisschen Ruhe herrschte.
Mathematik interessiert uns eigentlich alle überhaupt nicht und wir haben eh kein Interesse daran,
den Kids spannende Themen zu vermitteln – die Ruhe ist viel wichtiger.

Nach dem Abendessen schickte man die Kinder dann in Wahrheit schon um 19:00 Uhr auf ihre Zimmer
und drehte die Sicherungen raus, damit die Kids schnell einschlafen würden.
Es galt ein generelles Redeverbot auf den Zimmern nach 19:00 Uhr.

Nun hatten die Betreuenden das Haus für sich:
Das Fraktivitätenboard für die Kids wurde durch eines für die Betreuenden ersetzt: Neben Betreuendenkegeln, Spikeball
und ganz vielen Betreuenden-Fraktivitäten gab es auch die Fraktivitäten Kinder ärgern, Kinder nicht schlafen lassen,
sowie Kinder erschrecken – man war das ein Spaß!

Ins Bett ging man entweder gar nicht oder sehr spät.
Gegen 4:45 Uhr wurden die Kids dann geweckt, damit sie die Tiere und Felder des Bruder-Klaus-Heims versorgen
und bestellen konnten.
Pünktlich um 8:00 Uhr standen sie dann wieder bereit, um ihrem:ihrer Tischregierenden das Frühstück zu reichen.
