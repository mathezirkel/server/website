---
title: Mathecamp 2021 - Klasse 5 bis 8
---

## Rückblick

<div class="highlight-box">
18. bis 22. August 2021

72 Teilnehmer:innen

12 Betreuer:innen

5 wunderschöne Tage!!
</div>

Kegeln!
Spikeball!
Filme auswählen!
Wanderung!
Programmieren!
Pool-Party!
Pokern!
Basteln!
Zauberei!
Capture the Flag!
Zirkel!
Power-Point-Karaoke!
Lightning-Talks!
Schach! Zauberwürfel!
Tischtennis!
Das Essen!
Neue Sachen lernen!
Alle hatten viel Spaß!
Gregor!
Wahrheit oder Pflicht!
Wanderrallye!
Zeit mit Freund:innen verbringen!
Light-Painting!
Schatzsuche!
Frisbee! Völkerball!
Billard!
Rätsel des Tages!
Malen! Mathe! Die Atmosphäre!
Mathecamp-Quiz!
Frühsport!
Alles!

## Betreuer:innenbericht

Domi hat einen teilweise nicht ganz ernst gemeinten [Bericht](./bericht) des Mathecamps verfasst.

## Materialien zu einigen Zirkeln

### 5\. Klasse

#### Josephus Problem (Binärzahlen)

Auf YouTube gibt es ein cooles [Video von Numberphile](https://www.youtube.com/watch?v=uCsD3ZGzMgE) (englisch)
mit Animation und Erklärung dazu.

### 6\. Klasse

#### Mastermind

Der [Wikipedia-Artikel](https://de.wikipedia.org/wiki/Mastermind_(Spiel)#Strategien) zu Mastermind ist sehr interessant.

Wenn du etwas tiefer einsteigen möchtest, kannst du dir auch [diese Bachelorarbeit](https://docplayer.org/8373938-Mastermind-bachelorarbeit-im-fach-mathematik-an-der-ruhr-universitaet-bochum-von-michael-kussmann-aus-essen.html)
aus Bonn ansehen. Der Text ist auch für andere Klassenstufen geeignet.

### 7\. Klasse

#### Künstliche Intelligenz

Wir haben über die Theorie der künstlichen Intelligenz philosophiert.
Den im Zirkel gezeigten Python-Code, mit dem man gegen den Computer Tic-Tac-Toe spielen kann,
findest du im [Github-Repository](https://github.com/jonas-kell/ML-python-TTT) von Jonas.

Auf YouTube gibt es außerdem ein [Video](https://www.youtube.com/watch?v=trKjYdBASyQ),
in dem der Minimax-Algorithmus für Tic-Tac-Toe implementiert wird.

Die Inspiration für den Zirkel kommt aus den Videos von Computerphile (Englisch):

* [General AI Won't Want You To Fix its Code - Computerphile](https://www.youtube.com/watch?v=4l7Is6vOAOA)
* [Concrete Problems in AI Safety (Paper) - Computerphile](https://www.youtube.com/watch?v=AjyM-f8rDpg)
* [AI "Stop Button" Problem - Computerphile](https://www.youtube.com/watch?v=3TYT1QfdfsM)
* [Stop Button Solution? - Computerphile](https://www.youtube.com/watch?v=9nktr1MgS-A)
* [Deadly Truth of General AI? - Computerphile](https://www.youtube.com/watch?v=tcdVC4e6EV4)

### 8\. Klasse

#### Strange-Spheres

Die "Komischen Kugeln" in höheren Dimensionen sind von einem [Video](https://www.youtube.com/watch?v=mceaM2_zQd8)
von Matt Parker vom YouTube-Kanal Numberphile inspiriert (englisch):

Wenn du Geometrie in höheren Dimensionen "verstehen" willst,
dann fang doch mal bei diesem [YouTube-Video](https://www.youtube.com/watch?v=j-ixGKZlLVc) an:

#### Vickrey-Auktionen

Die Vickrey-Auktionen (Zweitpreisauktionen) sind auf [Wikipedia](https://de.wikipedia.org/wiki/Zweitpreisauktion)
nochmal im Detail erklärt.

#### Wahlsysteme

* [Beweis Satz des Diktators](https://pnp.mathematik.uni-stuttgart.de/igt/eiserm/popularisation/Satz-vom-Diktator/Satz-vom-Diktator-2x2.pdf)
  (Arrows Unmöglichkeitstheorem)
* [Wikipedia Artikel](https://en.wikipedia.org/wiki/Comparison_of_electoral_systems#Comparisons)
  mit einer Übersicht zu Wahlsystemen und Merkmalen (englisch)
* [Blogeintrag](https://timodenk.com/blog/voting-systems/) mit Einführung zum Thema (englisch)

Fazit: Das einzig wahre faire Wahlsystem ist es Tanja zum Diktator zu machen ;)

## Materialien zu einigen Fraktivitäten

### Alienzirkel (Cosmic Call)

Im Jahr 1999 sendete die Menschheit eine Radiobotschaft an ausgewählte Sterne, in der Hoffnung,
dass eine dort lebende Zivilisation die Nachrichtempfangen, verstehen und auf sie antworten würde:
Die Cosmic-Call-Botschaft. Diese Nachricht wurde natürlich nicht auf Deutsch oder Englisch formuliert,
sondern bedient sich einer mathematischen Symbolsprache.
Im Zirkel unterzogen wir die Botschaft einem Test:
Können wenigstens wir Menschen, die wir einen riesigen Heimvorteil haben, die Nachricht entziffern?

Du findest die Nachrichten und Erklärungen unter [Materialien](/materialien).

### Beweise ohne Worte

Beweise spielen in der Mathematik eine ganz zentrale Rolle und füllen auch häufig ein ganzes Buch.
Dass dies aber nicht immer so sein muss, sieht man an den Beispielen aus unserer Sammlung an Beweisen ohne Worte.
Hier geht es darum, anhand eines Bildes zu verstehen, wieso eine Aussage gilt.
Beispielsweise möchte man mit Hilfe einer Skizze herausfinden,
ob der Umfang eines Kreises genauso viele Punkte hat wie eine unendlich lange Gerade.
Wenn du daran gerne weiter tüfteln willst, findet du diese unter [Materialien](/materialien).

### Billard

[Regelwerk](https://www.spielregeln-spielanleitungen.de/sportspiel/regeln-billard/) für Profis

### Fraktale

In Alex [Gitlab-Repository](https://gitlab.com/CptMaister/spass-mit-fraktalen/-/tree/master/Malen-in-Python-Turtle)
findest du eine ausführliche Anleitung zum Programmieren von Fraktalen mit Python-Turtle.

Außerdem kannst du auf [dieser Website](http://hirnsohle.de/test/fractalLab/) durch den Menger-Schwamm fliegen.

### Fortgeschrittenen-Programmieren

Den Code, den wir zusammen beim Fortgeschrittenen-Programmieren geschrieben haben,
(und mehr qualitativ SEHR hochwertigen Code (Nicht)) findest du
im [Github-Repository](https://github.com/jonas-kell/ML-python-TTT) von Jonas.

Wir haben versucht haben, ein [Perzeptron](https://de.wikipedia.org/wiki/Perzeptron) zu bauen.
Dafür benötigen wir [Matrizenmultiplikation](https://de.wikipedia.org/wiki/Matrizenmultiplikation).

### Jonglieren

Falls du jonglieren lernen möchtest oder schon immer mal deine eigenen Jonglierbälle basteln wolltest,
findest du [hier](https://utopia.de/ratgeber/jonglieren-lernen-eine-anleitung-fuer-anfaenger/) Tipps.
Eine [Video-Anleitung](https://www.youtube.com/watch?v=e6GbXfkXbAA) für Anfänger gibt es auch auf Youtube:

Wenn du natürlich schon jonglieren kannst, dein Repertoire aber noch um weitere Tricks ergänzen willst,
kannst du dich gerne [hier](https://spielelux.de/jongliertricks-fortgeschrittene/) inspirieren lassen.

### Pi-Kärtchen

Zum selbst ausdrucken:

* [Pi](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/pi-lernen.pdf)
* [Tau](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/tau-lernen.pdf)
* [Euler'sche Zahl](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/e-lernen.pdf)
* [Zeta](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/zeta-lernen.pdf)

### Poolparty

[Spotify-Link](https://open.spotify.com/playlist/37i9dQZF1DX4Y4RhrZqHhr?si=ea79e1f2613e4f2e) zu Beach-Party-Playlist.

### Power-Point-Karaoke

Spontan einfach mal \*\*\*\* zu einem random Thema labern? - Das ist PowerPoint-Karaoke!
Anstatt wie beim normalen Karaoke einen Liedtext zu singen, muss beim Power-Point-Karaoke ein Vortrag zu einer
unbekannten, zufälligen Präsentation gehalten werden.
Was man erzählt, ist egal, solange es irgendwie zu den Folien passt.
Dabei kommt es insbesondere nicht auf einen wissenschaftlich korrekten Vortrag an - das ist ja auch gar nicht möglich,
da man sich in dem Themenbereich in der Regel überhaupt nicht auskennt.

Power-Point-Karaoke kann also jede:r und es entstehen immer viele Lacher.
Wenn du zu Hause mit Freunden oder Familie spielen willst und gerade keine passenden Folien zur Hand hast,
gibt es [hier](https://kapopo.de/) einige kostenlose Präsentationen.

### Russisch für Anfänger

Wer Lust hat nebenbei ein wenig Russisch zu lernen, kann das mit dem folgenden Podcast tun.
Die Folgen gibt es auch auf YouTube und im Internet aber am bequemsten ist es nach meiner Meinung über Spotify
([Link zur Playlist](https://open.spotify.com/show/6tsJtmemAyX9Kw6BAQgMpM?si=A3KzoxPkRdWHUr720Up-lA&utm_source=copy-link&dl_branch=1)).

### Spikeball (eigentlich "Roundnet")

Dieses [YouTube-Video](https://www.youtube.com/watch?v=cPmLwuZ5lWM) erklärt nochmal ausführlich alle Regeln.

Das Set, dass wir im Mathecamp verwenden (cool, weil es extrem robust ist), kannst du u.a.
[hier](https://www.roundnet-deutschland.de/shop/spikeball-pro-set.htm) oder [hier](https://spikeball.eu/products/spikeball-pro-kit)
kaufen.

Ein [Highlight-Video](https://www.youtube.com/watch?v=mo4qTH-VYGM) zum Nacheifern (auf Englisch) findest du auf YouTube.
Zudem findet man viele weitere Videos mit Tipps und Tricks auf Youtube - einfach mal danach suchen!

### Völkerball

Falls du nochmal die Regeln von Völkerball checken oder neue fancy Spielvarianten kennen lernen möchtest,
kannst du beispielsweise auf [dieser Website](https://wimasu.de/voelkerball/) vorbeischauen.

### Wanderrallye

Die [Route](https://www.komoot.de/tour/449340910?share_token=aS4hHKSbkXdx9KjhczrFW8SUFAlAe3Ui5IC6nN5K8W9mYZ1EJS&ref=wtd),
die wir genommen haben, findest du auch auf Komoot.

### Wikingerschach / Kubb

Die offiziellen Regeln kannst du [hier](https://www.kubb-deutschland.de/kubb-regeln-anleitung/) nochmal ausführlich nachlesen.

### Zauberwürfel

Die Anleitung zum Lösen des Zauberwürfels findest du auf [Tims Website](https://timbaumann.info/zauberwuerfel).

## Abschluss und Mathecamp 2022

Schön, dass du bis hier her durchgehalten hast!
Vermutlich hätten wir mit den zusätzlichen Materialien auch noch ein Camp füllen können :-)

A propos, das Mathecamp 2022 findet statt vom **20. bis 28. August 2022**. Wir freuen uns schon auf euch!

<div class="centering">
    <figure>
        {{< responsive-image
            src="gregor-mc-21.png"
            alt="Gregor 2021"
            width=500
            loading="lazy"
        >}}
    </figure>
</div>
