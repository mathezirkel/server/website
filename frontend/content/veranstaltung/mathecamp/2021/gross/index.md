---
title: Mathecamp 2021 - Klasse 9 bis 12
---

## Rückblick

<div class="highlight-box">
14. bis 18. August 2021

74 Teilnehmer:innen

12 Betreuer:innen

5 wunderschöne Tage in Violau!!
</div>

Wanderung!
Matboj!
Mathematische Vorträge!
Lightning-Talks! Werwolf!
Wer bin ich!
Das Essen!
Fraktale Programmieren!
Wasserballett! Kontakt!
Tolle Leute! Kegeln!
Zeichnen!
Die Gemeinschaft!
Discord-Bots Programmieren!
Filmeabend!
Coole Nachmittagszirkel!
Aluhut-Zirkel!
Tanzen! Zirkel!
Nuss-Nougat-Creme!
Mathe! Escape Game!
Pantomime! Boot fahren!
Ultimate Frisbee!
Malerei! Coole Betreuer:innen!
Durak!
Schnitzeljagd!
Apfeltheke!
Völkerball!
Das Mathecamp!
Alles!

## Zusätzliche Materialien zu einigen Zirkeln und Fraktivitäten

### Agda, das frustrierende mathematische Beweisspiel (Vortrag von Ingo)

Wer so richtig in Agda einsteigen möchte, kann ihre:seine Reise [hier](https://agdapad.quasicoherent.io/) beginnen.

### Alienzirkel (Cosmic Call)

Im Jahr 1999 sendete die Menschheit eine Radiobotschaft an ausgewählte Sterne, in der Hoffnung,
dass eine dort lebende Zivilisation die Nachricht empfangen, verstehen und auf sie antworten würde: die Cosmic-Call-Botschaft.
Diese Nachricht wurde natürlich nicht auf Deutsch oder Englisch formuliert, sondern bedient sich einer mathematischen Symbolsprache.
Im Zirkel unterzogen wir die Botschaft einem Test:
Können wenigstens wir Menschen, die wir einen riesigen Heimvorteil haben, die Nachricht entziffern?
Wenn du gerne weiter knobeln möchtest, findest du die Botschaften und Erklärungen unter [Materialien](/materialien).

### Beweise ohne Worte

Beweise spielen in der Mathematik eine ganz zentrale Rolle und füllen auch häufig ein ganzes Buch.
Dass dies aber nicht immer so sein muss, sieht man an den Beispielen aus unserer Sammlung an Beweisen ohne Worte.
Hier geht es darum, anhand eines Bildes zu verstehen, wieso eine Aussage gilt.
Beispielsweise möchte man mit Hilfe einer Skizze herausfinden,
ob der Umfang eines Kreises genauso viele Punkte hat wie eine unendlich lange Gerade.
Wenn du daran gerne (weiter) tüfteln willst, findest du die Aufgaben unter [Materialien](/materialien).

### Borromäische Ringe (Zirkel von Max in der 11. Klasse)

In diesem Zirkel haben wir uns mit den Borromäischen Ringen beschäftigt.
Für weitere Infos kannst du dir z.B.
den [Wikipedia-Artikel](https://en.wikipedia.org/wiki/Borromean_rings) zu den borromäischen Ringen durchlesen,
für den Inhalt des Vortrags kannst du in das ‘Buch der Beweise’ (Proofs from the book) schauen.
Dort haben wir das Kapitel ‘The borromean rings don’t exist’ größtenteils besprochen.

### Discord-Bots

Wenn du auch weiterhin Discord-Bots programmieren willst,
check auf jeden Fall die [Dokumentation von discord.py](https://discordpy.readthedocs.io/en/stable/index.html),
die wir auch im Kurs benutzt haben - ab.
Wir haben das [Template](https://github.com/nonchris/discord-bot) verwendet und darauf aufgebaut.
Das ist aber natürlich kein muss.
Als Beispiel kannst du auch den [Bot](https://github.com/kesslermaximilian/JustOneBot), den Max geschrieben hat,
unter die Lupe nehmen.
Natürlich findest du auch noch viele weitere Beispiele online.
Bei Fragen kannst du jederzeit an Max wenden, z.B. auf Discord (max31415#5467)
oder per Mail an [mathecamp@maximilian-kessler.de](mailto:mathe@maximilian-kessler.de).
Viel Spaß beim Programmieren der Bots!

### Fraktale mit Python programmieren

Den Code gibt es [hier](https://etherpad.wikimedia.org/p/36c3-python).

### Funktionalgleichungen (Zirkel von Sven in der 10. Klasse)

Wer sich noch weiter mit Funktionalgleichungen und dem Lösen dieser auseinandersetzen möchte,
der:dem seien die [Skripte der Schweizer Mathematikolympiade](https://mathematical.olympiad.ch/de/skripts)
zu Funktionalgleichungen sehr zu empfehlen.

### Große, sehr große und sehr sehr große Zahlen (Vortrag von Ingo)

Den Vortrag gibt es [hier](https://github.com/iblech/mathezirkel-kurs/raw/master/35c3/large-numbers/slides-weihnachtsvorlesung2019.pdf).
Außerdem gibt es zu dem Thema noch ein sehr cooles Youtube-Video [hier](https://www.youtube.com/watch?v=aIg8gAbqqZc).

### Jonglieren

Falls du jonglieren lernen möchtest oder schon immer mal deine eigenen Jonglierbälle basteln wolltest,
findest du [hier](https://utopia.de/ratgeber/jonglieren-lernen-eine-anleitung-fuer-anfaenger/) Tipps.
Eine Video-Anleitung für Anfänger gibt es auch auf [Youtube](https://www.youtube.com/watch?v=e6GbXfkXbAA).
Wenn du schon sehr gut mit drei Bällen jonglieren kannst, dein Repertoire aber noch um weitere Tricks ergänzen willst,
kannst du dich beispielsweise [hier](https://spielelux.de/jongliertricks-fortgeschrittene/) inspirieren lassen.

### Knete in die Ohren stecken

Die Fraktivität wurde von diesem [Youtube-Video](https://www.youtube.com/watch?v=Oai7HUqncAA) inspiriert.
Fun-Frage: An welcher Stelle merkt man, dass das Video aus Texas stammt?

Es kommt auch ein Drohnenpilot vor, der Drohnen schneller in Bäume fliegt als Ingo.
(Der Kanal ist im Allgemeinen zu empfehlen).
Ein weiteres [gutes Video](https://www.youtube.com/watch?v=-nAGXmUi6j0) liefert weitere Erklärungen auf französisch
mit guten englischen Untertiteln.

### Lightning Talks

#### Gastvortrag von Taulexa zu Kleidungs-Zauberei

Falls du den Lightning Talk von Taulexa zum Thema Kleidungs-Zauberei verpasst hast oder noch einmal anschauen möchtest,
findest du [hier](https://www.speicherleck.de/iblech/stuff/.taulexa-lightning-talk-mathecamp2021.mp4) eine Aufzeichnung davon.

#### Koch-Hacks von Meru

Als Zusatz zu Merus Lightning Talk über Koch-Hacks, findest du [hier](https://www.youtube.com/watch?v=MEm9Z_on5q4)
noch weitere Tipps und Tricks.
Eine sehr coole Serie zum Thema Kochen ist 4 Levels von Epicurious.
Falls du Lust hast dir Essens-Inspiration zu holen oder dich einfach amüsieren willst,
können wir dir [dieses](https://www.youtube.com/watch?v=imXIizmWd14) Video empfehlen.

### Was ist ein Loch? (Zirkel von Max in der 12. Klasse)

[Hier](https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2021/gross/zirkel-12-was-ist-ein-loch.pdf)
findest du eine grobe Übersicht über das, was wir im Zirkel gemacht haben.
Diese ist aber nur teilweise ausgearbeitet und enthält auch einfach nur Notizen für mich.
Wenn du dich weiter für das Thema interessierst, schau dort in die Literaturangaben.
Für eine erste Einführung empfiehlt sich Jänich oder die Vorlesungsnotizen davon,
für einen tieferen Einstieg in das Thema Hatcher.

### Matboj

Wenn du dich schon einmal für den Matboj auf dem Mathecamp nächstes Jahr vorbereiten möchtest,
kannst du dich genauer mit den [Regeln](https://gitlab.com/mathezirkel/content/matboj-regelwerk) auseinandersetzen.
Vielleicht findest du ja eine optimale Strategie!

### Mathecamp-Quiz (1,2 oder 3)

Am letzten Abend haben wir gemeinsam 1,2 oder 3 in der Mathecamp-Edition gespielt.
Wenn du dir mal wieder eine “echte” Folge 1,2 oder 3 anschauen möchtest,
gibt es [hier](https://www.zdf.de/kinder/1-2-oder-3) die aktuellen Folgen.

### Multiversenphilosophie in Mengenlehre (Vortrag von Ingo)

Einige Gedanken zu dem Thema findet ihr [hier](https://iblech.gitlab.io/bb/multiverse.html).
Darüber hinaus gibt es spannende Literatur, die sich mit dem Thema beschäftigt, z.B.
[diese Fan-Fiction](https://www.fanfiction.net/s/5389450/1/The_Finale_of_the_Ultimate_Meta_Mega_Crossover)
oder der Science-Fiction-Roman _Permutation City_ von Greg Egan
oder die Monografie _Towards a Philosophy of Real Mathematics_ von David Corfield.

### Ordinalzahlen (Vortrag von Ingo)

Den Vortrag gibt es [hier](https://github.com/iblech/mathezirkel-kurs/raw/master/mathecamp-2016/verbotene-geheimzirkel/unendlich-grosse-zahlen.pdf).

### Pi-Kärtchen

Wenn in deiner Sammlung noch eine der Karten mit mathematischen Funfacts bzw.
Konstanten fehlt, kannst du dir diese gerne unter folgenden Links runterladen und ausdrucken.

* [Pi](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/pi-lernen.pdf)
* [Tau](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/tau-lernen.pdf)
* [Euler’sche Zahl](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/e-lernen.pdf)
* [Zeta](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/zeta-lernen.pdf)

### Die Welt aus den Augen einer Primzahl (Vortrag von Ingo)

Den Vortrag gibt es [hier](https://github.com/iblech/mathezirkel-kurs/raw/master/mathecamp-2017/10-adische-zahlen/10-adische-zahlen.pdf).

### P vs. NP (Vortrag von Ingo)

Den Vortrag gibt es [hier](https://github.com/iblech/mathezirkel-kurs/raw/master/mathecamp-2019/p-vs-np/slides-36c3.pdf).

### Raumfahrt (Nachmittagszirkel von Sven)

In der Schule lernt man, dass sich Himmelskörper grundsätzlich auf Kegelschnitten durch das Weltall bewegen.
Diese Erkenntnis geht bereits auf Johannes Kepler und Tycho Brahe zurück.
In diesem Vortrag haben wir gesehen, wie man das mit Schulmathematik (ok, ein klein wenig mehr) relativ kurz herleiten kann,
indem man sogenannte Erhaltungsgrößen ausrechnet.
An dieser Stelle wird bestimmt bald ein Skript liegen :-)

### Raumfahrtvortrag vom letzten Abend (Vortrag von Sven)

Am letzten Abend haben wir mit ca.
5h Verspätung einen Vortrag vom 35C3 in kleiner Runde gehört und diskutiert:
Dort ging es um Raumfahrt und mit welchen Manövern man Raumschiffe zu super weit entfernten Planeten bringen kann.
Wer sich das nochmal in voller Länge anschauen möchte,
kann das gerne [im Original auf Englisch](https://www.youtube.com/watch?v=ax6cSCgmHQw) tun.

### Reelle Divisionsalgebren (Nachmittagszirkel von Max)

In diesem Nachmittagszirkel haben wir sehr viele Resultate angerissen, aber keines davon bewiesen.
[Hier](https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2021/gross/nachmittagszirkel-reelle-divisionsalgebren.pdf)
findest du einerseits die Folien vom Vortrag (diese sind aber sicher fehlerbehaftet, die sind auch vor dem Vortrag
nochmal etwas durcheinandergewürfelt worden).
Für bessere Quellen gibt es außerdem die Ausarbeitung meines gleichnamigen
[Seminarvortrags](https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2021/gross/klassifikation-reelle-divisionsalgebren.pdf).
Dort findest du in der Literatur auch nochmal eine Referenz zum Buch ‘Zahlen’, das Max dir sehr empfehlen kann,
wenn du weiter in das Thema einsteigen willst - aber auch,
wenn du dich für reelle, komplexe, p-adische oder hyperreale Zahlen interessierst.

### Das SIR-Modell (Vortrag von Sven)

Die letzten anderthalb Jahre waren wohl für die meisten von uns sehr ungewöhnlich wegen Corona.
In den Medien ist dabei sehr häufig die Infektionskurve oder der exponentielle Ausbruch erwähnt wurden.
Daher haben wir in diesem Vortrag mal das zugrundeliegende mathematische Modell,
das SIR-Modell (für Susceptible, Infectious, Removed), analysiert und genauer betrachtet.
Die Folien findet ihr [hier](https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2021/gross/sir-model.pdf).

### Spikeball (auch bekannt unter Roundnet)

Du kannst dich für deine zukünftigen Spike-Matches nochmals über die genauen
[Regeln](https://www.youtube.com/watch?v=cPmLwuZ5lWM) informieren.
Im Mathecamp haben wir [dieses](https://spikeball.eu/products/spikeball-pro-kit) Set verwendet
(cool, weil es extrem robust ist).

Ein paar Highlight-Videos zum Nacheifern (auf Englisch) findest du beispielsweise hier: [https://www.youtube.com/watch?v=mo4qTH-VYGM](https://www.youtube.com/watch?v=mo4qTH-VYGM).
Zudem findet man viele Videos mit Tipps und Tricks auf Youtube - einfach mal danach suchen.

### Stricken

Falls du bis zum nächsten Mathecamp eure Strick-Skills verbessern möchtest,
kannst du dir für die Grundlagen beispielsweise das [folgende Youtube-Video](https://www.youtube.com/watch?v=PSGZrIihtHw)
anschauen.
Und falls dich direkt die Lust auf ein kleines Strickprojekt packt,
ist vielleicht eine der folgenden Projekt-Ideen was für dich:

* ein [Einkaufsnetz](https://www.talu.de/einkaufsnetz-stricken/)
* eine [Bommel-Mütze](https://www.brigitte.de/leben/wohnen/selbermachen/stricken/strickmuster--rote-muetze-mit-bommel-stricken---eine-anleitung-10206872.html)

### Tanzalarm

Für eine Extraportion gute Laune sei auf den [Tanzalarm](https://www.youtube.com/watch?v=xw0dS9022aE) verwiesen.

### Teilbarkeitsregeln (Zirkel bei Georg in der 9. Klasse)

Dieser Zirkel war größtenteils von [diesem Video](https://m.youtube.com/watch?v=yi-s-TTpLxY) von Numberphile inspiriert.

### Völkerball

Falls du nochmal die Regeln von Völkerball checken oder neue fancy Spielvarianten kennen lernen möchtest,
kannst du beispielsweise auf der folgenden [Webseite](https://wimasu.de/voelkerball/) vorbeischauen.

### Zauberwürfel

Die Anleitung zum Lösen eines 3x3 Zauberwürfels findest du auf [Tims Webseite](https://timbaumann.info/zauberwuerfel).

### Zufall (Zirkel von Anna in der 11. Klasse)

In diesem Vormittagszirkel haben wir uns mit dem Zufall beschäftigt und wie man Pseudozufallszahlen
mit dem Computer generieren kann.
Wenn du dazu noch mehr wissen möchtest:
In [diesem Artikel](https://www.spiegel.de/wissenschaft/mensch/mathematik-enthaelt-die-zahl-pi-ein-geheimes-muster-a-353818.html)
geht es darum, ob Pi ein guter Zufallszahlengenerator ist.
Wenn du weiter darüber philosophieren willst, was genau eigentlich Zufall ist,
ist vielleicht [diese Seite](https://www.zufallsphilosoph.ch/zufall-mathematisch) spannend für dich.

## Abschluss und Mathecamp 2022

Schön, dass du bis hier her durchgehalten hast! Vermutlich hätten wir mit den zusätzlichen Materialien
auch noch ein Camp füllen können :-)

A propos, das Mathecamp 2022 findet statt vom **20. bis 28. August 2022**.
Wir freuen uns schon auf euch!

<div class="centering">
    <figure>
        {{< responsive-image
            src="gregor-mc-21.png"
            alt="Gregor 2021"
            width=500
            loading="lazy"
        >}}
    </figure>
</div>
