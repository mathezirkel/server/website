---
title: Mathecamp 2016
---

## Materialien zu manchen Zirkeln und Fraktivitäten

### 3D-Druck

Unser Drucker war der "Prusa i3".
Wir haben am zweitägigen Workshop des OpenLab Augsburgs teilgenommen, um aus dem Bausatz den fertigen Drucker zu bauen.
Auf [http://www.thingiverse.com/](http://www.thingiverse.com/) gibt es eine große Datenbank an 3D-Modellen.

### Alienzirkel (Cosmic Call)

Im Jahr 1999 sendete die Menschheit eine Radiobotschaft an ausgewählte Sterne, in der Hoffnung,
dass eine dort lebende Zivilisation die Nachricht empfangen, verstehen und auf sie antworten würde: die Cosmic-Call-Botschaft.
Diese Nachricht wurde natürlich nicht auf Deutsch oder Englisch formuliert, sondern bedient sich einer mathematischen Symbolsprache.
Im Zirkel unterzogen wir die Botschaft einem Test: Können wenigstens wir Menschen, die wir einen riesigen Heimvorteil haben,
die Nachricht entziffern?

Ihr könnt die Nachrichten und Erklärungen unter [Materialien](/materialien/) nochmal ansehen.

### Dynamische Labyrinthe

Die dynamischen Labyrinthe ("Maschinen bauen") könnt ihr auch am Computer ausprobieren:

[http://www.ikm.uni-osnabrueck.de/aktivitaeten/dl/dynamische-labyrinthe.htm](http://www.ikm.uni-osnabrueck.de/aktivitaeten/dl/dynamische-labyrinthe.htm)

Mit Händen zu experimentieren macht natürlich mehr Spaß;
in den Präsenzzirkeln während des Schuljahrs habt ihr dazu bei uns die Möglichkeit!

### Fraktalprogrammierung mit Logo (Schneeflocken)

Das geht ganz ohne Installation unter [http://www.trinket.io/](http://www.trinket.io/).
Willst du nochmal nachsehen, wie der Code für die Kochsche Schneeflocke und andere Fraktale aussieht?

Das geht auf [https://github.com/iblech/mathezirkel-kurs/tree/master/mathecamp-2014/fraktale-programmieren](https://github.com/iblech/mathezirkel-kurs/tree/master/mathecamp-2014/fraktale-programmieren)
(nur die Dateien mit Endung ".py" sind relevant).

### Haskell

Haskell ist eine rein funktionale Programmiersprache.
Matthias und Ingo sowie Felix, Martin und Thomas empfehlen sie mit Nachdruck all denen,
die schon mit anderen Programmiersprachen vertraut sind.
Ein guter Einstieg ist das kostenlose Buch [http://learnyouahaskell.com/chapters](http://learnyouahaskell.com/chapters).

### Knotenmemory

Hier gibt es die Datei zum Selberausdrucken und Spielen des Knotenmemories:

[http://www.speicherleck.de/iblech/stuff/knotenmemory-einfach.pdf](http://www.speicherleck.de/iblech/stuff/knotenmemory-einfach.pdf)

Die schwierigere Version gibt es noch nicht digital, wird aber auf Anfrage gerne gescannt :-)

### Mathe-Olympiade-Bücher

* "Mathematical Olympiad Treasures" von Titu Andreescu, Bogdan Enescu, Birkhäuser, ISBN 0-8176-4305-2
* "Mathematical Olympiad Challenges" von Titu Andreescu, Razvan Gelca, Birkhäuser, ISBN 978-0-8176-4528-1
* "Problem Solving Strategies" von Arthur Engel, Springer, ISBN 978-0-387-22641-5

### Musik programmieren

Das geht mit Sonic Pi, welches es für Linux, Mac und Windows gibt.
Es ist freie Software, kostet nichts und muss nicht mal installiert werden: [http://sonic-pi.net/](http://sonic-pi.net/)
Eine umfassende deutsche Anleitung liegt dem Programm bei.

### Neidfreie Teilung

Der englische Originalartikel zum Thema: [https://www.cs.cmu.edu/~arielpro/mfai_papers/BT95.pdf](https://www.cs.cmu.edu/~arielpro/mfai_papers/BT95.pdf)

### Numberphile-Videos zu den Zirkeln von Rolf

* [https://www.youtube.com/watch?v=sG_6nlMZ8f4](https://www.youtube.com/watch?v=sG_6nlMZ8f4)
* [https://www.youtube.com/watch?v=Sa9jLWKrX0c](https://www.youtube.com/watch?v=Sa9jLWKrX0c)

### Pi-Kärtchen

Zum selbst ausdrucken: [http://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/pi-lernen.pdf](http://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/pi-lernen.pdf)

### Quadcopter (Flugdrohne)

Der Quadcopter, den wir auf dem Camp dabei hatten, war ein "Syma X5HC".
Die kostet zwischen 50 und 70 Euro.
Es empfiehlt sich ein zusätzliches Akku, um länger als sieben Minuten Flugspaß zu haben.
Die Drohne gibt es zum Beispiel auf [https://www.drohnenstore24.de/?ActionCall=WebActionArticleSearch&BranchId=0&Params%5BSearchParam%5D=x5hc](https://www.drohnenstore24.de/?ActionCall=WebActionArticleSearch&BranchId=0&Params%5BSearchParam%5D=x5hc)
zu kaufen.
Aber Achtung: Wir sind selbst keine Quadcopterexperten und kennen keine anderen Drohnen als Vergleich.
Dies soll also keine Kaufempfehlung sein.
Mittlerweile haben wir auch die Syma X5HW gekauft, die kostet genauso viel.
Mit ihr kann man live Fotos und Videos an ein Smartphone übertragen. Allerdings ist sie in der Höhe weniger stabil.

### Soma-Würfel

Einiges an Theorie zum Würfel:

* [http://www.fam-bundgaard.dk/SOMA/NEWS/N990201.HTM](http://www.fam-bundgaard.dk/SOMA/NEWS/N990201.HTM) (englisch)
* [http://www.mathematische-basteleien.de/somawuerfel.htm](http://www.mathematische-basteleien.de/somawuerfel.htm) (deutsch)

Weitere Figuren zum Zusammenbauen:
[http://www.muehlenau.de/Site/01%20Hauptmenue/03%20Projekte/Projekte%202010-2011/Projektdateien/Somaelemente%204d/soma.pdf](http://www.muehlenau.de/Site/01%20Hauptmenue/03%20Projekte/Projekte%202010-2011/Projektdateien/Somaelemente%204d/soma.pdf)

### Verbotene Geheimzirkel

* Alle Unterlagen zu den verbotenen Geheimzirkeln gibt es auf
 [https://github.com/iblech/mathezirkel-kurs/tree/master/mathecamp-2016/verbotene-geheimzirkel](https://github.com/iblech/mathezirkel-kurs/tree/master/mathecamp-2016/verbotene-geheimzirkel)
* Mehr zur Zetafunktion: [http://rawgit.com/iblech/mathezirkel-kurs/master/thema12-analytische-zahlentheorie/skript.pdf](http://rawgit.com/iblech/mathezirkel-kurs/master/thema12-analytische-zahlentheorie/skript.pdf)
* Mehr zu Gödels Unvollständigkeitssatz: [http://rawgit.com/iblech/mathezirkel-kurs/master/thema11-goedel/skript.pdf](http://rawgit.com/iblech/mathezirkel-kurs/master/thema11-goedel/skript.pdf)
* Mehr zu surrealen Zahlen: [http://rawgit.com/iblech/mathezirkel-kurs/master/thema02-surreale-zahlen/blatt02.pdf](http://rawgit.com/iblech/mathezirkel-kurs/master/thema02-surreale-zahlen/blatt02.pdf)
* Boxratespiel: [https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2016/zahlen-raten/boxen.html](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2016/zahlen-raten/boxen.html)

### Videospielprogrammierung mit Python

Holt euch den Beispielcode von
[https://github.com/iblech/mathezirkel-kurs/blob/master/mathecamp-2016/mein-erstes-videospiel/](https://github.com/iblech/mathezirkel-kurs/blob/master/mathecamp-2016/mein-erstes-videospiel/)
und schreibt [Ingo](mailto:iblech@web.de) eine Mail.

### Wettervorhersage

Daten der vorhergesagten Vertikalprofile der Atmosphäre:

[http://www.weatheronline.co.uk/cgi-bin/expertcharts?LANG=en&amp;MENU=0000000000&amp;CONT=euro&amp;MODELL=modtemps&amp;MODELLTYP=1&amp;BASE=-&amp;VAR=modtemps&amp;HH=3&amp;ZOOM=0&amp;ARCHIV=0&amp;RES=0&amp;WMO=10865&amp;PERIOD=](http://www.weatheronline.co.uk/cgi-bin/expertcharts?LANG=en&amp;MENU=0000000000&amp;CONT=euro&amp;MODELL=modtemps&amp;MODELLTYP=1&amp;BASE=-&amp;VAR=modtemps&amp;HH=3&amp;ZOOM=0&amp;ARCHIV=0&amp;RES=0&amp;WMO=10865&amp;PERIOD=)

Weitere Wetterkarten: [http://www1.wetter3.de/animation.html](http://www1.wetter3.de/animation.html)

Anleitung zur Analyse der Vertikalprofile (auf englisch):

* [http://www.wetterklima.de/flug/temp/overview.htm](http://www.wetterklima.de/flug/temp/overview.htm)
* [https://www.youtube.com/watch?v=CNBzdkmaAKE](https://www.youtube.com/watch?v=CNBzdkmaAKE)

### Zauberwürfel

Die Anleitung zum Lösen des Zauberwürfels ist online: [http://timbaumann.info/zauberwuerfel](http://timbaumann.info/zauberwuerfel)
