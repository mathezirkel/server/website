---
title: Mathecamp 2015
---

## Stratosphärenflug

Wir haben es geschafft! Der Ballon stieg nach seinem langsamen Start noch so weit auf,
dass er die zum Platzen benötigte Höhe erreichte.

Video: [https://www.youtube.com/watch?v=Ruv2ZmiM5H8](https://www.youtube.com/watch?v=Ruv2ZmiM5H8)
(stellt auf hohe Qualität! Und auf langsame Wiedergabe!)

Zeitungsartikel:

* [http://uni.de/hochschul-news/2053/wie+die+welt+aus+35+kilometer+hoehe+oder+aus+den+augen+einer+primzahl+aussieht](http://uni.de/hochschul-news/2053/wie+die+welt+aus+35+kilometer+hoehe+oder+aus+den+augen+einer+primzahl+aussieht)
* [http://www.augsburger-allgemeine.de/augsburg-land/Schueler-schicken-Ballon-ins-Weltall-id35269627.html](http://www.augsburger-allgemeine.de/augsburg-land/Schueler-schicken-Ballon-ins-Weltall-id35269627.html)
* [http://www.augsburger-allgemeine.de/augsburg-land/Junge-Mathe-Asse-schicken-20-000-Bilder-von-Weltraumflug-id35310592.html](http://www.augsburger-allgemeine.de/augsburg-land/Junge-Mathe-Asse-schicken-20-000-Bilder-von-Weltraumflug-id35310592.html)
* [http://www.augsburger-allgemeine.de/augsburg-land/Die-ersten-Bilder-vom-Violauer-Himmelsflug-id35311247.html](http://www.augsburger-allgemeine.de/augsburg-land/Die-ersten-Bilder-vom-Violauer-Himmelsflug-id35311247.html)
* [http://www.augsburger-allgemeine.de/augsburg-land/Der-Weltraumflug-von-Violau-in-Bildern-id35311107.html](http://www.augsburger-allgemeine.de/augsburg-land/Der-Weltraumflug-von-Violau-in-Bildern-id35311107.html)
* [http://www.augsburger-allgemeine.de/augsburg-land/Mathematik-Asse-sind-immer-noch-im-Hoehenflug-id35325772.html](http://www.augsburger-allgemeine.de/augsburg-land/Mathematik-Asse-sind-immer-noch-im-Hoehenflug-id35325772.html)

## Materialien zu manchen Zirkeln und Fraktivitäten

### 3D-Druck

Unser Drucker war der "Prusa i3".
Wir haben am zweitägigen Workshop des OpenLab Augsburgs teilgenommen, um aus dem Bausatz den fertigen Drucker zu bauen.
Auf [http://www.thingiverse.com/](http://www.thingiverse.com/) gibt es eine große Datenbank an 3D-Modellen.

### Dynamische Labyrinthe

Die dynamischen Labyrinthe ("Maschinen bauen") könnt ihr auch am Computer ausprobieren:

[http://www.ikm.uni-osnabrueck.de/aktivitaeten/dl/dynamische-labyrinthe.htm](http://www.ikm.uni-osnabrueck.de/aktivitaeten/dl/dynamische-labyrinthe.htm)

Mit Händen zu experimentieren macht natürlich mehr Spaß;
in den Präsenzzirkeln während des Schuljahrs habt ihr dazu bei uns die Möglichkeit!

### Fraktalprogrammierung mit Logo (Schneeflocken)

Das geht ganz ohne Installation unter [http://logo.twentygototen.org/](http://logo.twentygototen.org/).
Willst du nochmal nachsehen, wie der Code für die Kochsche Schneeflocke und andere Fraktale aussieht?
Das geht auf
[https://github.com/iblech/mathezirkel-kurs/tree/master/mathecamp-2014/fraktale-programmieren](https://github.com/iblech/mathezirkel-kurs/tree/master/mathecamp-2014/fraktale-programmieren)
(nur die Dateien mit Endung ".logo" sind relevant).

### Knotenmemory

Hier gibt es die Datei zum Selberausdrucken und Spielen des Knotenmemories:
[http://www.speicherleck.de/iblech/stuff/knotenmemory-einfach.pdf](http://www.speicherleck.de/iblech/stuff/knotenmemory-einfach.pdf)

Die schwierigere Version gibt es noch nicht digital, wird aber auf Anfrage gerne gescannt :-)

### Mathe-Olympiade-Bücher

* "Mathematical Olympiad Treasures" von Titu Andreescu, Bogdan Enescu, Birkhäuser, ISBN 0-8176-4305-2
* "Mathematical Olympiad Challenges" von Titu Andreescu, Razvan Gelca, Birkhäuser, ISBN 978-0-8176-4528-1
* "Problem Solving Strategies" von Arthur Engel, Springer, ISBN 978-0-387-22641-5

### Pi-Kärtchen

Zum selbst ausdrucken:
[https://github.com/iblech/mathezirkel-kurs/raw/master/mathecamp-2014/pi-lernen/pi-lernen.pdf](https://github.com/iblech/mathezirkel-kurs/raw/master/mathecamp-2014/pi-lernen/pi-lernen.pdf)

### Programmieren mit Kara (Marienkäfer)

Bei Kara programmiert man die Steuerung eines Marienkäfers, der sich dann in einer virtuellen Welt bewegt
und dabei verschiedene Aufgaben bewältigt, wie zum Beispiel aus einem Labyrinth herauszufinden.
Das Programm findest du unter
[http://www.swisseduc.ch/informatik/karatojava/kara/classes/kara.jar](http://www.swisseduc.ch/informatik/karatojava/kara/classes/kara.jar)
und eine Anleitung unter
[http://www.swisseduc.ch/informatik/karatojava/kara/docs/leitprogramm.pdf](http://www.swisseduc.ch/informatik/karatojava/kara/docs/leitprogramm.pdf).
Das Programm enthält eine Sammlung zahlreicher interessanter Aufgaben, an denen man sich versuchen kann.

### Unterlagen des verbotenen Geheimzirkels

* [https://github.com/iblech/mathezirkel-kurs/tree/master/mathecamp-2015/geheimer-zirkel](https://github.com/iblech/mathezirkel-kurs/tree/master/mathecamp-2015/geheimer-zirkel)
* Scilab-Programme zur Satellitenorbit- und Wärmeverteilungssimulation: [https://github.com/iblech/mathezirkel-kurs/tree/master/thema06-numerik-von-differentialgleichungen](https://github.com/iblech/mathezirkel-kurs/tree/master/thema06-numerik-von-differentialgleichungen)

### Videospielprogrammierung mit Python

Neben den vielen anderen Sachen haben wir diesen Zirkel leider etwas vergessen.
Mein ursprünglicher Plan war es, euch an mehreren Abenden spielerisch in die Spieleprogrammierung mit Python einzuführen.
Wenn euch dieses Thema interessiert, dann holt euch den Beispielcode von
[https://github.com/iblech/mathezirkel-kurs/blob/master/mathecamp-2015/videospielprogrammierung/game.py](https://github.com/iblech/mathezirkel-kurs/blob/master/mathecamp-2015/videospielprogrammierung/game.py)
und schreibt eine Mail an [Ingo](mailto:iblech@web.de).

### Zauberwürfel

Die Anleitung zum Lösen des Zauberwürfels ist online: [http://timbaumann.info/zauberwuerfel](http://timbaumann.info/zauberwuerfel)
