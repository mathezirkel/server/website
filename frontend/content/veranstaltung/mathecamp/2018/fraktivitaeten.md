---
title: Mathecamp 2018 - Fraktivitäten
---

## Auf einen Blick: Was gab es?

* Sport und Bewegung:
  Boot fahren •
  Bumerang •
  Football •
  Frühsport •
  Fußball •
  Jonglieren •
  Kegeln •
  Pool •
  Schachturnier •
  Slackline •
  Tanzkurs •
  Tischtennis •
  Ultimate Frisbee •
  Yoga
* Computer und Technisches:
  Bildbearbeitung mit GIMP •
  Briar-Installationsparty •
  NFC-Armbänder •
  Programmieren •
  Satelliten
* Knobeleien:
  Alienzirkel •
  Dynamische Labyrinthe •
  Aufgabe des Tages •
  Fold & Cut •
  Zaubertricks
* Kreatives, Musik und Entspannung:
  Chor •
  Hexaflexagons •
  Greenscreen Session •
  Gutenachtgeschichten •
  Impro-Theater •
  Känguru-Chroniken anhören •
  Kammerchor •
  Lamas •
  Light Painting •
  Malerei •
  Nähen •
  Orchester •
  Origami •
  Pantomime •
  Pi Lernen •
  Schafkopfen •
  Schlafen / Fußboden-/Tisch-Powernap •
  Stickbombs •
  Traumfänger
* Besondere Ereignisse:
  Bunter Abend •
  Karaoke •
  Pool Party •
  Star Walk •
  Sterne gucken •
  Theaterstück Parallelen •
  Wanderung •
  Zettelmord
* Vorträge:
  4D-Vortrag •
  Lightning Talks •
  Netzwerkvortrag •
  Vortrag zu Studium und -Finanzierung •
  Vorträge der Profs
* Hinter den Kulissen:
  Fraktivitäten •
  Laptop löschen •
  Pizza •
  WLAN einrichten

## Vorträge

### 4D-Vortrag

Der Vortrag zur vierdimensionalen Geometrie fand diesmal an der frischen Luft statt,
um mit viel Sauerstoff noch besser mitdenken zu können.
Es gab faszinierende Bilder und Videos von vierdimensionalen Objekten, eine Einführung in die "Kraft des negativen Denkens",
einen schlechten Sketch, bei dem Ingo aus einem Gefängnis befreit wurde, und ein vierdimensionales Labyrinth-Spiel.
Dank vieler Zwischenfragen wurde es ein angenehmer und lockerer Vortrag und auch nach dem Ende gab es noch kleine Erklärungsrunden,
zum Beispiel wie man einen vierdimensionalen Würfel aus einem "Netz" aus dreidimensionalen Würfeln zusammenfaltet.

### Lightning Talks

Vielfältige, schnelle, kurze Vorträge zu überraschenden, skurrilen Themen von vielen aus dem Mathecamp.
Die beiden Runden waren nur durch euren Zuspruch als Publikum und Vortragende möglich!

### Netzwerkvortrag

Wir haben uns angeschaut, was für Daten bereits beim Öffnen vom Browser verwendet werden,
was der Browser insgesamt für Daten beim Besuchen von Websiten mitschickt,
was ein Proxy macht und was das Tor-Netzwerk ist und warum man das verwenden möchte.
Auch wurde demonstriert, wie man unverschlüsselte Passwörter im Netzwerk auslesen kann.
Als weiteres Resultat enstand, dass freie Software wichtig ist.

### Vortrag zu Studium und -Finanzierung

Beim Vortrag zu Studienfinanzierung und zum dualen Studium konnten die Oberstüfler lernen,
wie man am meisten Geld für das Studium abgreift.
Die Schüler erhielten Infos über BAföG, Studienkredite und Stipendien, insbesondere für angehende Kamelforscher.
Außerdem wurde ausführlich über die Möglichkeit eines dualen Studiums anhand von konkreten Beispielen berichtet.

### Vorträge der Professoren

Nach dem Wandertag besuchten uns zwei hervorragende Professoren der Uni Augsburg:
Professor Bernhard Möller vom Lehrstuhl für Datenbanken und Informationssysteme und
Professor Urs Frauenfelder vom Lehrstuhl für Analysis und Geometrie.
Die Kleinen bekamen einen Einblick in Poincarés Welt der unendlichen Parallelen.
Die Großen entdeckten die Programmiersprache Haskell.

## Sport und Bewegung

### Boot fahren

Jeden Tag wagten sich mutige Matrosen auf die unruhigen Wasser des Eichholzsees.
Tapfer paddelten sie unter Anleitung eines waschechten Kapitäns durch die ungetüme See
und absolvierten dabei die ein oder andere Umrundung der kleinen beschaulichen Insel.
Während der Überfahrt wurde zudem Bekanntschaft mit der heimischen Flora und Fauna gemacht.
So zum Beispiel war der Kontakt mit dem gemeinen bayrischen Flusskrokodil trotz großer Vorsichtigkeit nicht zu vermeiden.

### Bumerang

Wir waren Bumerangwerfen bei einem kleinen Hügel in Violau.
Der Berg war zwar an einem Hang aber es gab Platz für bis zu fünf Kinder.
Bereits nach einer halben Stunde hatten die Ersten schon ein paar Erfolge und ihren Bumerang gegfangen.

### Football

Zunächst wurde das Footballwerfen durch Betreuende ausprobiert.
Als dann auch die älteren Teilnehmenden darauf aufmerksam wurden, bot man eine Fraktivität dazu an.
Am Ende konnten zumindest einige Betreuende und Teilnehmende doch recht gut einen Football werfen.

### Frühsport

Jeden Morgen wurden Kinder und Betreuende, die richtig wach und frisch in den Tag starten wollen,
schon eine Stunde früher als die anderen, um 6:30, geweckt, um an den Mathecamp-Frühsportklassikern teilzunehmen:
Joggen, Todesübungen, Schwimmen, Frisbee, Boot fahren und mehr.
Dabei hatten alle Kinder ausnahmslos viel Spaß, vor allem beim Aufwachen.

Ingo bedankt sich an dieser Stelle bei Taulex fürs liebevolle Wecken jeden Tag!
(Ingo hatte ja kein Handy dabei (wurde ihm vor Wochen gestohlen) und hatte seinen Ersatzwecker leider zu Hause vergessen.)

### Fußball

Wir spielten jeden Tag mehrmals mit verschiedenen Jungen und Mädchen zwischen der 5. und 9. Jahrgangsstufe Fussball.
Dabei teilten diese sich selbst in Teams ein und verhielten sich äußerst fair.

Am Montag fand ein Fußballturnier statt, dessen Sieger gegen ein exzellentes Team aus hervorragend
spielenden Betreuenden spielen durfte.
Um die Freude der vielen mitfiebernden Kinder im Finale zu erhöhen, ließen wir das Schülerteam gewinnen.

### Jonglieren

Nicht vergessen für die 3-Ball-Kaskade: Oberarme locker runterhängen lassen, Unterarme waagrecht, Hände Schulterbreit
und die Hände jeweils um ihre so erreichte Position nach außen kreisen lassen, dabei werfen und fangen, Bälle fliegen
bis zum Gesicht hoch und im schönen Bogen in oder außen neben die Hand.
Und glei nomml!

### Kegeln

Hinter dicken Wänden im ehemaligen Luftschutzbunker, fand die Fraktivität Kegeln statt.
Im Lauf der Woche konnte man trotz der eigenen Physik der Kegelbahn große Fortschritte bei den Kindern sehen,
sodass man sich als Betreuende immer mehr anstrengen musste, um sie zu besiegen.

### Pool

Auf Grund der hohen Temperaturen war der Pool dieses Jahr äußerst beliebt.
Jeden Tag kamen viele verschiedene Gruppen von Kindern ans Betreuendenbüro, um Baden zu gehen.
Im Wasser angekommen verbrachten die Jungen und Mädchen die Zeit mit Schwimmen sowie den unterschiedlichsten Spielen.
Aber nicht nur die Kinder, sondern auch die Betreuenden hatten sichtlich Spaß am und vor allem im Pool.

### Schachturnier

Es fand kein Schachturnier statt.
Aber einige interessierte Kinder kamen zur Freizeitaktivität und hatten viel Spaß dabei, das Spiel zu lernen!

### Tanzkurs

Ein Grundkurs für Anfänger und Aufbaukurs für Fortgeschrittene.
Haben Standard- und Lateintänze getanzt.
Insbeondere der Freestyle, der auch auf der Bunter Abend-Disko getanzt wurde, kam gut an.

### Tischtennis

Ping-Pong.
Das war das typische Geräusch, das man jeden Nachmittag in Nähe der Tischtennisplatten vernehmen konnte.
Dort vergnügten sich die Kinder mit Duellen, Tischtennisturnieren sowie Rundlauf.
Aber nicht nur die Mädchen und Jungen, sondern auch die Betreuende beteiligten sich gerne an dieser Fraktivität.

### Ultimate Frisbee

Jeden Tag am Nachmittag haben wir einige Stunden Ultimate Frisbee gespielt.
Das hat sehr viel Spaß gemacht, und die Kinder haben dabei auch viel über die Regeln,
Taktik und am wichtigsten über den Spirit of the Game gelernt.

### Yoga (Max)

Yoga, eine aus Indien stammende philosophische Lehre, die körperliche und geistige Übungen umfasst.
Wir lernen unseren Körper besser zu verstehen und sehen was uns gut tut.
Dabei legen wir den Fokus nicht auf das meditative Yoga sondern blicken auf die Übungen der Asanas.
Von jeder Übung profitiert unser Körper auf verschiedene Weise.

## Computer und Technisches

### Bildbearbeitung mit GIMP

In eng verpackten, zwei Sitzungen großen Kursen haben wir in die Bildbearbeitung mit GIMP reingeschaut.
Ebenen wurden als aufeinanderliegende Folien verstanden und Betreuende wurden aus Greenscreens herausgearbeitet.
Auch einfache Bilder haben wir gemalt und mit lustigen Effekten rumgespielt.

### Briar-Installationsparty

WhatsApp behauptet von sich, Ende-zu-Ende-verschlüsselt zu sein.
Weil WhatsApp aber nicht freie Software ist, kann das niemand nachprüfen.
Tatsächlich ist davon auszugehen, dass in WhatsApp Hintertüren versteckt sind:
Denn die Leute, die an WhatsApp programmieren, sind amerikanische Bürgerinnen und Bürger und müssen sich
an amerikanisches Recht halten.
Über Geheimgerichte hat die NSA die Möglichkeit, den Einbau von Hintertüren anzuordnen.

Eine freie Alternative zu WhatsApp ist Signal.
An der Uni Augsburg haben alle Signal.
Signal ist genauso wie WhatsApp, nur eben frei.
Fallen die WhatsApp-Server aus oder sind nicht erreichbar, funktioniert WhatsApp nicht; und das ist bei Signal ganz genauso.

Wer es dezentraler mag, der greift zu Briar! Briar ist ein dezentraler Messenger, der Verschlüsselung etc.
richtig macht.
In der Fraktivität haben wir die apk dezentral über Bluetooth verteilt, weil die Internetverbindung down war.
Die Kinder waren sehr interessiert.

### NFC-Armbänder

Wir haben Armbänder aus Kupferdraht, einer Leuchtdiode und einem winzigen Kondensator zusammengelötet.
Kondensator und Spule (der Draht) bilden einen Schwingkreis, der genau auf die Frequenz von 13 MHz abgestimmt ist,
die ein Handy mit NFC-Funktion ausstrahlt.
So fängt die LED zu blinken an, wenn man das Armband flach an ein Handy hält.

### Programmieren

Das diesjährige Thema in der Viereckschanze war das Programmieren von Handyspielen in JavaScript.
Nach ersten Schritten, in denen wir JavaScript nur verwendeten, um statische Bilder zu zeichnen,
machten wir bald unsere Programme zu echten Spielen, indem wir sie um die Fähigkeit erweiterten,
auf Tastatur-, Maus- oder Touchpadeingaben zu reagieren.
Teilweise wurde bis spät in die Nacht programmiert.
Die Chiefs der Viereckschanze waren sehr stolz auf die vielen tollen Ergebnisse!

Für die Oberstufe gab es noch zwei Geheimprojekte: ein Klon von Flappy Bird in Haskell und ein Ultraschall-WhatsApp.

### Satelliten

Aufgrund der suboptimalen Internetsituation gestaltete sich das Satnogs-Bodenstations-Programm etwas schwieriger als geplant.
Nachdem wir schnelle Erfolge beim Zusammenbau der Antennen verzeichneten, gerieten wir bei der Elektronik in Probleme:
Zunächst dauerte der Bestellvorgang aller 8-Cent-0,1µF-Kondensatoren sowie 4-Cent-100-Ω-Widerständen einige Nächte
länger als geplant und dann mangelte es an einem konkreten nutzbaren Schaltplan.
Dafür gelang die Inbetriebnahme des Verstärkers sehr schnell,
so dass wir mit beiden Antennen zig Flugzeuge in der Umgebung sehr schnell orten konnten.
Am Ende des Mathecamps konnten wir eine funktionierende registrierte Satnogs-Bodenstation mit einer Standantenne vorweisen.

## Knobeleien

### Alienzirkel

Wir haben versucht, eine Botschaft zu entschlüsseln, die vor einigen Jahren wirklich ins Weltall geschickt wurde,
damit Alien sie empfangen können, falls es überhaupt Aliens gibt.
Am Anfang der Botschaft wird erst mal ganz viel Mathematik erklärt, dann kommen Physik und Chemie an die Reihe.

### Aufgabe des Tages

Um die Zeit zwischen den Zirkeln für die Kinder sinnvoll zu gestalten, wurde die Aufgabe des Tages etabliert.
Für jeden Tag gab es ein Rätsel, das am Ende des Tages abgegeben werden konnte, um anschließend korrigiert zu werden.
Am Ende der Woche gab es für die besten eine Kleinigkeit.

### Dynamische Labyrinthe

Dynamische Labyrinthe wurden größtenteils eigenständig ausprobiert und einige vorgegebene Ziele wie Sortieren,
Getränkeautomat bauen und die wichtigsten Grundrechenarten auf dem Steckbrett umgesetzt.

### Fold & Cut

Man kann jede beliebige Form aus geraden Linien mit nur einem einzigen geraden Schnitt ausschneiden,
wenn man das Papier vorher richtig faltet! Zuerst haben wir uns bei Dreiecken und Vierecken genau überlegt,
wie und warum das funktioniert, dann die Konstruktion der Faltlinen für beliebige Formen verstanden.
Dank Vorlagen mit eingezeichneten Faltkanten konnten wir sehr schöne Tierformen nach einem filigranen Faltprozess
mit einem Schnitt ausschneiden.

### Zaubertricks

Bei einigen Zaubertricks, jeweils mit immer trickreicheren Eskalationsstufen, lernten die Kinder die Paritäten
von Münzen wertzuschätzen sowie die unerwarteten Auf- und Abstiege von Spielkarten.

## Kreatives, Musik und Entspannung

### Chor

Auch dieses Jahr haben wir wieder gemeinsam im Chor gesungen.
Highlights waren dabei die Lieder "Burn" von Ellie Goulding und "Männerwelt" aus einem Choralbuch.

### Greenscreen Session

Haben die meisten von euch nicht mitgekriegt, aber für einige Stunden hing draußen unser Mini-Greenscreen,
vor dem wir tolle Posen von Betreuenden für die Bildbearbeitung im und nach dem Mathecamp angefertigt haben.
Alle Posen waren geil.

### Gutenachtgeschichten

Jeden Abend gab es noch vor der Bettruhe zur Entspannung die spannenden Erkenntnisse eines träumenden Jungen
mit dem "Zahlenteufel" für die Kleinen.
Für die Großen ging es um eine populärwissenschaftliche Einführung in die Rechtswissenschaft nach Kolumnen
des ehemaligen Bundesrichters Thomas Fischer.

Der volle Titel des Buchs für die Kleinen war "Der Zahlenteufel.
Ein Kopfkissenbuch für alle, die Angst vor der Mathematik haben".
Viele BerufsmathematikerInnen schwärmen von diesem Buch.
Wer es nicht kennt, sollte es unbedingt lesen!

### Hexaflexagons

Wir bastelten viele Hexaflexagons.
Ein Flexagon ist ein Sechseck, das man aus einem Streifen aus gleichseitigen Dreiecken faltet.
Der Gag besteht darin, dass man das Sechseck in der Mitte öffnen kann.
Es erscheint ein neues Sechseck, das vorher verborgen war.
Hexaflexagons wurden durch die Mathemusikerin Vi Hart bekannt: [https://www.youtube.com/watch?v=VIVIegSt81k](https://www.youtube.com/watch?v=VIVIegSt81k)

### Impro-Theater

Beim Impro-Theater kann jeder ohne Vorkenntnisse seiner Kreativität und Spontanität freien Lauf lassen.
Zunächst wärmten wir uns mit ein paar kleineren Übungen auf.
Später konnten wir dann sogar einige richtige Szenen improvisieren.

### Känguru-Chroniken anhören

Nachdem die Känguru-Chroniken bei der Wanderung recht hohen Anklang bei Kindern und Betreuern fanden,
gab es an den letzten zwei Tagen nach dem Mittagessen die Möglichkeit, den Worten Marc Uwe Klings zu lauschen.

### Karaoke

An zwei Abenden haben wir im Schwäbischen Himmelreich Karaoke gesungen und gefeiert.
Dabei wurden wir von ziemlich vielen gesanglichen Talenten überrascht und die Stimmung im Saal vibrierte.
Nächstes Jahr auf jeden Fall wieder!

### Lamas

Lediglich ein Kind wollte die Lamas mit mir besuchen.
Wir waren mutig genug, sie zu streicheln!

### Lamas II

Ich habe spontan ein paar Kinder gefragt, die dann aber alle drei mitwollten
(Äpfel vom Apfelbaum im Gehege schützt gegen Spuckattaken).

### Light Painting

Wenn man bunte Knicklichter, eine gute Kamera und die Nacht mischt, entstehen schöne Kunstwerke.

### Malerei

Beim Malen konnte jeder der Lust hatte seiner Kreativität freien Lauf lassen.
Nicht nur mit Pinseln, sondern auch mit Schwamm, Klopapierrollen und sogar mit Zahnstochern, entstanden traumhafte Bilder
von Landschaften, dem Weltall und viele Figuren wurden zum Leben erweckt.
Gemalt wurde mit Acrylfarben.
Dabei wurde selbstständig ausprobiert wie verschiedene Farbtöne und sogar Hautfarben gemischt werden können.
Die vollendeten Werke wurden dann am Bunten Abend präsentiert.

### Nähen

Beim Nähen haben wir als Einstiegsübung ein Reise-Tic-Tac-Toe oder kleine Taschen genäht.
Danach wurde dann das große Nähprojekt begonnen! Getreu dem Motto: "aus alt mach neu" wurden alte Kleidungsstücke (wie T-Shirts)
mit Textilmarkern, Taschen, Knöpfen und Kragen aufgewertet.

### Orchester

Dieses Jahr haben wir im Orchester das Stück Fluch der Karibik gespielt.
Es war ein schwereres Stück, weswegen wir die ganze Woche daran geprobt haben.
Der Auftritt am bunten Abend war sehr schön.

### Origami

Gemeinsam haben wir uns mit den doch etwas kniffligen Origamianleitungen auseinandergesetzt.
Herauskam eine wunderschöne Landschaft mit Bären, Pinguinen, Schmetterlingen, Walen, Bäumen und vielem mehr.

### Pantomime

Beim Pantomime waren zunächst nur wenige Kinder da.
Wir starteten mit einer kleinen Einführung, was wichtig für ein erfolgreiches Begriffe-Raten ist und wie man als Vormachender
an das Publikum herantreten muss.
Als das recht schnell große Erfolge erzielte, wurden schnell weitere Kinder aufmerksam darauf, warum das denn so gut klappt,
so dass wir die Pantomimerunde am Ende mit knapp 10 Teilnehmenden sehr erfolgreich beendeten.

### Pi Lernen

Wir trafen uns mehrere Male eine Viertelstunde vor dem Abendessen, um gemeinsam die Ziffern von Pi auswendig zu lernen.
Mit dabei waren sowohl alte Pi-Enthusiastinnen und -Enthusiasten, die seit Jahren Freude an den Ziffern von Pi haben,
als auch Neueinsteigende, die wir in der Welt des Pi-Lernens begrüßten.
Uns war klar, dass die Ziffern auswendig zu lernen nichts Mathematisches ist, können die Ziffern doch einfach jederzeit
auf Wikipedia nachgeschlagen werden.
Uns machte es trotzdem Spaß! Problematisch waren nur die unterschiedlichen Betonungen, die manche von uns an den Tag legten.

### Schafkopfen

An bis zu 6 Tischen mit je 4 Tischen wurde bayerische Kultur gelebt und gespielt.
Auch Anfänger durften mitspielen und so hatte (hoffentlich) jeder Spaß! -Stefan

### Schlafen / Fußboden-/Tisch-Powernap

In zwei Kursen wurde geballtes Wissen aus mehr als fünf Jahren professioneller Erholung unter härtesten Bedingungen
vermittelt und ausprobiert.
Ob auf dem Boden, auf einem Tisch, oder beobachtet durch Dozierende: hier konnte jeder etwas mitnehmen.

### Slackline

Wir hatten Spaß auf Slacklines.
Wir haben uns elegant auf gespannten Seilen bewegt, akrobatische Kunststücke geübt und bald auch gekonnt.

### Stickbombs

Stickbombs wurden dieses Jahr hauptsächlich alleine gebaut.
Nach einer wirklich kurzen Einleitung versuchten sich viele Kinder an kleineren Ketten.
Da diese Ketten leider immer wieder aufbrachen und damit kaputtgingen, gab es dieses Jahr keine große Kette
durch das gesamte Haus.
Das war aber trotzdem ok.

### Traumfänger

Der Kurs war sehr schnell ausgebucht und alle Teilnehmenden sehr zufrieden mit ihrem Ergebnis.
Vereinzelt wurden sogar Anleitungen mit geschrieben, um zu Hause auch welche zu basteln.
Wir hatten viel Spaß und die Kinder halfen sich teilweise sogar gegenseitig.
Während und nach dem Kurs wurde ich des öfteren darauf angesprochen, ob der Kurs nochmal angeboten werden würde.

## Besondere Ereignisse

### Bunter Abend

Der Bunte Abend gehörte dieses Jahr zu den kürzeren der Mathecamp-Geschichte.
Als Finale des Camps gab es Spiele, Sketches, Präsentationen und mehr von Betreuenden und vor allem von euch Teilnehmenden.
Die anschließende bettruhefreie Nacht mit Disko, Light Painting und Powernapping
in der Viereckschanze hattet ihr euch voll verdient!

### Pool Party

Dieses Jahr wurde die Pool Party von zwei Teilnehmenden organisiert und für 2h an einem Nachmittag durchgeführt.
Neben kleineren Spielen und einer Limbosession waren vor allem die Wasserbälle viel in Gebrauch.
Mit sommerlicher Musik ein rundum gelungener Nachmittag mit doch recht großer Beteiligung.

### Star Walk

Eine entspannte Wanderung zu einem hohen Hügel in Violau.
Dort haben wir uns auf Handtücher gelegt, Musik gehört und Sternbilder angeschaut.
Geplant ist auch (Karten)spielen, evtl. ein Powernap.

### Sterne gucken

Ein paar kurze Infos über Sterne und Planeten vor dem Zubettgehen, vor allem für kleinere Kinder.
Wir haben einen Blick auf die bemerkenswertesten Sternbilder geworfen.

### Theaterstück Parallelen

War super kurzweilig!

### Wanderung

Bei größter Hitze machten wir uns nach dem Frühstück auf den Weg in die pitoreske bayrische Landschaft auf und ein Trupp
von 130 Personen machte die Flora und Fauna unsicher.
Auf Grund der hervorragenden Planung der Chiefs konnten wir uns an den Bäumen als Donatoren von Schatten erlaben.
Bezeichnend für die Wanderung war die charakteristische Kopfbedeckung, die aus einem kunstvoll verknoteten T-Shirt enstand.
Als die Sonne am Zenit stand, wurde uns dankenswerterweise vom Haus das Mittagessen an unseren Rastplatz an der Spechthütte
nahe dreier Seen gebracht und wir konnten im Schutz der Hütte unser Essen als Stärkung genießen.
Auf dem Rückweg erfrischte uns Max an einer Erfrischungsstation mit Wasser und Äpfeln.

### Zettelmord

Mit einem einzigen Wort auf einem Stück Papier ausgestattet starteten alle Teilnehmenden und Betreuenden ins durchgehend
andauernde Spiel.
Halte dein Wort geheim.
Denn sprichst du das Wort von jemandem aus, wird dir dein Zettel aus deinen kalten, aus dem Spiel ausgeschiedenen Händen
entrissen und mit deinem eigenen Wort weiter gemordet.

## Hinter den Kulissen

### Fraktivitäten

Wir hatten unglaublich viele Fraktivitäten jeden Tag.
Nachts wurden von den magischen Matheelfen Aktionen auf der Pinnwand angebracht, die durch wunderschöne Bilder
illustriert wurden.
Jeden Tag wurden mehr als 10 Fraktivitäten gleichzeitig angeboten, von Sport (der in Tunieren zwischen Betreuenden
und Kindern gipfelte), über Pi lernen bis zu Nähen.
Und natürlich geliebte Projekte wie Programmieren.

### Laptop lösche

Es war die erste Nacht auf dem Mathecamp.
Kilian, Matthias, Fresh Xave und Ingo saßen bei der Steckdose beim Plenum und stellten die sakralen Linux-USB-Sticks fertig.
Das dauerte eine ganze Weile, weil uns immer wieder auffiel, dass wir ein wichtiges Programm vergessen hatten,
das wir euch eigentlich mitgeben wollten.
Dann aber war es soweit! Wir hatten ein Speicherabbild, mit dem wir zufrieden waren, und mussten es nur noch auf viele
USB-Sticks schreiben.
Dazu gibt es unter Linux einen Befehl, "dd" genannt.
(Unter Windows muss man sich erst umständlich ein Werkzeug dazu herunterladen und installieren.)
Man muss "dd" sagen, welches Speicherabbild gebrannt werden soll und wohin es gebrannt werden soll.
Dabei darf man sich nicht vertippen: Schreibt man etwa aus Versehen `/dev/sda` statt `/dev/sdb`,
so wird nicht der erste eingesteckte USB-Stick überschrieben, sondern die Festplatte.
Linux-Konsolen-Programme behandeln ihre Benutzende stets wie Erwachsene, die wissen, was sie tun,
und fragen nicht nach Bestätigung ("wollen Sie wirklich?").
Kilian vertippte sich.

### Pizza

An einem Abend im Camp gibt es immer Pizzas, die zuvor von ein paar der Campteilnehmer belegt werden.
Dieses Jahr durften wir dies zusammen mit acht Kindern machen, was uns sehr viel Spaß bereitet hat :D
Besser als das Belegen war da nur noch die „Belohnung“ am Abend dafür!

### WLAN einrichten

Gestaltete sich schwierig, weil das Setup recht komplex war.
Hat ab Montag zuverlässig funktioniert, die Internetanbindung des Hauses leider nicht.
