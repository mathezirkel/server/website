---
title: Mathecamp 2018
---

## Rückblick aufs Mathecamp 2018

<div class="highlight-box">
18.  bis 26. August 2018

132 Teilnehmende

27 Betreuende

9 wunderschöne Tage!
</div>

## Fraktivitäten

Einen Bericht zu fast allen Fraktivitäten auf dem Camp findet ihr [hier](./fraktivitaeten).

## Materialien zu manchen Zirkeln und Fraktivitäten

### 4D-Vortrag von Matthias und Ingo

Vortragsfolien und Aufzeichnung einer früheren Ausgabe des Vortrags: [http://4d.speicherleck.de/](http://4d.speicherleck.de/)

### Alienzirkel (Cosmic Call)

Im Jahr 1999 sendete die Menschheit eine Radiobotschaft an ausgewählte Sterne, in der Hoffnung,
dass eine dort lebende Zivilisation die Nachricht empfangen, verstehen und auf sie antworten würde: die Cosmic-Call-Botschaft.
Diese Nachricht wurde natürlich nicht auf Deutsch oder Englisch formuliert, sondern bedient sich einer mathematischen Symbolsprache.
Im Zirkel unterzogen wir die Botschaft einem Test: Können wenigstens wir Menschen, die wir einen riesigen Heimvorteil haben,
die Nachricht entziffern?

Ihr könnt die Nachrichten und Erklärungen unter [Materialien](/materialien/) nochmal ansehen.

### Beweise ohne Worte

Beweise spielen in der Mathematik eine ganz zentrale Rolle und füllen auch häufig ein ganzes Buch.
Dass dies aber nicht immer so sein muss, sieht man an den Beispielen aus unserer Sammlung an Beweisen ohne Worte.
Hier geht es darum, anhand eines Bildes zu verstehen, wieso eine Aussage gilt.
Beispielsweise möchte man mit Hilfe einer Skizze herausfinden,
ob der Umfang eines Kreises genauso viele Punkte hat wie eine unendlich lange Gerade.

Wenn ihr daran gerne weiter tüfteln wollt, findet ihr unter [Materialien](/materialien/) die Aufgaben.

Viel Spaß beim Knobeln!

### Bildbearbeitung mit GIMP

Runterladen könnt ihr euch GIMP unter [https://www.gimp.org/downloads/](https://www.gimp.org/downloads/)

Dort findet ihr auch schnelle und einfache Tutorials: [https://www.gimp.org/tutorials/](https://www.gimp.org/tutorials/)

Nicht erschrecken beim ersten Start! In der neuesten Version ist das Standarddesign leider dunkel eingestellt mit weißen,
statt farbigen, Symbolen.
Um das alte, auch im Mathecamp benutzte Design zu kriegen, geht ihr oben in der Menüleiste auf "Bearbeiten",
da dann ziemlich weit unten auf "Einstellungen".
Es geht ein neues, kleines Fenster auf.
Hier in der linken Spalte auf "Thema" klicken, und dann im Fenster Doppelklick auf "System".
Um die Werkzeugsymbole wieder bunt zu kriegen, klickt ihr im selben kleinen Fenster links in der Spalte auf "Symbol Thema".
Da dann Doppelklick auf "Legacy".
Zum Schluss noch unten rechts im Fenster auf "OK" klicken.

Und nicht vergessen: Strg+Z zum rückgängig machen.
Neue Bildelemente als neue Ebenen einfügen.
Wenn Radierer und Löschen Weiß statt Transparenz erzeugen, Rechtsklick auf die Ebene im Ebenenreiter
und "Alphakanal hinzufügen" anklicken.

Wenn etwas nicht klappt:

* Ist die richtige Ebene ausgewählt?
* Schwebt irgendwo eine Auswahl rum (Rechtklick -> Auswahl -> Nichts, oder Strg+Shift+A)?
* Sind die Werkzeugeinstellungen ausversehen crazy verstellt worden?
* Wird gerade die Ebenenmaske bearbeitet, statt die Ebene selbst
  (nur möglich, wenn manuell eine Ebenenmaske hinzugefügt wurde)?

[Hier](https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2018/GIMPlan_V2.pdf) habt ihr noch
das überarbeitete Skript, welches vor allem dem 2. Durchgang der Fraktivität entspricht,
aber auch nicht mehr geschaffte extra Notizen enthält.
Damit könnt ihr auch alles gelernte nochmal wiederholen.

### Binärsystem und Zaubertricks

[https://rawgit.com/iblech/mathezirkel-kurs/master/tag-der-mathematik/2018/zaubertrick/zaubertrick.pdf](https://rawgit.com/iblech/mathezirkel-kurs/master/tag-der-mathematik/2018/zaubertrick/zaubertrick.pdf)

### Digitaltechnik

Alles (und mehr) findet ihr hier: [LINK NICHT MEHR VERFÜGBAR]

Falls ihr Programme damit selbst simulieren wollt: [https://drive.google.com/drive/folders/1B-7h_24MH4pNO2BNZauVoWtQIDyEprZO?usp=sharing](https://drive.google.com/drive/folders/1B-7h_24MH4pNO2BNZauVoWtQIDyEprZO?usp=sharing)

(einfach ganzen Ordner runterladen und mit

```shell
java -jar RetiDebugger.jar
```

oder mit Doppelklick ausführen)

### Dynamische Labyrinthe

Die dynamischen Labyrinthe ("Maschinen bauen") könnt ihr auch am Computer ausprobieren: [http://www.ikm.uni-osnabrueck.de/aktivitaeten/dl/dynamische-labyrinthe.htm](http://www.ikm.uni-osnabrueck.de/aktivitaeten/dl/dynamische-labyrinthe.htm)

Mit Händen zu experimentieren macht natürlich mehr Spaß;
in den Präsenzzirkeln während des Schuljahrs habt ihr dazu bei uns die Möglichkeit!

### Fold and Cut

Jede Figur aus geraden Linien kann man mit einem einzigen geraden Schnitt ausschneiden,
wenn man das Papier vorher richtig faltet!
Hier gibt es die Beispiele, an denen wir es ausprobiert haben, Erklärungen dazu und auch Fachartikel mit Beweisen: [http://erikdemaine.org/foldcut/](http://erikdemaine.org/foldcut/)

### Haskell

Haskell ist eine rein funktionale Programmiersprache.
Felix, Fresh Xave, Ingo, Kilian und Matthias empfehlen sie mit Nachdruck all denen,
die schon mit anderen Programmiersprachen vertraut sind.

Ein guter Einstieg ist das kostenlose Buch [http://learnyouahaskell.com/chapters](http://learnyouahaskell.com/chapters).
Haskell selbst kann man unter Windows und Mac auf [https://www.haskell.org/platform/](https://www.haskell.org/platform/)
herunterladen.
Unter Linux ist Haskell schon dabei.

```haskell
quicksort :: Ord a => [a] -> [a]
quicksort []     = []
quicksort (p:xs) = (quicksort lesser) ++ [p] ++ (quicksort greater)
    where
        lesser  = filter (< p) xs
        greater = filter (>= p) xs
```

### Herkula vs. Hydra

Schafft es Herkula, die Hydra zu besiegen?

1. Ja, sie schafft es!
2. Ja, sie schafft es sogar, egal wie dumm sie sich anstellt!
3. Der Beweis dazu benötigt transfinite Ordinalzahlen!
4. Man kann beweisen, dass der Beweis transfinite Ordinalzahlen oder etwas genauso Mächtiges benötigt!

Im Browser spielbar: [http://math.andrej.com/wp-content/uploads/2008/02/Hydra/hydraApplet.html](http://math.andrej.com/wp-content/uploads/2008/02/Hydra/hydraApplet.html)

Download: [http://math.andrej.com/wp-content/uploads/2008/02/hydra.jar](http://math.andrej.com/wp-content/uploads/2008/02/hydra.jar)

Erklärung auf Englisch: [https://www.youtube.com/watch?v=uWwUpEY4c8o](https://www.youtube.com/watch?v=uWwUpEY4c8o)

### Hexaflexagons

Anleitung zum Basteln der Hexaflexagons: [http://www.mathematische-basteleien.de/flexagon.htm](http://www.mathematische-basteleien.de/flexagon.htm)

Hexaflexagons wurden durch die Mathemusikerin Vi Hart bekannt.
Ihre Videos dazu sind online: [http://vihart.com/hexaflexagons/](http://vihart.com/hexaflexagons/)

### How to be less wrong

Buch (Online): [https://www.lesswrong.com/rationality](https://www.lesswrong.com/rationality)

Forum: [https://lesswrong.com/](https://lesswrong.com/)

### Knotenmemory

Hier gibt es die Datei zum Selberausdrucken und Spielen des Knotenmemories: [http://www.speicherleck.de/iblech/stuff/knotenmemory-einfach.pdf](http://www.speicherleck.de/iblech/stuff/knotenmemory-einfach.pdf)

Die schwierigere Version gibt es noch nicht digital, wird aber auf Anfrage gerne gescannt :-)

### Kosmische Distanzleiter

* [Ohne Erklärungen](https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2018/kosmische-distanzleiter.pdf)
* [Von Terence Tao, einem der besten zur Zeit lebenden Mathematiker; mit Erklärungen auf Englisch](https://terrytao.files.wordpress.com/2010/10/cosmic-distance-ladder.pdf)

### Linux-USB-Sticks verwenden

Mit den Linux-USB-Sticks, die wir euch mitgegeben haben, könnt ihr Linux auf euren Computern testen,
ohne irgendwas installieren zu müssen.
Wenn man den Computer ohne eingesteckten Stick startet, ist alles wie immer.
Auf den Sticks haben wir euch allerlei Programme zusammengestellt.
Kleine Highlights sind vielleicht [Stellarium](http://stellarium.org/) (Sternkonstellationen)
und [Sonic Pi](https://sonic-pi.net/) (Musik programmieren).
Der Tor-Browser ist natürlich ebenso dabei.
Spiele wie Tux Racer und Neverball findet ihr auch.
So startet ihr euer Linux:

1. Einmalig muss unter Windows das sog.
  "Fast Boot" ausgeschaltet werden.
  Wenn Fast Boot aktiviert ist, sucht der Computer beim Starten nämlich gar nicht nach angesteckten USB-Sticks.
  Das Ausschalten ist mit wenigen Klicks getan.
  Folgendes Video erklärt, wie: [https://youtu.be/kNN8cOowYrg?t=151](https://youtu.be/kNN8cOowYrg?t=151)
2. Den Computer herunterfahren und den Linux-USB-Stick einstecken.
3. Den Computer anschalten und direkt nach Betätigen des Einschaltknopfes immer wieder panisch die Tasten `F1` bis `F12`
  in schneller Abfolge drücken, um das Bootmenü herzuholen, in dem ihr dann den USB-Stick auswählen könnt.
  Sobald das Windows-Lade-Logo erscheint, könnt ihr aufhören: Dann wart ihr nicht schnell genug
  und müsst zurück zu Schritt 2.

  Der Hintergrund ist folgender.
  Nach dem Betätigen des Einschaltknopfes und vor dem Erscheinen des Windows-Lade-Logos gibt es einen kurzen Moment,
  während dem der Computer prüft, ob die Bootmenütaste gedrückt wird.
  Leider ist der genaue Zeitpunkt und die genaue Taste bei jedem Laptop verschieden.
  Oft ist es `F8`, `F12` oder `F9`, aber das ist nicht immer so.
  Daher muss man alle Tasten in schneller Abfolge ausprobieren.

Bei Fragen einfach uns schreiben: [mathezirkel@math.uni-augsburg.de](mailto:mathezirkel@math.uni-augsburg.de).
Am besten gleich den Modellnamen des Laptops oder Computers dazu schreiben.

```cowsay
 _________________________________________
/ Don't put off for tomorrow what you can \
| do today because if you enjoy it today, |
\ you can do it again tomorrow.           /
 _________________________________________
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

In diesem Sinne viel Spaß beim Installieren.
Das Programm, das die Kuh sprechen lässt heißt übrigens `cowsay` und ist auf unseren USB-Sticks enthalten.

### Nähen

Hier findet ihr ein paar (kostenlose) Schnittmuster, als Inspiration für eure nächsten großen Näh-Projekte :-)

* Rollmäppchen: [https://www.pattydoo.de/schnittmuster-rollmaeppchen-daisy](https://www.pattydoo.de/schnittmuster-rollmaeppchen-daisy)
* Geometrische Taschen: [https://www.pattydoo.de/schnittmuster-kosmetiktasche-geo-bag](https://www.pattydoo.de/schnittmuster-kosmetiktasche-geo-bag)
* Brillenetui: [https://www.pattydoo.de/schnittmuster-brillenetui](https://www.pattydoo.de/schnittmuster-brillenetui)
* Filzmonster: [https://www.pattydoo.de/schnittmuster-filzmonster](https://www.pattydoo.de/schnittmuster-filzmonster)

Außerdem findet ihr hier ein Tutorial, wie ihr euren eigenen Turnbeutel nähen könnt (alle Nähte, die im Video mit der
Nähmaschine gemacht werden könnt ihr auch mit Hand machen):
[https://www.youtube.com/watch?v=cgCj1kFYQ6I](https://www.youtube.com/watch?v=cgCj1kFYQ6I)

### Pi-Kärtchen

Zum selbst ausdrucken:

* [Pi](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/pi-lernen.pdf)
* [Tau](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/tau-lernen.pdf)
* [Eulersche Zahl](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/e-lernen.pdf)
* [Zeta](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/zeta-lernen.pdf)

### Star Walk

Die Software, die wir fürs Sterneschauen verwendet haben, heißt "Stellarium" und kann kostenlos
auf [https://stellarium.org/de](https://stellarium.org/de) heruntergeladen werden.
Stellarium ist auch als App erhältlich.

### Superturingmaschinen

* [https://www.youtube.com/watch?v=sUqwFbbwHQo](https://www.youtube.com/watch?v=sUqwFbbwHQo)
* [https://www.youtube.com/watch?v=7QQ4Z8QwXUc](https://www.youtube.com/watch?v=7QQ4Z8QwXUc)

### Tor

Tor ist ein wichtiges Werkzeug um anonym im Internet zu surfen.
Diese Seite ist auch im Tor-Netzwerk unter der Adresse

violauflurmpxs5s.onion

zu erreichen.
Diese Adresse könnt ihr nicht mit einem normalen Browser öffnen, ihr braucht dazu den [Tor-Browser](https://www.torproject.org/),
der als [Download](https://www.torproject.org/download/download-easy.html.en) für alle gängigen Plattformen bereitsteht.
Lest auf dieser Seite auch die Hinweise für ein anonymes Nutzen von Tor durch.

> Arguing that you don't care about the right to privacy because you have nothing to hide is no different than saying
> you don't care about free speech because you have nothing to say. -- <cite>Edward Snowden</cite>

### Transfinites Nim

Die Idee dazu übernahmen wir (Georg, Ingo und Matthias) von Joel David Hamkins,
einem der berühmtesten Mengentheoretiker der Welt:
[http://jdh.hamkins.org/transfinite-nim/](http://jdh.hamkins.org/transfinite-nim/)

Der hat noch mehr Spiele in dieser Art, zum Beispiel eine Variante der "Jetzt weiß ich es auch"-Rätsel:
[http://jdh.hamkins.org/now-i-know/](http://jdh.hamkins.org/now-i-know/)

### Über unendlich hinaus zählen

* auf Deutsch, aber mit weniger Details: [https://youtu.be/sUqwFbbwHQo?t=1240](https://youtu.be/sUqwFbbwHQo?t=1240)
* auf Englisch, aber mit mehr Details: [https://www.youtube.com/watch?v=wZzn4INtbwY](https://www.youtube.com/watch?v=wZzn4INtbwY)

### Weltall

Super süße Erklärungsvideos zu Rosetta von der ESA

* [https://www.youtube.com/watch?v=HD2zrF3I_II](https://www.youtube.com/watch?v=HD2zrF3I_II)
* [https://www.youtube.com/watch?v=trljrwTbr4w](https://www.youtube.com/watch?v=trljrwTbr4w)
* [https://www.youtube.com/watch?v=s35Jlwobcqk](https://www.youtube.com/watch?v=s35Jlwobcqk)
* [https://www.youtube.com/watch?v=9pTd_JHULBM](https://www.youtube.com/watch?v=9pTd_JHULBM)
* [https://www.youtube.com/watch?v=0YwrcZVM1MA](https://www.youtube.com/watch?v=0YwrcZVM1MA)
* [https://www.youtube.com/watch?v=AvkPFXdpOQQ](https://www.youtube.com/watch?v=AvkPFXdpOQQ)
* [https://www.youtube.com/watch?v=gDmp4ZhSuIc](https://www.youtube.com/watch?v=gDmp4ZhSuIc)

### Wurzel von Pi

Die Ziffern von Wurzel von Pi kann man auf [Wolfram Alpha](http://m.wolframalpha.com/input/?i=what+is+the+square+root+of+pi%3F)
anzeigen lassen: (Einfach auf "More digits" klicken, wenn man noch mehr Ziffern sehen möchte.)

### Wireshark

Das Programm, das wir in dem Vortrag zu Netzwerken verwendet haben, um uns den Netwerkverkehr anzuschauen,
ist [Wireshark](https://www.wireshark.org/) und ist für alle gängigen [Platformen](https://www.wireshark.org/download.html)
verfügbar.

### Zauberwürfel

Die Anleitung zum Lösen des Zauberwürfels ist [online](http://timbaumann.info/zauberwuerfel).
