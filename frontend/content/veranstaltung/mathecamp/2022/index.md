---
title: Mathecamp 2022
---

## Rückblick

<div class="highlight-box">
20. bis 28. August 2022

140 Teilnehmer:innen

25 Betreuer:innen

9 wunderschöne Tage!
</div>

## Fotogalerie

Wir haben eine [kleine Auswahl](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/top72/)
an Bildern für euch zusammengestellt.
Wenn ihr euch alle Fotos ansehen möchtet, findet ihr die Galerien unter den folgenden Links:

* [Gruppenbilder](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/gruppenbilder/)
* [Zirkel](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/zirkel/)
* [Fraktivitäten](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/fraktivitaeten/)
    * [Bootfahren](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/fraktivitaeten/boot/)
    * [Chor & Orchester](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/fraktivitaeten/chor-orchester/)
    * [Fotofraktivität](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/fraktivitaeten/fotofraktivitaet/)
    * [Karaoke](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/fraktivitaeten/karaoke/)
    * [Lightning Talks](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/fraktivitaeten/lightning-talks/)
    * [Pool](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/fraktivitaeten/pool/)
    * [Programmieren](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/fraktivitaeten/programmieren/)
    * [Spiele](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/fraktivitaeten/spiele/)
    * [Sport](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/fraktivitaeten/sport/)
    * [Tanzen](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/fraktivitaeten/tanzen/)
* [Plenum](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/plenum/)
* [Essen](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/essen/)
* [Wandertag](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/wandertag/)
* [Nachtwanderung](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/nachtwanderung/)
* [Bunter Abend](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/bunter-abend/)
* [Fahrradtour](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/fahrradtour/)
* [Sonstiges](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/sonstiges/)

Die Zugangsdaten hast du von uns per Mail erhalten. Bitte
geb die Zugangsdaten nicht an Fremde weiter und verbreite einzelne Fotos
nur, wenn du die Erlaubnis von allen auf dem Foto abgebildeten Personen
hast.

Möchtest du eine ganze Galerie in Orginalqualität herunterladen,
kannst du oben links auf das Save-Icon klicken.

Hast du selbst noch Fotos oder Videos, die du gerne teilen möchtest?
Dann schickt sie uns an [mathezirkel@math.uni-augsburg.de](mailto:mathezirkel@math.uni-augsburg.de)

Falls du noch Sachen vermisst, gibt es eine Galerie von
[Fundsachen](https://assets.mathezirkel-augsburg.de/intranet/veranstaltung/mathecamp/2022/fotos/fundsachen/). Solltest
du etwas wiedererkennen, schreib uns auch eine E-Mail.

## Auswertung Fragebögen

Auf dem diesjährigen Mathecamp habt ihr immer wieder kurze Fragebögen
ausgefüllt. Dabei haben wir euch unter anderem nach euren (bisherigen)
Lieblingszirkeln und -fraktivitäten gefragt. Das habt ihr in der ersten
Hälfte geantwortet:

<div class="grid-gallery">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2022/doku/doku-zirkel-1.png"
            alt="Wordcloud Zirkel 1"
            width=400
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2022/doku/doku-frakti-1.png"
            alt="Wordcloud Fraktivitäten 1"
            width=400
            loading="lazy"
        >}}
    </figure>
</div>

In der zweiten Hälfte kamen dann natürlich noch viele andere
spannende Sachen hinzu:

<div class="grid-gallery">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2022/doku/doku-zirkel-2.png"
            alt="Wordcloud Zirkel 2"
            width=400
            loading="lazy"
        >}}
    </figure>
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2022/doku/doku-frakti-2.png"
            alt="Wordcloud Fraktivitäten 2"
            width=400
            loading="lazy"
        >}}
    </figure>
</div>

Und auch bei den Nachmittagszirkeln hatten viele von euch sehr viel Spaß.

<div class="centering">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2022/doku/doku-namizi.png"
            alt="Wordcloud Nachmittagszirkel"
            width=400
            loading="lazy"
        >}}
    </figure>
</div>

Bei den Umfragen ist uns noch ein weiterer interessanter Zusammenhang
aufgefallen! Es scheint, als hätten Personen, die mehr als 100
Nachkommastellen von Pi können, im Schnitt deutlich weniger Schlaf, als
Personen, die weniger Nachkommastellen von Pi können. (Was ja auch kaum
verwunderlich ist. Wenn man permanent wilde Zahlen auswendig lernt,
bleibt keine Zeit zum Schlafen!)

In der folgenden Grafik seht ihr auf der x-Achse, was die
Teilnehmer:innen des Mathecamps als durchschnittliche Schlafdauer
angegeben haben. Auf der y-Achse ist die Anzahl der Nachkommastellen von
Pi vermerkt. Die hellroten Punkte sind eure Antworten. In Blau ist
jeweils der Wert eingetragen, den die Personen, die beispielsweise eine
Nachkommastelle von Pi auswendig können, so ungefähr im Durchschnitt an
Stunden schlaf haben. Wenn man sich das jetzt anschaut, sieht man ganz
deutlich, dass die mittlere Schlafdauer bei Personen, die mehr als 100
Nachkommastellen von Pi können, deutlich unter den anderen Gruppen
liegt. Am meisten zuträglich für den Schlaf auf dem Mathecamp ist es
scheinbar, wenn man keine einzige Nachkommastelle kennt.

<div class="centering">
    <figure>
        {{< responsive-image
            src="https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2022/doku/schlaf-pi.png"
            alt="Graph Zusammenhang Schlaf und Kennen von Nachkommastellen von Pi"
            width=600
            loading="lazy"
        >}}
    </figure>
</div>

Kannst du dir erklären, wie man zu dieser Korrelation kommen kann? [^1]

Zum Schluss konntet ihr uns noch mitteilen, an welche Begebenheiten
auf dem Mathecamp ihr besonders gerne zurückdenken werdet. Hier sind
eure [Antworten](https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2022/doku/doku-erlebnisse.pdf).

## Mathematik

### Beweise ohne Worte

Beweise spielen in der Mathematik eine ganz zentrale Rolle und füllen auch
häufig ein ganzes Buch. Dass dies aber nicht immer so sein muss, sieht man an
den Beispielen aus unserer Sammlung an Beweisen ohne Worte.  Hier geht es darum,
anhand eines Bildes zu verstehen, wieso eine Aussage gilt. Beispielsweise möchte
man mithilfe einer Skizze herausfinden, ob der Umfang eines Kreises genauso
viele Punkte hat wie eine unendlich lange Gerade. Wenn du daran gerne (weiter)
tüfteln willst, findest du [hier](/materialien) die Aufgaben.

### Matboj

Wenn du dich schon einmal für den Matboj auf dem Mathecamp nächstes
Jahr vorbereiten möchtest, kannst du dich genauer mit den
[Regeln](https://mathezirkel.gitlab.io/content/matboj-regelwerk/Matboj_Regelwerk.pdf)
auseinandersetzen. Vielleicht findest du ja eine optimale Strategie!

{{< comment "<!-- markdownlint-disable-next-line MD022 MD026 -->" >}}
### Nachmittagszirkel zu Slitherlink und Co.

Wir haben im Nachmittagszirkel die Logikspiele Slitherlink, Kakuro
und Bokkusu kennengelernt. Weiterrätseln könnt ihr beispielsweise
hier:

* [Slitherlink](https://www.janko.at/Raetsel/Slitherlink/index.htm)
* [Kakuro](https://www.janko.at/Raetsel/Kakuro/index.htm)
* [Bokkusu](https://www.janko.at/Raetsel/Kakurasu/index.htm)

### Logikrätsel

Wenn du Spaß an Logikrätseln hast, findest du beispielsweise
[hier](https://www.janko.at/Raetsel/Logik/index.htm) eine große Auswahl an
verschiedenen Rätseln.

### Pi-Kärtchen

Wenn in deiner Sammlung noch eine der Karten mit mathematischen
Funfacts bzw. Konstanten fehlt, kannst du dir diese gerne unter
folgenden Links herunterladen und ausdrucken.

* [Pi](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/pi-lernen.pdf)
* [Tau](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/tau-lernen.pdf)
* [Euler'sche Zahl](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/e-lernen.pdf)
* [Zeta](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/zeta-lernen.pdf)

Willst du das Pi-Kärtchen von uns laminiert haben? Dann schick uns
einfach eine E-Mail an [mathezirkel@math.uni-augsburg.de](mailto:mathezirkel@math.uni-augsburg.de)
oder einen (adressierten, aber nicht unbedingt frankierten)
Rückumschlag.

## Fraktivitäten

### OpenStreetMap

Wir haben Daten für die offene Geodatenbank [OpenStreetMap](https://www.openstreetmap.org/) eingetragen.
Jeder kann die Kartendaten verbessern und neue Daten eintragen. Ein
einfacher Einstieg ist die App *StreetComplete*:

* [Google PlayStore](https://play.google.com/store/apps/details?id=de.westnordost.streetcomplete)
* [F-Droid](https://f-droid.org/packages/de.westnordost.streetcomplete/)

In dieser kann man einfache Fragen beantworten. Wenn man komplizierte
Bearbeitungen vornehmen will oder gerne neue Strukturen eintragen möchte, bietet
es sich an, einen [Editor aus dem OpenStreetMap
Wiki](https://wiki.openstreetmap.org/wiki/Editors) auszusuchen.

### Mathecamp-Reddit

Täglich wurde ein neuer Hirnstupser von einer uns immer noch
unbekannten Person ausgehängt. Wir haben uns Notizen gemacht und können
die täglichen Themen hier für euch auflisten:

* "Ist Wasser nass?"
* Theseus Schiff
    * interessant dazu: [Do Chairs Exist?](https://youtu.be/fXW-QjBsruE)
* Trolley-Problem
    * weitere Probleme: [Absurd Trolley Problems](https://neal.fun/absurd-trolley-problems/)
* Diskontinuierliche Existenz
    * Serienempfehlung: [Living With Yourself](https://en.wikipedia.org/wiki/Living_with_Yourself)
* 500.000-Jahre-Knopf
* [Teletransportations-Paradox](https://en.wikipedia.org/wiki/Teletransportation_paradox)
* Realitätssimulation
* [5-Minuten-These](https://en.wikipedia.org/wiki/Omphalos_hypothesis#Five-minute_hypothesis)

### Programmieren

#### Agda

Wer so richtig in Agda einsteigen möchte, kann die spannende Reise
[hier](https://agdapad.quasicoherent.io/) beginnen.

#### Python Turtle

Direkt losprogrammieren kann man auf [trinket.io](https://trinket.io). Eine grundlegende Einleitung,
auch zu ein paar einfachen Fraktalen, gibt es
[hier](https://gitlab.com/CptMaister/spass-mit-fraktalen/-/tree/master/Malen-in-Python-Turtle)
(ein bisschen runterscrollen).

#### Spieleprogrammierung mit Godot

[Godot](https://godotengine.org/) muss nicht installiert
werden, um Spiele zu programmieren. Es muss nur eine ca. 30 MB große
Datei heruntergeladen und dann doppelgeklickt werden. Wir haben auf dem
Mathecamp mit Godot 3.4.4 gearbeitet, mittlerweile gibt es schon Version
3.5 und alle warten gerade schon gespannt auf den nächsten großen
Versionssprung zu Version 4. Hier sind ein paar Downloadlinks für die
gängigsten Betriebssysteme:

* [Windows](https://godotengine.org/download/windows)
* [Linux/Ubuntu](https://godotengine.org/download/linux)
* [MacOS](https://godotengine.org/download/osx)
* in vielen Linux-Distributionen lässt sich godot auch direkt über den Paketmanager installieren

Im Mathecamp haben wir einen sehr funktionsarmen Space Shooter
programmiert. Hier sind ein paar Tutorials, aus denen ich unseren
Mathecamp-Space-Shooter zusammengestellt habe. Es gibt bestimmt auch
bessere Tutorials, vor allem für aufwendigere Space Shooter. Für den
Anfang kann ich empfehlen, sich einfach eine Playliste auszusuchen und
diese durchzuarbeiten. Dauert sowieso höchstens ein paar Stunden:

* [Playlist: “Make a Space Shooting Game in Godot](https://www.youtube.com/watch?v=Z9W6dlP-RB8&amp;list=PLv3l-oZCXaqkUEqrLsKJIAhAxK_Im6Qew)
* [Top Down 2D Shooting in Godot](https://www.youtube.com/watch?v=cei9BZMzVLY)
* [Ein sehr schnelles Tutorial für Top-Down-Shooter](https://www.youtube.com/watch?v=HycyFNQfqI0)
* Gute Tutorials für Anfänger zu verschiedensten Spielen gibt es wohl auch bei [GDQuest](https://www.gdquest.com/)
* Ein sehr gefeiertes Tutorial ist außerdem [Godot Action RPG Series](https://www.youtube.com/watch?v=mAbG8Oi-SvQ&amp;list=PL9FzW-m48fn2SlrW0KoLT4n5egNdX-W9a)

Nachdem man ein kurzes Tutorial durchgearbeitet hat (höchstens einen Tag, nicht
aufwendiger für den Anfang!), kann es sich sehr lohnen, sich durch die
[offizielle Dokumentation](https://docs.godotengine.org/en/stable/)
Dokumentation von Godot durchzulesen, mindestens die ersten zwei großen Teile
“General” und “Getting Started”. Den Teil “Tutorials” und seine Unterkapitel
immer genau dann, wenn man zu den Unterkapiteln direkt etwas für das nächste
Projekt wissen muss.

### Fotografie

* [Tipps](https://phototravellers.de/smartphone-fotografie-tipps/) zur Komposition für Handyfotos
* [Belichtungsdreieck](https://www.foto-kurs.com/belichtungsdreieck.htm)
* Fotos von euch findet ihr im Ordner Fotofraktivität

### Stricken

Eine gut verständliche Anleitung zu den Anfangsmaschen, rechten Maschen und
linken Maschen findet ihr beispielsweise
[hier](https://www.youtube.com/watch?v=PSGZrIihtHw).Falls ihr auf der Suche nach
einem neuen Strick-Projekt seid: Wie wärs denn mit einer
[Handytasche](https://www.frag-mutti.de/handytasche-stricken-a45184/).

### Häkeln

Die Grundlagen zum Häkeln werden nochmal in diesem
[Video](https://www.youtube.com/watch?v=ulhE7NX4gVs) erklärt. Wenn ihr zu Hause
auch einen coolen Blumen-Schlüsselanhänger häkeln wollt, könnt ihr
beispielsweise die [folgende
Anleitung](https://blog.myboshi.net/blumen-haekeln-mini-diy/) nutzen.

### Alienzirkel (Cosmic Call)

Im Jahr 1999 sendete die Menschheit eine Radiobotschaft an ausgewählte Sterne,
in der Hoffnung, dass eine dort lebende Zivilisation die Nachricht empfangen,
verstehen und auf sie antworten würde: die Cosmic-Call-Botschaft. Diese
Nachricht wurde natürlich nicht auf Deutsch oder Englisch formuliert, sondern
bedient sich einer mathematischen Symbolsprache. Im Zirkel unterzogen wir die
Botschaft einem Test: Können wenigstens wir Menschen, die wir einen riesigen
Heimvorteil haben, die Nachricht entziffern? Wenn du gerne weiter knobeln
möchtest, findest du
[hier](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2016/cosmic-call/message.pdf)
die Botschaften und
[hier](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2016/cosmic-call/spoiler.pdf)
die Auflösung.

### Power-Point-Karaoke

Spontan einfach mal **** zu einem random Thema labern? - Das ist
PowerPoint-Karaoke! Anstatt wie beim normalen Karaoke einen Liedtext zu
singen, muss beim Power-Point-Karaoke ein Vortrag zu einer unbekannten,
zufälligen Präsentation gehalten werden. Was man erzählt, ist egal,
solange es irgendwie zu den Folien passt. Dabei kommt es insbesondere
nicht auf einen wissenschaftlich korrekten Vortrag an - das ist ja auch
gar nicht möglich, da man sich in dem Themenbereich in der Regel
überhaupt nicht auskennt.

Power-Point-Karaoke kann also jede:r und es entstehen immer viele Lacher. Wenn
du zu Hause mit Freunden oder Familie spielen willst und gerade keine passenden
Folien zur Hand hast, gibt es [hier](https://kapopo.de/) einige kostenlose
Präsentationen.

### Zauberwürfel

Die Anleitung zum Lösen eines 3x3 Zauberwürfels findest du auf [Tims
Website](https://timbaumann.info/zauberwuerfel).

## Sport

### Tanzalarm

Für eine Extraportion gute Laune sei auf den
[Tanzalarm](https://www.youtube.com/watch?v=duLCpDo5K7I) verwiesen.

### Yoga

Wenn ihr weiter regelmäßig Yoga machen möchtet, kann ich euch diesen
[30-Tage-Plan](https://madymorrison.com/go/30-tage-yoga) empfehlen. Auf der
Seite sind YouTube-Videos verlinkt, mit denen ihr einfach zu Hause die Einheiten
durchführen könnt.

### Kegeln

Kegeln ist eine Präzisionssportart, bei der ein Spieler von einem Ende einer
glatten Bahn aus (Kegelbahn) mit kontrolliertem Schwung eine Kugel ins Rollen
bringt, um die am anderen Ende der Bahn aufgestellten neun Kegel umzulegen. Die
Kegel sind dabei gleichmäßig in Form eines Quadrats angeordnet, das auf der
Spitze steht (quadratische Raute).  Falls ihr außerhalb des Mathecamps kegeln
wollt, findet ihr sicher in eurer näheren Umgebung eine Kegelbahn, die für einen
schmalen Taler gemietet werden kann. Inspiration für verschiedene Spiele beim
Kegeln lässt sich zum Beispiel
[hier](https://www.planet-wissen.de/gesellschaft/sport/kegeln/pwiekegelspiele100.html)
finden.

### Völkerball

Falls du nochmal die Regeln von Völkerball checken oder neue fancy
Spielvarianten kennenlernen möchtest, kannst du beispielsweise auf der folgenden
[Website](https://wimasu.de/voelkerball/) vorbeischauen.

### Spikeball (eigentlich “Roundnet”)

Dieses [YouTube-Video](https://www.youtube.com/watch?v=cPmLwuZ5lWM)
erklärt nochmal ausführlich alle Regeln.

Das Set, das wir im Mathecamp verwenden (cool, weil es extrem robust
ist), kannst du u.a. [hier](https://www.roundnet-deutschland.de/shop/spikeball-pro-set.htm)
oder [hier](https://spikeball.eu/products/spikeball-pro-kit)
kaufen.

Ein [Highlight-Video](https://www.youtube.com/watch?v=mo4qTH-VYGM)
zum Nacheifern (auf Englisch) findest du auf YouTube.

Zudem findet man viele weitere Videos mit Tipps und Tricks auf
YouTube - einfach mal danach suchen!

## Abschluss und Mathecamp 2023

Schön, dass du bis hierher durchgehalten hast! Vermutlich hätten wir
mit den zusätzlichen Materialien auch noch ein Camp füllen können
:-)

Apropos, das Mathecamp 2023 findet vom **19. bis 27. August
2023** statt. Wir freuen uns schon auf dich!

<div class="centering">
    <figure>
        <img src="gregor-mc-22.svg" width="500" alt="Gregor 2022">
    </figure>
</div>

[^1]: Es ist natürlich etwas willkürlich einen Zusammenhang
zwischen Schlafdauer und wie viele Nachkommastellen von Pi man auswendig
kann zu vermuten. Die resultierenden Unterschiede könnten daher kommen,
dass eher jüngere Teilnehmer:innen Pi gar nicht kennen oder wenn dann
nur sehr wenige Nachkommastellen. Die Kreiszahl behandelt man nämlich in
der Schule zum ersten Mal erst in der 8. Klasse. Wenn man noch etwas
jünger ist, braucht man natürlich auch deutlich mehr Schlaf, als
beispielsweise in der Oberstufe. Die Grafik zeigt also vermutlich eher,
dass jüngere Mathecampteilnehmer:innen mehr schlafen als ältere. Das
muss ja auch so sein, weil alle immer ganz brav zu den Nachtruhe-Zeiten
im Bett waren ;-)
