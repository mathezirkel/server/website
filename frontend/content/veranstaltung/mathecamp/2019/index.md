---
title: Mathecamp 2019
---

## Rückblick aufs Mathecamp 2019

<div class="highlight-box">
17. bis 25. August 2019

144 Teilnehmende

26 Betreuende

9 wunderschöne Tage!
</div>

## Materialien zu manchen Zirkeln und Fraktivitäten

### Alienzirkel (Cosmic Call)

Im Jahr 1999 sendete die Menschheit eine Radiobotschaft an ausgewählte Sterne, in der Hoffnung,
dass eine dort lebende Zivilisation die Nachricht empfangen, verstehen und auf sie antworten würde: die Cosmic-Call-Botschaft.
Diese Nachricht wurde natürlich nicht auf Deutsch oder Englisch formuliert, sondern bedient sich einer mathematischen Symbolsprache.
Im Zirkel unterzogen wir die Botschaft einem Test: Können wenigstens wir Menschen, die wir einen riesigen Heimvorteil haben,
die Nachricht entziffern?

Ihr könnt die Nachrichten und Erklärungen unter [Materialien](/materialien/) nochmal ansehen.

### Beweise ohne Worte

Beweise spielen in der Mathematik eine ganz zentrale Rolle und füllen auch häufig ein ganzes Buch.
Dass dies aber nicht immer so sein muss, sieht man an den Beispielen aus unserer Sammlung an Beweisen ohne Worte.
Hier geht es darum, anhand eines Bildes zu verstehen, wieso eine Aussage gilt.
Beispielsweise möchte man mit Hilfe einer Skizze herausfinden,
ob der Umfang eines Kreises genauso viele Punkte hat wie eine unendlich lange Gerade.

Wenn ihr daran gerne weiter tüfteln wollt, findet ihr unter [Materialien](/materialien/) die Aufgaben.

### Bildbearbeitung mit GIMP

Runterladen könnt ihr euch GIMP unter [https://www.gimp.org/downloads/](https://www.gimp.org/downloads/)

Dort findet ihr auch schnelle und einfache Tutorials: [https://www.gimp.org/tutorials/](https://www.gimp.org/tutorials/)

Nicht erschrecken beim ersten Start!
In der neuesten Version ist das Standarddesign leider dunkel eingestellt mit weißen, statt farbigen, Symbolen.
Um das alte, auch im Mathecamp benutzte Design zu kriegen, geht ihr oben in der Menüleiste auf "Bearbeiten",
da dann ziemlich weit unten auf "Einstellungen".
Es geht ein neues, kleines Fenster auf.
Hier in der linken Spalte auf "Thema" klicken und dann im Fenster Doppelklick auf "System".
Um die Werkzeugsymbole wieder bunt zu kriegen, klickt ihr im selben kleinen Fenster links in der Spalte auf "Symbol Thema".
Da dann Doppelklick auf "Legacy".
Zum Schluss noch unten rechts im Fenster auf "OK" klicken.

Und nicht vergessen:

* Strg+Z zum rückgängig machen.
* Neue Bildelemente als neue Ebenen einfügen.
* Wenn Radierer und Löschen Weiß statt Transparenz erzeugen,
* Rechtsklick auf die Ebene im Ebenenreiter und "Alphakanal hinzufügen" anklicken.

Wenn etwas nicht klappt:

* Ist die richtige Ebene ausgewählt?
* Schwebt irgendwo eine Auswahl rum (Rechtklick -> Auswahl -> Nichts, oder Strg+Shift+A)?
* Sind die Werkzeugeinstellungen ausversehen crazy verstellt worden?
* Wird gerade die Ebenenmaske bearbeitet, statt die Ebene selbst
  (nur möglich, wenn manuell eine Ebenenmaske hinzugefügt wurde)?

Hier habt ihr noch das
[überarbeitete Skript](https://assets.mathezirkel-augsburg.de/static/veranstaltung/mathecamp/2018/GIMPlan_V2.pdf),
welches vor allem dem 2. Durchgang der Fraktivität entspricht,
aber auch nicht mehr geschaffte extra Notizen enthält.

### Binärsystem und Zaubertricks

[https://rawgit.com/iblech/mathezirkel-kurs/master/tag-der-mathematik/2018/zaubertrick/zaubertrick.pdf](https://rawgit.com/iblech/mathezirkel-kurs/master/tag-der-mathematik/2018/zaubertrick/zaubertrick.pdf)

### Blender

Weitere Informationen und einen Downloadlink zum OpenSource 3D-Animationsprogramm Blender findet ihr unter: [https://www.blender.org](https://www.blender.org)

### Climate Interactive

Wie wirken sich politische Entscheidungen auf den Klimawandel aus?
Das könnt ihr nochmal hier online ausprobieren: [https://croadsworldclimate.climateinteractive.org/](https://croadsworldclimate.climateinteractive.org/)
Es gibt auch eine etwas erweiterte Version des Programms, die ihr euch
über [https://www.climateinteractive.org/](https://www.climateinteractive.org/) herunterladen könnt.
Hier sieht man z.B. auch gleich noch die Auswirkungen auf den Meeresspiegel.

### Domino-Addierer

Mit den Dominosteinen kann man logische Verknüpfungen bauen und Binärzahlen addieren.
Die Baupläne dazu findet ihr [hier](http://www.think-maths.co.uk/sites/default/files/2018-01/Think%20Maths%20-%20Domino%20Computer%20Worksheets_0.pdf).

### Dynamische Labyrinthe

Die dynamischen Labyrinthe ("Maschinen bauen") könnt ihr auch am Computer ausprobieren: [http://www.ikm.uni-osnabrueck.de/aktivitaeten/dl/dynamische-labyrinthe.htm](http://www.ikm.uni-osnabrueck.de/aktivitaeten/dl/dynamische-labyrinthe.htm)

Mit Händen zu experimentieren macht natürlich mehr Spaß;
in den Präsenzzirkeln während des Schuljahrs habt ihr dazu bei uns die Möglichkeit!

### Fold and Cut

Jede Figur aus geraden Linien kann man mit einem einzigen geraden Schnitt ausschneiden,
wenn man das Papier vorher richtig faltet!
Hier gibt es die Beispiele, an denen wir es ausprobiert haben, Erklärungen dazu und auch Fachartikel mit Beweisen:
[http://erikdemaine.org/foldcut/](http://erikdemaine.org/foldcut/)

### Forth

Es gibt viele verschiedene Implementationen von FORTH (d.h. viele verschiedene Programme, die FORTH-Code ausführen können).
Nicht alle verhalten sich genau gleich, aber es gibt zwei Standards, namens "ANS FORTH" und "Forth 2012",
an die sich viele halten.
(Deine Lieblingssuchmaschine wird sicher spannendes zu diesen beiden Begriffen sagen können.)

Michael verwendet gforth: [https://www.gnu.org/software/gforth/](https://www.gnu.org/software/gforth/)

Das Handbuch von gforth ([http://www.complang.tuwien.ac.at/forth/gforth/Docs-html/](http://www.complang.tuwien.ac.at/forth/gforth/Docs-html/))
ist sehr umfangreich und enthält ein Tutorial für Einsteiger.

### Fraktale Programmieren

Auf [https://gitlab.com/CptMaister/spass-mit-fraktalen/tree/master/Malen-in-Python-Turtle](https://gitlab.com/CptMaister/spass-mit-fraktalen/tree/master/Malen-in-Python-Turtle)
ist eine ausführliche Anleitung (einfach ein bisschen nach unten scrollen) zu allen wichtigen Schildkrötenbewegungen
und -eigenheiten, die ihr braucht, um Fraktale zu malen.
Dort findet ihr auch Anleitungen zu drei der bekanntesten Fraktale.
Jeweils bei diesen Fraktalanleitungen findet ihr ganz unten noch Bonusprogrammierherausforderungen.

### Haskell

Haskell ist eine rein funktionale Programmiersprache.
Felix, Fresh Xave, Ingo, Kilian und Matthias empfehlen sie mit Nachdruck all denen,
die schon mit anderen Programmiersprachen vertraut sind.

Ein guter Einstieg ist das kostenlose Buch [http://learnyouahaskell.com/chapters](http://learnyouahaskell.com/chapters).
Haskell selbst kann man unter Windows und Mac auf [https://www.haskell.org/platform/](https://www.haskell.org/platform/)
herunterladen. Unter Linux ist Haskell schon dabei.

```haskell
quicksort :: Ord a => [a] -> [a]
quicksort [] = []
quicksort (p:xs) = (quicksort lesser) ++ [p] ++ (quicksort greater)
where
    lesser = filter (< p) xs
    greater = filter (>= p) xs
```

### Hexaflexagons

Anleitung zum Basteln der Hexaflexagons: [http://www.mathematische-basteleien.de/flexagon.htm](http://www.mathematische-basteleien.de/flexagon.htm)

Hexaflexagons wurden durch die Mathemusikerin Vi Hart bekannt.
Ihre Videos dazu sind online: [http://vihart.com/hexaflexagons/](http://vihart.com/hexaflexagons/)

### Kryptographie

Hier findet ihr ein kleines Programm, mit dem ihr verschiedene Verschlüsselungen ausprobieren könnt:
[https://github.com/PSeminarKryptographie/MonkeyCrypt/releases](https://github.com/PSeminarKryptographie/MonkeyCrypt/releases)
(dafür müsst ihr die neueste JAR-Datei herunterladen).

### Linux-USB-Sticks verwenden

Mit den Linux-USB-Sticks, die wir euch mitgegeben haben, könnt ihr Linux auf euren Computern testen,
ohne irgendwas installieren zu müssen.
Wenn man den Computer ohne eingesteckten Stick startet, ist alles wie immer.
Auf den Sticks haben wir euch allerlei Programme zusammengestellt.
Kleine Highlights sind vielleicht [Stellarium](http://stellarium.org/) (Sternkonstellationen)
und [Sonic Pi](https://sonic-pi.net/) (Musik programmieren).
Der Tor-Browser ist natürlich ebenso dabei.
Spiele wie Tux Racer und Neverball findet ihr auch.

So startet ihr euer Linux:

1. Einmalig muss unter Windows das sog. "Fast Boot" ausgeschaltet werden.
Wenn Fast Boot aktiviert ist, sucht der Computer beim Starten nämlich gar nicht nach angesteckten USB-Sticks.
Das Ausschalten ist mit wenigen Klicks getan.
Folgendes Video erklärt, wie: [https://youtu.be/kNN8cOowYrg?t=151](https://youtu.be/kNN8cOowYrg?t=151)
2. Den Computer herunterfahren und den Linux-USB-Stick einstecken.
3. Den Computer anschalten und direkt nach Betätigen des Einschaltknopfes immer wieder panisch die Tasten <kbd>F1</kbd>
  bis <kbd>F12</kbd> in schneller Abfolge drücken, um das Bootmenü herzuholen, in dem ihr dann den USB-Stick auswählen könnt.
  Sobald das Windows-Lade-Logo erscheint, könnt ihr aufhören:
  Dann wart ihr nicht schnell genug und müsst zurück zu Schritt 2.

  Der Hintergrund ist folgender.
  Nach dem Betätigen des Einschaltknopfes und vor dem Erscheinen des Windows-Lade-Logos gibt es einen kurzen Moment,
  während dem der Computer prüft, ob die Bootmenütaste gedrückt wird.
  Leider ist der genaue Zeitpunkt und die genaue Taste bei jedem Laptop verschieden.
  Oft ist es <kbd>F8</kbd>, <kbd>F12</kbd> oder <kbd>F9</kbd>, aber das ist nicht immer so.
  Daher muss man alle Tasten in schneller Abfolge ausprobieren.

Bei Fragen einfach uns schreiben: [mathezirkel@math.uni-augsburg.de](mailto:mathezirkel@math.uni-augsburg.de).
Am besten gleich den Modellnamen des Laptops oder Computers dazu schreiben.

```cowsay
 _________________________________________
/ Don't put off for tomorrow what you can \
| do today because if you enjoy it today, |
\ you can do it again tomorrow.           /
 _________________________________________
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

In diesem Sinne viel Spaß beim Installieren.
Das Programm, das die Kuh sprechen lässt heißt übrigens `cowsay` und ist auf unseren USB-Sticks enthalten.

### Mandelbrotmenge (und andere Fraktale)

Das Program, mit dem ihr euch verschiedene Fraktale anschauen könnt:
[http://matek.hu/xaos/doku.php](http://matek.hu/xaos/doku.php)
und das Mandelbrotmengenlied findet ihr unter folgendem Link: [https://www.youtube.com/watch?v=6tsutU92rrE](https://www.youtube.com/watch?v=6tsutU92rrE)

### Nähen

Hier findet ihr ein paar (kostenlose) Schnittmuster, als Inspiration für eure nächsten großen Näh-Projekte :-)

* Rollmäppchen: [https://www.pattydoo.de/schnittmuster-rollmaeppchen-daisy](https://www.pattydoo.de/schnittmuster-rollmaeppchen-daisy)
* Geometrische Taschen: [https://www.pattydoo.de/schnittmuster-kosmetiktasche-geo-bag](https://www.pattydoo.de/schnittmuster-kosmetiktasche-geo-bag)
* Brillenetui: [https://www.pattydoo.de/schnittmuster-brillenetui](https://www.pattydoo.de/schnittmuster-brillenetui)
* Filzmonster: [https://www.pattydoo.de/schnittmuster-filzmonster](https://www.pattydoo.de/schnittmuster-filzmonster)

Außerdem findet ihr hier ein Tutorial, wie ihr euren eigenen Turnbeutel nähen könnt
(alle Nähte, die im Video mit der Nähmaschine gemacht werden könnt ihr auch mit Hand machen):
[https://www.youtube.com/watch?v=cgCj1kFYQ6I](https://www.youtube.com/watch?v=cgCj1kFYQ6I)

### NixOS

Auf den Computern in der Vierecksschanze haben wir nicht Windows verwendet, sondern NixOS, eine Variante von Linux.
Falls ihr mehr über NixOS lernen wollt oder ihr es auch selbst Mal ausprobieren wollt,
seien euch die folgenden Ressourcen ans Herz gelegt:

* [https://nixos.org/](https://nixos.org/)
* [https://github.com/NixOS/nixpkgs](https://github.com/NixOS/nixpkgs)

### Open Street Maps

Ihr könnt OSM (Open Street Maps) auch einfach selbst als Karte und zur Navigation nutzen.
Dafür habt ihr unter anderem die folgenden Möglichkeiten:

* die offizielle Webseite: [https://www.openstreetmap.org/](https://www.openstreetmap.org/)
* OsmAnd ([https://osmand.net](https://osmand.net)) eine App für Android und iOS:
    * [https://play.google.com/store/apps/details?id=net.osmand](https://play.google.com/store/apps/details?id=net.osmand)
      (Playstore)
    * [https://f-droid.org/en/packages/net.osmand.plus/](https://f-droid.org/en/packages/net.osmand.plus/) (FDroid).

### Pen and Paper

Wenn ihr auf dem Camp auf den Geschmack von Pen and Paper gekommen seid und zu Hause weiter spielen möchtet,
können euch die folgenden Links weiterhelfen:

* Link zu PDF-Download/ Beispielcharakteren/ Programm zur Charaktererstellung: [https://ilarisblog.wordpress.com/downloads/](https://ilarisblog.wordpress.com/downloads/)
* Link zum Regelbuch im Onlineshop: [https://www.f-shop.de/rollenspiele/das-schwarze-auge-aventurien/limitiert-sondereditionen/68095/ilaris-das-alternative-regelwerk-fuer-dsa](https://www.f-shop.de/rollenspiele/das-schwarze-auge-aventurien/limitiert-sondereditionen/68095/ilaris-das-alternative-regelwerk-fuer-dsa)

### Pi-Kärtchen

Zum selbst ausdrucken:

* [Pi](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/pi-lernen.pdf)
* [Tau](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/tau-lernen.pdf)
* [Eulersche Zahl](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/e-lernen.pdf)
* [Zeta](https://rawgit.com/iblech/mathezirkel-kurs/master/mathecamp-2014/pi-lernen/zeta-lernen.pdf)

### Pixelflut

Weitere Informationen zu Pixelflut findet ihr auf den folgenden Webseiten:

* Erklärungen: [https://cccgoe.de/wiki/Pixelflut](https://cccgoe.de/wiki/Pixelflut)
* Code: [https://gitlab.com/search?utf8=%E2%9C%93&search=pixelflut&group_id=&repository_ref=](https://gitlab.com/search?utf8=%E2%9C%93&search=pixelflut&group_id=&repository_ref=)

### Povray

Wenn ihr euch noch weiter mit Povray beschäftigen möchtet, seien euch folgende Links ans Herz gelegt:

* [https://github.com/POV-Ray/povray](https://github.com/POV-Ray/povray)
* [http://povray.org/](http://povray.org/)

### Raspberry Pi

Auf dem Mathecamp haben wir mit Raspberry Pis coole LED-Bildschirme programmiert.
Wenn ihr noch weitere Projekte starten möchtet, findet ihr unter folgenden Links dazu Hilfe:

* [https://max7219.readthedocs.io/en/0.2.3/](https://max7219.readthedocs.io/en/0.2.3/)
* [https://tutorials-raspberrypi.de/led-dot-matrix-zusammenbau-und-installation/](https://tutorials-raspberrypi.de/led-dot-matrix-zusammenbau-und-installation/)
* [https://tutorials-raspberrypi.de/pong-auf-max7219-matrizen-spielen-mit-joystick/](https://tutorials-raspberrypi.de/pong-auf-max7219-matrizen-spielen-mit-joystick/)

### Star Walk

Die Software, die wir fürs Sterneschauen verwendet haben, heißt "Stellarium"
und kann kostenlos auf [https://stellarium.org/de](https://stellarium.org/de) heruntergeladen werden.
Stellarium ist auch als App erhältlich.

### Sterne schauen und Zeit ablesen

* Spacemuel und Taulex verwendeten das kostenlose Programm Stellarium um tagsüber auf dem Laptop in die Sterne zu schauen.
Das Programm findet ihr auf [http://stellarium.org/](http://stellarium.org/)
* Tipp für Zeitableser und Profis: im Programm fährt am linken Bildschirmrand eine Leiste heraus.
Dort kann man Ort und Zeit bequem verstellen.
Vor allem beim Zeitablesen ist es wichtig, dass man sich immer relativ zur Zeitzonengrenze an der gleichen Stelle aufhält.
Da Paris sich eine Zeitzone mit Augsburg teilt, aber etwa eine Zeitzonenbreite westlich von Augsburg liegt,
entsteht so ein wesentlicher Fehler, wenn man Paris einstellt, sich aber in Augsburg befindet.

### Superturingmaschinen

* [https://www.youtube.com/watch?v=sUqwFbbwHQo](https://www.youtube.com/watch?v=sUqwFbbwHQo)
* [https://www.youtube.com/watch?v=7QQ4Z8QwXUc](https://www.youtube.com/watch?v=7QQ4Z8QwXUc)

### Tor

Tor ist ein wichtiges Werkzeug um anonym im Internet zu surfen.
Diese Seite ist auch im Tor-Netzwerk unter der Adresse

[violauflurmpxs5s.onion](violauflurmpxs5s.onion)

zu erreichen.
Diese Adresse könnt ihr nicht mit einem normalen Browser öffnen, ihr braucht dazu den [Tor-Browser](https://www.torproject.org/),
der als [Download](https://www.torproject.org/download/download-easy.html.en) für alle gängigen Plattformen bereitsteht.
Lest auf dieser Seite auch die Hinweise für ein anonymes Nutzen von Tor durch.

> Arguing that you don't care about the right to privacy because you have nothing to hide is no different than saying
> you don't care about free speech because you have nothing to say. -- <cite>Edward Snowden</cite>

### Transfinites Nim

Die Idee dazu übernahmen wir (Georg, Ingo und Matthias) von Joel David Hamkins,
einem der berühmtesten Mengentheoretiker der Welt: [http://jdh.hamkins.org/transfinite-nim/](http://jdh.hamkins.org/transfinite-nim/)

Der hat noch mehr Spiele in dieser Art, zum Beispiel eine Variante der "Jetzt weiß ich es auch"-Rätsel: [http://jdh.hamkins.org/now-i-know/](http://jdh.hamkins.org/now-i-know/)

### Über unendlich hinaus zählen

* auf Deutsch, aber mit weniger Details: [https://youtu.be/sUqwFbbwHQo?t=1240](https://youtu.be/sUqwFbbwHQo?t=1240)
* auf Englisch, aber mit mehr Details: [https://www.youtube.com/watch?v=wZzn4INtbwY](https://www.youtube.com/watch?v=wZzn4INtbwY)

### Videobearbeitung

Auf dem Mathecamp haben wir Kdenlive verwendet zum Videos schneiden.

Als Inspiration für die Videos verwendeten wir ein [Video von Ed Banger Records](https://www.youtube.com/watch?v=hpjV962DLWs)
und das [Intro zu Mob Psycho 2](https://www.youtube.com/watch?v=OMUb8GWVIvc).
Kdenlive läuft, wie jedes Videobearbeitungsprogramm, nur begrenzt gut auf Rechnern,
die nicht furchtbar stark oder spezialisiert dafür sind.
Trotzdem kann man sich natürlich mal Kdenlive runterladen, denn es ist open source und kostenlos.
Mehr Infos unter [https://kdenlive.org/en/](https://kdenlive.org/en/).

### Zauberwürfel

Die Anleitung zum Lösen des Zauberwürfels findet ihr auf Tims Webseite: [http://timbaumann.info/zauberwuerfel](http://timbaumann.info/zauberwuerfel).
