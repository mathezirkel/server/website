---
title: Impressum
---

## Angaben gemäß § 5 TMG

Mathematisch-Physikalischer Verein e.V.\
Universität Augsburg\
Universitätsstraße 2\
86159 Augsburg

Vereinsregister: 1487\
Registergericht: Amtsgericht Augsburg

**Vertreten durch**:

* [Prof. Dr. Urs Frauenfelder](https://www.uni-augsburg.de/en/fakultaet/mntf/math/prof/geom/urs-frauenfelder/) (Vorsitzender)
* [Dr. Sven Prüfer](https://sven.musmehl.de) (Stellvertretender Vorsitzender)
* Dr. Martin Cram (Schatzmeister)

## Kontakt

Telefon: [+49 821 598-5806](tel:+49-821-5985806)\
E-Mail: [mathezirkel@math.uni-augsburg.de](mailto:mathezirkel@math.uni-augsburg.de)

## Postadresse

Mathematisch-Physikalischer Verein e.V.\
Universität Augsburg\
86135 Augsburg

## Redaktionell verantwortlich

Felix Stärk\
Universität Augsburg\
Universitätsstraße 2\
86159 Augsburg

---

Dieses Impressum wurde auf Basis des Musters von [eRecht24](https://www.e-recht24.de) erstellt und für unsere Zwecke angepasst.
