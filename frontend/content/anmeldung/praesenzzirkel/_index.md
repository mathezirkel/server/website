---
title: Anmeldung Präsenzzirkel
type: registration
eventName: praesenzzirkel
---

Die Teilnahme am Präsenzzirkel ist kostenlos!

Um die dennoch anfallenden Kosten für Dinge wie Materialien zu decken,
sind wir daher auf [Spenden](/kontakt) angewiesen.
Wir würden uns freuen, wenn Sie einen Beitrag zur Fortführung des Mathezirkels leisten wollen!

Es besteht über die Universität Augsburg kein Versicherungsschutz für die Teilnahme am Präsenzzirkel.

Solltest du Schwierigkeiten mit der Online-Anmeldung oder weitere Fragen haben,
kontaktiere uns gerne direkt über unser [Kontaktformular](/kontakt).
