---
title: Anmeldung Abschlussveranstaltung
type: registration
eventName: abschlussveranstaltung
---

Es besteht über die Universität Augsburg kein Versicherungsschutz für die Teilnahme an der Abschlussveranstaltung.

Solltest du Schwierigkeiten mit der Online-Anmeldung oder weitere Fragen haben,
kontaktiere uns gerne direkt über unser [Kontaktformular](/kontakt).
