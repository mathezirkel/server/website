---
title: Anmeldung Astrotag
type: registration
eventName: astrotag
---

Es besteht über die Universität Augsburg kein Versicherungsschutz für die Teilnahme am Astrotag.

Solltest du Schwierigkeiten mit der Online-Anmeldung oder weitere Fragen haben,
kontaktiere uns gerne direkt über unser [Kontaktformular](/kontakt).
