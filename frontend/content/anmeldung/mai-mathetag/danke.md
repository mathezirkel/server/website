---
title: Anmeldung Mai-Mathetag
---

<div class="form">
Vielen Dank! Wir haben deine Anmeldung erhalten.

Du hast eine Zusammenfassung deiner Anmeldung per E-Mail erhalten.

Bitte beachte, dass die Anmeldung erst gültig ist, wenn diese von uns bestätigt wurde.
Die Zusammenfassung ist noch keine Zusage.

Wenn du die E-Mail nicht in deinem Posteingang findest, überprüfe bitte auch deinen Spam-Ordner.

Bei Fragen kannst du dich jederzeit [an uns wenden](/kontakt).
</div>
