---
title: Anmeldung Mai-Mathetag
type: registration
eventName: mai-mathetag
---

Es besteht über die Universität Augsburg kein Versicherungsschutz für die Teilnahme am Mai-Mathetag.

Solltest du Schwierigkeiten mit der Online-Anmeldung oder weitere Fragen haben,
kontaktiere uns gerne direkt über unser [Kontaktformular](/kontakt).
