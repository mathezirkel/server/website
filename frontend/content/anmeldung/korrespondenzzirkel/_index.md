---
title: Anmeldung Korrespondenzzirkel
type: registration
eventName: korrespondenzzirkel
---

Die Teilnahme am Korrespondenzzirkel ist kostenlos!

Um die dennoch anfallenden Kosten für Dinge wie Briefe zu decken,
sind wir daher auf [Spenden](/kontakt) angewiesen.
Wir würden uns freuen, wenn Sie einen Beitrag zur Fortführung des Mathezirkels leisten wollen!

Solltest du Schwierigkeiten mit der Online-Anmeldung oder weitere Fragen haben,
kontaktiere uns gerne direkt über unser [Kontaktformular](/kontakt).
