# Repository License Information

Unless explicitly stated, this repository is licensed under
[GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.txt), see [LICENSE](./licenses/gpl-3.0.txt).

This repository contains [modified icons](./frontend/assets/icons/atlas) from [Atlas Icons](https://atlasicons.vectopus.com/).
The original icons are licensed under [MIT](frontend/assets/icons/atlas/LICENSE).
