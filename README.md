# Mathezirkel Website

[![Hugo](https://img.shields.io/badge/Hugo-black.svg?style=for-the-badge&logo=Hugo)](https://gohugo.io/)
[![Vite](https://img.shields.io/badge/vite-%23646CFF.svg?style=for-the-badge&logo=vite&logoColor=white)](https://vitejs.dev)
[![Nginx](https://img.shields.io/badge/nginx-%23009639.svg?style=for-the-badge&logo=nginx&logoColor=white)](https://www.nginx.com/)
[![Django](https://img.shields.io/badge/django-%23092E20.svg?style=for-the-badge&logo=django&logoColor=white)](https://www.djangoproject.com)

[![linting: markdownlint](https://img.shields.io/badge/linting-markdownlint-yellowgreen)](https://github.com/DavidAnson/markdownlint)
[![linting: yamllint](https://img.shields.io/badge/linting-yamllint-yellowgreen)](https://github.com/adrienverge/yamllint/tree/81e9f98ffd059efe8aa9c1b1a42e5cce61b640c6)
[![linting: pylint](https://img.shields.io/badge/linting-pylint-yellowgreen)](https://github.com/pylint-dev/pylint)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)

Repository containing sources of the website at <https://www.mathezirkel-augsburg.de/>.

## Contribution

1. **Feature Branch**: From `main`, create a new branch with a descriptive name for your task.
2. **Implementation**: Make your changes!
3. **Merge request**: Upon completion, open a merge request to merge your branch into **main**, detailing the changes made.

Before merging the merge request, it will first be deployed to the staging environment.

### Adding and editing pages

All pages of the website are maintained as Markdown files within the [content directory](./frontend/content/) directory.
Additionally, TeX files can be added to this directory, and their compiled PDF versions will be accessible on the website.

## Local development

### Static files only

If you only want to test the static files version of the website using Hugo, you can use the following Docker command:

```shell
UID=$(id -u) \
CI_COMMIT_SHA="$(git rev-parse HEAD)" \
docker compose up liveserver
```

This will start a container running the Hugo web server which is available at <http://localhost:8080>..
The files are updated in realtime.

### Full Stack

To run the full stack version of the website locally, execute the following steps

1. Create an environment variable file.

    ```shell
    cp .backend/dev/cabret.example.env ./backend/dev/cabret.env
    ```

    Customize the environment variables as required, see [here](./docs/deployment.md) for details.
    If email functionality is not needed, no changes are necessary.

2. Create a zirkel config file.

    ```shell
    cp ./frontend/data/zirkel.example.json ./frontend/data/zirkel.json
    ```

    For details, see [here](./docs/config.md).

3. Run the dev environment.

    ```shell
    UID=$(id -u) \
    CI_COMMIT_SHA="$(git rev-parse HEAD)" \
    docker compose up hugo_build && \
    docker compose --profile fullstack up --build
    ```

Once the containers are up and running, you can access the website by going to <http://localhost:8080> in your web browser.

If you require any credentials, please reach out to the organization team for assistance.

### Linting

#### Markdownlint

```shell
docker compose up markdownlint
```

#### Yamllint

```shell
docker compose up yamllint
```

#### Pylint

```shell
docker compose up pylint --build
```

#### Bandit

```shell
docker compose up bandit --build
```

### Running unittests

You can run the backend unit tests and generate a coverage report locally with the following command:

```shell
UID=$(id -u) \
docker compose up hugo_build && \
UID=$(id -u) \
docker compose up coverage --build
```

Alternatively, to generate and view an HTML version of the coverage report, run:

```shell
UID=$(id -u) \
docker compose up hugo_build && \
UID=$(id -u) \
docker compose up report --build
```

Then, visit `localhost:8080` in your browser to view the detailed coverage report.

### Clean up

To clean any build artifacts, run the following command:

```shell
UID=$(id -u) \
docker compose --profile clean up
```

Alternatively, you can clean artifacts of single jobs by

```shell
UID=$(id -u) \
docker compose up latex_clean hugo_clean coverage_clean
```

### Styling

Hugo static site generator has built in syntax highlighting for code blocks that uses Chroma under the hood.
To regenerate the stylesheet or use another theme, run

```shell
UID=$(id -u) \
docker compose up styles --build
```

A list of available styles can be found [here](https://xyproto.github.io/splash/docs/all.html).

## Acknowledgments

This website was built using the following frameworks and third-party code:

* [Hugo](https://gohugo.io/) - The static site generator used to build this website.
* [Vite](https://vitejs.dev) - The modern build tool used to bundle assets for this website.
* [NGINX](https://www.nginx.com) - A free, open-source, high-performance HTTP server to serve the static files.
* [Python Django](https://www.djangoproject.com/) - The web framework used to develop the backend of this website.
* Assets used in this website's design:
    * [Feather Icons](https://feathericons.com/)
    * [Atlas Icons](https://atlasicons.vectopus.com/)
    * [OpenSans Font](https://fonts.google.com/specimen/Open+Sans)

We appreciate the contributions of these tools and resources to our website's development.

## License

The repository is mainly licensed under [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.txt),
see [LICENSE.md](./LICENSE.md) for details.
